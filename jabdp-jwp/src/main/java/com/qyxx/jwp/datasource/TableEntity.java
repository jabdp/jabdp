/*
 * @(#)TableEntity.java
 * 2011-9-28 下午10:30:41
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.datasource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;

import com.qyxx.jwp.bean.Form;

/**
 *  表实体对象
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-9-28 下午10:30:41
 */
public class TableEntity {
	
	//实体名，包含模块名
	private String entityName;
	
	//实体名，不包含模块名
	private String name;

	//表名
	private String tableName;
	
	//是否子实体
	private Boolean isSubEntity;
	
	//被子实体引用时的实体名
	private String refEntityName;
	
	//实体属性列表
	private List<TeAttr> teAttr;
	
	//子实体列表，只存放子实体部分属性
	private List<TableEntity> subEntitys;
	
	//主实体名
	private String masterEntityName;
	
	//被主实体引用时的实体名
	private String refSubEntityName;

	//子实体列表展示类型，同Form类listType属性
	private String listType = Form.LIST_TYPE_DATA_GRID;
	
	//中文名称
	private String caption;
	
	/**
	 * 是否公用表
	 */
	private Boolean isCommon = false;
	
	/**
	 * 是否关联公用表
	 */
	private Boolean isReferCommon = false;
	
	/**
	 * 关联实体名列表
	 */
	private List<String> referEntityNameList = new ArrayList<String>();
	
	/**
	 * 公用表类型--通用型、库存型等
	 */
	private String formType = "";
	
	/**
	 * 库存类型--出库、入库、出入库、无
	 */
	private String stockType = "";
	
	/**
	 * 出入库默认值--出库或入库
	 */
	private String inOutVal = Form.STOCK_TYPE_OUT;
	
	/**
	 * 开启导入
	 */
	private Boolean enableImport = false;
	
	/**
	 * 开启导出
	 */
	private Boolean enableExport = false;
	
	/**
	 * 是否虚拟实体
	 */
	private Boolean isVirtual = false;
	
	/**
	 * 所在模块类型
	 */
	private String moduleType;
	
	/**
	 * @return entityName
	 */
	@XmlAttribute
	public String getEntityName() {
		return entityName;
	}

	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @return name
	 */
	@XmlAttribute
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return tableName
	 */
	@XmlAttribute
	public String getTableName() {
		return tableName;
	}

	
	/**
	 * @param tableName
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	
	/**
	 * @return isSubEntity
	 */
	@XmlAttribute
	public Boolean getIsSubEntity() {
		return isSubEntity;
	}

	
	/**
	 * @param isSubEntity
	 */
	public void setIsSubEntity(Boolean isSubEntity) {
		this.isSubEntity = isSubEntity;
	}

	
	/**
	 * @return refEntityName
	 */
	@XmlAttribute
	public String getRefEntityName() {
		return refEntityName;
	}

	
	/**
	 * @param refEntityName
	 */
	public void setRefEntityName(String refEntityName) {
		this.refEntityName = refEntityName;
	}

	
	/**
	 * @return teAttr
	 */
	@XmlElement
	public List<TeAttr> getTeAttr() {
		return teAttr;
	}

	
	/**
	 * @param teAttr
	 */
	public void setTeAttr(List<TeAttr> teAttr) {
		this.teAttr = teAttr;
	}


	/**
	 * @return subEntitys
	 */
	@XmlElement
	public List<TableEntity> getSubEntitys() {
		return subEntitys;
	}

	
	/**
	 * @param subEntitys
	 */
	public void setSubEntitys(List<TableEntity> subEntitys) {
		this.subEntitys = subEntitys;
	}


	/**
	 * @return masterEntityName
	 */
	@XmlAttribute
	public String getMasterEntityName() {
		return masterEntityName;
	}

	
	/**
	 * @param masterEntityName
	 */
	public void setMasterEntityName(String masterEntityName) {
		this.masterEntityName = masterEntityName;
	}

	
	/**
	 * @return refSubEntityName
	 */
	@XmlAttribute
	public String getRefSubEntityName() {
		return refSubEntityName;
	}

	
	/**
	 * @param refSubEntityName
	 */
	public void setRefSubEntityName(String refSubEntityName) {
		this.refSubEntityName = refSubEntityName;
	}

	
	/**
	 * @return listType
	 */
	@XmlAttribute
	public String getListType() {
		return listType;
	}


	
	/**
	 * @param listType
	 */
	public void setListType(String listType) {
		this.listType = listType;
	}


	/**
	 * @return isCommon
	 */
	@XmlAttribute
	public Boolean getIsCommon() {
		return isCommon;
	}

	
	/**
	 * @param isCommon
	 */
	public void setIsCommon(Boolean isCommon) {
		this.isCommon = isCommon;
	}

	
	/**
	 * @return isReferCommon
	 */
	@XmlAttribute
	public Boolean getIsReferCommon() {
		return isReferCommon;
	}

	
	/**
	 * @param isReferCommon
	 */
	public void setIsReferCommon(Boolean isReferCommon) {
		this.isReferCommon = isReferCommon;
	}

	
	/**
	 * @return referEntityNameList
	 */
	@XmlElement(name="referEntityName")
	public List<String> getReferEntityNameList() {
		return referEntityNameList;
	}

	
	/**
	 * @param referEntityNameList
	 */
	public void setReferEntityNameList(List<String> referEntityNameList) {
		this.referEntityNameList = referEntityNameList;
	}


	/**
	 * 将关联的实体名称加入列表
	 * 
	 * @param entityName
	 */
	public void addReferEntityNameToList(String entityName) {
		if(!referEntityNameList.contains(entityName)) {
			referEntityNameList.add(entityName);
		}
	}

	
	/**
	 * @return formType
	 */
	@XmlAttribute
	public String getFormType() {
		return formType;
	}


	/**
	 * @param formType
	 */
	public void setFormType(String formType) {
		this.formType = formType;
	}

	
	/**
	 * @return stockType
	 */
	@XmlAttribute
	public String getStockType() {
		return stockType;
	}

	
	/**
	 * @param stockType
	 */
	public void setStockType(String stockType) {
		this.stockType = stockType;
	}


	/**
	 * @return inOutVal
	 */
	@XmlAttribute
	public String getInOutVal() {
		return inOutVal;
	}

	
	/**
	 * @param inOutVal
	 */
	public void setInOutVal(String inOutVal) {
		this.inOutVal = inOutVal;
	}


	/**
	 * @return caption
	 */
	@XmlAttribute
	public String getCaption() {
		return caption;
	}

	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	
	/**
	 * @return enableImport
	 */
	@XmlAttribute
	public Boolean getEnableImport() {
		return enableImport;
	}


	/**
	 * @param enableImport
	 */
	public void setEnableImport(Boolean enableImport) {
		this.enableImport = enableImport;
	}

	
	/**
	 * @return enableExport
	 */
	@XmlAttribute
	public Boolean getEnableExport() {
		return enableExport;
	}


	/**
	 * @param enableExport
	 */
	public void setEnableExport(Boolean enableExport) {
		this.enableExport = enableExport;
	}
	
	
	/**
	 * @return isVirtual
	 */
	@XmlAttribute
	public Boolean getIsVirtual() {
		return isVirtual;
	}

	
	/**
	 * @param isVirtual
	 */
	public void setIsVirtual(Boolean isVirtual) {
		this.isVirtual = isVirtual;
	}


	/**
	 * @return moduleType
	 */
	@XmlAttribute
	public String getModuleType() {
		return moduleType;
	}


	/**
	 * @param moduleType
	 */
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	/**
	 * 获取导入时检查重复字段属性列表
	 * 
	 * @return
	 */
	@XmlTransient
	public List<String> getCheckImportUniqueAttrs() {
		List<String> ciuList = new ArrayList<String>();
		if(enableImport && null!=teAttr) {
			for(TeAttr ta : teAttr) {
				if(ta.getEnableImport() && ta.getCheckImportUnique()) {
					ciuList.add(ta.getName());
				}
			}
		}
		return ciuList;
	}


	/**
	 * 获取作为共享用户字段属性列表
	 * 
	 * @return
	 */
	@XmlTransient
	public List<TeAttr> getUseAsShareUserAttrs() {
		List<TeAttr> ciuList = new ArrayList<TeAttr>();
		if(null!=teAttr) {
			for(TeAttr ta : teAttr) {
				if(ta.getUseAsShareUser()) {
					ciuList.add(ta);
				}
			}
		}
		return ciuList;
	}

	
	/**
	 * 获取实体字段自定义编号规则
	 * 
	 * @return
	 */
	@XmlTransient
	public Map<String, String> getAutoIdRules() {
		Map<String, String> result = new HashMap<String,String>();
		if(null!=teAttr) {
			for(TeAttr ta : teAttr) {
				String air = ta.getAutoIdRule();
				if(StringUtils.isNotBlank(air)) {
					result.put(ta.getName(), air);
				}
			}
		}
		return result;
	}

	
	/**
	 * 获取作为权限控制字段属性列表
	 * 
	 * @return
	 */
	@XmlTransient
	public List<TeAttr> getUseAsAuthFieldAttrs() {
		List<TeAttr> ciuList = new ArrayList<TeAttr>();
		if(null!=teAttr) {
			for(TeAttr ta : teAttr) {
				if(ta.getUseAsAuthField()) {
					ciuList.add(ta);
				}
			}
		}
		return ciuList;
	}
}
