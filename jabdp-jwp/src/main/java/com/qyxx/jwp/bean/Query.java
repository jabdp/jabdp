/*
 * @(#)Query.java
 * 2012-7-11 上午09:47:40
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


/**
 *  查询表单属性
 *  @author gxj
 *  @version 1.0 2012-7-11 上午09:47:40
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Query {
	
	/**
	 * 树形查询
	 */
	public static final String TYPE_TREE = "1";
	
	@XmlAttribute
	private String type;
	
	@XmlAttribute
	private String key;

	@XmlElement
	private Formula formula;

	
	
	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}


	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}


	
	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}



	
	/**
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}



	/**
	 * @return formula
	 */
	public Formula getFormula() {
		return formula;
	}

	
	/**
	 * @param formula
	 */
	public void setFormula(Formula formula) {
		this.formula = formula;
	}
	
	
}
