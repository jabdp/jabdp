/*
 * @(#)ModuleProperties.java
 * 2012-5-8 下午04:08:54
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  模块属性节点
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午04:08:54
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ModuleProperties {
	
	/**
	 * 模块类型--普通模块
	 */
	public static final String TYPE_MODULE = "module";
	
	/**
	 * 模块类型--业务字典
	 */
	public static final String TYPE_DICT = "dict";
	
	/**
	 * 模块类型--自定义表单
	 */
	public static final String TYPE_FORM = "form";
	
	/**
	 * 模块类型--APP模块
	 */
	public static final String TYPE_APP = "app";
	
	@XmlAttribute
	private String caption;
	@XmlAttribute
	private String key;
	@XmlAttribute
	private String languageText;
	@XmlAttribute
	private String version;
	@XmlElement
	private OwnerProperties ownerProperties = new OwnerProperties();	
	@JsonIgnoreProperties
	@XmlTransient
	private String i18nKey;
	@XmlAttribute
	private String relevanceModule;//关联模块的，模块集合key.模块key
	
	/**
	 * 模块类型
	 */
	@XmlAttribute
	private String type;
	
	/**
	 * @return caption
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	/**
	 * @return languageText
	 */
	public String getLanguageText() {
		return languageText;
	}
	
	/**
	 * @param languageText
	 */
	public void setLanguageText(String languageText) {
		this.languageText = languageText;
	}
	
	/**
	 * @return version
	 */
	public String getVersion() {
		return version;
	}
	
	/**
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
	/**
	 * @return ownerProperties
	 */
	public OwnerProperties getOwnerProperties() {
		return ownerProperties;
	}
	
	/**
	 * @param ownerProperties
	 */
	public void setOwnerProperties(OwnerProperties ownerProperties) {
		this.ownerProperties = ownerProperties;
	}

	
	/**
	 * @return i18nKey
	 */
	public String getI18nKey() {
		return i18nKey;
	}

	
	/**
	 * @param i18nKey
	 */
	public void setI18nKey(String i18nKey) {
		this.i18nKey = i18nKey;
	}

	
	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	public String getRelevanceModule() {
		return relevanceModule;
	}

	public void setRelevanceModule(String relevanceModule) {
		this.relevanceModule = relevanceModule;
	}
	

}
