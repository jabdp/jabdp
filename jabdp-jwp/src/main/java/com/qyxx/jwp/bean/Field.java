/*
 * @(#)Field.java
 * 2012-5-8 下午05:58:39
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  字段属性
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午05:58:39
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Field {

	@XmlAttribute
	private String caption;
	@XmlAttribute
	private String key;
	@XmlAttribute
    private String languageText;
	@XmlAttribute
	private Boolean isVirtual = false;
	//列表多级标题，作为标题显示字段标识
	@XmlAttribute
	private Boolean isCaption = false;
	@XmlAttribute
	private Integer index;
	
	@XmlAttribute
	private Integer tabRows;
	@XmlAttribute
	private Integer tabCols;
	

	@XmlElement
	private DataProperties dataProperties = new DataProperties();
	@XmlElement
	private EditProperties editProperties = new EditProperties();
	@XmlElement
	private QueryProperties queryProperties = new QueryProperties();
	
	@JsonIgnoreProperties
	@XmlTransient
	private String i18nKey;
	
	/**
	 * 列表多级标题，作为标题显示字段标识
	 */
	public Boolean getIsCaption() {
		return isCaption;
	}
	/**
	 * 列表多级标题，作为标题显示字段标识
	 */
	public void setIsCaption(Boolean isCaption) {
		this.isCaption = isCaption;
	}

	/**
	 * 字段所属实体，关联表实体名
	 */
	@XmlAttribute
	private String entityName;
	
	/**
	 * 同步实体，需要将字段值同步至对应的实体下
	 */
	@XmlAttribute
	private String syncEntityName;
	
	/**
	 * 同步属性，需要将值更新至同表不同记录对应的属性下，用于拆分、合并记录
	 */
	@XmlAttribute
	private String syncKey;
	
	/**
	 * 属性源自实体名，表示该属性原先属于哪个实体
	 */
	@XmlAttribute
	private String fromEntityName = "";
	
	/**
	 * 开启导入
	 */
	@XmlAttribute
	private Boolean enableImport = false;
	
	/**
	 * 开启导出
	 */
	@XmlAttribute
	private Boolean enableExport = false;
	
	/**
	 * 导入时是否检查重复性，允许多个字段联合唯一
	 */
	@XmlAttribute
	private Boolean checkImportUnique = false;
	
	/**
	 * 存放导出标签信息
	 */
	@XmlTransient
	private String tagInfo;
	
	/**
	 * 动态显示属性
	 */
	@XmlElement
	private DynamicProp dynamicProp;
	
	/**
	 * 开启字段权限检查
	 */
	@XmlAttribute
	private Boolean enableFieldAuthCheck = false;
	
	/**
	 * 开启修订功能
	 */
	@XmlAttribute 
	private Boolean enableRevise = false;
	
	/**
	 * @return caption
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	/**
	 * @return languageText
	 */
	public String getLanguageText() {
		return languageText;
	}
	
	/**
	 * @param languageText
	 */
	public void setLanguageText(String languageText) {
		this.languageText = languageText;
	}
	
	/**
	 * @return isVirtual
	 */
	public Boolean getIsVirtual() {
		return isVirtual;
	}
	
	/**
	 * @param isVirtual
	 */
	public void setIsVirtual(Boolean isVirtual) {
		this.isVirtual = isVirtual;
	}
	
	/**
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	/**
	 * @return tabRows
	 */
	public Integer getTabRows() {
		return tabRows;
	}

	
	/**
	 * @param tabRows
	 */
	public void setTabRows(Integer tabRows) {
		this.tabRows = tabRows;
	}

	
	/**
	 * @return tabCols
	 */
	public Integer getTabCols() {
		return tabCols;
	}

	
	/**
	 * @param tabCols
	 */
	public void setTabCols(Integer tabCols) {
		this.tabCols = tabCols;
	}

	/**
	 * @return dataProperties
	 */
	public DataProperties getDataProperties() {
		return dataProperties;
	}
	
	/**
	 * @param dataProperties
	 */
	public void setDataProperties(DataProperties dataProperties) {
		this.dataProperties = dataProperties;
	}
	
	/**
	 * @return editProperties
	 */
	public EditProperties getEditProperties() {
		return editProperties;
	}
	
	/**
	 * @param editProperties
	 */
	public void setEditProperties(EditProperties editProperties) {
		this.editProperties = editProperties;
	}
	
	/**
	 * @return queryProperties
	 */
	public QueryProperties getQueryProperties() {
		return queryProperties;
	}
	
	/**
	 * @param queryProperties
	 */
	public void setQueryProperties(QueryProperties queryProperties) {
		this.queryProperties = queryProperties;
	}

	
	/**
	 * @return i18nKey
	 */
	public String getI18nKey() {
		return i18nKey;
	}

	
	/**
	 * @param i18nKey
	 */
	public void setI18nKey(String i18nKey) {
		this.i18nKey = i18nKey;
	}

	
	/**
	 * @return entityName
	 */
	public String getEntityName() {
		return entityName;
	}

	
	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	
	/**
	 * @return syncEntityName
	 */
	public String getSyncEntityName() {
		return syncEntityName;
	}

	
	/**
	 * @param syncEntityName
	 */
	public void setSyncEntityName(String syncEntityName) {
		this.syncEntityName = syncEntityName;
	}

	
	/**
	 * @return syncKey
	 */
	public String getSyncKey() {
		return syncKey;
	}

	
	/**
	 * @param syncKey
	 */
	public void setSyncKey(String syncKey) {
		this.syncKey = syncKey;
	}
	
	/**
	 * @return fromEntityName
	 */
	public String getFromEntityName() {
		return fromEntityName;
	}

	
	/**
	 * @param fromEntityName
	 */
	public void setFromEntityName(String fromEntityName) {
		this.fromEntityName = fromEntityName;
	}

	
	/**
	 * @return enableImport
	 */
	public Boolean getEnableImport() {
		return enableImport;
	}

	
	/**
	 * @param enableImport
	 */
	public void setEnableImport(Boolean enableImport) {
		this.enableImport = enableImport;
	}

	
	/**
	 * @return enableExport
	 */
	public Boolean getEnableExport() {
		return enableExport;
	}

	
	/**
	 * @param enableExport
	 */
	public void setEnableExport(Boolean enableExport) {
		this.enableExport = enableExport;
	}

	
	/**
	 * @return checkImportUnique
	 */
	public Boolean getCheckImportUnique() {
		return checkImportUnique;
	}

	
	/**
	 * @param checkImportUnique
	 */
	public void setCheckImportUnique(Boolean checkImportUnique) {
		this.checkImportUnique = checkImportUnique;
	}

	
	/**
	 * @return tagInfo
	 */
	public String getTagInfo() {
		return tagInfo;
	}

	
	/**
	 * @param tagInfo
	 */
	public void setTagInfo(String tagInfo) {
		this.tagInfo = tagInfo;
	}

	
	/**
	 * @return dynamicProp
	 */
	public DynamicProp getDynamicProp() {
		return dynamicProp;
	}

	
	/**
	 * @param dynamicProp
	 */
	public void setDynamicProp(DynamicProp dynamicProp) {
		this.dynamicProp = dynamicProp;
	}

	
	/**
	 * @return enableFieldAuthCheck
	 */
	public Boolean getEnableFieldAuthCheck() {
		return enableFieldAuthCheck;
	}

	
	/**
	 * @param enableFieldAuthCheck
	 */
	public void setEnableFieldAuthCheck(Boolean enableFieldAuthCheck) {
		this.enableFieldAuthCheck = enableFieldAuthCheck;
	}

	
	/**
	 * @return enableRevise
	 */
	public Boolean getEnableRevise() {
		return enableRevise;
	}

	
	/**
	 * @param enableRevise
	 */
	public void setEnableRevise(Boolean enableRevise) {
		this.enableRevise = enableRevise;
	}
	
	
	
}
