<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<link href="${ctx}/js/easyui/themes/blue/panel.css" rel="stylesheet"
      type="text/css"/>
 <script type="text/javascript" src="${ctx}/js/easyui/easyui.xpPanel.js">
</script>
<script type="text/javascript">
    $(function(){
    	/* $("#queryForm input").click(function() {
    		
        });	 */	
    	loadQueryInfo();
    	/*  $("#a_exp_clp").toggle(expandAll, collapseAll); */
    	/*  $("#logInfo" ).edatagrid({ toolbar:[ {
					text:'新增',
					iconCls:'icon-add',
				},'-',{
					text:'删除',
					iconCls:'icon-remove',
				},'-',{
					text:'查看',
					iconCls:'icon-search',
				},'-',{
					text:'复制',
					iconCls:'icon-copy',
				},'-',{
					text:'报表',
					iconCls:'icon-report',
				},'-',{
					text:'附件',
					iconCls:'icon-attach',
				}
				
				]}); */
   }); 
    
   /*  function collapseAll() {
		$(".xpstyle-panel .xpstyle").panel("collapse");
		$(this).removeClass("icon-collapse");
		$(this).addClass("icon-expand");
		$(this).attr("title", "全部展开");
	} 

	 function expandAll() {
		$(".xpstyle-panel .xpstyle").panel("expand");
		$(this).removeClass("icon-expand");
		$(this).addClass("icon-collapse");
		$(this).attr("title", "全部收缩");
	} */
    
   function loadQueryInfo(){
	   var module = $.fn.module.getModule();
	   var formLength = module.dataSource.formList.length;
	   for(var i =0;i<formLength;i++){
			  var form = module.dataSource.formList[i];
			  if(form.isMaster){
				  showQueryView(form);
			  }
	   }
   }
   /*
   function showLineInfo(tableIndex,fieldIndex){
	   //alert(tabIndex+" : "+fieldIndex);
	   var data = $.fn.module.getFieldPropJson(tableIndex, fieldIndex);
	   $("#module_table_propGrid").propertygrid("loadData",data);
   }*/
   //
   function showQueryView(form){
	   var fieldLength = form.fieldList.length;
	   var module = $.fn.module.getModuleProp().ownerProperties;
	   var colType = [[]];
	   var fieldObj = {};
	//   var j=0;
	   var divs=[];
	 //  var el = document.getElementById("queryFormId");
	   for(var i=0;i<fieldLength;i++){
		   var field = form.fieldList[i];
		   if(field.key) {
			 //如果查询属性为真转化后显示到查询条件中
			   if(field.queryProperties.showInSearch === true || field.queryProperties.showInSearch == "true"){
				 	 var str = $.fn.module.covQueryType(field,form.index,true);
				 	/*  var divs = document.createElement("div"); 
					 divs.innerHTML= str;
				 	//	el.innerHTML= str;
				 	// el.appendChild(divs); */
				 	divs.push(str);
			   }
			   //如果显示属性为真则生成datagrid显示到对应的表格中
			   if(field.queryProperties.showInGrid === true || field.queryProperties.showInGrid == "true"){
			   		colType[0].push({field:field.key,width:80,title:field.caption});
			   		var value = $.fn.module.covQueryType(field,form.index,false);
			   		fieldObj[field.key] = value;
			   }
		   }
			//   j++;
		  // }
	   }
   		divs.push("<div class='easyui-panel  xpstyle' style='width:180px;' collapsible='true' collapsed='true' title='所有者'><input type='text'  value="+module.creator+"></input></div>");
   		colType[0].push({field:"creator",width:80,title:"所有者"});
   	//	var value = covQueryType(field,form.index,false);
   		fieldObj["creator"] = module.creator;
   		$("#queryFormId").html(divs.join(""));
	   $("#logInfo").edatagrid({
			width: 1000,
			height: 'auto',
			columns:colType,
			toolbar:[ {
				text:'新增',
				iconCls:'icon-add'
			},'-',{
				text:'删除',
				iconCls:'icon-remove'
			},'-',{
				text:'查看',
				iconCls:'icon-search'
			},'-',{
				text:'复制',
				iconCls:'icon-copy'
			},'-',{
				text:'报表',
				iconCls:'icon-report'
			},'-',{
				text:'附件',
				iconCls:'icon-attach'
			}
			]
		});
	 //  $.each(fieldObj,function(k,v){alert(k+":"+v);})
		$("#logInfo").edatagrid("addRow",fieldObj);
		
   }
</script>
<div class="easyui-layout" fit="true">
	<div region="west" title="查询条件" split="true" style="width:200px;padding:0px;background-color: #E0ECFF;" iconCls="icon-search" tools="#pl_tt">
			
				<!-- <table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="loginName">登录名:</label></th>
							<td><input type="text" name="filter_LIKES_loginName" id="loginName"></input></td>
						</tr>
						<tr>
							<th><label for="operationMethod">操作名:</label></th>
							<td><input type="text" name="filter_LIKES_operationMethod" id="operationMethod"></input></td>
						</tr>
						<tr>
						    <th><label for="ip">IP地址:</label></th>
							<td><input type="text" name="filter_LIKES_ip" id="ip"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query">查询</button>&nbsp;&nbsp;
								<button type="button" id="bt_reset">重置</button>
							</td>
						</tr>
					</tbody>
				</table>	-->
				<div class="xpstyle-panel" id="queryFormId">
					
				</div>
			
	</div>
	<div region="center" title="查询显示">
			<table id="logInfo" fit="true">
			</table>
	</div>
</div>
<div id="pl_tt">
				<a href="#" id="a_exp_clp" class="icon-expand" title="全部展开"></a>
				<a href="#" id="a_switch_query" class="icon-menu" title="切换查询方式" style="display:none;"></a>
</div>
