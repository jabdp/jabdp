<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>导入导出设置</title>
	<%@ include file="/common/meta.jsp" %>
<script type="text/javascript">  		
$(function() {
	var importTreeSetting = {
		data : {
			simpleData : {
				enable : true,
				pIdKey : "pid"
			}
		},
		check : {
			enable : true
		},
		callback: {
			onRightClick: setCheckImportUnique
		}
	};
	var importTreeData = parent.$.fn.module.getModuleDataTree("enableImport");
	var importTreeObj = $.fn.zTree.init($("#importTree"),importTreeSetting, importTreeData);
	//importTreeObj.expandAll(true);
	
	var exportTreeSetting = {
		data : {
			simpleData : {
				enable : true,
				pIdKey : "pid"
			}
		},
		check : {
			enable : true
		}
	};
	var exportTreeData = parent.$.fn.module.getModuleDataTree("enableExport");
	console.log(exportTreeData)
	var exportTreeObj = $.fn.zTree.init($("#exportTree"),exportTreeSetting, exportTreeData);
	//exportTreeObj.expandAll(true);
});

function setCheckImportUnique(event, treeId, treeNode) {
	if(treeNode && treeNode.level == 2 && treeNode.checked) {
		$("#importTree").data("node", treeNode);
		if(treeNode.checkImportUnique) {
			$("#cancelCheckUniqueMenu").menu('show', {						
				left: event.pageX,
				top: event.pageY
			});
		} else {
			$("#checkUniqueMenu").menu('show', {						
				left: event.pageX,
				top: event.pageY
			});
		}
	}
}

function setCheckUnique() {
	var treeNode = $("#importTree").data("node");
	if(treeNode) {
		var importTreeObj = $.fn.zTree.getZTreeObj("importTree");
		treeNode["iconSkin"] = "icon05";
		treeNode["checkImportUnique"] = true;
		importTreeObj.updateNode(treeNode);
	}
}

function cancelCheckUnique() {
	var treeNode = $("#importTree").data("node");
	if(treeNode) {
		var importTreeObj = $.fn.zTree.getZTreeObj("importTree");
		treeNode["iconSkin"] = "icon03";
		treeNode["checkImportUnique"] = false;
		importTreeObj.updateNode(treeNode);
	}
}

function setImportExportData() {
	var importTreeObj = $.fn.zTree.getZTreeObj("importTree");
	var importNodes = importTreeObj.getNodes();
	parent.$.fn.module.setImportExportData(importNodes, "enableImport");

	var exportTreeObj = $.fn.zTree.getZTreeObj("exportTree");
	var exportNodes = exportTreeObj.getNodes();
	console.log(exportNodes)
	parent.$.fn.module.setImportExportData(exportNodes, "enableExport");
	
	parent.changeModuleProp();
	
	$.messager.alert('提示','设置成功','info');
}

function closeImportExportWin() {
	parent.changeModuleProp();
	parent.$('#importExportSetWin').window('close');
}
</script>
</head>
<body class="easyui-layout" fit="true">
	<div region="center" border="false" fit="true">
		<div id="importExportTab" class="easyui-tabs" border="false" fit="true">
			<div title="导入设置">
				<div>
					<ul style="text-align:center;" id="importTree" class="ztree"></ul>
				</div>
			</div>
			<div title="导出设置">
				<div>
					<ul style="text-align:center;" id="exportTree" class="ztree"></ul>
				</div>
			</div>
		</div>
	</div>
	<div region="south" border="false" style="text-align:right;padding:2px 0;">
		<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="setImportExportData()">设置</a>
		<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="closeImportExportWin()">关闭</a>
	</div>
	<div id="checkUniqueMenu" class="easyui-menu" style="width:100px;">
		<div iconCls="icon-edit" onclick="setCheckUnique();">检查重复</div>
	</div>
	<div id="cancelCheckUniqueMenu" class="easyui-menu" style="width:100px;">
		<div iconCls="icon-remove" onclick="cancelCheckUnique();">取消检查</div>
	</div>
</body>
</html>