<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>iDesigner</title>
	<%@ include file="/common/meta.jsp" %>
	<script type="text/javascript" src="${ctx}/js/getzTreeEvent/jquery.getzTree.js"></script>
	<script type="text/javascript">
		var i=1;
		var cols=1;
		var langFlag = "";
		//var formulaType=0;
		//获得子页面内函数
		//$.each(tabs,function(k,v){alert(k+":"+v)});
		/* var tb = tbs[0];
		if(tb) {
			var tbby = tb.panel("body");
			if(tbby) {
				var tb_ifr = tbby.find("iframe");
				if(tb_ifr) {
					var win = tb_ifr[0].contentWindow.window;
					}
					}*/
	function addChildTab(tit) {
		var moduleTypes = $.fn.module.getModuleProp().type;
		var moduleCaption = $.fn.module.getModuleProp().caption;
		var title = tit + "" + i++;
		var cap = moduleCaption + title;
		var newTemp = $.fn.toPinyin(cap);
		var newKey = "";
		$.each(newTemp.split("_"), function(k, v) {
			newKey += v;
		});
		var flag = $('#module_table_tab').tabs('exists', cap);
		var fg = $.fn.module.getFormByKey(newKey);
		if (flag || fg) {
			addChildTab(tit);
		} else {
			var frm = $.fn.module.getFormProp();
			frm.caption = moduleCaption + title;
			var temp = $.fn.toPinyin(frm.caption);
			var newTemp = "";
			$.each(temp.split("_"), function(k, v) {
				newTemp += v;
			});
			frm.key = newTemp;
			var tableName = temp.replace("_", "").toUpperCase();
			frm.tableName = $.fn.module.covToDBRule(tableName);
			//添加子表分组
			var tabList = {};
			var tabsList = $.fn.module.getTabsList(2);
			if (tabsList) {
				tabList = tabsList.tabList;
				if (tabList) {
					for ( var j = 0; j < tabList.length; j++) {
						cols = cols > tabList[j].cols ? cols : tabList[j].cols;
					}
				}
				cols += 1;
				//$.fn.module.setTabsList({"rows":2,"tabList":[{"rows":2,"cols":cols,"form":"ChildTable"+cols,"caption":"子表信息"+cols,	"languageText":""}]});
				$.fn.module.setTabList({
					"rows" : 2,
					"cols" : cols,
					"form" : frm.key,
					"caption" : frm.caption,
					"languageText" : ""
				});
			} else {
				$.fn.module.setTabsList({
					"rows" : 2,
					"tabList" : [ {
						"rows" : 2,
						"cols" : cols,
						"form" : frm.key,
						"caption" : frm.caption,
						"languageText" : ""
					} ]
				});
			}
			title = frm.caption;
			var index = frm.index;
			var url = "${ctx}/jsp/table.jsp";
			url = url + "?index=" + index;
			$('#module_table_tab').data(title, index);
			$("#module_table_tab").tabs("add", {
				title : title,
				href : url,
				closable : false,
				cache : true,
				fit : true,
				selected : frm.isMaster
			});
		}
	}

	function addTab(tit, frm, isMaster) {
		var moduleTypes = $.fn.module.getModuleProp().type;
		var moduleCaption = $.fn.module.getModuleProp().caption;
		var title = "";
		if (tit && !isMaster) {
			title = tit + "" + i++;
		} else {
			title = tit;
		}
		var cap = moduleCaption + title;
		if ($('#module_table_tab').tabs('exists', cap)) {
			addTab(tit, frm, isMaster);
		} else {
			if (!frm) {
				frm = $.fn.module.getFormProp();
				if (isMaster) {
					frm.isMaster = true;
					frm.caption = moduleCaption + title;
					var temp = $.fn.toPinyin(frm.caption);
					var newTemp = "";
					$.each(temp.split("_"), function(k, v) {
						newTemp += v;
					});
					frm.key = newTemp;
					var tableName = temp.replace("_", "").toUpperCase();
					frm.tableName = $.fn.module.covToDBRule(tableName);
					//添加主表分组
					if (moduleTypes == "dict") {
						frm.cols = 2;
					}
					$.fn.module.setTabsList({
						"rows" : 1,
						"tabList" : [ {
							"rows" : 1,
							"cols" : 1,
							"form" : frm.key,
							"caption" : "主要信息",
							"languageText" : ""
						} ]
					});
				} else {
					frm.caption = moduleCaption + title;
					var temp = $.fn.toPinyin(frm.caption);
					var newTemp = "";
					$.each(temp.split("_"), function(k, v) {
						newTemp += v;
					});
					frm.key = newTemp;
					var tableName = temp.replace("_", "").toUpperCase();
					frm.tableName = $.fn.module.covToDBRule(tableName);
					//添加子表分组
					var tabList = {};
					var tabsList = $.fn.module.getTabsList(2);
					if (tabsList) {
						tabList = tabsList.tabList;
						if (tabList) {
							for ( var j = 0; j < tabList.length; j++) {
								cols = cols > tabList[j].cols ? cols
										: tabList[j].cols;
							}
						}
						cols += 1;
						//$.fn.module.setTabsList({"rows":2,"tabList":[{"rows":2,"cols":cols,"form":"ChildTable"+cols,"caption":"子表信息"+cols,	"languageText":""}]});
						$.fn.module.setTabList({
							"rows" : 2,
							"cols" : cols,
							"form" : frm.key,
							"caption" : frm.caption,
							"languageText" : ""
						});
					} else {
						$.fn.module.setTabsList({
							"rows" : 2,
							"tabList" : [ {
								"rows" : 2,
								"cols" : cols,
								"form" : frm.key,
								"caption" : frm.caption,
								"languageText" : ""
							} ]
						});
					}

				}
			}
			//if(!title){
			title = frm.caption;
			//}else{
			//	title = title + "" + i++;
			//}
			var index = frm.index;
			var url = "${ctx}/jsp/table.jsp";
			url = url + "?index=" + index;
			$('#module_table_tab').data(title, index);
			$("#module_table_tab").tabs("add", {
				title : title,
				href : url,
				//content:content,
				closable : false,
				cache : true,
				fit : true,
				selected : frm.isMaster
			});

		}

	}
	//给field设置事件及方法
	function setEvents(flag) {
		//获得iframe
		if ($(mt_id).data("type") == "按钮属性"
				&& $(mt_id).data("buttonType") == "normal"
				&& $("#main_tabs_div_1").data("title") == "编辑页面设计"
				&& $(mt_id).data("buttonKey")) {
			var tabs = $('#eventIframe').find("iframe");
			var win = tabs[0].contentWindow.window;
			var content = win.getFunText();
			var caption = $("#addEventWin").data("nodeCaption");
			var params = $("#fun_ztree_params").html();
			var eventKey = $("#eventTree").data("eventName");
			var buttonKey = $(mt_id).data("buttonKey");
			var tabs = $('#edit_ifr').find("iframe");
			var win = tabs[0].contentWindow.window;
			var mainTableIndex = win.mainTableIndex;
			if (eventKey && content && content.length > 0) {
				//设置事件
				var eventData = {
					"key" : eventKey,
					"caption" : caption,
					"content" : content,
					"params" : params
				};
				$.fn.module.setButtonEvents(mainTableIndex, buttonKey,
						"editPage", eventData);
				//showLineInfo(tableIndex,fieldIndex);
				if (!flag) {
					$("#addEventWin").window("close");
				}
			} else {
				alert("请先选中事件并添加方法后再保存！");
			}
			$(mt_id).propertygrid({
				title : "按钮属性"
			});
			$(mt_id).data("type", "按钮属性");
			data = $.fn.module.getButtonPropJson(mainTableIndex, buttonKey,
					"editPage");
			$(mt_id).propertygrid("loadData", data);
			//菜单按钮设置事件
		} else if (($(mt_id).data("type") == "按钮属性" || $(mt_id).data("type") == "表单属性")
				&& $(mt_id).data("buttonType") == "menu"
				&& ($("#main_tabs_div_1").data("title") == "编辑页面设计" || $("#main_tabs_div_1").data("title") == "数据库设计")) {
			var tabs = $('#eventIframe').find("iframe");
			var win = tabs[0].contentWindow.window;
			var rowIndex = $('#menuButtonList').data("rowIndex");
			var dataJson = $('#menuButtonList').edatagrid("getRows");
			var eventKey = $("#eventTree").data("eventName");
			var content = win.getFunText();
			var caption = $("#addEventWin").data("nodeCaption");
			var params = $("#fun_ztree_params").html();
			var data = {
				"key" : eventKey,
				"caption" : caption,
				"content" : content,
				"params" : params
			};
			if (eventKey && content && content.length > 0) {
				var eventList = dataJson[rowIndex].eventList;
				var fg = -1;
				if (eventList.length > 0 && $.isArray(eventList)) {
					for ( var i = 0; i < eventList.length; i++) {
						if (eventList[i].key == data.key) {
							fg = i;
							break;
						}
					}
					if (fg >= 0) {
						$.extend(eventList[fg], data);
					} else {
						eventList.push(data);
					}
				} else {
					dataJson[rowIndex].eventList = new Array();
					dataJson[rowIndex].eventList.push(data);
				}
				if (!flag) {
					$("#addEventWin").window("close");
				}
			} else {
				alert("请先选中事件并添加方法后再保存！");
			}
		} else if ($(mt_id).data("type") == "按钮属性"
				&& $("#main_tabs_div_1").data("title") == "查询页面设计") {

		} else if ($(mt_id).data("type") == "表单属性"
				&& $("#main_tabs_div_1").data("title") == "数据库设计") {
			var tabs = $('#eventIframe').find("iframe");
			var win = tabs[0].contentWindow.window;
			var eventKey = $("#eventTree").data("eventName");
			var content = win.getFunText();
			var caption = $("#addEventWin").data("nodeCaption");
			var params = $("#fun_ztree_params").html();
			var data = {
				"key" : eventKey,
				"caption" : caption,
				"content" : content,
				"params" : params
			};
			var tableIndex = $(mt_id).data("tableIndex");
			if (eventKey && content && content.length > 0) {
				var extension = $.fn.module.getExtension(tableIndex);
				var eventList = extension[$(mt_id).data("pageType")].eventList;
				if (eventList.length > 0) {
					var fg = -1;
					for ( var i = 0; i < eventList.length; i++) {
						if (eventList[i].key == data.key) {
							fg = i;
							break;
						}
					}
					if (fg >= 0) {
						$.extend(eventList[fg], data);
					} else {
						eventList.push(data);
					}
				} else {
					eventList.push(data);
				}
				if (!flag) {
					$("#addEventWin").window("close");
				}
			} else {
				alert("请先选中事件并添加方法后再保存！");
			}
			$(mt_id).propertygrid("loadData",
					$.fn.module.getFormPropJson(tableIndex));
		} else {
			var tabs = $('#eventIframe').find("iframe");
			var win = tabs[0].contentWindow.window;

			var tableIndex = $(mt_id).data("tableIndex");
			var fieldIndex = $(mt_id).data("fieldIndex");
			var eventKey = $("#eventTree").data("eventName");
			var content = win.getFunText();
			var caption = $("#addEventWin").data("nodeCaption");
			var params = $("#fun_ztree_params").html();
			var data = {
				"key" : eventKey,
				"caption" : caption,
				"content" : content,
				"params" : params
			};
			if (eventKey && content && content.length > 0) {
				$.fn.module.setFieldEventProp(tableIndex, fieldIndex, data);
				showLineInfo(tableIndex, fieldIndex);
				if (!flag) {
					$("#addEventWin").window("close");
				}
			} else {
				alert("请先选中事件并添加方法后再保存！");
			}
		}
	}
	var setting = {
		data : {
			simpleData : {
				enable : true

			}
		},
		callback : {
			onClick : initFunGrid,
			//	onDblClick: appendFunction
			onRightClick : appendFunction,
			//	onRename: saveZtreeModule
			onMouseUp : getTreeNode
		}
	};
	function getTreeNode(event, treeId, treeNode) {
		if (treeId == "eventTree") {
			$("#eventTree").data("treeNode", treeNode);
		}
	}
	//单击树形节点时候获得相应操作
	function initFunGrid(event, treeId, treeNode) {
		if (treeId == "functionTree") {/*函数列表*/
			if (treeNode.getParentNode()) {
				var methodsName = treeNode.getParentNode().key;
				getFunctionGrid(methodsName, treeNode.key);/*加载函数列表*/
			}
		} else if (treeId == "eventTree") {/*事件列表*/

			if (($(mt_id).data("type") == "按钮属性"
					&& $("#main_tabs_div_1").data("title") == "编辑页面设计")
				 || ($(mt_id).data("type") == "表单属性" && $(mt_id).data("buttonType") == "menu"
						&& $("#main_tabs_div_1").data("title") == "数据库设计")) {/*关于按钮的事件编辑，但不包括查询页面的*/
				//编辑页面按钮属性加载
				var tabs = $('#eventIframe').find("iframe");
				var win = tabs[0].contentWindow.window;
				var textVal = win.getFunText();
                /*保存函数编辑页面的内容——start 2019/8/5 13:59*/
                if (textVal && textVal.length > 0
                    && $("#eventTree").data("eventName")) {
                    setEvents(true);
                    var treeObj = $.fn.zTree.getZTreeObj("eventTree");
                    var tree_node = $("#eventTree").data("eventNode");
                    if (tree_node) {
                        tree_node["iconSkin"] = "icon08";
                        treeObj.updateNode(tree_node);
                    }

                }
                /*保存函数编辑页面的内容——end 2019/8/5 13:59*/
				$("#addEventWin").data("nodeCaption", treeNode.caption);
				$("#fun_ztree_params").html("");
				$("#fun_ztree_description").html("");
				if (!treeNode.params) {
					treeNode.params = "";
				}
				win.setFunParams(treeNode.params);
				$("#fun_ztree_params").html(treeNode.params);
				$("#fun_ztree_description").html(treeNode.description);
				var eventList = [];
				if ($(mt_id).data("buttonType") == "menu") {//编辑页面菜单按钮事件属性加载
					var rowIndex = $('#menuButtonList').data("rowIndex");
					var dataJson = $('#menuButtonList').edatagrid("getRows");
					eventList = dataJson[rowIndex].eventList;
				} else if ($(mt_id).data("buttonType") == "normal") {
					var key = $(mt_id).data("buttonKey");
					var edittabs = $('#edit_ifr').find("iframe");
					var editwin = edittabs[0].contentWindow.window;
					var mainTableIndex = editwin.mainTableIndex;
					eventList = $.fn.module.getButtonProp(mainTableIndex, key,
							"editPage").eventList;
				}
				/*  if(textVal&&textVal.length>0&&$("#eventTree").data("eventName")){
						setEvents(true);
						var ztreeObj = $.fn.zTree.getZTreeObj("eventTree");
					 	var tree_node = $("#eventTree").data("eventNode");
					 	if(tree_node){
							tree_node["iconSkin"] = "icon08";
							ztreeObj.updateNode(tree_node); 
					 	}
					 	
					} */
				var event_flag = false;
				var eventName = treeNode.name;
				for ( var i = 0; i < eventList.length; i++) {
					if (eventList[i].key == eventName) {
						//$("#functionText").val(eventList[i].content);
						win.setFunText(eventList[i].content);
						event_flag = true;
						break;
					}
				}

				if (!event_flag) {
					win.setFunText("");
				}
				$("#eventTree").data("eventName", eventName);
				$("#eventTree").data("eventNode", treeNode);
			} else if ($(mt_id).data("type") == "按钮属性"
					&& $("#main_tabs_div_1").data("title") == "查询页面设计") {
				//查询页面按钮属性加载

			} else if ($(mt_id).data("type") == "表单属性"
					&& $("#main_tabs_div_1").data("title") == "数据库设计") {/*表单属性的事件编辑*/
				//表单属性的编辑属性加载数据
				var tabs = $('#eventIframe').find("iframe");
				var win = tabs[0].contentWindow.window;
				var textVal = win.getFunText();
				/*保存函数编辑页面的内容——start 2019/8/5 13:59*/
				if (textVal && textVal.length > 0
						&& $("#eventTree").data("eventName")) {
					setEvents(true);
					var treeObj = $.fn.zTree.getZTreeObj("eventTree");
					var tree_node = $("#eventTree").data("eventNode");
					if (tree_node) {
						tree_node["iconSkin"] = "icon08";
						treeObj.updateNode(tree_node);
					}

				}
				/*保存函数编辑页面的内容——end 2019/8/5 13:59*/
				$("#addEventWin").data("nodeCaption", treeNode.caption);
				$("#fun_ztree_params").html("");
				$("#fun_ztree_description").html("");
				if (!treeNode.params) {
					treeNode.params = "";
				}
				win.setFunParams(treeNode.params);
				$("#fun_ztree_params").html(treeNode.params);
				$("#fun_ztree_description").html(treeNode.description);
				var extension = $.fn.module.getExtension($(mt_id).data(
						"tableIndex"));

				var eventList = extension[$(mt_id).data("pageType")].eventList;

				var event_flag = false;
				var eventName = treeNode.name;
				if (eventList && $.isArray(eventList)) {
					for ( var i = 0; i < eventList.length; i++) {
						if (eventList[i].key == eventName) {
							win.setFunText(eventList[i].content);
							event_flag = true;
							break;
						}
					}
				}

				if (!event_flag) {
					win.setFunText("");
				}
				$("#eventTree").data("eventName", eventName);
				$("#eventTree").data("eventNode", treeNode);

			} else { /*字段属性中的编辑事件*/
				var tabs = $('#eventIframe').find("iframe");
				var win = tabs[0].contentWindow.window;

				var textVal = win.getFunText();
                /*保存函数编辑页面的内容*/
				if (textVal && textVal.length > 0
						&& $("#eventTree").data("eventName")) {
					setEvents(true);
					var treeObj = $.fn.zTree.getZTreeObj("eventTree");
					var tree_node = $("#eventTree").data("eventNode");
					if (tree_node) {
						tree_node["iconSkin"] = "icon08";
						treeObj.updateNode(tree_node);
					}

				}
				$("#addEventWin").data("nodeCaption", treeNode.caption);
				//$("#funParam").html("");
				$("#fun_ztree_params").html("");
				$("#fun_ztree_description").html("");
				if (!treeNode.params) {
					treeNode.params = "";
				}
				//$("#funParam").html("function("+treeNode.params+") try{");
				win.setFunParams(treeNode.params);
				$("#fun_ztree_params").html(treeNode.params);
				$("#fun_ztree_description").html(treeNode.description);
				var event_flag = false;
				var eventName = treeNode.name;
				if (eventName) {
					//eventName = eventName.substring(2,treeNode.length);
					var tableIndex = $(mt_id).data("tableIndex");
					var fieldIndex = $(mt_id).data("fieldIndex");
					var field = $.fn.module
							.getFieldProp(tableIndex, fieldIndex);
					if (field) {
						var editProp = field.editProperties;
						if (editProp) {
							var eventList = editProp.eventList;
							for ( var i = 0; i < eventList.length; i++) {
								if (eventList[i].key == eventName) {
									//$("#functionText").val(eventList[i].content);
									win.setFunText(eventList[i].content);
									event_flag = true;
									break;
								}
							}
						}
					}
					if (!event_flag) {
						win.setFunText("");
					}
					$("#eventTree").data("eventName", eventName);
					$("#eventTree").data("eventNode", treeNode);
				}
			}
		}
	}
	//右键单击树形节点时候获得相应操作
	function appendFunction(event, treeId, treeNode) {
		if (treeId == "functionTree") {/*函数列表*/
			//alert("rightclick");
			var functions = treeNode.type;
			var fun_val = $("#functionText").val() + functions + ";";
			//$("#functionText").append(functions+";");
			$("#functionText").val(fun_val);
		} else if (treeId == "eventTree") {
			$("#eventTree").data("delNode", treeNode);
			$("#eventTree").data("eventName", treeNode.name);
			$("#delEvent").menu('show', {
				left : event.pageX,
				top : event.pageY
			});
		}
	}

	//移除控件上面绑定事件
	function delEvents() {

		if (($(mt_id).data("type") == "按钮属性"
				&& $("#main_tabs_div_1").data("title") == "编辑页面设计")
				|| ($(mt_id).data("type") == "表单属性" && $(mt_id).data("buttonType") == "menu"
					&& $("#main_tabs_div_1").data("title") == "数据库设计")) {
			var key = $(mt_id).data("buttonKey");
			var eventKey = $("#eventTree").data("eventName");
			//var fieldIndex = $(mt_id).data("fieldIndex");
			var edittabs = $('#edit_ifr').find("iframe");
			var editwin = edittabs[0].contentWindow.window;
			var mainTableIndex = editwin.mainTableIndex;
			var data = {
				"key" : eventKey
			};
			if ($(mt_id).data("buttonType") == "normal") {//普通按钮设置		 
				$.fn.module.delButtonEvents(mainTableIndex, key, "editPage",
						data);
				//showLineInfo(mainTableIndex,fieldIndex);
			} else if ($(mt_id).data("buttonType") == "menu") {//菜单按钮设置
				var rowIndex = $('#menuButtonList').data("rowIndex");
				var dataJson = $('#menuButtonList').edatagrid("getRows");
				var eventList = dataJson[rowIndex].eventList;
				if (eventList.length > 0) {
					var flag = -1;
					for ( var i = 0; i < eventList.length; i++) {
						if (eventList[i].key == eventKey) {
							flag = i;
							break;
						}
					}
					if (flag >= 0) {
						eventList.splice(flag, 1);
					}
				}
			}
			/*函数编辑界面值清空——start*/
			var tabs = $('#eventIframe').find("iframe");
			var win = tabs[0].contentWindow.window;
			win.setFunText("");
			win.setFunParams("");
			/*函数编辑界面值清空——end*/
			var treeObj = $.fn.zTree.getZTreeObj("eventTree");
			var tree_node = $("#eventTree").data("delNode");
			tree_node["iconSkin"] = "icon03";
			treeObj.updateNode(tree_node);
			//$("#addEventWin").window("close");
		} else if ($(mt_id).data("type") == "表单属性"
				&& $("#main_tabs_div_1").data("title") == "数据库设计") {
			var eventKey = $("#eventTree").data("eventName");
			var edittabs = $('#edit_ifr').find("iframe");
			var editwin = null;
			if(edittabs[0]){
				editwin = edittabs[0].contentWindow.window;
			}else{			
				var tb =$('#formEdit_ifr').find("iframe");
				editwin = tb[0].contentWindow.window;
			}
			var mainTableIndex = editwin.mainTableIndex;
			var data = {
				"key" : eventKey
			};

			var extension = $.fn.module.getExtension($(mt_id)
					.data("tableIndex"));
			var eventList = extension[$(mt_id).data("pageType")].eventList;
			if (eventList.length > 0) {
				var flag = -1;
				for ( var i = 0; i < eventList.length; i++) {
					if (eventList[i].key == eventKey) {
						flag = i;
						break;
					}
				}
				if (flag >= 0) {
					eventList.splice(flag, 1);
				}
			}
			/*函数编辑界面值清空——start*/
            var tabs = $('#eventIframe').find("iframe");
            var win = tabs[0].contentWindow.window;
            win.setFunText("");
            win.setFunParams("");
            /*函数编辑界面值清空——end*/
			var treeObj = $.fn.zTree.getZTreeObj("eventTree");
			var tree_node = $("#eventTree").data("delNode");
			tree_node["iconSkin"] = "icon03";
			treeObj.updateNode(tree_node);
			//$("#addEventWin").window("close");
		} else if ($(mt_id).data("type") == "按钮属性"
				&& $("#main_tabs_div_1").data("title") == "查询页面设计") {

		} else {
			var eventName = $("#eventTree").data("delNode").name;
			//var eventName = delName.substring(2,delName.length);
			var tableIndex = $(mt_id).data("tableIndex");
			var fieldIndex = $(mt_id).data("fieldIndex");
			$.fn.module.deleteEvent(tableIndex, fieldIndex, eventName);
			var tabs = $('#eventIframe').find("iframe");
			var win = tabs[0].contentWindow.window;
			win.setFunText("");
			win.setFunParams("");
			var treeObj = $.fn.zTree.getZTreeObj("eventTree");
			var tree_node = $("#eventTree").data("delNode");
			tree_node["iconSkin"] = "icon03";
			treeObj.updateNode(tree_node);
			showLineInfo(tableIndex, fieldIndex);
		}
	}
	//将整个页面数据保存成字符串json
	function saveModule() {
		/*保存表格内字段编辑——2019/8/6*/
		var tableClass = $('.tableClass');
		tableClass.edatagrid('saveRow');
		/*保存表格内字段编辑——2019/8/6*/
		var date = new Date().format();
		var module = $.extend(true, {}, $.fn.module.getModule());
		var key = "";
		var temp = "";
		var mdf = module.dataSource.formList;
		var all_module_tables = getAllModuleMainTables();/*获取所有表*/
		var module_moduleProperties_key = module.moduleProperties.key;
		var copy_no_duplication = true;//是否重复
		if(module.moduleProperties.relevanceModule && module.moduleProperties.relevanceModule!="") {//去除关联表
			var deleteform = "";
			for(var i in mdf) {
				if(mdf[i].index==99999){
					deleteform = mdf[i].key;
					mdf.splice(i,1);
					break;
				}
			}
			var tabs = module.dataSource.tabsList;
			for(var i in tabs) {
				for(var j=0; j < tabs[i].tabList.length; j++) {
					if(tabs[i].tabList[j].form == deleteform) {
						tabs[i].tabList.splice(j,1);
						j--;
					}
				}
			}
		}
		for(var i = 0; i < mdf.length; i++) {
			var frm = mdf[i];
			var frm_tableName = frm.tableName;
			for(var j = i+1; j < mdf.length; j++) {
				//判断模块内是否存在表属性名重复
				if(mdf[j].tableName == frm_tableName) {
					copy_no_duplication = false;
					break;
				}
			}
			if(copy_no_duplication === false) {
				break;
			}
			for(var j in all_module_tables) {
				//判断模块间是否有表属性名重复
				var module_key = all_module_tables[j].key;
				module_key = module_key.substring(0, module_key.indexOf("."));
				if(all_module_tables[j].column == frm_tableName && module_key != module_moduleProperties_key.toLowerCase()) {
					copy_no_duplication = false;
					break;
				}
			}
			if(copy_no_duplication == false) {
				break;
			}
		}
		//判断是否重复
		if(copy_no_duplication) {
			module.moduleProperties.ownerProperties.lastModifiedTime = date;
			if (!module.moduleProperties.ownerProperties.createTime) {
				module.moduleProperties.ownerProperties.createTime = date;
			}
			var newKey = "${param.newKey}";
			if ("${param.flag}" != "undefined") {
				var arr = "${param.newKey}".split(".");
				newKey = "${param.newKey}".replace(arr[1], "${param.flag}");
				//alert("arr:"+arr+"  flag:"+"${param.flag}");
			} else {
				temp = "${param.allKey}.";
			}
			if (module.moduleProperties.key) {
				key = "${param.allKey}." + module.moduleProperties.key;
				if ("${param.oldKey}") {
					module.moduleProperties.key = "${param.key}";
				}
			} else {
				if ("${param.key}") {
					key = "${param.allKey}." +
					"${param.oldKey}";
				} else {
					key = "${param.allKey}." + "${param.key}";
				}
				$.extend(module.moduleProperties, {
					"key" : "${param.key}"
				});
			}

			//	 alert("${param.key}"+":"+"${param.oldKey}");
			//	 alert(module.moduleProperties.key);
			//	var key = "${param.allKey}." + module.moduleProperties.key;
			//	var key = "${param.allKey}." + "${param.key}";
			var checkArray = [];
			//module.moduleProperties.caption是叶子节点的中文名称
			checkArray.push(module.moduleProperties.caption);
			//module.moduleProperties.key是叶子节点的英文名称
			checkArray.push(module.moduleProperties.key);
			for ( var i = 0; i < mdf.length; i++) {
				if (mdf[i].caption) {
					checkArray.push(mdf[i].caption);
				} else {
					checkArray.push("");
				}

				if (mdf[i].tableName) {
					checkArray.push(mdf[i].tableName);
				} else {
					checkArray.push("");
				}
				if (mdf[i].key) {
					checkArray.push(mdf[i].key);
				} else {
					checkArray.push("");
				}
				//添加报表按钮
				if(mdf[i].isMaster){
					var buttonAry = mdf[i].extension.editPage.buttonList;
					if(!buttonAry.length){
						 var data = {"caption":'报表',"key":'bt_report',"iconCls":"icon-report"};
						 $.fn.module.setButtonProp(mdf[i].index,'bt_report','editPage',data);
					}
				}
			}
			//alert(checkArray);
			if ($.inArray("", checkArray) >= 0) {
				alert("请检验模块及表信息是否填写完整");
			} else {
				var tab_isNull = false;
				for ( var i = 0; i < mdf.length; i++) {
					if (mdf[i].fieldList.length == 0) {
						tab_isNull = true;
					}
				}
				if (tab_isNull) {
					alert("主表或子表至少要有一行");
				} else {
					var jsonStr = JSON.stringify(module);
					var treeId = "${param.treeId}";
					//	alert(treeId);
					var options = {
						data : {
							"key" : key,
							"treeId" : treeId,
							"newKey" : newKey,
							"moduleJson" : jsonStr
						},
						//data: jsonStr,
						url : "${ctx}/design/module/saveModule",
						success : function(data) {
							var formlist = module.dataSource.formList;
							for(var i=0;i<formlist.length;i++){
							    formlist[i] = $.extend(true,{},formlist[i],{"flag":false})
                            }
							$.fn.module.setModule(module);
							$MsgUtil.alert("保存模块成功！","success");
						}
					};
					fnFormAjaxWithJson(options, true);
				}
			}
		} else {
			alert("新增表的属性名经过处理与已有表重复");
		}
	}

	/*初始化模块*/
	function initModuleByKey(key, treeId) {
		//alert(treeId);
		var options = {
			data : {
				"key" : key,
				"treeId" : treeId
			//	"newKey":newKey
			},
			url : "${ctx}/design/module/getModule",
			success : initModule
		};
		fnFormAjaxWithJson(options);
	}

	//删除表格
	function delectTab() {
		var bl = $.fn.module.deleteTable($(mt_id).data("tableIndex"));
		if (bl) {
			$("#module_table_tab").tabs("close",
					$("#module_table_tab").data("tabTitle"));
		}

	}

	//加载时判断内存中是否存在如果存在直接加载不存在新建table
	function initModule(data) {
		/* data.msg.moduleProperties.caption = decodeURI("${param.name}");
		data.msg.moduleProperties.key = "${param.key}"; */
		if (data.msg.moduleProperties.key == null) {
			var module = $.fn.module.getModule();
			module.moduleProperties.caption = decodeURI("${param.name}");
			module.moduleProperties.key = "${param.key}";
			$.extend(data.msg, module);
		}
		var module = data.msg;
		var forms = module.dataSource.formList;
		var tabs = module.dataSource.tabsList;
		$("#main_tabs_div_1").data("forms", forms);
		if (!forms || forms.length == 0) {
			//alert("${param.nodesType}");
			var nodesType = "${param.nodesType}";
			if (nodesType == "dict") {
				var module = $.fn.module.getModule();
				module.moduleProperties.type = "dict";
				addTab("主表", null, true);

			}else if (nodesType == "form" || nodesType == "app") {
				var module = $.fn.module.getModule();
				module.moduleProperties.type = nodesType;
				addTab("主表", null, true);
			}else {
				addTab("主表", null, true);
				$.fn.module.setTabsList({
					"rows" : 1,
					"tabList" : [ {
						"rows" : 1,
						"cols" : 1,
						"form" : "MainTable",
						"caption" : "主要信息",
						"languageText" : ""
					} ]
				});
				addTab("子表");
				$.fn.module.setTabsList({
					"rows" : 2,
					"tabList" : [ {
						"rows" : 2,
						"cols" : 1,
						"form" : "ChildTable1",
						"caption" : "子表信息1",
						"languageText" : ""
					} ]
				});
			}

			changeModuleProp();

		} else {
			$.ajax({
				async : false,
				url : "${ctx}/design/module/getRelevanceModuleMaster",
				data : {"key" : "module." + module.moduleProperties.relevanceModule, "treeId" : "${param.treeId}"},
				success : function(data) {
					var frm = data.msg.form;
					var tabs = data.msg.tabs;
					if(frm) {
						var url = "${ctx}/jsp/table.jsp?index=99999";
						var title = module.moduleProperties.caption + "关联主表";
						module.dataSource.formList.push(frm);
						
						for(var i in tabs) {
							var tabsList = module.dataSource.tabsList;
							var b = true;
							for(var j in tabsList) {
								if(tabsList[j].rows === tabs[i].rows) {
									var tabList_ = tabsList[j].tabList;
									for(var h in tabList_) {
										if(tabList_[h].cols >= tabs[i].cols) {
											tabList_[h].cols++;
										}
									}
									tabList_.push(tabs[i]);
									b = false;
									break;
								}
							}
							if(b) {
								tabsList.push({rows:i, tabList:[tabs[i]]});
							}
						}
						
						frm.index = 99999;
						frm.caption = title;
						$('#module_table_tab').data(title, 99999);
						$("#module_table_tab").tabs("add", {
							title : title,
							href : url,
							closable : false,
							cache : true,
							fit : true,
							selected : true
						});
					}
				},
				dataType : "json"
			});
		
			for ( var i = 0; i < forms.length; i++) {
				var frm = forms[i];
				if ("${param.copyTreeId}" && "${param.copyTreeId}" == "${param.treeId}") {
					if (frm.isMaster) {
						var frm_key_clone =  frm["key"];
						frm["caption"] = "${param.name}" + "主表";
						//frm["tableName"] = $.fn.toPinyin(frm.caption).replace("_", "").toUpperCase();
						//frm["key"] = "${param.key}" + "ZhuBiao";
						frm["tableName"] = $.fn.module.covToDBRule($.fn.toPinyin(frm.caption).replace("_", "").toUpperCase());
						frm["key"] = $.fn.module.convertProp(frm["tableName"], true);
						for ( var j = 0; j < tabs.length; j++) {
							var tabList = tabs[j]["tabList"];
							if (tabList.length > 0) {
								for ( var k = 0; k < tabList.length; k++) {
									if (tabList[k]["form"] == frm_key_clone) {
										//tabList[k]["form"] = "${param.key}" + "ZhuBiao";
										tabList[k]["form"] = frm["key"];
									}
								}
							}
						}
					} else {
						var frm_key_clone =  frm["key"];
						frm["caption"] = "${param.name}" + "子表" + i;
						//frm["tableName"] = $.fn.toPinyin(frm.caption).replace("_", "").toUpperCase();
						//frm["key"] = "${param.key}" + "ZiBiao" + i;
						frm["tableName"] = $.fn.module.covToDBRule($.fn.toPinyin(frm.caption).replace("_", "").toUpperCase());
						frm["key"] = $.fn.module.convertProp(frm["tableName"],true);
						for ( var j = 0; j < tabs.length; j++) {
							var tabList = tabs[j]["tabList"];
							if (tabList.length > 0) {
								for ( var k = 0; k < tabList.length; k++) {
									if (tabList[k]["form"] == frm_key_clone) {
										//tabList[k]["form"] = "${param.key}" + "ZiBiao" + i;
										tabList[k]["form"] = frm["key"];
									}
								}
							}
						}
					}
				}
				if(frm.index!=99999) {//关联表
					addTab("", frm);
				}
			}

			if ("${param.copyTreeId}") {
				var moduleP = module.moduleProperties;
				moduleP["caption"] = "${param.name}";
				moduleP["key"] = "${param.key}";
				moduleP["type"]="${param.nodesType}";
				module.flowList = [];
			}
			$.fn.module.setModule(module);
		}
	}
	//加载时候初始化sqlTab表格
	function initSqlTab(type) {
		/*var data = $.fn.module.getSqlPropJson($(mt_id).data("tableIndex"),$(mt_id).data("fieldIndex"));
		if(data){
			if(data.data.rows.length>0||data.sql){
				$("#sqlContent").val(data.sql);
			$("#sqlTab").edatagrid("loadData", data.data); 
			}else{ 
				//var data = $("#module_table_propGrid").edatagrid("getData");
			//var type = data.rows[10].value;
			initsqlTabGrid(type);
			}
		}*/
		doInitComboSql("dsRefKey");
	}//加载时候初始化treeSqlTab表格
	function initTreeSqlTab() {
		var comboboxData = [];
		var comboData = {};
		var data = $.fn.module.getFormProp($(mt_id).data("tableIndex"));
		var queryData = [];
		if (data) {
			if (!data.queryType) {
				if (data.fieldList) {
					if ($('input[name=queryType]:checked').val()) {
						//循环生成combobox所需显示类容
						for ( var i = 0; i < data.fieldList.length; i++) {
							comboData = $.extend({}, {
								"id" : data.fieldList[i].key,
								"text" : data.fieldList[i].caption
							});
							comboboxData.push(comboData);
						}
						$('#treeComboBox').combobox({
							data : comboboxData,
							valueField : 'id',
							textField : 'text'
						});
						$('#treeComboBox').combobox({
							'onSelect' : function(record) {
								$("#treeComboBox").data("text", record.text);
							}
						});
					}
					/*$('#treeSqlTab').edatagrid("loadData",queryData);
					$('#treeSqlTab').edatagrid("addRow");*/
				}
			}
			doInitComboSql("treeQueryRefKey");
		}

	}
	//初始化fixTab表格
	function initFixTab(type) {
		var data = $.fn.module.getFixtabJson($(mt_id).data("tableIndex"), $(
				mt_id).data("fieldIndex"));
		if (data) {
			//if(data.data.rows.length>0){
			$("#fixTab").edatagrid("loadData", data.data);
			//	}else{
			//var data = $("#module_table_propGrid").edatagrid("getData");
			//var type = data.rows[10].value;
			//initfixTabGrid(type);
			//	}

		}
	}
	var langType = [];
	var flagData = [];
	var orderByData = [];
	var dataTypes = [];
	var _sqlQueryWhere = [];

	$(function() {
		langType = $.fn.module.getLangType();
		flagData = $.fn.module.getFlagData();
		orderByData = $.fn.module.getOrderByData();
		dataTypes = $.fn.module.getDataType();
		_sqlQueryWhere = $.fn.module.getSqlQueryWhere();
	});
	function langDataType(value) {
		for ( var i = 0; i < langType.length; i++) {
			if (langType[i].key == value)
				return langType[i].caption;
		}
		return value;
	}

	function flagDataType(value) {
		for (var i = 0; i < flagData.length; i++) {
			if (flagData[i].key == value)
				return flagData[i].caption;
		}
		return value;
	}

	function orderByDataType(value) {
		for ( var i = 0; i < orderByData.length; i++) {
			if (orderByData[i].key == value)
				return orderByData[i].caption;
		}
		return value;
	}

	function dataTypeFormatter(value) {
		for ( var i = 0; i < dataTypes.length; i++) {
			if (dataTypes[i].key == value)
				return dataTypes[i].caption;
		}
		return value;
	}

	function sqlQueryType(value) {
		for ( var i = 0; i < _sqlQueryWhere.length; i++) {
			if (_sqlQueryWhere[i].key == value)
				return _sqlQueryWhere[i].caption;
		}
		return value;
	};

	var mt_id = "#module_table_propGrid";

	function changeTableProp(index) {
		changePropGrid('表单属性', index);
	}

	function changeFieldProp(tableIndex, fieldIndex, rowIndex) {
		changePropGrid('字段属性', tableIndex, fieldIndex, rowIndex);
	}
	
	//批量设置属性
	function changeFieldsProp(tableIndex, fieldIndexes, rowIndexes){
		changePropGrid('字段公共属性', tableIndex, fieldIndexes, rowIndexes);
	}
	
	function changeModuleProp() {
		changePropGrid('模块属性');
	}
	
	function changeLayoutProp(rows, cols, isChild) {
		changePropGrid('布局属性', "", "", isChild, cols, rows);
	}

	function changePropGrid(title, tableIndex, fieldIndex, rowIndex, cols, rows) {
		//	alert(title+" : "+tableIndex+" : "+fieldIndex+" : "+rowIndex);
		var data = {};
		if (title == "表单属性") {
			data = $.fn.module.getFormPropJson(tableIndex);
			$(mt_id).data("tableIndex", tableIndex);
			$(mt_id).data("type", title);
			$(mt_id).propertygrid({
				title : title
			});
	 		var modAry = [];
			modAry = getAllModuleMainTables();
 			if(modAry.length>0){
				var drows = data.rows;
				for ( var i = 0, len = drows.length; i < len; i++) {
					if (drows[i].code == "referEntityTableName") {
						drows[i].editor.options.data = modAry;
						break;
					}
				} 
 			}
			$(mt_id).propertygrid("loadData", data);
		} else if (title == "字段属性") {
			data = $.fn.module.getFieldPropJson(tableIndex, fieldIndex);
			var layoutData = $.fn.module.getAllTabList();
			var drows = data.rows;
			for ( var i = 0, len = drows.length; i < len; i++) {
				if (drows[i].code == "layoutTab") {
					drows[i].editor.options.data = layoutData;
					break;
				}
			}
			$(mt_id).data("tableIndex", tableIndex);
			$(mt_id).data("fieldIndex", fieldIndex);
			$(mt_id).data("rowIndex", rowIndex);
			$(mt_id).data("type", title);
			$(mt_id).propertygrid({
				title : title
			});
			$(mt_id).propertygrid("loadData", data);
		} else if (title == "模块属性") {
			data = $.fn.module.getModulePropJson();
			$(mt_id).propertygrid({
				title : title
			});		
			$(mt_id).propertygrid("loadData", data);
		} else if (title == "布局属性") {
			$(mt_id).propertygrid({
				title : title
			});
			if (rowIndex) {
				data = $.fn.module.getTabListJsonProp(rows, cols, true);
			} else {
				data = $.fn.module.getTabListJsonProp(rows, cols);
			}
			$(mt_id).propertygrid("loadData", data);
		} else if (title == "字段公共属性") {
			//console.log($(mt_id));
			var layoutData = $.fn.module.getAllTabList();
			//设置值不同的属性以第一个字段的属性显示
			data = [];
			data.push($.fn.module.getFieldPropJson(tableIndex, fieldIndex[0]));
			//过滤非批设置字段属性
			var rule = ["isVirtual"  //虚拟字段
			            ,"enableFieldAuthCheck"  //字段权限启用
			            ,"enableRevise" //字段修订启用
			            ,"dataType" //数据类型
			            ,"size"  //长度
			            ,"scale"  //精度
			            ,"notNull"  //非空
			            ,"unique"  //唯一
			            ,"editType"  //编辑类型
			            ,"multiple"  //允许多选
						,"visible"	//可见
			            ,"readOnly"  //只读
			            ,"validInView"  //查看状态可操作
			            ,"charCase"  //编辑属性
			            ,"align"  //内容对齐方式
			            ,"rows"  //占用行数
			            ,"cols"  //占用列数
			            ,"layer"  //表头层级
			            ,"rowspan"  //表头跨行数
			            ,"colspan"  //表头跨列数
			            ,"quickAdd"  //快速添加数据
			            ,"autoCollapse"  //树形自动折叠
			            ,"layoutTab"  //分组归属
			            ,"width"  //宽度
			            ,"height"  //高度
			            ,"top"  //上边距
			            ,"left"  //左边距
			            ,"showInGrid"  //列表显示
			            ,"align"  //表格内对齐方式
			            ,"sortable"  //是否排序字段
			            ,"filterable"  //是否页面过滤
			            ,"width"  //列宽
			            ,"showInTabSearch"  //作为标签页查询
			            ,"showInSearch"  //作为查询条件
			            ,"useAsShareUser"  //作为共享用户属性
			            ,"frozenColumn"  //作为冻结列
			            ,"mergeable" //合并相同值的单元格
			            ];
			var oldarraydata = data[0].rows;
			var newarraydata = [];
			for(var i in oldarraydata) {
				for(var j in rule) {
					if(oldarraydata[i].code == rule[j]){
						if(oldarraydata[i].code == "layoutTab") {
							oldarraydata[i].editor.options.data = layoutData;
						}
						newarraydata.push(oldarraydata[i]);
						break;
					}
				}
			}
			data[0].rows = newarraydata;
			for(var i = 1; i < fieldIndex.length; i++) {
				var adata = $.fn.module.getFieldPropJson(tableIndex, fieldIndex[i]);
				var rows = adata.rows;
				for(var j in rows){
					if(rows[j].code == "layoutTab") {
						rows[j].editor.options.data = layoutData;
						break;
					}
				}
				data.push(adata);
			}
			$(mt_id).data("tableIndex", tableIndex);
			$(mt_id).data("fieldIndexes", fieldIndex);
			$(mt_id).data("rowIndexes", rowIndex);
			$(mt_id).data("type", title);
			$(mt_id).propertygrid({
				title : title
			});
			$(mt_id).propertygrid("loadData", data[0]);
		}
		$(mt_id).data("type", title);
		/* $(mt_id).propertygrid({title:title});
		$(mt_id).propertygrid("loadData",data);  */
	}

	function hideAllDataSourceView() {
		$("#sqlView").hide();
		$("#fixView").hide();
		$("#moduleView").hide();
	}

	function loadModuleTables() {
		var options = {
			data : {
				"treeId" : "${param.treeId}",
				"urlType" : "${param.title}"
			},
			url : "${ctx}/design/doXml/getModuleTables",
			success : function(data) {
				var tbData = data.msg;
				if (tbData) {
					$("#cb_moduleTable").combobox({
						"data" : tbData,
						"valueField" : "key",
						"textField" : "caption",
						 "onSelect" : function(record) {
							loadModuleTableFields(record.key);
						}  
					});
					var dsData = $.fn.module.getDataStatus();
	    			$("#dataStatus").combobox({  
					    "data":dsData,
					    "valueField":"key",  
					    "textField":"caption"
					});
					var formula = $("#moduleFields").data("formula");
					if (formula && formula.sql) {
						var en = formula.sql.entityName;						
						$("#cb_moduleTable").combobox("setValue", en);
						var ds = formula.sql.dataStatus;
						$("#dataStatus").combobox("setValue", ds);
						if(en){
							loadModuleTableFields(en);
						}
						if (formula.sql.isCheckAuth) {
							$("#isCheckAuth").attr("checked", "checked");
						} else {
							$("#isCheckAuth").removeAttr("checked");
						}
					}
				}
			}
		};
		fnFormAjaxWithJson(options);
	}
	function loadCommonTables() {
		var options = {
			data : {
				"treeId" : "${param.treeId}",
				"urlType" : "${param.title}"
			},
			url : "${ctx}/design/doXml/getCommonTables",
			success : function(data) {
				var tbData = data.msg;
				if (tbData) {
					$("#cb_commonTable").combobox({
						"data" : tbData,
						"valueField" : "key",
						"textField" : "caption",
						"onChange" : function(newVal, oldVal) {
							loadModuleCommonTableFields(newVal);
						}
					});
				}
			}
		};
		fnFormAjaxWithJson(options);
	}
	function loadFormTables(){
		var tbData = getAllModuleMainTables();
			/* var form =$.fn.module.getFormProp($(mt_id).data("tableIndex"));
			var tName = form["tableName"]; */				
			if (tbData) {
				/* 	for(var i=0 ;i<tbData.length;i++){
						if(tbData[i].column==tName){
							tbData.splice(i,1);
							break;
						}
					} */
				$("#formTables").combobox({
					"data" : tbData,
					"valueField" : "key",
					"textField" : "caption",
					"onChange" : function(newVal, oldVal) {
						loadFormFields(newVal);
					}
				});
		}
	}
	function getAllModuleMainTables(){
		var tbData = []; 
		var options = {
				data : {
					"treeId" : "${param.treeId}",
					"urlType" : "${param.title}"||"${param.tabTitle}"
				},
				url : "${ctx}/design/doXml/getFormTables",
				async:false,
				success : function(data) {
					if(data.msg){
						 tbData =data.msg;
					}
				}
			};
		fnFormAjaxWithJson(options);
		return tbData
	}
	var __moduleTableFieldsColumns = [];
	var _moduleCommonTableFieldsColumns = [];
	var _eName = "";
	function loadModuleTableFields(en) {
		var options = {
			data : {
				"treeId" : "${param.treeId}",
				"entityName" : en
			},
			url : "${ctx}/design/doXml/getTableFields",
			success : function(data) {
				__moduleTableFieldsColumns = data.msg;
				initModuleTableFields();
			}
		};
		fnFormAjaxWithJson(options);
	}
	function loadModuleCommonTableFields(en) {
		var options = {
			data : {
				"treeId" : "${param.treeId}",
				"entityName" : en
			},
			url : "${ctx}/design/doXml/getCommonTableFields",
			success : function(data) {
				_moduleCommonTableFieldsColumns = data.msg;
				_eName = en;
				initModuleCommonTableFields();
			}
		};
		fnFormAjaxWithJson(options);
	}
	function loadFormFields(en) {
		var options = {
			data : {
				"treeId" : "${param.treeId}",
				"entityName" : en
			},
			url : "${ctx}/design/doXml/getCommonTableFields",
			success : function(data) {				
				__moduleTableFieldsColumns = data.msg;
				_eName = en;
				initFormFields(data.msg);
			}
		};
		fnFormAjaxWithJson(options);
	}
	function initFormFields(dataFields) {
		$('#formFields').edatagrid({
			nowrap : true,
			striped : true,
			rownumbers : true,
			fitColumns : true,
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			} ] ],
			columns : [ [ {
				field : 'key',
				title : '显示名',
				width : 100,
				editor : "text"
			}, {
				field : 'colName',
				title : '字段名',
				width : 150,
				editor : "text"
			}, {
				field : 'dataType',
				title : '字段类型',
				width : 150,
				editor : "text",
				formatter : function(value, rowData, rowIndex) {
					if ("dtString" == value) {
						return "字符串";
					} else if ("dtLong" == value) {
						return "整数";
					} else if ("dtDouble" == value) {
						return "小数";
					} else if ("dtDateTime" == value) {
						return "时间";
					} else if ("dtText" == value) {
						return "文本";
					} else if ("dtImage" == value) {
						return "图片";
					}
				}
			}, {
				field : 'size',
				title : '字段长度',
				width : 60,
				editor : "text"
			}, {
				field : 'scale',
				title : '字段精度',
				width : 60,
				editor : "text"
			}, {
				field : 'notNull',
				title : '非空',
				width : 50,
				editor : "text"
			}, {
				field : 'unique',
				title : '唯一',
				width : 50,
				editor : "text"
			} ] ]
		});
		var commonFields = [];
		for ( var i = 0; i < dataFields.length; i++) {
			var data = dataFields[i];
			if(data["caption"]){
				var fd = {};
				var dp = data["dataProperties"];
				fd['key'] = data["caption"];
				fd['colName'] = dp.colName;
				fd['dataType'] = dp.dataType;
				fd['colName'] = dp.colName;
				fd['size'] = dp.size;
				fd['scale'] = dp.scale;
				fd['notNull'] = dp.notNull;
				fd['unique'] = dp.unique;
				commonFields.push(fd);
			}
		}
		$('#formFields').edatagrid("loadData", commonFields);
		
	}

	function addFields() {
		var fields = $('#formFields').edatagrid("getSelections");
		var tableIndex = $(mt_id).data("tableIndex");
		var fieldsData = [];
		for ( var i = 0; i < fields.length; i++) {
			var name = fields[i].key;
			if(name){
				for ( var j = 0; j < __moduleTableFieldsColumns.length; j++) {
					if (__moduleTableFieldsColumns[j].caption == name) {
						var obj = $.extend(__moduleTableFieldsColumns[j],{"tabCols": 1,"tabRows": 1});
						fieldsData.push(obj);
					}
				}
			}
		}
		$.fn.module.setFields(fieldsData, tableIndex, _eName);
		$('#formFieldWin').window('close');
		
	 	var data = {
			"total" : 0,
			"rows" : []
		};
		var frm = $.fn.module.getFormProp(tableIndex);
		var fl = frm.fieldList;
		data.total = fl.length;
		$.each(fl, function(k, v) {
			if (v.key) {
				var tv = $.fn.module.convertFieldToDgJson(v, tableIndex);
				data.rows.push(tv);
			}
		});
		$("#table_grid_" + tableIndex).edatagrid("loadData", data); 
		var addData = {};
		addData["dataType_" + tableIndex] = "dtString";
		addData["size_" + tableIndex] = "100";
		addData["scale_" + tableIndex] = "0";
		$("#table_grid_" + tableIndex).edatagrid("addRow", addData);
	}
	function initModuleTableFields() {
		$('#moduleFields').edatagrid({
			nowrap : true,
			striped : true,
			rownumbers : true,
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			} ] ],
			columns : [ [ {
				field : 'key',
				title : '字段Key',
				width : 80,
				editor : "text"
			}, {
				field : 'caption',
				title : '显示名称',
				width : 100,
				editor : "text"
			}, {
				field : 'column',
				title : '字段名(column)',
				width : 150,
				editor : {
					type : 'combobox',
					options : {
						valueField : 'key',
						textField : 'caption',
						data : __moduleTableFieldsColumns
					}
				}
			}, {
				field : "order",
				title : '显示或查询方式(order)',
				width : 150,
				formatter : orderByDataType,
				editor : {
					type : 'combobox',
					options : {valueField:'key',textField:'caption',data:orderByData}
				}
			}, {
				field : "width",
				title : '列宽(width)',
				width : 100,
				editor : "text"
			}, {
				field : "columntype",
				title : 'key字段类型',
				width : 100,
				editor : {
					type : 'combobox',
					options : {valueField:'key',textField:'caption',data:[{key:'string',caption:'字符串'},{key:'long',caption:'整型'}]}
				}
			} ] ],
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$("#moduleFields").edatagrid("selectRow", rowIndex);
				$('#delSql').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
				$("#moduleFields").data("rowIndex", rowIndex);
			}
		});
		var formula = $("#moduleFields").data("formula");
		if (formula && formula.sql) {
			var data = $.extend([], formula.sql.setValueItemList);
			if (data) {
				$('#moduleFields').edatagrid("loadData", {
					"total" : data.length,
					"rows" : data
				});
				$("#moduleFields").edatagrid('addRow', {
					"key" : "",
					"caption" : "",
					"column" : ""
				});
			}
		} else {
			initModuleTableGrid();
		}
	}
	function initModuleCommonTableFields() {
		$('#commonFields').edatagrid({
			nowrap : true,
			striped : true,
			rownumbers : true,
			fitColumns : true,
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			} ] ],
			columns : [ [ {
				field : 'key',
				title : '显示名',
				width : 100,
				editor : "text"
			}, {
				field : 'colName',
				title : '字段名',
				width : 150,
				editor : "text"
			}, {
				field : 'dataType',
				title : '字段类型',
				width : 150,
				editor : "text",
				formatter : function(value, rowData, rowIndex) {
					if ("dtString" == value) {
						return "字符串";
					} else if ("dtLong" == value) {
						return "整数";
					} else if ("dtDouble" == value) {
						return "小数";
					} else if ("dtDateTime" == value) {
						return "时间";
					} else if ("dtText" == value) {
						return "文本";
					} else if ("dtImage" == value) {
						return "图片";
					}
				}
			}, {
				field : 'size',
				title : '字段长度',
				width : 60,
				editor : "text"
			}, {
				field : 'scale',
				title : '字段精度',
				width : 60,
				editor : "text"
			}, {
				field : 'notNull',
				title : '非空',
				width : 50,
				editor : "text"
			}, {
				field : 'unique',
				title : '唯一',
				width : 50,
				editor : "text"
			} ] ]
		});

		var commonFields = [];
		for ( var i = 0; i < _moduleCommonTableFieldsColumns.length; i++) {
			var data = _moduleCommonTableFieldsColumns[i];
			if(data["key"]) {
				var fd = {};
				var dp = data["dataProperties"];
				fd['key'] = data["caption"];
				fd['colName'] = dp.colName;
				fd['dataType'] = dp.dataType;
				fd['colName'] = dp.colName;
				fd['size'] = dp.size;
				fd['scale'] = dp.scale;
				fd['notNull'] = dp.notNull;
				fd['unique'] = dp.unique;
				commonFields.push(fd);
			}
		}
		$('#commonFields').edatagrid("loadData", commonFields);
	}

	function addCommonFields() {
		var fields = $('#commonFields').edatagrid("getSelections");
		var tableIndex = $(mt_id).data("tableIndex");
		var commonFieldsData = [];
		for ( var i = 0; i < fields.length; i++) {
			var name = fields[i].key;
			for ( var j = 0; j < _moduleCommonTableFieldsColumns.length; j++) {
				if (_moduleCommonTableFieldsColumns[j].caption == name) {
					var obj = $.extend({}, _moduleCommonTableFieldsColumns[j]);
					commonFieldsData.push(obj);
				}
			}
		}
		$.fn.module.setCommonFields(commonFieldsData, tableIndex, _eName);
		$('#openCommonWin').window('close');
		var data = {
			"total" : 0,
			"rows" : []
		};
		var frm = $.fn.module.getFormProp(tableIndex);
		var fl = frm.fieldList;
		data.total = fl.length;
		$.each(fl, function(k, v) {
			if (v.key) {
				var tv = $.fn.module.convertFieldToDgJson(v, tableIndex);
				data.rows.push(tv);
			}
		});
		$("#table_grid_" + tableIndex).edatagrid("loadData", data);
		var addData = {};
		addData["dataType_" + tableIndex] = "dtString";
		addData["size_" + tableIndex] = "100";
		addData["scale_" + tableIndex] = "0";
		$("#table_grid_" + tableIndex).edatagrid("addRow", addData);
	}
	//删除公式SQL语句属性行
	function delSqlViewRow() {
		var rows = $("#moduleFields").edatagrid('getRows');
		if (rows.length > 1) {
			var index = $("#moduleFields").data("rowIndex");
			$("#moduleFields").edatagrid('deleteRow', parseInt(index));
		} else {
			alert("默认保留一行不允许删除,如需改变请单击右键修改!");
		}
	}

	function initModuleTableGrid() {
		var type = $('#openFormulaWin').data("editType");
		if (type == "ComboTree") {
			$("#moduleFields").edatagrid('addRow', {
				"key" : "id",
				"caption" : "编号",
				"column" : "id"
			});
			$("#moduleFields").edatagrid('addRow', {
				"key" : "text",
				"caption" : "名称",
				"column" : "caption"
			});
			$("#moduleFields").edatagrid('addRow', {
				"key" : "pid",
				"caption" : "父编号",
				"column" : "pid"
			});
		} else {
			$("#moduleFields").edatagrid('addRow', {
				"key" : "key",
				"caption" : "编号",
				"column" : "id"
			});
			$("#moduleFields").edatagrid('addRow', {
				"key" : "caption",
				"caption" : "显示名称",
				"column" : "caption"
			});
		}
	}

	$(document)
			.ready(
					function() {
						//alert("maxIndex:"+_index);
						//加载时候判断类型然后隐藏相应的模块
						if ("${param.nodesType}" == "dict") {
							$("#main_tabs_div_1").tabs('close', '编辑页面设计').tabs(
									'close', '业务流程设计');
						}else if ("${param.nodesType}" == "form"){
							$("#main_tabs_div_1").tabs('close', '查询页面设计').tabs(
									'close', '业务流程设计');
						}

						$("#ruleMainTabs input")
								.live(
										'keydown',
										'return',
										function(evt) {
											$("#sqlWhereTable").edatagrid(
													"addRow", {});
											return false;
										});

						$('#openFormulaWin')
								.window(
										{
											cache : false,
											onOpen : function() {
												//打开及回车时sql语句表格自动添加行
												$("#fixTab").edatagrid(
														"loadData", {
															"total" : 0,
															"rows" : []
														});
												$("#sqlView").show();
												$("#fixView").show();
												var data = $(
														"#module_table_propGrid")
														.edatagrid("getData");
												var rows = data.rows;
												var type = "";
												for ( var i = 0; i < rows.length; i++) {
													if (rows[i].code == "editType") {
														type = rows[i].value;
														break;
													}
												}
												initFixTab(type);
												initSqlTab(type);
												$("#fixTab").edatagrid(
														'addRow', {
															"key" : "",
															"caption" : ""
														});
												var formula = $.fn.module
														.getFormulaProp(
																$(mt_id)
																		.data(
																				"tableIndex"),
																$(mt_id)
																		.data(
																				"fieldIndex"));
												var formulaType = "0";
												if (formula && formula.type) {
													formulaType = formula.type;
													if (formulaType == "5") {
														$("#moduleFields")
																.data(
																		"formula",
																		formula);
													}
												}
												$('#openFormulaWin').data(
														"editType", type);
												$(
														"input[name=checkSql][value="
																+ formulaType
																+ "]").trigger(
														"click");
											},
											onClose : function() {
												$("#radio0").trigger("click");
												$("#fixTab").edatagrid(
														"cancelRow");
												$("#moduleFields").edatagrid(
														"loadData", {
															total : 0,
															rows : []
														});
												$("#moduleFields").removeData(
														"formula");
												$("#openFormulaWin")
														.removeData("editType");
												$("#isCheckAuth").removeAttr(
														"checked");
											}
										});
						$('#openCommonWin').window({
							cache : false,
							onOpen : function() {
								loadCommonTables();
							}
						});
						$('#formFieldWin').window({
							cache : false,
							onOpen : function() {
								loadFormTables();
							}
						});
						$('#i18nSetWinSet').window({
							cache : false,
							onOpen : function() {
								loadAllI18n();
							}
						});
						//初始化sql语句及固定值表格
						$("#fixTab").edatagrid({
							onRowContextMenu : function(e, rowIndex, rowData) {
								e.preventDefault();
								$("#fixTab").edatagrid("selectRow", rowIndex);
								$('#delFix').menu('show', {
									left : e.pageX,
									top : e.pageY
								});
								$("#fixTab").data("rowData", rowData);
							}
						});
						$("#fixView input").live('keydown', 'return',
								function(evt) {
									$("#fixTab").edatagrid('addRow');
								});
						$("#moduleFieldsView input").live('keydown', 'return',
								function(evt) {
									$("#moduleFields").edatagrid('addRow');
								});
						//$("#sqlView input").live('keydown', 'return',function (evt){$("#sqlTab").edatagrid('addRow',{"flag":"sqlWhere","order":""});});
						//初始化查询方式表格
						/*$("#treeSqlTab").edatagrid({onRowContextMenu:function(e, rowIndex, rowData){
							e.preventDefault(); 
							$("#tree").edatagrid("selectRow",rowIndex);
							$('#delTree').menu('show',{
									left:e.pageX,
									top:e.pageY
							});  
							$("#treeSqlTab").data("rowData",rowData);
							}});
						$("#sqls input").live('keydown', 'return',function (evt){$("#treeSqlTab").edatagrid('addRow',{"flag":"sqlWhere","order":""});});
						 */
						//加载时初始化国际化设置datagrid并绑定回车自动添加行事件
						//	$("#langTab").edatagrid();
						$("#langView input").live('keydown', 'return',
								function(evt) {
									$("#langTab").edatagrid('addRow', {
										"langType" : "",
										"langText" : ""
									});
								});
						//点击单选按钮显示相应的控件并执行相应的方法
						$("#radio0").click(function() {
							hideAllDataSourceView();
						});
						$("#radio1").click(function() {
							hideAllDataSourceView();
							$("#fixView").show();
						});
						$("#radio2").click(function() {
							hideAllDataSourceView();
							$("#sqlView").show();
						});
						$("#radio5").click(function() {
							hideAllDataSourceView();
							$("#moduleView").show();
							loadModuleTables();
						});
<%-- $("#radio4").click(function() {
			$("#sqlView").hide();
			$("#fixView").hide();
			//$("#expressView").show();
		}); --%>
		//点击多选按钮显示查询方式相应的控件及执行相应的方法
		$("#checkBox1").click(function(){
				$("#queryField").show();
				initTreeSqlTab();
						
		}); 
		//$("div.datagrid-view2 tr[datagrid-row-index='4']").c
		$(mt_id).propertygrid({
			title:'模块属性',
			fit:true,
			url:null,
			showGroup:true,
			columns:[[  
			          {field:'name',title:'属性',width:60,sortable:true},
			          {field:'value',title:'值',width:100}
			      ]],
			onBeforeEdit:function(rowIndex, rowData){
				var title = $(mt_id).data("type");
				/*if(title=="字段属性"){
					if(rowData["code"]=="formulaInfo") {
						$('#openFormulaWin').window('open');
					} else if(rowData["code"]=="expCode") {
						$('#expCodeWin').window('open');
					} else if(rowData["code"]=="sqlList") {
						$('#sqlEditWin').window('open');
					}
				}
				
				if(title=="模块属性"){
					if(rowData["code"]=="key"){
					}					
				}*/
				if(title=="布局属性"){
					if(rowData["code"]=="caption"){
						$("#module_table_propGrid").data("caption",rowData["value"]);
					}
				}
			},
			/*属性一栏中的行点击事件——2019/8/2 10:58*/
		 	onClickRow:function(rowIndex, rowData){
		 		var title = $(mt_id).data("type");
		 		if(title=="模块属性") {
		 			if(rowData["code"]=="setImportExport") {
			 			//导入导出设置
			 			$("#importExportSetWin").window("open");
			 		}else if(rowData["code"]=="allLanguageText"){
			 			$("#i18nSetWinSet").window("open");
			 		}
		 		} else if(title=="表单属性"){	 			 		
		 			if(rowData["code"]=="eventList") {
		 				var type = "EditPage";
			 			var extension =$.fn.module.getExtension($(mt_id).data("tableIndex"));
			 			var pageType =null;
						if(rowData["group"]=="编辑属性"){	
							type = "EditPage";
							pageType="editPage";
							$(mt_id).data("pageType",pageType);
			 			}else if(rowData["group"]=="查询属性"){
			 				type = "QueryPage";
			 				pageType="queryPage";
							$(mt_id).data("pageType",pageType);
			 			}
						var ztreeJson = $.fn.getzTreeJson(type);
						var eventList = extension[pageType].eventList;		
			 			for(var i=0;i<eventList.length;i++){
			 				var eve = ztreeJson.children;
		 					for(var j=0;j<eve.length;j++){
		 						if(eve[j].name == eventList[i].key){
		 							$.extend(eve[j],{"iconSkin":"icon08"});
		 							break;
		 						}
		 					}
			 			}
			 			$.fn.zTree.init($("#eventTree"), setting, ztreeJson);
			 			var treeObj = $.fn.zTree.getZTreeObj("eventTree");
						treeObj.expandAll(true);
                        $("#fun_ztree_params").html("");
                        $("#fun_ztree_description").html("");
						var tabs = $('#eventIframe').find("iframe");
						var win = tabs[0].contentWindow.window;
						win.getFunction("eventType");
						$("#addEventWin").window("open");
		 			} else if(rowData["code"]=="queryType") {
		 				var form =$.fn.module.getFormProp($(mt_id).data("tableIndex"));
			 			//var allData = $.fn.module.getQueryType($(mt_id).data("tableIndex"));
						if(form.isMaster){
							 $("#openFormWin").window("open");
							//加载时候添加属性
							if(form.queryType){
								var key="";
								$.each(form.queryList,function(k,v){
								//	$("input[name=queryType][value="+ v.type + "]");
									$('input[name="queryType"]').val([v.type]);    
									key = v.key;
									/*var formula =v.formula;
									if(formula.sql){
										if(formula.sql.content){
											$("#tabSqlContent").val(formula.sql.content);
										}
									}*/
								});
								//$("#treeSqlTab").edatagrid("loadData",allData.data);
								//$("#treeSqlTab").edatagrid("addRow",{"flag":"sqlWhere","order":""});
								$.each(form.fieldList,function(k,v){
									 if(key==v.key){
										 var newData=[{"id":key,"text":v.caption,"selected":true}];
										 $('#treeComboBox').combobox({
												data:newData,
												valueField:'id',
												textField:'text'
											});
									} 
									
								});
							}else{
								 $("#openFormWin").window("open");
							}
						}else{
							alert("子表没有查询方式");
						}
		 			} else if(rowData["code"]=="stsList") {
		 				var tableIndex = $(mt_id).data("tableIndex");
			 			var fieldData = $.fn.module.getAllField(tableIndex);
			 			var orderData = $.fn.module.getFieldOrder();
			 			var pageType =null;
						if(rowData["group"]=="编辑属性"){					
							pageType="editPage";
							$(mt_id).data("pageType",pageType);
			 			}else if(rowData["group"]=="查询属性"){
			 				pageType="queryPage";
							$(mt_id).data("pageType",pageType);
			 			}
			 			var extension =$.fn.module.getExtension(tableIndex);
			 			var stsList = extension[pageType].stsList;
			 			
			 			$('#sumList').edatagrid({
			 				width : 'auto',
			 				height : 'auto',
							nowrap: true,
							striped: true,	
							rownumbers:true,
							frozenColumns:[[
							                {field:'ck',checkbox:true}
							]],
							columns :[[
								{field:'key',title:'统计列',width:120, sortable:true,		
								editor : {							
	                              	type:'combobox',
	                                options:{
	                                          valueField:'key',
	                					      textField:'caption',
	                					      data:fieldData
	    								}
									},
									
								    formatter:function(value, rowData, rowIndex){
								    	for(var i=0;i<fieldData.length;i++){
								    		if(fieldData[i].key==value){
								    			return	fieldData[i].caption;
								    			}
								    		}						    	                                                            
	                                   }
								},
								{field:'caption',title:'显示名称',width:100, sortable:true,editor:"text"},
								{field:'column',title:'显示列',width:100, sortable:true,
									editor : {							
		                              	type:'combobox',
		                                options:{
		                                          valueField:'key',
		                					      textField:'caption',
		                					      data:fieldData
		    								}
										},
										
									    formatter:function(value, rowData, rowIndex){
									    	for(var i=0;i<fieldData.length;i++){
									    		if(fieldData[i].key==value){
									    			return	fieldData[i].caption;
									    			}
									    		}						    	                                                            
		                                   }
								},
								{field:'order',title:'统计类别',width:100, sortable:true,  
									editor : {							
	                              	type:'combobox',
	                                options:{
	                                          valueField:'key',
	                					      textField:'caption',
	                					      data:orderData
	    								}
									},
								    formatter:function(value, rowData, rowIndex){
								    	for(var i=0;i<orderData.length;i++){
								    		if(orderData[i].key==value){
								    			return	orderData[i].caption;
								    			}
								    		}						    	                                                            
	                                   }
								},
								{field:'value',title:'统计规则',width:120, sortable:true,editor:"textarea"}
						]],
							toolbar:[{
								id:'mb_add',
								text:'添加',
								iconCls:'icon-add',
								handler:function(){			
								$("#sumList").edatagrid('addRow',{"key":"","caption":"","order":""});
								}
							},'-',{
								id:'mb_del',
								text:'删除',
								iconCls:'icon-remove',
								handler:function(){	
									
									$("#sumList").edatagrid('destroyRow');
								}
							}]
						});
			 			 var data = {"total":0,"rows":[]};
						 $.each(stsList,function(k,v){
							 data.rows.push($.extend({},v));	 						
		 					 });
						 data.total=stsList.length;
			 			$('#sumList').edatagrid("loadData",data);
			 			$("#sumWin").window("open");
		 			} else if(rowData["code"]=="querySql") {
		 				var tableIndex = $(mt_id).data("tableIndex");
			 			var pageType =null;
						if(rowData["group"]=="查询属性"){
			 				pageType="queryPage";
							$(mt_id).data("pageType",pageType);
			 			}
						var sql = {};
						var queryPage = $.fn.module.getQueryPage(tableIndex);
						sql=queryPage["querySql"];
						if(jQuery.isEmptyObject(sql)){
							$("#querySqlContent").val("");
						}else{
							$("#querySqlContent").val(sql["content"]);
						}
						$('#querySqlWin').window('open');
		 			} else if(rowData["code"]=="buttonList") { //自定义页面按钮
		 				var tableIndex = $(mt_id).data("tableIndex");
		 				var buttonList = $.fn.module.getQueryPage(tableIndex).buttonList;
			 			buttonList = buttonList || [];
			 			doInitMenuButtonList(buttonList);
						$("#addMenuButton").data("oper","queryPageButtons");
						$("#addMenuButton").window("open");
		 			}
		 		} else if(title=="字段属性") {
		 			$(mt_id).data("editCode", rowData["code"]);
					if(rowData["code"]=="formulaInfo") {/*数据源设置*/
						$('#openFormulaWin').window('open');
					} else if(rowData["code"]=="expCode" || rowData["code"]=="style") {/*公式代码或者样式内容*/
						$('#expCodeWin').window('open');
					} else if(rowData["code"]=="sqlList") {/*sql编辑*/
						$('#sqlEditWin').window('open');
					} else if(rowData["code"]=="syncKey") {/*同步属性名*/
						$('#openSyncWin').window('open');
						loadFields();
						var tableIndex = $(mt_id).data("tableIndex");
						var fieldIndex = $(mt_id).data("fieldIndex");
						var fp = $.fn.module.getFieldProp(tableIndex,fieldIndex);
						var val =  $("#cb_syncField").combobox("setValue",fp["syncKey"]);
					} else if(rowData["code"]=="eventList") {/*事件编辑*/
						var controlType = "";
			 			var rows = $(mt_id).propertygrid("getData").rows;
			 			for(var i=0;i<rows.length;i++){
			 				if(rows[i].code=="editType"){
			 					controlType=rows[i].value;
			 					break;
			 				}
			 			}
			 			var event_data =rowData.value;
			 			var ztreeJson = $.fn.getzTreeJson(controlType);
			 			if(event_data.length>0){/*给事件列加选中的图标*/
			 				$.each(event_data,function(k,v){
			 					var eventList = ztreeJson.children;
			 					for(var i=0;i<eventList.length;i++){
			 						if(eventList[i].name == v.key){
			 							$.extend(eventList[i],{"iconSkin":"icon08"});
			 							break;
			 						}
			 					}
			 				});
			 			}
			 			/*初始化事件列表节点*/
			 			$.fn.zTree.init($("#eventTree"), setting, ztreeJson);
			 			var treeObj = $.fn.zTree.getZTreeObj("eventTree");
						treeObj.expandAll(true);
						/*打开事件编辑页面，并初始化函数列表*/
                        $("#fun_ztree_params").html("");
                        $("#fun_ztree_description").html("");
						var tabs = $('#eventIframe').find("iframe");
						var win = tabs[0].contentWindow.window;
						win.getFunction("eventType");
						$("#addEventWin").window("open");
					}else if(rowData["code"]=="dynamicProp"){				/*动态显示设置*/
						var ti = $(mt_id).data("tableIndex");
						var fi = $(mt_id).data("fieldIndex");
						var form = $.fn.module.getFormProp(ti);
						if(form.isMaster){
							var url = "${ctx}/jsp/dynamicProp.jsp?tableIndex="+ti+"&fieldIndex="+fi+"";
							$("#ifr_dp")[0].contentWindow.window.location.replace(url);
							$('#dynamicPropWin').window('open');
						}else{
							alert("只有主表字段才能设置动态子表的字段");
						}
					
					}
				} else if(title=="字段公共属性") {
					$(mt_id).data("fieldIndex", $(mt_id).data("fieldIndexes")[0]);
					$(mt_id).data("rowIndex", $(mt_id).data("rowIndexes")[0]);
					$(mt_id).data("editCode", rowData["code"]);
	 			 	if(rowData["code"]=="formulaInfo") {
						$('#openFormulaWin').window('open');
					} else if(rowData["code"]=="expCode" || rowData["code"]=="style") {
						$('#expCodeWin').window('open');
					} else if(rowData["code"]=="sqlList") {
						$('#sqlEditWin').window('open');
					} else if(rowData["code"]=="syncKey") {
						$('#openSyncWin').window('open');
						loadFields();
						var tableIndex = $(mt_id).data("tableIndex");
						var fieldIndex = $(mt_id).data("fieldIndex");
						var fp = $.fn.module.getFieldProp(tableIndex,fieldIndex);
						var val =  $("#cb_syncField").combobox("setValue",fp["syncKey"]);
					} else if(rowData["code"]=="eventList") {
						var controlType = "";
			 			var rows = $(mt_id).propertygrid("getData").rows;
			 			for(var i=0;i<rows.length;i++){
			 				if(rows[i].code=="editType"){
			 					controlType=rows[i].value;
			 					break;
			 				}
			 			}
			 			var event_data =rowData.value;
			 			var ztreeJson = $.fn.getzTreeJson(controlType);
			 			if(event_data.length>0){
			 				$.each(event_data,function(k,v){
			 					var eventList = ztreeJson.children;
			 					for(var i=0;i<eventList.length;i++){
			 						if(eventList[i].name == v.key){
			 							$.extend(eventList[i],{"iconSkin":"icon08"});
			 							break;
			 						}
			 					}
			 				});
			 			}
			 			
			 			$.fn.zTree.init($("#eventTree"), setting, ztreeJson);
			 			var treeObj = $.fn.zTree.getZTreeObj("eventTree");
						treeObj.expandAll(true);
                        $("#fun_ztree_params").html("");
                        $("#fun_ztree_description").html("");
						var tabs = $('#eventIframe').find("iframe");
						var win = tabs[0].contentWindow.window;
						win.getFunction("eventType");
						$("#addEventWin").window("open");
					}else if(rowData["code"]=="dynamicProp"){						
						var ti = $(mt_id).data("tableIndex");
						var fi = $(mt_id).data("fieldIndex");
						var form = $.fn.module.getFormProp(ti);
						if(form.isMaster){
							var url = "${ctx}/jsp/dynamicProp.jsp?tableIndex="+ti+"&fieldIndex="+fi+"";
							$("#ifr_dp")[0].contentWindow.window.location.replace(url);
							$('#dynamicPropWin').window('open');
						}else{
							alert("只有主表字段才能设置动态子表的字段");
						}
					}
	 			} else if(title=="按钮属性"){ //添加按钮属性事件
		 			if(rowData["code"]=="eventList") {
		 				var rows = $(mt_id).edatagrid("getRows");
			 			$(mt_id).data("buttonKey",rows[0].value);
			 			$(mt_id).data("buttonType","normal");
			 			var type = "Button";
			 			var ztreeJson = $.fn.getzTreeJson(type);
			 			
			 			 var key = $(mt_id).data("buttonKey");
						 var edittabs = $('#edit_ifr').find("iframe");
				 		 var editwin = edittabs[0].contentWindow.window;
				 		 var mainTableIndex = editwin.mainTableIndex;
						 var eventList = $.fn.module.getButtonProp(mainTableIndex,key,"editPage").eventList;
					/* 	$.each(eventList,function(k,v){
			 					var eve = ztreeJson.children;
			 					for(var i=0;i<eve.length;i++){
			 						//alert("params:"+eve[i].params);
			 						if(eve.name == v.key){
			 							$.extend(eve[i],{"iconSkin":"icon08"});
			 						}
			 					}
			 				}); */
			 			for(var i=0;i<eventList.length;i++){
			 				var eve = ztreeJson.children;
		 					for(var j=0;j<eve.length;j++){
		 						//alert("params:"+eve[i].params);
		 						if(eve[j].name == eventList[i].key){
		 							$.extend(eve[j],{"iconSkin":"icon08"});
		 							break;
		 						}
		 					}
			 			}
			 			$.fn.zTree.init($("#eventTree"), setting, ztreeJson);
			 			var treeObj = $.fn.zTree.getZTreeObj("eventTree");
						treeObj.expandAll(true);
                        $("#fun_ztree_params").html("");
                        $("#fun_ztree_description").html("");
						var tabs = $('#eventIframe').find("iframe");
						var win = tabs[0].contentWindow.window;
						win.getFunction("eventType");
						$("#addEventWin").window("open");
						/*  var key = $(mt_id).data("buttonKey");
						 var edittabs = $('#edit_ifr').find("iframe");
				 		 var editwin = edittabs[0].contentWindow.window;
				 		 var mainTableIndex = editwin.mainTableIndex;
						 var eventList = $.fn.module.getButtonProp(mainTableIndex,key,"editPage").eventList;
						$.each(eventList,function(k,v){
			 					var eve = ztreeJson.children;
			 					for(var i=0;i<eve.length;i++){
			 						//alert("params:"+eve[i].params);
			 						if(eve.name == v.key){
			 							$.extend(eve[i],{"iconSkin":"icon08"});
			 						}
			 					}
			 				});
						
						 */
		 			} else if(rowData["code"]=="buttonList") {//添加菜单按钮
		 				var rows = $(mt_id).edatagrid("getRows");
			 			var menuKey = rows[0].value;
			 			 var edittabs = $('#edit_ifr').find("iframe");
				 		 var editwin = edittabs[0].contentWindow.window;
				 		 var mainTableIndex = editwin.mainTableIndex;
						 var buttonList = $.fn.module.getButtonProp(mainTableIndex,menuKey,"editPage").buttonList;
						 doInitMenuButtonList(buttonList); 
						$("#addMenuButton").data("oper","editPageMenuButtons");
						$("#addMenuButton").window("open");
						var rows = $(mt_id).edatagrid("getRows");
			 			$(mt_id).data("menuButtonKey",rows[0].value);
		 			}
		 		}
		 		if((title=="字段属性"||title=="表单属性"||title=="模块属性"||title=="布局属性"||title=="按钮属性")&&rowData["code"]=="languageText"){
		 			langFlag = title;
		 			$("#i18nWin").window("open");
		 			$("#langTab").edatagrid({onRowContextMenu:function(e, rowIndex, rowData){
		 					e.preventDefault(); 
		 					$("#langTab").data("rowdata",rowData);
		 					$('#deli18n').menu('show',{
		 		    			left:e.pageX,
		 		    			top:e.pageY
		 		    	});}});
		 			$("#langTab").edatagrid("loadData",{"total":0,"rows":[]});
		 			var data = $("#module_table_propGrid").edatagrid("getData");
	 				var field_name = data.rows[0].value;
	 				var gridProp = {"total":0,"rows":[]};
		 			 if(title=="字段属性"){
		 				 if(!rowData.value||rowData.value==""){
		 					$("#langTab").edatagrid("addRow",{"langType":"zh_CN","langText":field_name});
		 				 }else{
		 					 var json_obj =  jQuery.parseJSON(rowData.value);
		 					 var rIndex = 0;
		 					 $.each(json_obj,function(k,v){
		 						gridProp.rows[rIndex] = {"langType":k,"langText":v};
		 						rIndex++;
		 						gridProp.total=rIndex;
		 					 });
		 					$("#langTab").edatagrid("loadData",gridProp);
		 				 }
		 			 }else if(title=="表单属性"){
		 				if(!rowData.value||rowData.value==""){
		 					$("#langTab").edatagrid("addRow",{"langType":"zh_CN","langText":field_name});
		 				 }else{
		 					 var json_obj =  jQuery.parseJSON(rowData.value);
		 					 var rIndex = 0;
		 					 $.each(json_obj,function(k,v){
		 						gridProp.rows[rIndex] = {"langType":k,"langText":v};
		 						rIndex++;
		 						gridProp.total=rIndex;
		 					 });
		 					$("#langTab").edatagrid("loadData",gridProp);
		 				 }
		 			 }else if(title=="模块属性"){
		 				if(!rowData.value||rowData.value==""){
		 					$("#langTab").edatagrid("addRow",{"langType":"zh_CN","langText":field_name});
		 				 }else{
		 					 var json_obj =  jQuery.parseJSON(rowData.value);
		 					 var rIndex = 0;
		 					 $.each(json_obj,function(k,v){
		 						gridProp.rows[rIndex] = {"langType":k,"langText":v};
		 						rIndex++;
		 						gridProp.total=rIndex;
		 					 });
		 					$("#langTab").edatagrid("loadData",gridProp);
		 				 }
		 			 }else if (title=="布局属性"){
		 				if(!rowData.value||rowData.value==""){
		 					$("#langTab").edatagrid("addRow",{"langType":"zh_CN","langText":data.rows[4].value});
		 				 }else{
		 					 var json_obj =  jQuery.parseJSON(rowData.value);
		 					 var rIndex = 0;
		 					 $.each(json_obj,function(k,v){
		 						gridProp.rows[rIndex] = {"langType":k,"langText":v};
		 						rIndex++;
		 						gridProp.total=rIndex;
		 					 });
		 					$("#langTab").edatagrid("loadData",gridProp);
		 				 }
		 			 }else if(title=="按钮属性"){
		 				if(!rowData.value||rowData.value==""){
		 					$("#langTab").edatagrid("addRow",{"langType":"zh_CN","langText":data.rows[1].value});
		 				 }else{
		 					 var json_obj =  jQuery.parseJSON(rowData.value);
		 					 var rIndex = 0;
		 					 $.each(json_obj,function(k,v){
		 						gridProp.rows[rIndex] = {"langType":k,"langText":v};
		 						rIndex++;
		 						gridProp.total=rIndex;
		 					 });
		 					$("#langTab").edatagrid("loadData",gridProp);
		 				 }
		 			 }
		 		}
			},
			// 属性一栏编辑完后触发
			onAfterEdit:function(rowIndex, rowData, changes) {
						var title = $(mt_id).data("type");
						var tableIndex = parseInt($(mt_id).data("tableIndex"));
						//	var oldKey = $.fn.module.getFormPropJson(tableIndex).rows[2].value;
						if(title=="表单属性"){
							 //$.fn.module.setFormProp(tableIndex,rowData);
							 if(rowData["code"]== "tableName") {
								var key = $.fn.module.covToDBRule(rowData["value"]);
								//var newKey=$.fn.module.getFormPropJson(tableIndex).rows[2].value;
								var flag = $.fn.module.checkKey("table",[],key);
								if(flag){
									rowData["value"] = key+"1";
								}
								$.fn.module.setFormProp(tableIndex,rowData);
								changeTableProp(tableIndex);//需要刷新两次才行
							} else if(rowData["code"]== "caption"){
								$.fn.module.setFormProp(tableIndex,rowData);
								var rowVal = rowData["value"];
							 
								var value = $.fn.toPinyin(rowVal).replace("_",""); 
								
								var tab = $("#module_table_tab").tabs('getSelected');
								tab.panel("setTitle", rowVal);
								$("#module_table_tab").find("li.tabs-selected span.tabs-title").text(rowVal);
								$('#module_table_tab').data(rowVal,tableIndex);
								
								var data=$.fn.module.getFormPropJson(tableIndex);
								var nextRowData ={};
								if(data){
									if(data.rows[1].value){
										nextRowData=$.extend({},data.rows[1],{"value":value});
										$.fn.module.setFormProp(tableIndex,nextRowData);
									}
								}
								var flag = $.fn.module.checkKey("table",[],value);
								if(flag){
									nextRowData["value"] = value+"*";
									$.fn.module.setFormProp(tableIndex,nextRowData);
								}
								
								var from = $.fn.module.getFormProp(tableIndex);
								if(!from.isMaster){
									var beforeTitle = "";
									var key = from.key;
									var tabs = $('#edit_ifr').find("iframe");
									var win = tabs[0].contentWindow.window;
									var tabsList =$.fn.module.getModule().dataSource.tabsList;
									if(tabsList){
										for(var i=0;i<tabsList.length;i++){
											var tlist = tabsList[i].tabList;
											if(tlist.length){
												for(var j=0;j<tlist.length;j++){
													if(tlist[j].form==key){
														beforeTitle = tlist[j].caption;
												 		tlist[j].caption = rowVal;
													}
												}
											}
										}
									}
									if(win.changeTitle){
										win.changeTitle(beforeTitle,rowVal);
									}
								}
							//	$.fn.module.setFormProp(tableIndex,nextRowData);
							//	$.each(data.rows[1],function(k,v){alert(k+" "+v)});
							 } else if(rowData["code"]=="isMaster"){ //判断主表唯一
								$.fn.module.setFormProp(tableIndex,rowData);
								var table = $.fn.module.getModule().dataSource.formList;
								for(var i=0;i<table.length;i++){
									//alert(table[i].isMaster+":"+rowData["value"]);
									if(table[i].isMaster && rowData["value"]){
										if(table[i].index != tableIndex){
											//alert(table[i].index+" : "+tableIndex);
											 rowData["value"] = false;
											 $.fn.module.setFormProp(tableIndex,rowData);
											 $.messager.alert('警告','一个模块只允许一张主表!','warning');
										}
									} 
								}
							 } else {
								 $.fn.module.setFormProp(tableIndex,rowData); 	
							 }
							changeTableProp(tableIndex);
						}else if(title=="字段属性"){
						//	alert(" diaoyong");
						//	if(rowData["code"]!="formulaInfo"){
							//只能用虚拟字段作为表头
								var tableIndex = parseInt($(mt_id).data("tableIndex"));
								var fieldIndex = parseInt($(mt_id).data("fieldIndex"));
								if("isCaption" == rowData.code && "true" == rowData.value) {
									$.fn.module.setFieldProp(tableIndex,fieldIndex,{code:'isVirtual',value:'true'});
								}
								$.fn.module.setFieldProp(tableIndex,fieldIndex,rowData);
								var temp={};
								var flag=0;
								var form = $.fn.module.getFormProp(tableIndex);
								var field = form.fieldList;
								var field_length = field.length;
								if(field_length){
									for(var i=0;i<field_length;i++){
										if(field[i].queryProperties.showInTreeSearch){
											flag++;
										}
									}
								}
								if(form.isMaster){
									if(flag>1&&rowData.value=="true"){
										alert("一张表只允许一个树形查询字段！");
										temp = $.extend(rowData,{"value":"false"});
									}else{
										temp = rowData;
									}
									if(rowData.code=="layoutTab"){
										if(rowData.value){
											/* var tabs = $('#edit_ifr').find("iframe");
											var win = tabs[0].contentWindow.window;
											var rows = win.titleObj[rowData.value].replace(/[^0-9]/ig, "");;
											var colsArr = win.titToCols[rowData.value].split("_");
											var cols = colsArr[colsArr.length-1]; */
											var rc = rowData.value.split(".");
                                            /*2019/8/26*/
											if(rc.length<=1){
                                                var data = rowData.editor.options.data;
                                                for(var i=0;i<data.length;i++){
                                                    if(data[i].caption==rowData.value){
                                                        rc = data[i].key.split(".");
                                                        break;
                                                    }
                                                }
                                            }
                                            /*2019/8/26*/
											$.fn.module.setFieldLayoutTab(tableIndex,fieldIndex,rc[0],rc[1]);
										}
									}
								} else{
									if(rowData.code=="showInTreeSearch"){
										alert("子表不允许出现树形查询字段");
										temp = $.extend(rowData,{"value":"false"});
									}
									if(rowData.code=="layoutTab"){
										alert("子表字段不允许移动！");
										//rowData.value="";
									}
								}
								$.fn.module.setFieldProp(tableIndex,fieldIndex,temp);
								data = $.fn.module.getFieldPropJson(tableIndex, fieldIndex);
								/*2019/8/26*/
                                var layoutData = $.fn.module.getAllTabList();
                                var drows = data.rows;
                                for ( var i = 0, len = drows.length; i < len; i++) {
                                    if (drows[i].code == "layoutTab") {
                                        drows[i].editor.options.data = layoutData;
                                        break;
                                    }
                                }
                                /*2019/8/26*/
								if(temp.code=="editType"){									
								var val = temp.value;
                    		 		if(val=="CheckBox"||val=="ComboCheckBox"||val=="ComboTree"){
                    		 			var fildes = data.rows;
                    		 			for(var i=0;i<fildes.length;i++){
                    		 				if(fildes[i].code=="multiple"){
                    		 					fildes[i].value="true";
                    		 					
                    		 					var fd = $.fn.module.getFieldProp(tableIndex,fieldIndex);
                        		 				fd.editProperties.multiple = "true";
                        		 				$.fn.module.setFieldProp(tableIndex,fieldIndex,fd);
                    		 					break;
                    		 				}
                    		 			}
                    		 		}
								}
								if("editType" == rowData.code && "DialogueWindow" == rowData.value) {
                		 			var table_grid_ = $("#table_grid_"+tableIndex);
                		 			var rowIndex_ = table_grid_.edatagrid("getRowIndex", data.rows[1].value);
                		 			//var rowData_ = table_grid_.edatagrid("getRows")[rowIndex_];
                		 			var index = $.fn.module.getMaxIndex();
           		 					//rowData_["colName_"+tableIndex] = rowData_["colName_"+tableIndex] + "key";
           		 					//rowData_.index = _index;
                   		 			//内存中添加新插入的字段
                   		 			var fielddata = $.fn.module.getFieldProp(tableIndex, fieldIndex);
                   		 		    var newField = $.fn.module.setNewField(tableIndex, index);
                   		 			//var fielddata = jQuery.extend(true, {}, newField);
                   		 			newField.key = fielddata.key + "text";
                   		 			newField.caption = fielddata.caption + "显示值";
                   		 			newField.dataProperties = $.extend(true, {}, fielddata.dataProperties);
                   		 			newField.dataProperties.dataType = "dtString";
                   		 			newField.dataProperties.colName = fielddata.dataProperties.colName + "Text";
                   		 			newField.dataProperties.key = newField.key;
                   		 			newField.dataProperties.caption = newField.caption;
                   		 			newField.queryProperties.showInGrid = false;
                   					newField.editProperties.visible = false;
                   					//newField.index = _index;
                   					var form = $.fn.module.getFormProp(tableIndex);
                   	 				var fl = form.fieldList;
                   	 				var mark = true;
                   	 				for(var i in fl) {
	                   	 				if(fl[i].key == newField.key) {
		                   	 				mark = false;
	                   	 				}
                   	 				}
                   	 				if(mark) {
	                   	 				fl.splice(rowIndex_+1, 0, newField);
	                   	 				$.fn.module.setMaxIndex(index+1);
	                   	 				doRefreshTables();
                   	 				}
                		 		}
								/* $.fn.module.refreshTableGrid(tableIndex, fieldIndex, temp, function(newRow) {
									
									$("#table_grid_"+tableIndex).edatagrid("updateRow", {index:rIndex, row: newRow}); 
									$("#table_grid_"+tableIndex).edatagrid("refreshRow", rIndex); 
									var rIndex = parseInt($(mt_id).data("rowIndex"));
									changeFieldProp(tableIndex, fieldIndex, rIndex);
								});  */
								//console.log(data);
								
								$(mt_id).propertygrid("loadData",data);
							/*	var sIndex = 0;
								var sdata = $.fn.module.getTableFieldsJson(tableIndex);
    							$("#table_grid_"+tableIndex).edatagrid("loadData", sdata);
    							var nfildes = sdata.rows;
            		 			for(var i=0;i<nfildes.length;i++){
            		 				if(nfildes[i].index==fieldIndex){
            		 					sIndex = parseInt($("#table_grid_"+tableIndex).edatagrid("getRowIndex",nfildes[i]));
            		 					break;
            		 				}
            		 			}
								var srow = {};
								srow["size_"+tableIndex] = "100";
								srow["dataType_"+tableIndex]="dtString";
								srow["scale_"+tableIndex]="0";				
								$("#table_grid_"+tableIndex).edatagrid("addRow", srow);								
								$("#table_grid_"+tableIndex).edatagrid('cancelRow');
								$("#table_grid_"+tableIndex).edatagrid("beginEdit",sIndex);*/
						//	}
						}else if(title=="字段公共属性"){
							//	alert(" diaoyong");
							//	if(rowData["code"]!="formulaInfo"){
								var fieldIndexes = $(mt_id).data("fieldIndexes");
								var rowIndexes = $(mt_id).data("rowIndexes");
								for(var l in fieldIndexes){
									$(mt_id).data("fieldIndex", fieldIndexes[l]);
									$(mt_id).data("rowIndex", rowIndexes[l]);
									var tableIndex = parseInt($(mt_id).data("tableIndex"));
									var fieldIndex = parseInt($(mt_id).data("fieldIndex"));
									$.fn.module.setFieldProp(tableIndex,fieldIndex,rowData);
									var temp={};
									var flag=0;
									var form = $.fn.module.getFormProp(tableIndex);
									var field = form.fieldList;
									var field_length = field.length;
									if(field_length){
										for(var i=0;i<field_length;i++){
											if(field[i].queryProperties.showInTreeSearch){
												flag++;
											}
										}
									}
									if(form.isMaster){
										if(flag>1&&rowData.value=="true"){
											alert("一张表只允许一个树形查询字段！");
											temp = $.extend(rowData,{"value":"false"});
										}else{
											temp = rowData;
										}
										if(rowData.code=="layoutTab"){
											if(rowData.value){
												/* var tabs = $('#edit_ifr').find("iframe");
												var win = tabs[0].contentWindow.window;
												var rows = win.titleObj[rowData.value].replace(/[^0-9]/ig, "");;
												var colsArr = win.titToCols[rowData.value].split("_");
												var cols = colsArr[colsArr.length-1]; */
												var rc = rowData.value.split(".");											
												$.fn.module.setFieldLayoutTab(tableIndex,fieldIndex,rc[0],rc[1]);
											}
										}
									} else{
										if(rowData.code=="showInTreeSearch"){
											alert("子表不允许出现树形查询字段");
											temp = $.extend(rowData,{"value":"false"});
										}
										if(rowData.code=="layoutTab"){
											alert("子表字段不允许移动！");
											//rowData.value="";
										}
									}
									$.fn.module.setFieldProp(tableIndex,fieldIndex,temp);
									data = $.fn.module.getFieldPropJson(tableIndex, fieldIndex);
									if(temp.code=="editType"){									
										var val = temp.value;
	                    		 		if(val=="CheckBox"||val=="ComboCheckBox"||val=="ComboTree"){
	                    		 			var fildes = data.rows;
	                    		 			for(var i=0;i<fildes.length;i++){
	                    		 				if(fildes[i].code=="multiple"){
	                    		 					fildes[i].value="true";
	                    		 					var fd = $.fn.module.getFieldProp(tableIndex,fieldIndex);
	                        		 				fd.editProperties.multiple = "true";
	                        		 				$.fn.module.setFieldProp(tableIndex,fieldIndex,fd);
	                    		 					break;
	                    		 				}
	                    		 			}
	                    		 		}
									}
									if("editType" == rowData.code && "DialogueWindow" == rowData.value) {
	                		 			var table_grid_ = $("#table_grid_"+tableIndex);
	                		 			var rowIndex = table_grid_.edatagrid("getRowIndex", data.rows[1].value);
	                		 			var row = table_grid_.edatagrid("getRows")[rowIndex];
	                		 			var new_row = $.extend(true, {}, row);
	                		 			var index = $.fn.module.getMaxIndex();
	                		 			new_row['colName_'+tableIndex] = row['colName_'+tableIndex] + "Text";
	                		 			new_row['caption_'+tableIndex] = row['caption_'+tableIndex] + "显示值";
	                		 			new_row['key_'+tableIndex] = row['key_'+tableIndex] + "text";
	                		 			new_row['index'] = index;
	                   		 			var fielddata = $.fn.module.getFieldProp(tableIndex, fieldIndex);
	                   		 		    var newField = $.fn.module.setNewField(tableIndex, index);
	                   		 			newField.key = fielddata.key + "text";
	                   		 			newField.caption = fielddata.caption + "显示值";
	                   		 			newField.dataProperties = $.extend(true, {}, fielddata.dataProperties);
	                   		 			newField.dataProperties.dataType = "dtString";
	                   		 			newField.dataProperties.colName = fielddata.dataProperties.colName + "Text";
	                   		 			newField.dataProperties.key = newField.key;
	                   		 			newField.dataProperties.caption = newField.caption;
	                   		 			newField.queryProperties.showInGrid = false;
	                   					newField.editProperties.visible = false;
	                   					var form = $.fn.module.getFormProp(tableIndex);
	                   	 				var fl = form.fieldList;
	                   	 				var mark = true;
	                   	 				for(var i in fl) {
		                   	 				if(fl[i].key == newField.key) {
			                   	 				mark = false;
		                   	 				}
	                   	 				}
	                   	 				if(mark) {
	                   	 					table_grid_.edatagrid("insertRow",{index:rowIndex+1,row:new_row});
		                   	 				fl.splice(rowIndex+1, 0, newField);
		                   	 				$.fn.module.setMaxIndex(index+1);
	                   	 				}
	                		 		}
									/* if(fieldIndexes.length-1 == l+"") {
                   	 					doRefreshTables();
                   	 				} */
									//$(mt_id).propertygrid("loadData",data);
								}
								//$(mt_id).propertygrid("loadData",data);
							}else if(title=="模块属性"){
							$.fn.module.setModuleProp(rowData);
						// 获得模块的属性值保存到树形节点中
							var name="";
							var key="";
							var id="${param.nodeId}";
							var code = rowData["code"];
							if(code=="caption"){
								name=rowData["value"];
								if(parent.updataNodeName) {
									parent.updataNodeName(id,name);
								}
							}else if(code=="key"){
								key=rowData["value"];
								if(parent.updataNodeKey) {
									parent.updataNodeKey(id,key);
								}
							}
						}else if (title=="布局属性"){
							var tabs = $('#edit_ifr').find("iframe");
							var win = null;
							if(tabs[0]){
								win = tabs[0].contentWindow.window;
							}else{			
								var tb =$('#formEdit_ifr').find("iframe");
								win = tb[0].contentWindow.window;
							}
				 			
				 			var data = $("#module_table_propGrid").edatagrid("getData");
				 			//console.log(data);
							if(rowData.code=="caption"){
								var beforeTitle = $("#module_table_propGrid").data("caption");
								$.fn.module.setTabListJsonProp(data);
								var rowDataVal = rowData.value;
								win.changeTitle(beforeTitle,rowDataVal);
							//	win.revampTitlteObj(beforeTitle,rowDataVal);
								/* var tab = $("#edit_tabs").tabs('getSelected');
								tab.panel("setTitle", rowDataVal);
								$("#edit_tabs").find("li.tabs-selected span.tabs-title").text(rowDataVal); */

							//	alert(tab.panel);
							/*  	$("#edit_tabs").tabs("update",{"tab":tab,options: {
									title: rowData.value,
								}});  */ 
								
							}else if(rowData.code == "tableCols" || rowData.code == "isShowInQueryList"){
								$.fn.module.setTabListJsonProp(data);						
							}else if(rowData.code == "layout"){
								var formArray = data.rows;
								var tabObject = {};
								for(var i=0;i<formArray.length;i++){
									tabObject[formArray[i].code] = formArray[i].value;									
								}		
								if(rowData.value=="table"){
									win.replaceTabs(tabObject,"table");
								}else if(rowData.value=="absolute"){
									win.replaceTabs(tabObject,"absolute");
								}else if(rowData.value=="excel") {
									win.replaceTabs(tabObject,"excel");
								}
					 			$.fn.module.setTabListJsonProp(data);
							}else if(rowData.code == "height"){
								var formArray = data.rows;
								var tId= "editor"+formArray[0].value+"_"+formArray[1].value;
								win.changeHeight(tId,rowData.value);
								$.fn.module.setTabListJsonProp(data);
							}
							
							
						//	$("#module_table_propGrid").edatagrid("refreshRow",rowIndex);
						}else if(title == "按钮属性"){
							var title = $("#main_tabs_div_1").data("title");
							var flag = "";
							if(title == "编辑页面设计"){
								flag = "editPage";		
							}else if(title == "查询页面设计"){
								flag = "queryPage";
							}
							var tabs = $('#edit_ifr').find("iframe");
					 		var win = tabs[0].contentWindow.window;
					 		var mainTableIndex = win.mainTableIndex;				
							var rows = $("#module_table_propGrid").edatagrid("getRows");
							var key = rows[0].value;
							$.fn.module.setButtonProp(mainTableIndex,key,flag,rowData);
							//更新按钮属性
							var value = rowData["value"];
							var span = $('#'+key,win.document.body).find("span.l-btn-text");
							if(rowData["code"]=="caption"){							
								$(span[0]).text(value);
							}else if(rowData["code"]=="width"){
								$(span[0]).css("width",value);
							}else if (rowData["code"]=="height"){
								$(span[0]).css("height",value);
							}else if(rowData["code"]=="iconCls"){
								$(span[0]).css("padding-left", "20px");
								$(span[0]).addClass(value);
							}else if(rowData["code"]=="buttonType" && value=="normal"){	
								//改变按钮类型
								win.changeButtonType(key);
								/* $('#'+key,win.document.body).html("");
								
								$('#'+key,win.document.body).menubutton({   //('#'+key,win.document.body)查找win的页面中的key元素
									menu: '#menu'+key,
									text :"报表"
								});  */
							}
							
					}

		    	}
			
		}); 
		//加载页面时候公式属性模块自动添加一行
		$("#main_tabs_div_1").tabs({
			onSelect:function(title,index){
				$("#main_tabs_div_1").data("title",title);
				changeModuleProp();
				if(title == "编辑页面设计"){
					//console.debug("1---" + $("#editIframe").attr("src") + "--");
					if($("#editIframe").attr("src")=='') {
						//$("#editIframe")[0].contentWindow.window.location.replace("${ctx}/jsp/edit.jsp");
						$("#editIframe").attr("src", "${ctx}/jsp/edit.jsp");
					}
				} else if(title == "查询页面设计") {
					if($("#dictIfm").length > 0 && $("#dictIfm").attr("src")=='') {
						$("#dictIfm").attr("src", "${ctx}/jsp/dictionary.jsp");
					}
				}
			},
			onContextMenu:function(e, title){
				e.preventDefault();
				$("#main_tabs_div_1").data("title",title);
				$("#main_tabs_div_1").tabs("select",title);
				if(title == "数据库设计"){
					$("#pasteTabTitle").menu('show', {						
						left: e.pageX,
						top: e.pageY
					});
				}
				if(title == "编辑页面设计"){
					$("#create_tabsMenu").menu('show', {						
						left: e.pageX,
						top: e.pageY
					});
				}
				if(title == "查询页面设计"){
					$("#add_buttonMenu").menu('show', {						
						left: e.pageX,
						top: e.pageY
					});
				}
			}
		});
		
		$("#expCodeWin").window({
			onOpen:function() {
				var tabs = $('#expressView').find("iframe");
				var win = tabs[0].contentWindow.window;
				win.getFunction();
				var tableIndex = $(mt_id).data("tableIndex");
				var fieldIndex = $(mt_id).data("fieldIndex");
				var content = $.fn.module.getExpCode(tableIndex, fieldIndex);
				if(content) {
					win.setFunText(content);
				}				
			}});

		$("#module_table_tab").tabs({
			onSelect:function(title){
				var index = $('#module_table_tab').data(title);
				changeTableProp(parseInt(index));				
				},
			onContextMenu:function(e, title,index){
					e.preventDefault();
					$("#module_table_tab").data("tabTitle",title);
			//		$("#module_table_tab").tabs("select",title);
					$("#copyTabTitle").menu('show', {						
						left: e.pageX,
						top: e.pageY
					});
				}
		}); 
	//	根据树形节点传过来的不同的key生成对应的页面数据
		var allKey = "";
	//	var newKey = "${param.newKey}";
		//判断如果是粘贴/打开模块时给予不同路径
		 if("${param.oldKey}"!="undefined"){
			if("${param.oldKey}".length==0){
				allKey = "${param.allKey}.${param.key}";
			}else{
				allKey = "${param.allKey}.${param.oldKey}";
			}
		}else{
			allKey = "${param.allKey}.${param.key}";
		}
		if("${param.copyTreeId}"){
			initModuleByKey(allKey,"${param.copyTreeId}");
		}else{
			initModuleByKey(allKey,"${param.treeId}");
		} 
		
	//	alert("${param.key}"+":"+"${param.oldKey}");
	});
	
	//给国际化字段设值
	function saveLanguage(){
		$("#langTab").edatagrid("saveRow");
		var data = $("#langTab").edatagrid("getData");
		var rows = data.rows;
		var langObj = {};
		$.each(rows,function(k,v){
				langObj[v.langType]=$.fn.module.removeBlank(v).langText;
			});
		var jsonStr = JSON.stringify(langObj);
		if(rows.length == 0){
			jsonStr = "";
		}
		var grid_data = $("#module_table_propGrid").edatagrid("getData");
		if(langFlag=="模块属性"){
			grid_data.rows[3].value = jsonStr;
			$.fn.module.setModuleProp(grid_data.rows[3]);
			$("#module_table_propGrid").edatagrid("refreshRow",3);
		}else if(langFlag=="表单属性"){
			grid_data.rows[3].value = jsonStr;
			var tableIndex = $(mt_id).data("tableIndex");
			$.fn.module.setFormProp(tableIndex,grid_data.rows[3]);
			$("#module_table_propGrid").edatagrid("refreshRow",3);
		}else if(langFlag=="字段属性"){
			grid_data.rows[2].value = jsonStr;
			var fieldIndex = $(mt_id).data("fieldIndex");
			var tableIndex = $(mt_id).data("tableIndex");
			$.fn.module.setFieldProp(tableIndex,fieldIndex,grid_data.rows[2]);
			$("#module_table_propGrid").edatagrid("refreshRow",2);
		}else if(langFlag=="布局属性"){
			grid_data.rows[5].value = jsonStr;
			$.fn.module.setTabListJsonProp(grid_data);
			$("#module_table_propGrid").edatagrid("refreshRow",5);
		}else if("按钮属性"){
			grid_data.rows[2].value = jsonStr;
			var key = grid_data.rows[0].value;
			var title = $("#main_tabs_div_1").data("title");
			var flag = "";
			var mainTableIndex = 0;
			if(title=="编辑页面设计"){
				flag = "editPage";
				 var tabs = $('#edit_ifr').find("iframe");
		 		 var win = tabs[0].contentWindow.window;
		 		mainTableIndex = win.mainTableIndex;
			}else if(title == "查询页面设计"){
				
			}
			$.fn.module.setButtonProp(mainTableIndex,key,flag,grid_data.rows[2]);
			$("#module_table_propGrid").edatagrid("refreshRow",2);
		}
		$('#i18nWin').window('close');
	}
	
	//复制表格
	function copyTabs(){
		var copyIndex = $(mt_id).data("tableIndex");
		var tabJson = $.fn.module.getFormProp(copyIndex);
		if(parent.copyTables){
			parent.copyTables(tabJson);
		}
	
	}
	//粘贴表格
	function pasteTabs(){
		if(parent.getPasteTables){
			var tab = parent.getPasteTables();
			var covTab = $.fn.module.pasteTabs(null,tab);
			if(covTab.isMaster){
				covTab.isMaster = false;
					}
				covTab.caption = covTab.caption +'_复制';
				var tmp = $.fn.module.covToDBRule(covTab.tableName+'_FU_ZHI');
				covTab.tableName= tmp;
				var prop = $.fn.module.convertProp(tmp,true);
				covTab.key = prop;
			}
			if(covTab){
				var tabsList = $.fn.module.getModule().dataSource.tabsList;
				var cols = 0 ;
				for(var i=0;i<tabsList.length;i++){
					if(tabsList[i].rows==2){
						cols = tabsList[i].tabList.length+1;
						break;
					}
				}			
				$.fn.module.setTabList({
					"rows" : 2,
					"cols" : cols,
					"form" : covTab.key,
					"caption" :covTab.caption,
					"languageText" : ""
				});
				var title = covTab.caption;
				var index = covTab.index;
				var url = "${ctx}/jsp/table.jsp";
				url = url + "?index=" + index;
				$('#module_table_tab').data(title, index);
				$("#module_table_tab").tabs("add", {
					title : title,
					href : url,
					closable : false,
					cache : true,
					fit : true,
					selected : covTab.isMaster
				});
		}	
	}
	//把页面值赋给js中sqlProp
	function setSqlProp(){
		var id =  $('input[name=checkSql]:checked').val();
		if(id=="2"){
			var data = $("#dsRefKey").combobox("getValue");
			var refQsKey = $("#dsRefQsKey").val();
			$.fn.module.setFormulaProp($(mt_id).data("tableIndex"),$(mt_id).data("fieldIndex"),data,id, null, null, refQsKey);
		}else if(id=="1"){
			$("#fixTab").edatagrid("saveRow");
			var data = $("#fixTab").edatagrid("getRows");
			$.fn.module.setFormulaProp($(mt_id).data("tableIndex"),$(mt_id).data("fieldIndex"),data,id);
		}else if(id=="5"){
			$("#moduleFields").edatagrid("saveRow");
			var rows = $("#moduleFields").edatagrid("getRows");
			var newRows = [];
			$.each(rows, function(k, v) {
				if(v.key && v.caption && v.column) {
					newRows.push(v);
				}
			});
			var data = {};
			var tableVal = $("#cb_moduleTable").combobox("getValue");
			data["rows"] = newRows;
			data["entityName"] = tableVal;
			var ds = $("#dataStatus").combobox("getValue");
			data["dataStatus"] = ds;
			if($("#isCheckAuth").attr("checked")) {
				data["isCheckAuth"] = true;
			} else {
				data["isCheckAuth"] = false;
			}
			$.fn.module.setFormulaProp($(mt_id).data("tableIndex"),$(mt_id).data("fieldIndex"),data,id);
		}else{
			$.fn.module.setFormulaProp($(mt_id).data("tableIndex"),$(mt_id).data("fieldIndex"),[],id);
		}
		changePropGrid('字段属性',$(mt_id).data("tableIndex"), $(mt_id).data("fieldIndex"));
		$("#openFormulaWin").window('close');
	}
	  //设置查询方式
	  function setQueryType(){
		  var type =  $('input[name=queryType]:checked').val();
		  var tableIndex = $(mt_id).data("tableIndex");
		  /*var sql=$("#tabSqlContent").val();
		  $("#treeSqlTab").edatagrid("saveRow");
		  var data=$("#treeSqlTab").edatagrid("getRows");*/
		  var key = $("#treeComboBox").combobox("getValue");
		  var data = $("#treeQueryRefKey").combobox("getValue");
		  $.fn.module.setQueryType(tableIndex,type,key,sql,data);
		  $("#openFormWin").window('close');
		  
		  //改变查询方式显示值
		 var table = $.fn.module.getFormProp(tableIndex);
		 // alert(type);
		  if(type){
			  table.queryType = "树形查询";
		  }else{
			  table.queryType = "";
		  }
		  changeTableProp(tableIndex);
	  }
	  <%-- 
	  //删除表查询方式行 
	  function delTreeRow(){
		  var rows = $("#treeSqlTab").datagrid('getRows');
			if(rows.length>1){
				$.fn.module.delQueryRow($(mt_id).data("tableIndex"),$("#treeSqlTab").data("rowData"));
				$("#treeSqlTab").edatagrid('destroyRow');
				
			}else{
				alert("默认保留一行不允许删除,如需改变请单击右键修改!");
			}
	  }
	  --%>
	  //删除公式固定值属性行
	  function delFixViewRow(){
		  var rows = $("#fixTab").edatagrid('getRows');
			if(rows.length>1){
				$.fn.module.delSqlRow($(mt_id).data("tableIndex"),$(mt_id).data("fieldIndex"),$("#fixTab").data("rowData"));
				var row = $("#fixTab").data("rowData");
				var index = $("#fixTab").edatagrid("getRowIndex",row);
				$("#fixTab").edatagrid('deleteRow',index);
				//var row =$("#fixTab").edatagrid("getSelected");
				//alert($("#fixTab").edatagrid("getRowIndex",row));
				//$("#fixTab").edatagrid('destroyRow');
				
			}else{
				alert("默认保留一行不允许删除,如需改变请单击右键修改!");
			}
	  }

	  /**
	   * 刷新tab
	   */
	  function doRefreshTab(title, url, isIframe) {
		  var tab = $('#main_tabs_div_1').tabs('getTab', title);
		  if(tab) {
			  if(url) {
				  if(isIframe) {
				  	  tab.panel("body").find("iframe")[0].contentWindow.window.location.replace(url);
				  } else {
					  tab.panel('refresh', url);
				  }
			  } else {
				  if(isIframe) {
					  tab.panel("body").find("iframe")[0].contentWindow.window.location.reload();
				  } else {
					  tab.panel('refresh');	
				  }
			  }
		  }
      };

	  /**
	   * 显示字段数据
	   */
	 function showLineInfo(tableIndex,fieldIndex){
		 var data = $.fn.module.getFieldPropJson(tableIndex, fieldIndex);
		// changeFieldProp(tableIndex, fieldIndex,$(mt_id).data("rowIndex"));    	
	     $("#module_table_propGrid").propertygrid("loadData",data);
	  }

	 function showTableInfo(tableIndex) {
		 $(mt_id).propertygrid("loadData",$.fn.module.getFormPropJson(tableIndex));
	 }

     //新建tabs分组
     function createTabsGroup(){
    	var tabs = $('#edit_ifr').find("iframe");
		var win = tabs[0].contentWindow.window;
		win.createNewTabsGroup();
     }
     
     //删除国际化资源
     function deli18nRow(){
    	// alert("delete");
    	var rowdata = $("#langTab").data("rowdata");
    	var rindex =  $("#langTab").edatagrid('getRowIndex',rowdata);
    	$("#langTab").edatagrid('deleteRow',rindex);
     }
     
     //编辑国家化资源
     function editi18nRow(key){
    	// alert("edit");getRowIndex
    	 var rowdata = $("#"+key).data("rowdata");
    	 var rindex =  $("#"+key).edatagrid('getRowIndex',rowdata);
    	 $("#"+key).edatagrid('editRow',rindex);
     }
     //给界面添加按钮
     function addButton(){
    	 var title = $("#main_tabs_div_1").data("title");
    	 if(title == "查询页面设计"){
    		
    	 }else if(title == "编辑页面设计"){
    	 	var tabs = $('#edit_ifr').find("iframe");
 			var win = tabs[0].contentWindow.window;
 			var i =parseInt(Math.random()*10000+1); 
 			var key = 'addBt_'+i;
 			var caption = "默认按钮_"+i;
 			win.addbutton(caption,key);
    	 }
     }
     
  	//显示按钮属性
  	function showButtonProp(tableIndex,key,pageFlag){
  		$(mt_id).propertygrid({title:"按钮属性"});
  		$(mt_id).data("type","按钮属性");
		data = $.fn.module.getButtonPropJson(tableIndex,key,pageFlag);
		var drows = data.rows;
		for ( var i = 0, len = drows.length; i < len; i++) {
			if (drows[i].code == "iconCls") {
				drows[i].editor.options.url= '${ctx}/js/easyui/menu-icon-data.json';
				break;
			}
		} 
		$(mt_id).propertygrid("loadData",data);
  	}
  	function setMenuBtEvent(rowIndex){			
  		$(mt_id).data("buttonType", "menu");
  		$('#menuButtonList').data("rowIndex",rowIndex);
  		$('#menuButtonList').edatagrid("saveRow");
  		 var type = "Button";
		 var ztreeJson = $.fn.getzTreeJson(type);
		 var buttonData =  $('#menuButtonList').edatagrid("getRows");
		 var eventList = buttonData[rowIndex].eventList;
		 if(eventList.length>0){
			for(var i=0;i<eventList.length;i++){//读取按钮已经添加的事件
				var eve = ztreeJson.children;
				for(var j=0;j<eve.length;j++){
					if(eve[j].name == eventList[i].key){
						$.extend(eve[j],{"iconSkin":"icon08"});
						break;
					}
				}
			}
  		}
		$.fn.zTree.init($("#eventTree"), setting, ztreeJson);
		var treeObj = $.fn.zTree.getZTreeObj("eventTree");
		treeObj.expandAll(true);
        $("#fun_ztree_params").html("");
        $("#fun_ztree_description").html("");
		var tabs = $('#eventIframe').find("iframe");
		var win = tabs[0].contentWindow.window;
		win.getFunction("eventType");
		$("#addEventWin").window("open");
  	}
  	function saveMenuButton(){
  		$('#menuButtonList').edatagrid("saveRow");
  		var menuBtJson = $('#menuButtonList').edatagrid("getRows");
  		var operFlag = $("#addMenuButton").data("oper");
  		var tableIndex = $(mt_id).data("tableIndex");
  		if(operFlag == "queryPageButtons") { //自定义按钮
  			$.fn.module.setPageButtons(tableIndex, "queryPage", menuBtJson);
  			$('#addMenuButton').window('close');
  			showTableInfo(tableIndex);
  		} else { //下拉菜单按钮
  			var key = $(mt_id).data("menuButtonKey");
  	  		var tabs = $('#edit_ifr').find("iframe");
  			var win = tabs[0].contentWindow.window;
  			var mainTableIndex = win.mainTableIndex;
  	  		//先放入按钮中的buttonList;
  	  		$.fn.module.setMenuButton(mainTableIndex,key,"editPage",menuBtJson);
  	  		//在放入页面中
  	  		win.addMenu(key,menuBtJson);
  	  		$('#addMenuButton').window('close');
  			showButtonProp(tableIndex,key,"editPage");
  		}	
  	}
  	//保存公式代码
  	function saveExpCode() {
  		var tabs = $('#expressView').find("iframe");
			var win = tabs[0].contentWindow.window;
			var content  = win.getFunText();
			//var pointNum = win.getPointNum();
			var tableIndex = $(mt_id).data("tableIndex");
			var fieldIndex = $(mt_id).data("fieldIndex");
			var editCode = $(mt_id).data("editCode");
			//$.fn.module.setExpCode(tableIndex, fieldIndex, content);
			var json = {};
			json[editCode] = content;
			$.fn.module.setEditProp(tableIndex, fieldIndex, json);
			changePropGrid('字段属性',tableIndex, fieldIndex);
			$("#expCodeWin").window('close');
  	}
  	function saveSumRule(){
  		var pageType=$(mt_id).data("pageType");
  		$('#sumList').edatagrid("saveRow");
  		var sumJson = $('#sumList').edatagrid("getRows");
  		var tableIndex = $(mt_id).data("tableIndex");
  		//先放入 stsList中;
  		var extension =$.fn.module.getExtension(tableIndex);
  		var list = [];
  		$.each(sumJson, function(k, v) {
  			if(v.key && v.order) {
  				list.push(v);
  			}
  		});
			extension[pageType].stsList = list;
	  	//更新页面
	  	$(mt_id).propertygrid("loadData",$.fn.module.getFormPropJson(tableIndex));
		$('#sumWin').window('close');
  	}
  	
  	function getSqlListData() {
  		return $.fn.module.getSqlList($(mt_id).data("tableIndex"),$(mt_id).data("fieldIndex"));
  	}
	function addSqlListData(sqlData) {
		var tableIndex = $(mt_id).data("tableIndex");
		var fieldIndex = $(mt_id).data("fieldIndex");
  		$.fn.module.setSqlList(tableIndex,fieldIndex,sqlData);
  		showLineInfo(tableIndex,fieldIndex);
  	}
  	function setSqlListData(data) {
  		var tableIndex = $(mt_id).data("tableIndex");
		var fieldIndex = $(mt_id).data("fieldIndex");
  		$.fn.module.saveSql(tableIndex,fieldIndex,data);
  		showLineInfo(tableIndex,fieldIndex);
  	}
  	
  	function delSql(data) {
  		var tableIndex = $(mt_id).data("tableIndex");
		var fieldIndex = $(mt_id).data("fieldIndex");
  		$.fn.module.delSql(tableIndex,fieldIndex,data);
  		showLineInfo(tableIndex,fieldIndex);
  	}
  	
  	function doInitComboSql(id) {
  		var tableIndex = $(mt_id).data("tableIndex");
		var fieldIndex = $(mt_id).data("fieldIndex");
  		var data = $.fn.module.getSqlComboData(tableIndex,fieldIndex);
  		$('#'+id).combobox({  
  		    data:data,  
  		    valueField:'key',  
  		    textField:'caption',
  		    width:200
  		});
  		var value = null;
  		switch(id) {
  			case "dsRefKey":
  				var formula = $.fn.module.getFormulaProp(tableIndex,fieldIndex);
  				if(formula && formula.type=="2") {
  					value = formula.refKey;
  					$("#dsRefQsKey").val(formula.refQsKey || "");
  				}
  				break;
  			case "treeQueryRefKey":
  				//var allData = $.fn.module.getQueryType($(mt_id).data("tableIndex"));
  				break;
  			default:
  		}
  		if(value) {
  			$("#"+id).combobox("setValue", value);
  		}
  	}
  	
  	function doOpenSqlEdit(id) {
  		var val = $("#" + id).combobox("getValue");
  		$("#sqlEditWin").data("key", val);
  		$('#sqlEditWin').window('open');
  	}
  	
  	function getCurrentSqlKey() {
  		return $("#sqlEditWin").data("key");
  	}
  	
  	function getEntityName() {
  		var tableIndex = $(mt_id).data("tableIndex");
		var fieldIndex = $(mt_id).data("fieldIndex");
  		var en = $.fn.module.getEntityName(tableIndex,fieldIndex);
  		return en;
  	}
  	
  	$(function() {
  		$("#sqlEditWin").window({
  			onOpen:function() {
  				$("#ifr_sqlEdit")[0].contentWindow.window.location.replace("${ctx}/jsp/sqlList.jsp?treeId=${param.treeId}&title=${param.title}");
  			},
  			onClose:function() {
  				//$("#ifr_sqlEdit")[0].contentWindow.window.location.replace("#");
  				$("#sqlEditWin").removeData("key");
  				showLineInfo($(mt_id).data("tableIndex"),$(mt_id).data("fieldIndex"));
  			}
  		});
  		$("#importExportSetWin").window({
  			onOpen:function() {
  				$("#ifr_importExportSet")[0].contentWindow.window.location.replace("${ctx}/jsp/setImportExport.jsp");
  			},
  			onClose:function() {
  			}
  		});
  		$("#ifr_expEdit")[0].contentWindow.window.location.replace("${ctx}/jsp/funLayout.jsp");	
  	});
  function getDefaultSql(){
	  var tableIndex = $(mt_id).data("tableIndex");
	  var form = $.fn.module.getFormProp(tableIndex);
	 	var formKey =  form["tableName"];
		var options = {
				data : {
					"treeId":"${param.treeId}",
					"urlType":"${param.title}",
					"formKey":formKey
				},
				url: "${ctx}/design/doXml/getDefaultSql",
				success:function(data) {
					$("#querySqlContent").val("");
					$("#querySqlContent").val(data.msg);
				}
			}; 
  		fnFormAjaxWithJson(options);  
   }
  function saveQuerySql(){
		var content=$("#querySqlContent").val();
		var tableIndex = $(mt_id).data("tableIndex");
		var fieldIndex = $(mt_id).data("fieldIndex");
		$.fn.module.setQuerySql(tableIndex,content);
		$(mt_id).propertygrid("loadData",$.fn.module.getFormPropJson(tableIndex));
		$('#querySqlWin').window('close');
  }
  function loadFields(){
	  var tableIndex = $(mt_id).data("tableIndex");
	  var fieldIndex = $(mt_id).data("fieldIndex");
	  var form = $.fn.module.getFormProp(tableIndex);
	  var fData=form.fieldList;
	  var data = [];
	  for(var i=0,len=fData.length;i<len;i++) {
			if(fData[i]["index"] != fieldIndex) {
				data.push(fData[i]);
			}
		}
		$("#cb_syncField").combobox({  
		    "data":data,
		    "valueField":"key",  
		    "textField":"caption"
		});	
  }
  function syncField(){
	 var tableIndex = $(mt_id).data("tableIndex");
	 var fieldIndex = $(mt_id).data("fieldIndex");
	 var val =  $("#cb_syncField").combobox("getValue");
	 var fp = $.fn.module.getFieldProp(tableIndex,fieldIndex);
	 fp["syncKey"] = val;
	 showLineInfo(tableIndex,fieldIndex);
	 $('#openSyncWin').window('close');
  }
  function addSyncFieldsWin(){	  
	  var tableIndex = $(mt_id).data("tableIndex");
	  var form = $.fn.module.getFormProp(tableIndex);
	  if(form["isMaster"]){
		$('#syncFieldsWin').window('open');
		var fData = [];
		var formList =  $.fn.module.getModule().dataSource.formList;
	 		 for(var i=0,len=formList.length;i<len;i++) {
				if(!formList[i]["isMaster"]){		
					fData.push(formList[i]);	
				}
	 		 }
	 		 $("#cb_syncFields").combobox({
		    "data":fData,
		    "valueField":"key",  
		    "textField":"caption" ,
		    "onChange":function(newVal, oldVal){
		    	loadSyncFields(newVal);
		    }
       });
	  }else{
		  alert("只能主表引用同步字段！");
	  }
  }
  var _syncFieldsList = [];
  var _tableName="";
  function loadSyncFields(key){
	  $('#syncFields').edatagrid({
			nowrap: true,
			striped: true,	
			rownumbers:true,
			fitColumns:true,
			frozenColumns:[[
			    {field:'ck',checkbox:true}
			]],
			columns :[[
				{field:'key',title:'显示名',width:100, editor:"text"},
				{field:'colName',title:'字段名',width:150, editor:"text"},
				{field:'dataType',title:'字段类型',width:150, editor:"text",
					   formatter:function(value, rowData, rowIndex){
					    	if("dtString"==value){
					    		return "字符串";
					    	}else if("dtLong"==value){
					    		return "整数";
					    	}else if("dtDouble"==value){
					    		return "小数";
					    	}else if("dtDateTime"==value){
					    		return "时间";
					    	}else if("dtText"==value){
					    		return "文本";
					    	}else if("dtImage"==value){
					    		return "图片";
					    	}
					    }									    	                                                                                    
				},			
			]]
		});
	  var module =  $.fn.module.getModule();
	  var formList =  module.dataSource.formList;
		for(var i=0,len=formList.length;i<len;i++) {
			if(formList[i]["key"]==key){
				var moduleKey =  module.moduleProperties.key.toLowerCase();
				_tableName = moduleKey+"."+key;
				_syncFieldsList = formList[i].fieldList;	
				var syncFields = [];
				for(var i=0;i<_syncFieldsList.length;i++){
					var data = _syncFieldsList[i];
					var fd = {};			
					var dp = data["dataProperties"];
					fd['key']=data["caption"];
					fd['colName']=dp.colName;
					fd['dataType']=dp.dataType;	
					syncFields.push(fd);
				}
				$('#syncFields').edatagrid("loadData",syncFields);
				break;
			}
		}
  }
  function syncFields(){
		var fields = $('#syncFields').edatagrid("getSelections");
		var tableIndex = $(mt_id).data("tableIndex");
		var syncFieldsData = [];
		for(var i=0;i<fields.length;i++){				
			var name = fields[i].key;
			for(var j=0;j<_syncFieldsList.length;j++){
				if(_syncFieldsList[j].caption==name){
					var obj = $.extend({}, _syncFieldsList[j]);
					syncFieldsData.push(obj);
				}
			}
		} 
		$.fn.module.setSyncFields(syncFieldsData,tableIndex,_tableName);
		$('#syncFieldsWin').window('close');
		var data = {"total":0,"rows":[]};
		var frm = $.fn.module.getFormProp(tableIndex);
		var fl = frm.fieldList;
		data.total = fl.length;		
		$.each(fl, function(k, v) {
			if(v.key){
				var tv = $.fn.module.convertFieldToDgJson(v,tableIndex);
				data.rows.push(tv);
			}
		}); 
		$("#table_grid_"+tableIndex).edatagrid("loadData", data);
		var addData = {};
		addData["dataType_"+tableIndex] = "dtString";
		addData["size_"+tableIndex] = "100";
		addData["scale_"+tableIndex] = "0";
		$("#table_grid_"+tableIndex).edatagrid("addRow",addData);
  }
  function doRefreshModuleTab(url){
	  var tab = $("#module_table_tab").tabs('getSelected');
	  if(tab) {
		  if(url) {
				  tab.panel('refresh', url);
		  } else {
				  tab.panel('refresh');	
		  }
	  }
  }
	function loadAllI18n(){		
		var module = $.fn.module.getModule();
		//表-国际化数据初始化
		var il8nTables = module.dataSource.formList;
		$('#i18nTables').edatagrid({
			width : 'auto',
			height : 'auto',
			nowrap: true,
			striped: true,
			rownumbers:true,
			columns :[[
				{field:'key',title:'显示名',width:120},
				{field:'caption',title:'表名',width:100},
				{field:'langType',title:'语言选择',width:160,
					editor: {
						type:'combobox',
						options:{
							valueField:'key',
							textField:'caption',
							data:langType
						}
					},
					formatter:langDataType
				},
				{field:'langText',title:'对应语言',width:100, sortable:true,editor:{type:'validatebox'}}
			]],
			onClickRow:function(rowIndex, rowData){
				$("#i18nTables").edatagrid('editRow',rowIndex);
			}
		});
		var data = {"total":0,"rows":[]};

		$.each(il8nTables,function(k,v){
			var languageText = v.languageText;
			if(languageText){
				var json_obj =  jQuery.parseJSON(languageText);
				$.each(langType,function(k1,v1){
					var language = {
						'key': v.key,
						'caption': v.caption,
						'langType' : v1.key,
						'langText' : ''
					};
					$.each(json_obj,function(k2,v2){
						if(v1.key==k2){
							language['langText']=v2;
						}
					});
					data.rows.push(language);
				});
			}else{
				$.each(langType,function(k1,v1){
					var language = {
						'key': v.key,
						'caption': v.caption,
						'langType' : v1.key,
						'langText' : ''
					};
					if(v1.key=='zh_CN'){
						language['langText'] = v.caption;
					}
					data.rows.push(language);
				});
			}
		});

		data.total=data.rows.length;
		$('#i18nTables').edatagrid("loadData",data);		
		
		//字段-国际化数据初始化		
		$('#i18nFields').edatagrid({
			width : 'auto',
			height : 'auto',
			nowrap: true,
			striped: true,	
			rownumbers:true,
			columns :[[
				{field:'key',title:'所属表名',width:120},
				{field:'caption',title:'字段名',width:100},
				{field:'langType',title:'语言选择',width:160,
					editor : {
						type:'combobox',
						options:{
							valueField:'key',
							textField:'caption',
							data:langType
							}
						},
							formatter:langDataType
				},
				{field:'langText',title:'对应语言',width:100, sortable:true,editor:{type:'validatebox'}}
			]],
			onClickRow:function(rowIndex, rowData){
				 $("#i18nFields").edatagrid('editRow',rowIndex);
			}
		});
		var data2 = {"total":0,"rows":[]};
			$.each(il8nTables,function(k,v){
				var fields = v.fieldList;
				var tableName = v.caption;
				$.each(fields,function(k2,v2){
					if(v2.key){
						var languageText = v2.languageText;
						if(languageText){
							var json_obj =  jQuery.parseJSON(languageText);
							$.each(langType,function(k3,v3){
								var language = {
									'key':tableName,
									'caption': v2.caption,
									'langType' : v3.key,
									'langText' : ''
								};
								$.each(json_obj,function(k4,v4){
									if(v3.key==k4){
										language['langText']=v4;
									}
								});
								data2.rows.push(language);
							});
						} else {
							$.each(langType,function(k3,v3){
								var language = {
									'key': tableName,
									'caption': v2.caption,
									'langType' : v3.key,
									'langText' : ''
								};
								if(v3.key=='zh_CN'){
									language['langText'] = v2.caption;
								}
								data2.rows.push(language);
							});
						}
					}
				});
			});
	 
	 data2.total=data2.rows.length;
	$('#i18nFields').edatagrid("loadData",data2);
	
	//按钮-国际化数据初始化
	$('#i18nButtons').edatagrid({
			width : 'auto',
			height : 'auto',
			nowrap: true,
			striped: true,
			rownumbers:true,
			columns :[[
				{field:'key',title:'所属页面按钮',width:120},
				{field:'caption',title:'按钮名',width:100},
				{field:'langType',title:'语言选择',width:160,
					editor : {
						type:'combobox',
						options:{
							valueField:'key',
							textField:'caption',
							data:langType
							}
						},
							formatter:langDataType
				},
				{field:'langText',title:'对应语言',width:100, sortable:true,editor:{type:'validatebox'}}
			]],
			onClickRow:function(rowIndex, rowData){
				 $("#i18nButtons").edatagrid('editRow',rowIndex);
			}
	});
	 var data3 = {"total":0,"rows":[]};
		$.each(il8nTables,function(k,v){
			var q_buttons = v.extension.queryPage.buttonList;
			var e_buttons = v.extension.editPage.buttonList;
			  $.each(q_buttons,function(k2,v2){	  		  
						 var languageText = v2.languageText;
						 if(languageText){
									 var json_obj =  jQuery.parseJSON(languageText);
									 $.each(langType,function(k3,v3){
										 var language = {
											 'key':"查询页面",
											 'caption': v2.caption,
											 'langType' : v3.key,
											 'langText' : ''
									 };
										$.each(json_obj,function(k4,v4){
											if(v3.key==k4){
												language['langText']=v4;
											}
										});
										data3.rows.push(language);
									 });					 					
						 }else{
								 $.each(langType,function(k3,v3){
									 var language = {
										 'key': '查询页面',
										 'caption': v2.caption,
										 'langType' : v3.key,
										 'langText' : ''
									 };
									 if(v3.key=='zh_CN'){
				 						 language['langText'] = v2.caption;		
									 }
									 data3.rows.push(language);
								 });
						 }
			}); 
			$.each(e_buttons,function(k2,v2){	  		  
					 var languageText = v2.languageText;
					 if(languageText){
								 var json_obj =  jQuery.parseJSON(languageText);
								 $.each(langType,function(k3,v3){
									 var language = {
										 'key':'编辑页面',
										 'caption': v2.caption,
										 'langType' : v3.key,
										 'langText' : ''
								 };
									$.each(json_obj,function(k4,v4){
										if(v3.key==k4){
											language['langText']=v4;
										}
									});
									data3.rows.push(language);
								 });					 					
						}else{
							 $.each(langType,function(k3,v3){
								 var language = {
									 'key': '编辑页面',
									 'caption': v2.caption,
									 'langType' : v3.key,
									 'langText' : ''
							 };
								 if(v3.key=='zh_CN'){
			 						language['langText'] = v2.caption;		
			 					}
								data3.rows.push(language);
							 });
					}
			});  
		});		 						 	
 
		 data3.total=data3.rows.length;
		$('#i18nButtons').edatagrid("loadData",data3);
	}
	function saveAllLanguage(){
		//表-国际化数据保存
		$("#i18nTables").edatagrid("saveRow");
		var tableData = $("#i18nTables").edatagrid("getData");															
		var tableRows = tableData.rows;	
		//字段-国际化数据保存
		$("#i18nFields").edatagrid("saveRow");
		var fieldData = $("#i18nFields").edatagrid("getData");
		var fieldRows = fieldData.rows;
		//按钮-国际化数据保存
		$("#i18nButtons").edatagrid("saveRow");
		var buttonData = $("#i18nButtons").edatagrid("getData");
		var buttonRows = buttonData.rows;
		
		var module = $.fn.module.getModule();
		var il8nTables = module.dataSource.formList;
		$.each(il8nTables,function(k,v){
			var langObj = {};
			$.each(tableRows,function(k1,v1){
				if(v1.key==v.key){
					langObj[v1.langType]=$.fn.module.removeBlank(v1).langText;
				}									
			});
			
			var fl = v.fieldList;
			var tableName = v.caption;
			$.each(fl,function(k2,v2){
				if(v2.key){
					var langObj2 = {};
					$.each(fieldRows,function(k3,v3){
						if(v3.key==tableName && v3.caption==v2.caption){						
							langObj2[v3.langType]=$.fn.module.removeBlank(v3).langText;							
						}
					});
					var jsonStr = JSON.stringify(langObj2);
					v2.languageText = jsonStr;
				}			
			});
			
			var e_bl = v.extension.editPage.buttonList;
			$.each(e_bl,function(k3,v3){
				var langObj3 = {};
					$.each(buttonRows,function(k4,v4){
						if(v4.key=='编辑页面' && v4.caption==v3.caption){							
							langObj3[v4.langType]=$.fn.module.removeBlank(v4).langText;						
						}
					});
					var jsonStr = JSON.stringify(langObj3);
					v3.languageText = jsonStr;
			});
			
			var q_bl = v.extension.queryPage.buttonList;
			$.each(q_bl,function(k4,v4){
				var langObj4 = {};
					$.each(buttonRows,function(k5,v5){
						if(v5.key=='查询页面' && v4.caption==v5.caption){							
							langObj4[v5.langType]=$.fn.module.removeBlank(v5).langText;						
						}
					});
					var jsonStr = JSON.stringify(langObj4);
					v4.languageText = jsonStr;
			}); 
			
			var jsonStr = JSON.stringify(langObj);
			v.languageText = jsonStr;
		 });			
		$('#i18nSetWinSet').window('close');
	}
	function moveUp(){
		var rowIndex = parseInt($("#menuButtonList").data("rowIndex"));
		if (rowIndex > 0) {
			var rows = $("#menuButtonList").edatagrid("getRows");
			var currentRow = $.extend(true,{}, rows[rowIndex]);
			var lastRow = $.extend(true,{}, rows[rowIndex - 1]);
			$("#menuButtonList").edatagrid('updateRow', {
				index : rowIndex - 1,
				row : currentRow
			});
			$("#menuButtonList").edatagrid('updateRow', {
				index : rowIndex,
				row : lastRow
			});
			$("#menuButtonList").datagrid('unselectRow', parseInt(rowIndex));
			$("#menuButtonList").datagrid('selectRow', parseInt(rowIndex - 1));
		}else{
			$.messager.alert('警告！','该行是第一行，无法上移!','warning');
		}
		
	}
	function moveDown(){
		var rowIndex = parseInt($("#menuButtonList").data("rowIndex"));
		var rows = $("#menuButtonList").edatagrid("getRows");
		if (rowIndex < rows.length - 1) {
			var currentRow = $.extend(true,{}, rows[rowIndex]);
			var nextRow = $.extend(true,{}, rows[rowIndex + 1]);
			$("#menuButtonList").edatagrid('updateRow', {
				index : rowIndex + 1,
				row : currentRow
			});
			$("#menuButtonList").edatagrid('updateRow', {
				index : rowIndex,
				row : nextRow
			});
			$("#menuButtonList").datagrid('unselectRow', parseInt(rowIndex));
			$("#menuButtonList").datagrid('selectRow', parseInt(rowIndex + 1));
		}else{
			$.messager.alert('警告！','该行是最后一行，无法下移!','warning');
		}
	}
	/*数据库设计刷新事件——2019/8/1 13:56*/
	function doRefreshTables() {
		var tabs = $("#module_table_tab").tabs('tabs');/*返回全部的标签页面板*/
		if(tabs && tabs.length) {
			$.each(tabs, function(i, tab) {
				if(tab) tab.panel('refresh');
			});
		}
	}
	//初始化按钮列表
	function doInitMenuButtonList(buttonList) {
		$('#menuButtonList').edatagrid({
			nowrap: true,
			striped: true,	
			rownumbers:true,
			frozenColumns:[[
			                {field:'ck',checkbox:true}
			]],
			columns :[[
				{field:'key',title:'按钮key',width:80, sortable:true},
				{field:'caption',title:'显示名',width:80, sortable:true,editor:"text"},
				{field:'iconCls',title:'按钮图片',width:120, sortable:true,
					editor:{
                		"type":"combobox",
                		"options":{
                			"valueField":'id',
                			"textField":'text',
                			"url" : '${ctx}/js/easyui/menu-icon-data.json',
                			"formatter":function(row){  
            			    	return "<span style='width: 16px;height:16px;display:inline-block;vertical-align:middle;' class='"+row.id+"'></span><span class='item-text'>"+row.text+"</span>";
            			     } 
                			
                		}
				}},
				{field:'enableAuthCheck',title:'开启权限检查',width:80, editor:{"type":"checkbox","options":{on:"true", off:"false"}},
					formatter :  function(value, rowData, rowIndex) {
						return value+"";
					}
				},
				{field:'width',title:'宽度',width:50, sortable:true,editor:"text"},
				{field:'height',title:'高度',width:50, sortable:true,editor:"text"},
				{field:'buttonType',title:'按钮类型',width:80, sortable:true,
					formatter :  function(value, rowData, rowIndex) {
					if(value=="normal"){
						return "普通按钮";
						}
					}
				},								
				/*{field:'languageText',title:'国际化设置',width:90,align : 'center',	
					formatter :  function(value, rowData, rowIndex) {
						var arr = [];
						if(rowData.key!=null){
							arr.push('<a href="javascript:void(0);" onclick="setMenuBtEvent(');
							arr.push(rowIndex);
							arr.push(')">设置</a>');
							}
						return arr.join("");
						}
					},*/
				{field : 'eventList',title : '添加事件',width : 90,align : 'center',
					formatter :  function(value, rowData, rowIndex) {	
						var strArr = [];
						if(rowData.key!=null){
						strArr.push('<a href="javascript:void(0);" onclick="setMenuBtEvent(');
						strArr.push(rowIndex);
						strArr.push(')">设置</a>');
						}
					return strArr.join("");
			}
		}
		]],
		onRowContextMenu : function(e, rowIndex, rowData) {
			e.preventDefault();
			$('#updown_menu').menu('show', {
				left : e.pageX,
				top : e.pageY
			});
			$("#menuButtonList").data("rowIndex", rowIndex);
		},
		onClickRow:function(rowIndex, rowData){
			 $("#menuButtonList").edatagrid('editRow',rowIndex);
		}, 
			toolbar:[{
				id:'mb_add',
				text:'添加',
				iconCls:'icon-add',
				handler:function(){									 
						 	var i =parseInt(Math.random()*1000+1); 
				 			var key = 'addBt_'+i;		 									
				$("#menuButtonList").edatagrid('addRow',{"key":key,"caption":"","buttonType":"normal","eventList":[]});
				}
			},'-',{
				id:'mb_del',
				text:'删除',
				iconCls:'icon-remove',
				handler:function(){	
					
					$("#menuButtonList").edatagrid('destroyRow');
				}
			}]
		});	
		var data = {"total":0,"rows":[]};
		$.each(buttonList,function(k,v){
			 var obj = $.extend(true, {},v);
			 obj["enableAuthCheck"] = new String(obj["enableAuthCheck"]);//转化boolean值
			 data.rows.push(obj);	 						
		});
		data.total=buttonList.length;
		$('#menuButtonList').edatagrid("loadData",data);
	}
</script>
</head>
<body class="easyui-layout">
		<div region="center" <%-- iconCls="icon-user" title="<s:text name="system.index.workspace.title"/>" --%> style="overflow:hidden;">
			<div id="main_tabs_div_1" class="easyui-tabs" fit="true" border="false" tools="#tab-tools1">			
			
				<div title="数据库设计" style="padding:2px;" tools="#tab-tools-table">
					<%if("form".equals(request.getParameter("nodesType"))){%>
						<div id="module_table_tab" class="easyui-tabs" fit="true" style="height:100px;width:300px;" tools="#tab-tools_form"></div>
					<%}else{ %>
						<div id="module_table_tab" class="easyui-tabs" fit="true" style="height:100px;width:300px;" tools="#tab-tools2"></div>
					<%} %>
				</div>
	
				<%if("dict".equals(request.getParameter("nodesType"))){%>
						<div title="查询页面设计"  style="padding:2px;" <%-- href="${ctx}/jsp/dictionary.jsp" --%>  tools="#tab-tools-dictionary">
							<iframe id="dictIfm" scrolling='auto' frameborder='0' src='' style='width:100%;height:100%;'></iframe>
						</div>
				<%}else{ %>
						<div title="查询页面设计"  style="padding:2px;" href="${ctx}/jsp/query.jsp" tools="#tab-tools-query"></div>
				<%} %>
				<%-- <div title="查询页面设计"  style="padding:2px;" href="${ctx}/jsp/query.jsp" tools="#tab-tools-query">
				</div> --%>

					<div title="编辑页面设计" id="edit_ifr" style="padding:2px;" tools="#tab-tools-edit">
						<iframe id="editIframe" scrolling='auto' frameborder='0' src='' style='width:100%;height:100%;'></iframe>
					</div>

				<div title="业务流程设计" style="padding:2px;"  href="${ctx}/jsp/flow.jsp?treeId=${param.treeId}&title=${param.title}">
				</div>
			</div>
		</div>
		<div region="east"  title="属性" split="true" <%-- iconCls="icon-home" title="<s:text name="system.index.title"/>"  --%> style="width:325px;padding:0px;overflow:auto;" border="false">
			<table id="module_table_propGrid"></table>
		</div>
		<div id="tab-tools1">			
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-save" onclick="saveModule();" title="保存模块"></a>			
		</div>
		<div id="tab-tools2">
		<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-back" onclick="addSyncFieldsWin()" title="引用同步字段表"></a>
		<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-redo" onclick="javascript:$('#openCommonWin').window('open');" title="引用公用表"></a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add" onclick="addChildTab('子表');" title="添加子表"></a>
		</div>
		<div id="tab-tools_form">
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add" onclick="javascript:$('#formFieldWin').window('open');" title="引用字段">引用字段</a>
			<a href="#" class="easyui-linkbutton" plain="true"  iconCls="icon-add" onclick="addChildTab('子表');" title="添加子表">添加子表</a>
		</div>
		
		<div id="tab-tools-module">
			<a href="#" class="icon-mini-refresh" onclick="doRefreshModuleTab();" title="刷新本页"></a>
		</div>
		<div id="tab-tools-table">
			<a href="#" class="icon-mini-refresh" onclick="doRefreshTables();" title="刷新本页"></a>
		</div>
		<div id="tab-tools-query">
			<a href="#" class="icon-mini-refresh" onclick="doRefreshTab('查询页面设计','${ctx}/jsp/query.jsp');" title="刷新本页"></a>
		</div>
		<div id="tab-tools-dictionary">
			<a href="#" class="icon-mini-refresh" onclick="doRefreshTab('查询页面设计','${ctx}/jsp/dictionary.jsp', true);" title="刷新本页"></a>
		</div>
		<div id="tab-tools-edit">
			<a href="#" class="icon-mini-refresh" onclick="doRefreshTab('编辑页面设计','${ctx}/jsp/edit.jsp', true);" title="刷新本页"></a>
		</div>
		<div id="tab-tools-form-edit">
			<a href="#" class="icon-mini-refresh" onclick="doRefreshTab('编辑页面设计','${ctx}/jsp/form-edit.jsp', true);" title="刷新本页"></a>
		</div>
		<div id="tab-tools-flow">
			<a href="#" class="icon-mini-refresh" onclick="doRefreshTab('业务流程设计','${ctx}/jsp/flow.jsp?treeId=${param.treeId}');" title="刷新本页"></a>
		</div>
		<div id="copyTabTitle"  class="easyui-menu" style="width:100px;">
			<div onclick="copyTabs();">复制表格</div>
			<div onclick="delectTab();">删除表格</div>
		</div> 
		<div id="pasteTabTitle"  class="easyui-menu" style="width:100px;">
			<div onclick="pasteTabs();">粘贴表格</div>			
		</div> 
		
		<!-- 设置字段公式属性  -->
			<div id="openFormulaWin" class="easyui-window" title="数据源设置" closed="true" modal="false" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:810px;height:300px;padding:5px;">
				<div class="easyui-layout" fit="true" >
					<div region="north" border="false" style="padding:3px;background:#fff;border:1px solid #ccc;">
						<input type="radio" name="checkSql" value="0" id="radio0" checked="checked"/>无内容
						<input type="radio" name="checkSql" value="1" id="radio1" />固定值
						<input type="radio" name="checkSql" value="2" id="radio2" />SQL语句
						<input type="radio" name="checkSql" value="5" id="radio5" />模块
					</div>
				 	<div region="center" border="false"  style="padding:3px;background:#fff;border:1px solid #ccc;" >						
						<div id="sqlView">
							<div>编辑数据源：<input id="dsRefKey" name="dsRefKey" />
								<button onclick="doInitComboSql('dsRefKey')">刷新sql列表</button>
								<button onclick="doOpenSqlEdit('dsRefKey')">sql编辑</button>
							</div>
							<div>
								查询数据源：<input id="dsRefQsKey" name="dsRefQsKey" style="width:400px;"/>
							</div>
							<%-- 
						 		<table>
											<tr>
												<th><label >查询语句:</label></th>
												<td><textarea name="sqlContent" id="sqlContent" style="width:500px;" rows="3"></textarea></td>
											</tr>	
								</table>
							<table id="sqlTab" rownumbers="true">
								<thead>
									<tr>
										<th field="flag" width="100" formatter="flagDataType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:flagData}}">节点类型</th> 
										<th field="key" width="100" editor="{type:'validatebox'}">字段Key(key)</th>
										<th field="caption" width="100" editor="{type:'validatebox'}">显示名称(caption)</th>
										<th field="column" width="100" editor="{type:'validatebox'}">字段名(column)</th>
										<!-- <th field="operator" width="90" formatter="sqlQueryType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:_sqlQueryWhere}}">运算条件(operator)</th> -->
										<th field="order" width="90" formatter="orderByDataType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:orderByData}}">排序方式(order)</th>
									</tr>
								</thead>						
							</table>--%>
						</div>
						<div  id ="fixView" >					
							<table id="fixTab" rownumbers="true">
								<thead>
									<tr>
										<th field="key" width="120" editor="{type:'validatebox'}">字段Key(key)</th>
										<th field="caption" width="120" editor="{type:'validatebox'}">显示名称(caption)</th>
									</tr>
								</thead>						
							</table>
						</div>
						<div id="moduleView">
							<table border="0" cellpadding="0" cellspacing="1" class="table_form">
								<tr>
									<th style="width:80px;"><label for="cb_moduleTable">请选择模块表:</label></th>
									<td><input id="cb_moduleTable" style="width:220px;"></input></td>
									<th style="width:60px;"><label>数据权限:</label></th>
									<td><input id="isCheckAuth" name="isCheckAuth" type="checkbox" value="1"/><label for="isCheckAuth">过滤</label>
									</td>
								</tr>
								<tr>
									<th style="width:60px;"><label>数据状态:</label></th>
									<td><input style="width:160px;" id="dataStatus"/>
									</td>
									<th></th>
									<td></td>
								</tr>	
							</table>
							<div id="moduleFieldsView">
							<table id="moduleFields"></table>
							</div>
						</div>
					<%-- 
					<div id="expressView"  >
						 <iframe scrolling='auto' frameborder='0'  src='${ctx}/jsp/funLayout.jsp' style='width:100%;height:390px;'></iframe>
					</div> --%>
					</div>
					<div region="south" border="false" style="text-align:right;padding:3px 0;">
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="setSqlProp();">设置</a>
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#openFormulaWin').window('close');">关闭</a>
					</div>
				</div>
			</div> 
			<!-- 自定义表单中引用字段  -->
			<div id="formFieldWin" class="easyui-window" title="引用字段" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:700px;height:500px;padding:5px;">
				<div class="easyui-layout" fit="true" >		
				 	<div region="center" border="false"  style="padding:3px;background:#fff;border:1px solid #ccc;" >						
						<div >
							<table border="0" cellpadding="0" cellspacing="1" class="table_form">
								<tr>
									<th style="width:129px;"><label for="formTables">请选择引用字段的表:</label></th>
									<td><input id="formTables" style="width:180px;"></input></td>
								</tr>	
							</table>
							<div >
							<table id="formFields"></table>
							</div>
						</div>
					</div>
					<div region="south" border="false" style="text-align:right;padding:3px 0;">
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="addFields();">引用</a>
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#formFieldWin').window('close');">关闭</a>
					</div>
				</div>
			</div> 
			
				<!-- 引用公用表  -->
			<div id="openCommonWin" class="easyui-window" title="引用公用表" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:700px;height:500px;padding:5px;">
				<div class="easyui-layout" fit="true" >
		
				 	<div region="center" border="false"  style="padding:3px;background:#fff;border:1px solid #ccc;" >						
						<div id="commonView">
							<table border="0" cellpadding="0" cellspacing="1" class="table_form">
								<tr>
									<th style="width:129px;"><label for="cb_commonTable">请选择公用模块表:</label></th>
									<td><input id="cb_commonTable" style="width:180px;"></input></td>
								</tr>	
							</table>
							<div id="commonFieldsView">
							<table id="commonFields"></table>
							</div>
						</div>
					</div>
					<div region="south" border="false" style="text-align:right;padding:3px 0;">
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="addCommonFields();">引用</a>
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#openCommonWin').window('close');">关闭</a>
					</div>
				</div>
			</div> 
				<!-- 引用同步字段  -->
			<div id="syncFieldsWin" class="easyui-window" title="引用同步字段" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:500px;height:400px;padding:5px;">
				<div class="easyui-layout" fit="true" >
		
				 	<div region="center" border="false"  style="padding:3px;background:#fff;border:1px solid #ccc;" >						
						<div id="syncFView">
							<table border="0" cellpadding="0" cellspacing="1" class="table_form">
								<tr>
									<th style="width:129px;"><label for="cb_syncFields">请选择同步表 :</label></th>
									<td><input id="cb_syncFields" style="width:180px;"></input></td>
								</tr>	
							</table>
							<div id="syncFieldsView">
							<table id="syncFields"></table>
						</div>
						</div>
					</div>
					<div region="south" border="false" style="text-align:right;padding:3px 0;">
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="syncFields();">引用</a>
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#syncFieldsWin').window('close');">关闭</a>
					</div>
				</div>
			</div>
					<!-- 同步字段  -->
			<div id="openSyncWin" class="easyui-window" title="同步字段" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:400px;height:120px;padding:5px;">
				<div class="easyui-layout" fit="true" >
		
				 	<div region="center" border="false"  style="padding:3px;background:#fff;border:1px solid #ccc;" >						
						<div id="syncView">
							<table border="0" cellpadding="0" cellspacing="1" class="table_form">
								<tr>
									<th style="width:129px;"><label for="cb_syncField">请选择同步字段 :</label></th>
									<td><input id="cb_syncField" style="width:180px;"></input></td>
								</tr>	
							</table>
						</div>
					</div>
					<div region="south" border="false" style="text-align:right;padding:3px 0;">
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="syncField();">同步</a>
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#openSyncWin').window('close');">关闭</a>
					</div>
				</div>
			</div>
			
			<!-- 自定义查询  -->
			<div id="querySqlWin" class="easyui-window" title="设置自定义查询语句" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:600px;height:350px;padding:5px;">
				<div class="easyui-layout" fit="true" >
			<div region="north" border="false" style="padding:3px;background:#fff;border:1px solid #ccc;">
						<input type="button" value="默认语句" onclick="getDefaultSql()" />
					</div>
				 	<div region="center" border="false"  style="padding:3px;background:#fff;border:1px solid #ccc;" >						
						<div title="自定义语句">
								<table border="0" cellpadding="0" cellspacing="1" class="table_form">
											<tr>
												<th style="width:60px;"><label for="querySqlContent">查询语句:</label></th>
												<td><textarea name="querySqlContent" id="querySqlContent" style="width:98%;height:218px;"></textarea></td>
											</tr>	
								</table>
							</div>
					</div>
					<div region="south" border="false" style="text-align:right;padding:3px 0;">
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="saveQuerySql();">保存</a>
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#querySqlWin').window('close');">关闭</a>
					</div>
				</div>
			</div> 
		<!-- 设置公式代码 -->
		 <div id="expCodeWin" class="easyui-window" title="公式代码设置" closed="true" modal="false" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:788px;height:500px;">
			<div class="easyui-layout" fit="true">
				<div id="expressView" region="center" border="false" fit="true">					
					<iframe id="ifr_expEdit" scrolling='auto' frameborder='0'  src='' style='width:100%;height:100%;'></iframe>
				</div>
				<div region="south" border="false" style="text-align:right;padding:5px 0;border-top: 1px solid rgb(185, 179, 179);">
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:saveExpCode();">设置</a>
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#expCodeWin').window('close');">关闭</a>
				</div>
			</div>
		</div>	
		
		<!-- 设置国际化资源 -->
		 <div id="i18nWin" class="easyui-window" title="国际化资源设置" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:400px;height:240px;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="background:#fff;border:1px solid #ccc;" fit="true">					
					<div id=langView>
						<table id="langTab" rownumbers="true">
								<thead>
									<tr>
										<th field="langType" width="160" formatter="langDataType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:langType}}">语言选择</th> 
										<th field="langText" width="160" editor="{type:'validatebox'}">对应语言</th>
									</tr>
								</thead>						
							</table>
					</div>			
				</div>
				<div region="south" border="false" style="text-align:right;padding:5px 0;">
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:saveLanguage();">设置</a>
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#i18nWin').window('close');">关闭</a>
				</div>
			</div>
		</div>  
			<!-- 汇总统计 -->
			<div id="sumWin" class="easyui-window" title="汇总统计" closed="true" modal="true" minimizable="false" maximizable="false" collapsible="false" closable="true" style="width:650px;height:300px;">
				<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="background:#fff;border:1px solid #ccc;" fit="true">					
					<table id="sumList" fit="true">
					</table>	
				</div>
				<div region="south" border="false" style="text-align:right;padding:5px 0;">
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:saveSumRule();">保存</a>
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#sumWin').window('close');">关闭</a>
				</div>
			</div>		
		</div>
		<!-- 创建菜单按钮 -->
		<div id="addMenuButton" class="easyui-window" title="新建菜单按钮" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:700px;height:400px;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="background:#fff;border:0px solid #ccc;" fit="true">					
					<table id="menuButtonList">
					</table>		
				</div>
				<div region="south" border="false" style="text-align:right;padding:0;">
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:saveMenuButton();">保存</a>
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#addMenuButton').window('close');">关闭</a>
				</div>
			</div>		
		</div>
		<%-- 
		<!-- 设置表单查询方式属性 -->
		<div id="openFormWin" class="easyui-window" title="表单查询方式属性设置" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:630px;height:400px;padding:5px;">
				<div class="easyui-layout" fit="true" >
					<div region="north" border="false" style="padding:3px;background:#fff;border:1px solid #ccc;">
						<input type="checkbox" name="queryType" value="1" id="checkBox1" />树形查询
					<!-- 	<input type="checkbox" name="queryType" value="2" id="checkBox2" />XX查询
						<input type="checkbox" name="queryType" value="3" id="checkBox3" checked="checked"/>XX查询 -->
						<div id="queryField">主查询字段:<input  id="treeComboBox" class="easyui-combobox" style="width:100px;"  panelHeight="auto"></input></div>
					</div>
				 	<div region="center" border="false" style="padding:3px;background:#fff;border:1px solid #ccc;">						
						<div id="sqls">
							<input id="treeQueryRefKey" name="treeQueryRefKey" />
							<button onclick="doInitComboSql('treeQueryRefKey')">刷新sql列表</button>
							<button onclick="doOpenSqlEdit('treeQueryRefKey')">sql编辑</button>
							 
						 		<table>
											<tr>
												<th><label >查询语句:</label></th>
												<td><textarea name="sqlContent" id="tabSqlContent" style="width:500px;" rows="3"></textarea></td>
											</tr>	
								</table>
							<table id="treeSqlTab">
								<thead>
									<tr>
										<th field="flag" width="100" formatter="flagDataType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:flagData}}">节点类型</th> 
										<th field="key" width="100" editor="{type:'validatebox'}">字段Key(key)</th>
										<th field="caption" width="100" editor="{type:'validatebox'}">显示名称(caption)</th>
										<th field="column" width="100" editor="{type:'validatebox'}">字段名(column)</th>
										<!-- <th field="operator" width="100" formatter="sqlQueryType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:_sqlQueryWhere}}">运算条件(operator)</th> -->
										<th field="order" width="100" formatter="orderByDataType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:orderByData}}">排序方式(order)</th>
									</tr>
								</thead>						
							</table>
						</div>
						
					</div> 
					<div region="south" border="false" style="text-align:right;padding:3px 0;">
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="setQueryType();">设置</a>
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="$('#openFormWin').window('close');">关闭</a>
					</div>
				</div>
			</div> 
			--%>
			
			<!-- 添加事件页面 -->
			<div id="addEventWin" class="easyui-window" title="事件编辑" closed="true" modal="false" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:800px;height:500px;">
				 <div class="easyui-layout" fit="true" >
					 <div region="west"  title="事件类型" split="true" style="width:200px">
					 	<div class="easyui-layout" fit="true" >
					 		<div region="north" split="true" style="height:250px">
					 			<ul id="eventTree" class='ztree'></ul>
					 		</div>
					 		 <div region="center" title="参数">
					 		 	<span id="fun_ztree_params" style="font-size: 10pt"></span>
					 		 </div>
					 		  <div region="south" title="描述" split="true" style="height:80px">
					 		 	 <span id="fun_ztree_description" style="font-size: 10pt"></span>
					 		 </div>
					 	</div>
					 </div>  
					 <div region="center"  title="">
					 <!-- 添加页面事件iframe -->
					 <div id="eventIframe"> 
					 	 <iframe scrolling='auto' frameborder='0'  src='${ctx}/jsp/funLayout.jsp' style='width:100%;height:435px;;'></iframe>
					 </div> 
					
						  <!-- 	<iframe scrolling='auto' frameborder='0'  src='${ctx}/jsp/funLayout.jsp' style='width:100%;height:100%;'></iframe>
						  <div class="easyui-layout" fit="true" >
						  		<div region="west"  title="" style="width:350px;padding:1px;">
						  			<div class="easyui-layout" fit="true" >
						 				<div region="north"  title="函数列表" split="true" style="height:150px;padding:1px;">
						 					
						 				</div>
						 				<div region="center"  title="参数" style="height:150px;padding:1px;">
						 			
						 				</div>  
						 				<div region="south"  title="帮助" split="true" style="height:150px;padding:1px;">
						 			
						 				</div>
						 			</div> 
						 		</div> 
						 		<div region="center"  title="函数编辑界面" style="padding:1px;">
						 
						 		</div> 
					  		</div> -->
					  </div>
					  <div region="south" border="false" style="text-align:right;padding:3px 0;">
							<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="setEvents();">设置</a>
							<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#addEventWin').window('close');">关闭</a>
					 </div>
				</div>
			</div> 
			
		 <!-- sql编辑 -->
		 <div id="sqlEditWin" class="easyui-window" title="sql编辑" closed="true" modal="false" minimizable="false" maximized="true" maximizable="true" collapsible="false" closable="true" style="width:600px;height:500px;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" fit="true">
					<iframe id="ifr_sqlEdit" scrolling='auto' frameborder='0'  src='' style='width:100%;height:95%;'></iframe>
				</div>
				<div region="south" border="false" style="text-align:right;padding:2px 0;">
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#sqlEditWin').window('close');">关闭</a>
				</div>
			</div>
		</div>	
		<!-- 动态显示设置 -->
		<div id="dynamicPropWin" class="easyui-window" title="动态显示设置" closed="true" style="width:650px;height:400px;">
			<iframe  id="ifr_dp" src="" frameborder="0" style="width:100%;height:100%"></iframe>
		</div>	
		
		<!-- 设置导入导出规则 -->
		<div id="importExportSetWin" class="easyui-window" title="导入导出设置" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:400px;height:500px;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" fit="true">					
					<iframe id="ifr_importExportSet" scrolling='auto' frameborder='0' src='' style='width:100%;height:100%;'></iframe>
				</div>
			</div>
		</div>
			<!-- 设置所有表，字段，按钮国际化资源 -->
		<div id="i18nSetWinSet" class="easyui-window" title="国际化资源设置" closed="true" modal="true" minimizable="false" maximizable="true" collapsible="false" closable="true" style="width:600px;height:500px;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="background:#fff;border:1px solid #ccc;" fit="true">					
					<div id="tnotice" class="easyui-tabs"   border="false">
						<div selected="true" title="表-国际化设置">
							<table id="i18nTables" border="false"></table>
						</div>
						<div title="字段-国际化设置">
							<table id="i18nFields" border="false"></table>
						</div>
						<div title="按钮-国际化设置">
							<table id="i18nButtons" border="false"></table>
						</div>
					</div>		
				</div>
				
				<div region="south" border="false" style="text-align:right;padding:2px 0;">
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="saveAllLanguage();">保存</a>
					<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#i18nSetWinSet').window('close');">关闭</a>
				</div>
			</div>
		</div>  	
			
			<!-- 新建编辑界面tabs分组 -->
			<div id="create_tabsMenu" class="easyui-menu" style="width:100px;">
				<div onclick="createTabsGroup();">新建分组</div>
				<div onclick="javascript:addButton()">添加按钮</div>
			</div>
			<!-- 添加查询界面自定义按钮 -->
			<div id="add_buttonMenu" class="easyui-menu" style="width:100px;">
				<div onclick="javascript:addButton()">添加按钮</div>
			</div>
			<!-- 删除查询方式 -->
			<div id="delTree" class="easyui-menu" style="width:100px;">
				<div iconCls="icon-remove" onclick="delTreeRow();">删除</div>
			</div>
			<!-- 删除公式sql属性   -->
			<div id="delSql" class="easyui-menu" style="width:100px;">
				<div iconCls="icon-remove" onclick="delSqlViewRow();">删除</div>
			</div>
			<!-- 删除公式固定值属性  -->
			<div id="delFix" class="easyui-menu" style="width:100px;">
				<div iconCls="icon-remove" onclick="delFixViewRow();">删除</div>
			</div>
			<!-- 删除公式固定值属性  -->
			<div id="deli18n" class="easyui-menu" style="width:100px;">
				<div iconCls="icon-remove" onclick="deli18nRow();">删除</div>
				<div iconCls="icon-edit" onclick="editi18nRow('langTab');">修改</div>
			</div>
			<!-- 删除事件 -->
			<div id="delEvent" class="easyui-menu" style="width:100px;">
				<div iconCls="icon-remove" onclick="delEvents();">移除事件</div>
			</div>
			<div id="updown_menu" class="easyui-menu" style="width:80px;">
				<div  onclick="javascript:moveUp();" >上移</div>
				<div  onclick="javascript:moveDown();" >下移</div>					
			</div>
			
</body>
</html>

