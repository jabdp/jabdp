<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!-- 支持datagrid行的拖动引入 datagrid-dnd.js扩展 -->
<script type="text/javascript">
		var parm_filedIndex${param.index} = 0;
		var f_index${param.index} = -1;
		var f_flag = 0;
			$(function(){
				if("${param.index}"=="99999") {
					$("#table_grid_${param.index}").edatagrid({});
					var data = $.fn.module.getTableFieldsJson("${param.index}");
	    			$("#table_grid_${param.index}").edatagrid("loadData", data);
				} else {
				//var dragrows = [];//拖动的行数据
    			$("#table_grid_${param.index}").edatagrid({
    				/*  toolbar:[ {
        				id:'bt_add_${param.index}',
    					text:'增加',
    					iconCls:'icon-add',
    					handler:function(){
    						$('#tt').datagrid('endEdit', lastIndex);
    						$('#tt').datagrid('appendRow',{
    							colName:'',
    							key:'',
    							dataType:'dtString',
    							size:0,
    							scale:0,
    							notNull:'false',
    							unique:'false'
    						});
    						lastIndex = $('#tt').datagrid('getRows').length-1;
    						$('#tt').datagrid('selectRow', lastIndex);
    						$('#tt').datagrid('beginEdit', lastIndex);  
    						$("#table_grid_" + ${param.index}).edatagrid('addRow');
    						
    					}
    				},'-',  {
    					id:'bt_remove_${param.index}',
    					text:'删除',
    					iconCls:'icon-remove',
    					handler:function(){
    						var row = $('#tt').datagrid('getSelected');
    						if (row){
    							var index = $('#tt').datagrid('getRowIndex', row);
    							$('#tt').datagrid('deleteRow', index);
    						}
    						var rows = $("#table_grid_" + ${param.index}).datagrid('getRows');
    						
    						if(rows.length>1){
    							$("#table_grid_" + ${param.index}).edatagrid('destroyRow');
    						}else{
    							alert("默认保留一行不允许删除,如需改变请双击修改!");
    						}
    						
    					}
    				} ,  '-',{
    					id:'bt_save_${param.index}',
    					text:'保存',
    					iconCls:'icon-save',
    					handler:function(){
    						//$('#tt').datagrid('acceptChanges');
							$("#table_grid_" + ${param.index}).edatagrid('saveRow');	
						
        				}
    				},  '-' ,{
    					id:'bt_undo_${param.index}',
    					text:'撤销修改',
    					iconCls:'icon-undo',
    					handler:function(){
    						//$('#tt').edatagrid('rejectChanges');
    						$("#table_grid_" + ${param.index}).edatagrid('cancelRow');
    						
    					}
    				},'-'], 
    					onBeforeLoad:function(){
    					$(this).datagrid('rejectChanges');
    				},*/
    				frozenColumns : [ [{
    					field : 'ck',
    					checkbox : true
    				}] ],
    				onClickRow:function(rowIndex, rowData){
						if(rowData.index >= 0) {
							$("#table_grid_${param.index}").data("rowData",rowData);
	    					parm_filedIndex${param.index} = rowData.index;
	    					changeFieldProp("${param.index}", rowData.index, rowIndex);
						}
    				},
    				/* onDblClickRow:function(rowIndex, rowData){
    					$("#table_grid_${param.index}").edatagrid("beginEdit",rowIndex);

    				}, */
						onBeforeEdit:function(rowIndex,rowData){
    					if(rowData.key_${param.index}){
    						parm_filedIndex${param.index} = rowData.index;
    					}
    					//$.each(rowData,function(k,v){alert(k+":"+v)});
    				},
    				//添加右键功能
    				onRowContextMenu:function(e, rowIndex, rowData){
    					e.preventDefault();
    					$(mt_id).data("field_index",rowData.index);
    					$("#table_grid_${param.index}").data("rowData",rowData);
    					$("#table_grid_${param.index}").data("rowIndex", rowIndex);
    					$("#table_grid_${param.index}").edatagrid("selectRow",rowIndex);
    					if(rowData.index>=0){
    						changeFieldProp("${param.index}", rowData.index, rowIndex);
    					}
    					$('#mu_${param.index}').menu('show',{
	    	    			left:e.pageX,
	    	    			top:e.pageY
	    	    		});  
	    	    	},
	    	    	onAfterEdit:function(rowIndex, rowData, changes){
    					if($.fn.module.setTableProp("${param.index}",rowData)){
	    					$("#table_grid_${param.index}").edatagrid("refreshRow", rowIndex);
	    					changeFieldProp("${param.index}", rowData.index, rowIndex);
    					}
    				}
    				/* 支持拖动换行位置 */
    				/* dragSelection: true,
    				onLoadSuccess: function(data){
    					console.log($('.datagrid-cell-rownumber'));
    					 if(data.total) {
    						$(this).datagrid('enableDnd');
    					} 
    					$('tr.datagrid-row').draggable({
    						proxy:'clone',
    						
    						onBeforeDrag:function(e){
    							//var draggingRow = getDraggingRow(this);
    							//console.log($(this).text());
    							var table_grid = $("#table_grid_${param.index}");
    							var a = table_grid.edatagrid('selectRow',parseInt($(this).attr("datagrid-row-index")));
    							//var b = table_grid.edatagrid('getSelected');
    							//table_grid.edatagrid('deleteRow',parseInt($(this).attr("datagrid-row-index")));
    							//table_grid   deleteRow
    							//console.log(b);
    							dragrows = table_grid.edatagrid('getSelections');
    							//console.log(dragrows);
    							
    						},
    						onStartDrag:function(e){
    							//$(this).draggable('proxy').hide();
    							$(this).draggable('proxy').css({
									left: -10000,
									top: -10000
								});
    							var table_grid = $("#table_grid_${param.index}");
    							//table_grid.edatagrid('deleteRow',0);
    						},
    						onDrag:function(e){
    							console.log($(this).draggable('proxy'));
    							$(this).draggable('proxy').show();
    							
    						}
    					});
    					$('tr.datagrid-row').droppable({
    						onDrop:function(e){
    							var table_grid = $("#table_grid_${param.index}");
    							var rows = edatagrid('getPanel').panel('panel').find('.datagrid-view1 .datagrid-row-selected');
    							rows.each(function(i){
    								//var row = dragrows[i];
    								//console.log(row);
    								console.log(edatagrid('getPanel').panel('panel').find('.datagrid-view1 .datagrid-row-selected'));
    								
    								var index = $(this).attr("datagrid-row-index");
    								//table_grid.edatagrid('deleteRow',index);
    								table_grid.edatagrid('insertRow',{
    									index:index,
    									row:row
    								}); 
    							});
    						}
    					});
    					$('.datagrid-cell-rownumber').click(
    							//function () {alert();}
    					);
    				},
    				onDrop: function(targetRow,sourceRow,point) {
    					console.log(targetRow);
    				}*/
    			});
				
    			var data = $.fn.module.getTableFieldsJson("${param.index}");
    			$("#table_grid_${param.index}").edatagrid("loadData", data);
    			/* $.each(data.rows,function(k,v){
    				$("#table_grid_${param.index}").edatagrid('beginEdit', k);
				}); */
    			//加载页面时候自动生成一行并设置默认属性
    		 	$("#table_grid_${param.index}").edatagrid("addRow", {"size_${param.index}":"100","dataType_${param.index}":"dtString","scale_${param.index}":"0"});
    			//回车键自动添加一行并设置默认属性
    			//console.log($("input",$("#table_grid_${param.index}").parent()[0]));
    		 	$("input",$("#table_grid_${param.index}").parent()[0]).live('keydown','return',function (evt){
    				var tds = $(evt.target).closest("td[field]");
    				var trs = tds.parent();
    				var index = trs[0].rowIndex;
    				if(tds){
    					var field_id = tds.attr("field").replace(/[^0-9]/ig,"");
    					if(field_id == "${param.index}"){
    						$("#table_grid_${param.index}").edatagrid("addRow", 
    		    				{"size_${param.index}":"100","dataType_${param.index}":"dtString","scale_${param.index}":"0"},index);
    						return false;
    					}
    				}
    			}).live('keyup',function(evt){
    				// console.log("1111111111");
    				evt.preventDefault();
    				findField${param.index}(this);
    			});
    		 	//$(document).keydown(function(e){alert()});
    		 	//console.log($("#table_grid_${param.index}").edatagrid("getPanel").panel("panel").find("input[type='checkbox']").first().keydown(function(){alert()}));
    		 	//$("#table_grid_${param.index}").edatagrid("getPanel").panel("panel").find("input[type='checkbox']")
    		 	$(document).live('keyup',function(evt){
    				$("#table_grid_${param.index}").data("shiftisdowning",false);
    				$("#table_grid_${param.index}").data("ctrlisdowning",false);
    			}).live('keydown','shift+u',function(evt) {//console.log("a");
					var a = $("#table_grid_${param.index}").edatagrid("getSelections");
					if(a.length==1){
						//console.log($("#table_grid_${param.index}").edatagrid('getPanel').panel('panel').find('div.datagrid-view1 tr.datagrid-row-selected'));
						//$(this).attr("datagrid-row-index");
						$("#table_grid_${param.index}").data("rowIndex", $("#table_grid_${param.index}").edatagrid('getPanel').panel('panel').find('div.datagrid-view1 tr.datagrid-row-selected').attr("datagrid-row-index"));
						moveUp${param.index}();
					}else if(a.length>1){
						alert("sorry,快捷键只能选一行移动，多行移动请选右键移动至...");
					}else{
						alert("请在要移动的行前打勾!");
					}
				}).live('keydown','shift+d',function(evt){//console.log($(this));
					var a = $("#table_grid_${param.index}").edatagrid("getSelections");
					if(a.length==1){
						$("#table_grid_${param.index}").data("rowIndex", $("#table_grid_${param.index}").edatagrid('getPanel').panel('panel').find('div.datagrid-view1 tr.datagrid-row-selected').attr("datagrid-row-index"));
						moveDown${param.index}();
					}else if(a.length>1){
						alert("sorry,快捷键只能选一行移动，多行移动请选右键移动至...");
					}else{
						alert("请在要移动的行前打勾!");
					}
				}).live('keydown','shift',function(evt) {
			 		//alert();
			 		//console.log(evt);
			 		//console.log($(this));
					$("#table_grid_${param.index}").data("shiftisdowning",true);
				}).live('keydown','ctrl',function(evt) {
					$("#table_grid_${param.index}").data("ctrlisdowning",true);
				});
    		 	/* $("#table_grid_${param.index}").edatagrid("getPanel").panel("panel").live('keydown','shift',function(evt) {
    		 		alert();
    		 		console.log($(this));
					$("#table_grid_${param.index}").data("shiftisdowning",true);
				}); */
    				//修改好保存的字段后自动再次保存
    			/*   $("#module_table_tab input").live('change',function (evt){
    				  console.log("1111111111");
					var tds = $(this).closest("td[field]");
					var trs = tds.parent();
					var rowId = trs[0].rowIndex;
					 var field = $(tds).attr("field");
					if(field!="dataType_${param.index}"){
						$("#table_grid_${param.index}").edatagrid("endEdit", rowId);
						$("#table_grid_${param.index}").edatagrid("beginEdit", rowId);
					}
				});  
    			  $("#module_table_tab input.combo-text").live('focusin',function (evt){
 				 	var tds = $(this).closest("td[field]");
 					var trs = tds.parent();
 					f_index${param.index} = trs[0].rowIndex;
 			 		});  */
				}
			});
			
			function changeField${param.index}(record){
				if(record){
					if(f_index${param.index} !=-1){
						//alert(f_index);
						setTimeout(function(){$("#table_grid_${param.index}").edatagrid("endEdit",f_index${param.index});
    					$("#table_grid_${param.index}").edatagrid("beginEdit", f_index${param.index});}, 0);
					}
				}
			}
			
			//显示名自动生成字段名方法
		 	function findField${param.index}(obj){
				var tds = $(obj).closest("td[field]");
				var trs = tds.parent();
				var index = trs[0].rowIndex;
				//alert("index:"+index);
				if(tds){
					var field = tds.attr("field");
					var field_id = field.replace(/[^0-9]/ig,"");
					if(field_id == "${param.index}" && field == "caption_${param.index}"){
						var allRow = $("#table_grid_${param.index}").edatagrid("getRows");
						var fieldIndex  = allRow[index].index;
						var fieldProp = $.fn.module.getFieldProp("${param.index}",fieldIndex);
						var colName =$.fn.toPinyin($(obj).val());
						var colTemp ="";
						$.each(colName.split("_"),function(k,v){
							colTemp +=v; 
						});
						var ed = $("#table_grid_${param.index}").edatagrid('getEditor', {'index':index,'field':'colName_${param.index}'});
						if(!fieldProp.key){
							$(ed.target).val($.fn.module.covToDBRule(colTemp));
						}
					}
				}
			} 
    		//添加
    	 	function addRow${param.index}(){
    			//getRow();
    			$("#table_grid_${param.index}").edatagrid("addRow", {"size_${param.index}":"100","dataType_${param.index}":"dtString","scale_${param.index}":"0"});
    		}
    		//删除
    		function delRow${param.index}(){
    			var rows = $("#table_grid_${param.index}").datagrid('getRows');/*返回当前页面所有行*/
    			var selRowsData = $("#table_grid_${param.index}").edatagrid("getSelections");/*	返回所有选中的行，当没有选中的记录时，将返回空数组。*/
    			if(rows.length>1){
    				if(selRowsData.length>0){
	    					var dg = $("#table_grid_${param.index}");
	    					var opts = dg.edatagrid("options");
	    					$.messager.confirm(opts.destroyMsg.confirm.title,opts.destroyMsg.confirm.msg,function(r){
	    						if (r){/*fn(b)：回调函数，当用户点击确认按钮时传递一个 true 参数给函数，否则给它传递一个 false 参数。*/
	    							var selrow = [];
	    							for ( var j = 0; j < selRowsData.length; j++) {
	    								selrow.push(selRowsData[j]);
	    							}
	    							for(var i = 0;i<selrow.length;i++){
	    								//取到行号和列号,content为空
	    								var fieldIndex = selrow[i].index;
	    								var fieldProp = $.fn.module.getFieldProp(${param.index},fieldIndex);/*获得表单行字段属性*/
	    								var tabObj = $.fn.module.getTabList(fieldProp.tabRows,fieldProp.tabCols);/*获得分页tabsList下单个tab属性*/
											var mykey = fieldProp.key;
	    								if(tabObj.layout == "excel" && tabObj.content.html.indexOf("_"+ mykey +"_")>=0) {
	    									alert("请先在excel布局中删除控件[" + fieldProp.caption + "]");
	    									break;
	    								} else {
	    									var row = selrow[i];
												var index = dg.datagrid('getRowIndex', row);
											/*isNewRecord表示是否是新增的字段*/
												if (row.isNewRecord){
													dg.datagrid('cancelEdit', index);
													dg.datagrid('deleteRow', index);
													opts.editIndex = opts.editIndex-1; /*editIndex点击了一行时，为当前点击的行号，从零开始，没有一行被点击，就位当前所有的行数*/
												} else {
													dg.datagrid('cancelEdit', index);
													dg.datagrid('deleteRow', index);
													/*2019/8/1——join*/
													if (isNaN(opts.editIndex)){
														opts.editIndex = rows.length;
													}
													/*2019/8/1——join*/
													opts.editIndex = opts.editIndex-1;
													$.fn.module.deleteRowProp(${param.index},row.index);
												}
											}
											dg.datagrid('clearSelections');
    								}
	    						}
	    					});
    					}
				}else{
					alert("默认保留一行不允许删除,如需改变请单击右键修改!");
				}
    		}
    		//修改
    		function amendRow${param.index}(){
    			var rowIndex = $("#table_grid_${param.index}").data("rowIndex");
    			$("#table_grid_${param.index}").edatagrid("editRow", parseInt(rowIndex));
    			parm_filedIndex${param.index} = $("#table_grid_${param.index}").data("rowData").index;
    		}
    		//撤销修改
    		function undoRow${param.index}(){
    			$("#table_grid_${param.index}").edatagrid('cancelRow');
    		} 
    		//复制行
    		function copyRow${param.index}(){
    			var tableIndex = ${param.index};
    			//var fieldIndex = $(mt_id).data("field_index");
    			var table_grid = $("#table_grid_${param.index}");
    			var selRows = table_grid.edatagrid('getPanel').panel("panel").find("div.datagrid-view2 tr.datagrid-row-selected");
    			var allRows = table_grid.edatagrid('getRows');
    			//var selRowsData = table_grid.edatagrid('getSelections');
    			//console.log(selRowsData);
    			var selRowsData = [];
    			//console.log(selRows);
    			//console.log(allRows);
    			selRows.each(function(i){
    				//console.log(selRows[i]);
    				//console.log($(selRows[i]));
    				var x = parseInt($(this).attr("datagrid-row-index"));
    				//console.log(allRows[x]);
    				selRowsData.push(allRows[x]);
    			});
    			var allData = [];
    			for(var i = 0;i<selRowsData.length;i++){
    				var fieldIndex = selRowsData[i].index;
    				var data = $.fn.module.getFieldProp(tableIndex, fieldIndex);
    				allData.push(data);
    			} 
    			var formKey = $.fn.module.getFormProp(tableIndex).key;
    			if(parent.copyRows && parent.copyFormKey){
   					parent.copyRows(allData);
   					parent.copyFormKey(formKey);
    			}
    		}
    	 	//粘贴行
    	 	function pasteRow${param.index}(){
    	 		if(parent.getPasteRows){   	 	
    	 			var tableIndex = ${param.index};
						var rowObj = parent.getPasteRows();
						var copyFormKey = parent.getFormKey();
						var form = $.fn.module.getFormProp(tableIndex);
						var formKey = form.key;
						//var firstData = $("#table_grid_${param.index}").edatagrid("getSelected");
						var opts = $("#table_grid_${param.index}").edatagrid("options");
						$("#table_grid_${param.index}").edatagrid("endEdit",opts.editIndex);
						var firstData = $("#table_grid_${param.index}").data("rowData");
						$("#table_grid_${param.index}").edatagrid('clearSelections');
						var firstIndex = parseInt($("#table_grid_${param.index}").edatagrid("getRowIndex",firstData));
						var copyFields = [];
						for(var k=0;k<rowObj.length;k++){//组装复制的字段数据
							var _index = $.fn.module.getMaxIndex();
							var copyField  =  $.extend(true,{},rowObj[k]);
							var copyRow = $.fn.module.pasteRow(null,rowObj[k]);
							if(copyFormKey==formKey){
								copyField["dataProperties"]["colName"] = "Copy_"+copyRow['colName'];
								copyField["dataProperties"]["key"] = "copy_"+copyRow['key'];
								copyField["caption"] = "copy_"+copyRow['caption'];
							}else{
								copyField["dataProperties"]["colName"] = copyRow['colName'];
								copyField["dataProperties"]["key"] = copyRow['key'];
								copyField["caption"] = copyRow['caption'];
							}
							copyField["index"] =_index++;
							$.fn.module.setMaxIndex(_index);
							var rowData = $("#table_grid_${param.index}").edatagrid("getSelected");
							var index =null;
							if(rowData){
								index = $("#table_grid_${param.index}").edatagrid("getRowIndex",rowData);
							}else{
								index = $("#table_grid_${param.index}").edatagrid("getRowIndex",firstData);
							}
							var newIndex = parseInt(index);
						  if(copyRow){//datagrid添加复制的字段
			    	 		$("#table_grid_${param.index}").edatagrid("insertRow",{
										index:newIndex,
										row:{
											"size_${param.index}":copyRow['size'],
											"dataType_${param.index}":copyRow['dataType'],
											"scale_${param.index}":copyRow['scale'],
											"colName_${param.index}":copyField["dataProperties"]["colName"],
											"caption_${param.index}":copyField["caption"],
											"key_${param.index}":copyField["dataProperties"]["key"],
											"notNull_${param.index}":copyRow['notNull'],
											"unique_${param.index}":copyRow['unique'],
											"index":copyField["index"]
			    	 				}
		    	 			});
		    	 		}
							copyField.tabCols = 1;
							copyField.tabRows = 1;
							copyFields.push(copyField);
						}
    				var fl=form.fieldList;
    				var startIndex = 0;
    				for(var i = 0 ;i<fl.length;i++){
    					if(firstData["caption_${param.index}"]==""){
    						startIndex = fl.length;
    						break;
    					}else if(fl[i].caption==firstData["caption_${param.index}"]){
    						startIndex = i;
    						break;
    					}
    				}	   		
    				//内存中添加复制的字段
    				for(var j = copyFields.length-1 ;j>=0;j--) {
    					fl.splice(startIndex,0,copyFields[j]);
    				}
    				//console.debug(fl);
    				//showLineInfo(tableIndex,copyField["index"]);
						//$.fn.module.setTableProp("${param.index}",allRow[newIndex]);
					}
    	 	}   
    	 	//上移
    	 	function moveUp${param.index}() {
    	 		var rowIndex = parseInt($("#table_grid_${param.index}").data("rowIndex"));
    	 		/*2019/8/24*/
                $("#table_grid_${param.index}").edatagrid("cancelEdit",rowIndex)
                /*2019/8/24*/
    			if(rowIndex > 0) {
    				var rows = $("#table_grid_${param.index}").edatagrid("getRows");
    				var currentRow = $.extend({}, rows[rowIndex]);
    				var lastRow = $.extend({}, rows[rowIndex-1]);
    				var tableIndex = $(mt_id).data("tableIndex");
    				$.fn.module.swapTableField(tableIndex, lastRow.index, currentRow.index);
    				//改变datagrid列表顺序 
    				$("#table_grid_${param.index}").edatagrid('updateRow', {
    					index : rowIndex - 1,
    					row : currentRow
    				});
    				$("#table_grid_${param.index}").edatagrid('updateRow', {
    					index : rowIndex,
    					row : lastRow
    				});
    			}
    			$("#table_grid_${param.index}").edatagrid('unselectRow', parseInt(rowIndex));
    			$("#table_grid_${param.index}").edatagrid('selectRow', parseInt(rowIndex - 1));
    			/*2019/8/24*/
          $("#table_grid_${param.index}").edatagrid('editRow', parseInt(rowIndex - 1));
    	 	}
    	 	//下移
    	 	function moveDown${param.index}() {
    	 		var rowIndex = parseInt($("#table_grid_${param.index}").data("rowIndex"));
					/*2019/8/24*/
					$("#table_grid_${param.index}").edatagrid("cancelEdit",rowIndex)
					/*2019/8/24*/
    			var rows = $("#table_grid_${param.index}").edatagrid("getRows");
    			if (rowIndex < rows.length - 2) {
    				var currentRow = $.extend({}, rows[rowIndex]);
    				var nextRow = $.extend({}, rows[rowIndex + 1]);
    				var tableIndex = $(mt_id).data("tableIndex");
    				$.fn.module.swapTableField(tableIndex, currentRow.index, nextRow.index);
    				$("#table_grid_${param.index}").edatagrid('updateRow', {
    					index : rowIndex + 1,
    					row : currentRow
    				});
    				$("#table_grid_${param.index}").edatagrid('updateRow', {
    					index : rowIndex,
    					row : nextRow
    				});
    			}
    			$("#table_grid_${param.index}").edatagrid('unselectRow', parseInt(rowIndex));
    			$("#table_grid_${param.index}").edatagrid('selectRow', parseInt(rowIndex + 1));
					/*2019/8/24*/
					$("#table_grid_${param.index}").edatagrid('editRow', parseInt(rowIndex + 1));
    	 	}
    	 	function _changeSize(obj,fieldVal,fieldKey,fieldKey2,tableKey){
    	 		$.fn.module.doChangeSize(obj,fieldVal,fieldKey,fieldKey2,tableKey);
    	 	}
    	 	//插入行
    	 	function insertRow${param.index}(){
					var opts = $("#table_grid_${param.index}").edatagrid("options");
					$("#table_grid_${param.index}").edatagrid("endEdit",opts.editIndex);
				
					var rowData = $("#table_grid_${param.index}").data("rowData");
					var index = $("#table_grid_${param.index}").edatagrid("getRowIndex",rowData);
					var newIndex = parseInt(index);

					$("#table_grid_${param.index}").edatagrid("insertRow",{
						index:newIndex,
						row:{"size_${param.index}":"100","dataType_${param.index}":"dtString","scale_${param.index}":"0"}
					});
				 	opts.editIndex = newIndex;
				 	$("#table_grid_${param.index}").edatagrid("beginEdit",newIndex);
					var allRow = $("#table_grid_${param.index}").edatagrid("getRows");
					//datagrid中添加新插入的字段
					var newField = $.fn.module.setNewField("${param.index}",allRow[newIndex]);
				
					//内存中添加新插入的字段
					var form = $.fn.module.getFormProp(${param.index});
 					var fl=form.fieldList;
 					var startIndex = 0;
 					for(var i=0;i<fl.length;i++){
						if(fl[i].caption==rowData["caption_${param.index}"]){
							startIndex = i;
							break;
						}
					}
 					fl.splice(startIndex,0,newField);
    	 	}
    	 	//Datagrid选中行移动到相应位置
    	 	function moveTo${param.index}(){
    	 		var val = $('#moveto_${param.index}').find('input[type="text"]').val();
    	 		var num = parseInt(val)-1;
    	 		if(!isNaN(val)) {
    	 			var table_grid = $("#table_grid_${param.index}");
    	 			// var opts = table_grid.edatagrid("options");
						var rows = table_grid.edatagrid('getPanel').panel('panel').find('div.datagrid-view1 tr.datagrid-row-selected');
						//选中行字段
						var row = table_grid.edatagrid('getSelections');
						/*2019/8/24*/
						for(var i=0;i<row.length;i++){
							var rowIndex = parseInt(table_grid.edatagrid("getRowIndex",row[i]));
							table_grid.edatagrid("cancelEdit",rowIndex);
            }
            /*2019/8/24*/
						var allrows = table_grid.edatagrid('getRows');
						var c=0;
						//遍历选中行
						rows.each(function(ii) {
							//var row = dragrows[i];
							//console.log(row);
							//console.log(edatagrid('getPanel').panel('panel').find('.datagrid-view1 .datagrid-row-selected'));
							var index = parseInt($(this).attr("datagrid-row-index"));
							//如果选中行在插入行之前，向后交换
							if(index<num) {
								/* console.log(index);
								console.log(num); */
								var tableIndex = $(mt_id).data("tableIndex");
								table_grid.edatagrid('unselectRow', index);
								table_grid.edatagrid('selectRow', num-ii-1);
								for(var i=index-c; i<num-1; i++) {
										var currentRow = $.extend({}, allrows[i]);
										var lastRow = $.extend({}, allrows[i+1]);
										/* console.log(allrows);
										console.log(lastRow.index);
										console.log(currentRow.index);
										console.log(tableIndex); */
										$.fn.module.swapTableField(tableIndex, currentRow.index, lastRow.index);
										table_grid.edatagrid('updateRow', {
											index : i,
											row : lastRow
										});
										table_grid.edatagrid('updateRow', {
											index : i+1,
											row : currentRow
										});
									}
									c++;
							}
							//如果选中行在插入行之后，向前交换
							else if(index>num){
								var tableIndex = $(mt_id).data("tableIndex");
								table_grid.edatagrid('unselectRow', index);
								table_grid.edatagrid('selectRow', num);

									for(var i=index-c; i>num; i--) {
										var currentRow = $.extend({}, allrows[i-1]);
										var lastRow = $.extend({}, allrows[i]);
										/* console.log(allrows);
										console.log(lastRow.index);
										console.log(currentRow.index);
										console.log(tableIndex); */
										$.fn.module.swapTableField(tableIndex, currentRow.index, lastRow.index);
										table_grid.edatagrid('updateRow', {
											index : i-1,
											row : lastRow
										});
										table_grid.edatagrid('updateRow', {
											index : i,
											row : currentRow
										});
									}
									num++;
							}
							//插入至...
							/* table_grid.edatagrid('insertRow',{
								index:num,
								row:row[i]
							}); */
							//删除选中行
							/* table_grid.datagrid('cancelEdit',index);
							table_grid.edatagrid('deleteRow',index); */
							//table_grid.edatagrid('refreshRow',index);
							//table_grid.datagrid('reload');
							//opts.editIndex = opts.editIndex-1;
						});
    	 		} else {
    	 			alert("亲！只能输入数字");
    	 		}
    	 		$('#moveto_${param.index}').dialog('close');
                /*2019/8/24*/
                for(var i=0;i<row.length;i++){
                    var rowIndex = parseInt(table_grid.edatagrid("getRowIndex",row[i]));
                    table_grid.edatagrid("editRow",rowIndex);
                }
                /*2019/8/24*/
    	 	}
    	 	
    	 	//点击取消，直接关闭移动操作对话框
    	 	function moveToClose${param.index}(){
    	 		$('#moveto_${param.index}').dialog('close');
    	 	}
    	 	
    	 	//批量设置公用属性
    	 	function configFields${param.index}(){
    	 		var table_grid = $("#table_grid_${param.index}");
    	 		var fieldIndexes = [];
    	 		var rowIndexes = [];
    	 		var a = table_grid.edatagrid('getSelections');
    	 		//var rows = table_grid.edatagrid('getPanel').panel('panel').find('div.datagrid-view1 tr.datagrid-row-selected');
    	 		//rows.each(function(i) {
    	 			//var index = $(this).attr("datagrid-row-index");
    	 			//console.log(index);
    	 			//console.log(table_grid.edatagrid('getSelection',index));
    	 		//})
    	 		for(var i in a) {
    	 			fieldIndexes.push(a[i].index);
    	 			rowIndexes.push(table_grid.edatagrid('getRowIndex',a[i]));
    	 		}
    	 		changeFieldsProp("${param.index}", fieldIndexes, rowIndexes);
    	 		//console.log(fieldIndexes);
    	 		//console.log(rowIndexes);
    	 	}
</script>
<div class="easyui-layout" fit="true">
	<div region="center" <%-- iconCls="icon-user" title="<s:text name="system.index.workspace.title"/>" --%>style="overflow:hidden;">
		<table id="table_grid_${param.index}" style="width:700px;height:auto" class="cls_table_${param.index} tableClass"
		 rownumbers="true"
			idField="colName_${param.index}" fit="true">
			<thead>
				<tr>
			<%-- 		<th field="colName_${param.index}" width="100" editor="{type:'validatebox',options:{required:true}">字段名</th> --%>
					<th field="caption_${param.index}" width="100" editor="{type:'validatebox',options:{validType:'covCaptionToKey[]'}}">显示名</th>
					<th field="colName_${param.index}" width="100" editor="{type:'validatebox'}">字段名</th>
					<th field="dataType_${param.index}" width="80" formatter="dataTypeFormatter" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:dataTypes,required:true,onSelect:changeField${param.index},onChange:function(newVal,oldVal){if(oldVal && oldVal!=newVal){_changeSize(this,newVal,'size_'+${param.index},'scale_'+${param.index},'table_grid_'+${param.index});}}}}">字段类型</th>
					<%-- <th field="size_${param.index}" width="80" align="right"  editor="{type:'numberbox',options:{min:0,onChange:changeField${param.index}}}">字段长度</th>
					<th field="scale_${param.index}" width="80" align="right" editor="{type:'numberbox',options:{min:0}}">字段精度</th> --%>
					<th field="size_${param.index}" width="80" align="right"  editor="{type:'validatebox'}">字段长度</th>
					<th field="scale_${param.index}" width="80" align="right" editor="{type:'validatebox'}">字段精度</th> 
					<th field="notNull_${param.index}" width="40" align="center" editor="{type:'checkbox',options:{on:'true',off:'false'}}">非空</th>	 
					<th field="unique_${param.index}" width="40" align="center" editor="{type:'checkbox',options:{on:'true',off:'false'}}">唯一</th>
				</tr>
			</thead>
		</table>
	</div>
	<div id="mu_${param.index}" class="easyui-menu" style="width:100px;">
		<div iconCls="icon-up" onclick="moveUp${param.index}();">上移</div>
		<div iconCls="icon-down" onclick="moveDown${param.index}();">下移</div>
		<div onclick="javascript:$('#moveto_${param.index}').dialog('open');">移动至...</div>
		<div iconCls="icon-add" onclick="addRow${param.index}();">新增</div>
		<div iconCls="icon-remove" onclick="delRow${param.index}();">删除</div>
		<div iconCls="icon-edit" onclick="amendRow${param.index}()">修改</div>
		<div iconCls="icon-undo" onclick="undoRow${param.index}();">撤销</div>
		<div onclick="copyRow${param.index}();">复制</div>
		<div onclick="pasteRow${param.index}();">粘贴</div>
		<div onclick="insertRow${param.index}();">插入</div>
		<div onclick="configFields${param.index}();" title='批量修改公用属性'>属性</div>
	</div>
	<div id='moveto_${param.index}' closed="true" class="easyui-dialog" title="移动 " style='width:300px;height:150px'>
		移动至
		<input type='text'></input>
		行字段之前
		<br/>
		可以多选几行一起移动，不过不要确保每一行都是向统一方向移动，移动后上下顺序不变
		<br/>
		<input type='button' value='确定' onclick="moveTo${param.index}();"></input>
		<input type='button' value='取消' onclick="moveToClose${param.index}();"></input>
	</div>
</div>
