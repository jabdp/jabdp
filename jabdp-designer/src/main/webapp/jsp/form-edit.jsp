<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>iDesigner</title>
	<%@ include file="/common/meta.jsp" %>
	<script type="text/javascript" src="${ctx}/js/editor/ckeditor.js"></script>
	<script type="text/javascript" src="${ctx}/js/editorboost/jquery.webkitresize.min.js"></script> 
	<script type="text/javascript" src="${ctx}/js/editorboost/jquery.wysiwyg-resize.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/editor/plugins/tabledrag/redips-drag-source.js"></script>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/themes/blue/easyui-module.css" colorTitle="blue"/>
<script type="text/javascript">
   var titleObj = {};
   var titToCols = {};
   var autoIndex = 1;
   var nullForm = [];
   var nullFieldRows;
   var nullFieldCols;
   var mainTableIndex;
   var newFormIndex;
  // var maxFieldIndex = parent.$.fn.module.getModule().maxIndex; 
   var editProp = {"editType":"" ,"multiple":"false" ,"visible":"true" ,"readOnly":"false" ,"charCase":"normal" ,"defaultValue":"" ,"isPassword":"false" ,"align":"left", "rows":"1" ,"cols":"1", "maxSize":"2000" ,"dtFormat":"" ,"width":"","height":"" ,"top":"" ,"left":""};
   var nullfield = {"caption":"", "key":"" ,"languageText":"" ,"isVirtual":"false" ,"index":"" ,"tabRows":"" ,"tabCols":"","dataProperties":{},"queryProperties":{},"editProperties":editProp};
   $(function(){
	   CKEDITOR.on( 'instanceReady', function( evt ){
			if ($.browser.webkit) {
				    $(".cke_editor iframe")				
				    .webkitimageresize({ afterResize: function(img){
						var imgWidth = $(img).width();
						var imgHeight = $(img).height();
						var tableIndex = $(img).parent().attr("tabIndex");
						var fieldIndex = $(img).parent().attr("id");
						parent.$.fn.module.setEditProp(tableIndex,fieldIndex,{"width":imgWidth,"height":imgHeight});
						parent.showLineInfo(tableIndex,fieldIndex);
			        }}); 
				}else {
					var name = evt.editor.name;
					$("#"+ name).next().find("iframe").wysiwygResize({
				        selector: "img,label",
				        afterResize: function(img){
							var imgWidth = $(img).width();
							var imgHeight = $(img).height();
							var tableIndex = $(img).parent().attr("tabIndex");
							var fieldIndex = $(img).parent().attr("id");
							parent.$.fn.module.setEditProp(tableIndex,fieldIndex,{"width":imgWidth,"height":imgHeight});
							parent.showLineInfo(tableIndex,fieldIndex);
				        }
				    });
				}
		});
	   	var module = parent.$.fn.module.getModule();
	   	var formLength = module.dataSource.formList.length;
	   	var tabsList = module.dataSource.tabsList;
		  for(var i=0;i<tabsList.length;i++){
			  var tabList = tabsList[i].tabList;
			 var rows = tabsList[i].rows;
			 var tabsId = "edit_tabs_"+rows;
			//alert(tabsList.length);
			 $("#edit_page_tabs").append("<div ><div id='"+tabsId+"'  border='false' ></div>");
			
			 initCkeditor(tabsId);
			 
		
			 //根据tabs分组添加对应tab页
			 for(var j=0;j<tabList.length;j++){
				 nullFieldRows = 0;
				 nullFieldCols = 0;
				 var tab = tabList[j];
				 var form = parent.$.fn.module.getFormByKey(tab.form);
				 var cols = form.cols;
				 var rows = form.fieldList.length;
				 titleObj[tab.caption] =  tabsId;
				// titToCols[tab.caption] = tabList[j].cols;
				 if(form){
					 if(form.isMaster){
						 newFormIndex = form.index;
						 mainTableIndex = form.index;
						var editors ="editor"+tabList[j].rows+"_"+tabList[j].cols;
						titToCols[tab.caption] = editors;
						var tableCols = tabList[j].tableCols;
						 if(!tableCols){
							 tableCols = cols;
							 tabList[j].tableCols = cols;
						 }
						 nullFieldRows = tabList[j].rows;
						 nullFieldCols = tabList[j].cols;
						var tabStr = autoCreateTabs(rows,tableCols,form,tab,editors);
						 if(tab.layout=="table" || !tab.layout){	
							 content = "<textarea id='"+editors+"' name='"+editors+"' >"+tabStr+"</textarea>";
						 }else if(tab.layout=="absolute"){
							 var divHt =  tab.height;
							 if(!divHt){
							 	divHt = 200;
							 }
							 content="<div id='"+editors+"' style ='height:"+divHt+"px;'><iframe  src='${ctx}/jsp/absolute.jsp?tRows="+tabList[j].rows+"&tCols="+tabList[j].cols+"&editors="+editors+"' frameborder='0' style='width:100%;height:100%'></iframe></div>";
						 } else if(tab.layout=="excel") {
							 content = "<div class='excelForm' id='" + editors + "' style='"
								+ "padding-top:" + tabList[j].content.paddingTop
								+ "px;padding-bottom:" + tabList[j].content.paddingBottom
								+ "px;padding-left:" + tabList[j].content.paddingLeft
								+ "px;padding-right:" + tabList[j].content.paddingRight
								+ "px' excel='true'>" + tabList[j].content.html + "</div>";
						 }
						 $("#"+tabsId).tabs("add",{
								"title":tab.caption,
								"content":content,
								"cols":tabList[j].cols,
								"rows":tabList[j].rows,
								//打开编辑器界面按钮
								tools:[{
									iconCls:'icon-mini-edit',
									handler:function(){
										var title = $(this).parent().prev().find("span.tabs-title").text();
										var tabId = $(this).parents("div.tabs-container").attr("id");
										var pp = $('#' + tabId).tabs('getTab', title);
										var ppopt = pp.panel("options");
										if(parent.$.fn.module.getTabList(ppopt.rows,ppopt.cols).layout=='excel') {
											window.open("ueditor.jsp?rows=" + ppopt.rows + "&cols=" + ppopt.cols);
										}
										else {
											alert("目前编辑器只对表格布局有效");
										}
									}
								}]
							});
						// autoCreateTabs(rows,cols,form,tab,editors);
					 }else{
						if(form.visible){
						 var dgId = "tab_dg_"+tabList[j].rows+"_"+tabList[j].cols;
						 titToCols[tab.caption] = dgId;			
						 $("#"+tabsId).tabs("add",{
								"title":tab.caption,
								"content":"<div name='" + dgId + "'><table id='" + dgId + "'></table></div>"
							});
						 var tableCols = tabList[j].tableCols;
						 if(!tableCols){
							 tableCols = cols;
							 tabList[j].tableCols = cols;
						 }
						 autoCreateTabs(rows,tableCols,form,tab,dgId);
					 	}
					 }
				 }
				
			 }
			
		 } 
		 parent.$.fn.module.getFormProp(newFormIndex).fieldList = nullForm;//; alert(nullForm);
		 
		 
	  	/* //先在buttonList中添加“报表按钮”	
		 var data = {"caption":'报表',"key":'bt_report'};
		 parent.$.fn.module.setButtonProp(mainTableIndex,'bt_report','editPage',data);
		 //初始化时添加按钮
		 var buttonList = parent.$.fn.module.getEditPage(mainTableIndex).buttonList;
		 for(var i=0;i<buttonList.length;i++){
			 var key = buttonList[i].key;
			 var caption = buttonList[i].caption;
			 if(key=="bt_report"){//如果是报表按钮
				var buttonStr = "<a href='#'  id='bt_report' name='editPage' class='easyui-linkbutton' onclick='parent.showButtonProp(mainTableIndex,this.id,this.name)' plain='true' iconCls='icon-report' title='报表'>报表</a>"; 
			 }else{
			 	var buttonStr = "<a href='#' class='easyui-linkbutton' plain='true' id='"+key+"' name='editPage'  onclick='parent.showButtonProp(mainTableIndex,this.id,this.name)' title='"+caption+"'>"+caption+"</a>";
		 		}
			 $("#editbuttons").append(buttonStr);
		   	 $('#'+key).linkbutton({});
		 } 
	

		 $("a.addBt").live('contextmenu',function(e){
			 e.preventDefault();
			 $("#delete_menu").data("obj",this);
    		 $("#delete_menu").menu('show', {						
					left: e.pageX,
					top: e.pageY
				});
    		 
    		 }); */
		 $("div.excelForm img").live("click",function() {
				var formindex = parseInt($(this).attr("formindex"));
				var fieldindex = parseInt($(this).attr("fieldindex"));
				parent.$.fn.module.showLineInfo(formindex, fieldindex);//form.index;field.index
			});
   });
 
   //初始化ckeditor
   function initCkeditor(tabsId){
	   
		 $("#"+tabsId).tabs({
			   onContextMenu:function(e, title){
				  var tablesId = titleObj[title];
				  var tab = $("#"+tablesId).tabs("getTab",title);
				  $("#"+tablesId).tabs("select",title);
				  $("#edit_page_tabs").data("title",title);
				 // var editorId = tab.find("textarea")[0].name;
				  var editors = tab.find("textarea")[0];
				  if(editors){
					  var editorId = editors.name;
				  
			  		//if(editorId){
			  		//	$("#edit_page_tabs").data("title",title);
						e.preventDefault();
						$("#del_tabs").menu('show', {						
							left: e.pageX,
							top: e.pageY
						});
					}else{
			  		//	$("#edit_page_tabs").data("title",title);
						e.preventDefault();
			  			$("#grid_menu").menu('show', {						
							left: e.pageX,
							top: e.pageY
						});
					}
			 } , 
			 onSelect:function(title){  
				 var tablesId = titleObj[title];
				 var editOrtabId = titToCols[title];
				 var tableRows =tablesId.replace(/[^0-9]/ig, "");
				// var tableArr = tablesId.split("_");
				// var tableRows = tableArr[tableArr.length-1];
				 var tab = $("#"+tablesId).tabs("getTab",title);
				   var colsArr = editOrtabId.split("_");
				   var cols = colsArr[colsArr.length-1];
				 if(tab.find("textarea")[0]){
						var editor = CKEDITOR.instances[editOrtabId];
					   if(!editor) {
						 	CKEDITOR.replace( editOrtabId, {
								extraPlugins : 'tabledrag',
								width: 800,
								height:600
							});
					   } else {
							initTableDrag("drag", editor.window.$);
					   } 
					  // var colsArr = editOrtabId.split("_");
					  // var cols = editOrtabId.charAt(editOrtabId.length-1);
					 //  var cols = colsArr[colsArr.length-1];
					   parent.changeLayoutProp(tableRows,cols);
				    }else if(tab.find("iframe")[0]){
				    	 parent.changeLayoutProp(tableRows,cols);
				    }else if(tab.find("div[excel]")[0]) {
						parent.changeLayoutProp(tableRows,cols);
				    }else{
				 	    // var cols =  editOrtabId.charAt(editOrtabId.length-1);
						 parent.changeLayoutProp(tableRows,cols,true);
				    }  
				 }  
			 
		 });
		
   }
   //复制主表元素到新的对象中
   function copyFiledToNewForm(data){
		if(data.index){
			nullForm.push(data);   
		}
   }
   //新增占位符到fieldList
   function addFields(fieldIndex,newArry){
	   var startIndex = 0;
	   for(var i = 0 ;i<nullForm.length;i++){
			if(nullForm[i].index==fieldIndex){
				startIndex = i;
			}
		}	
	   for(var j = newArry.length-1 ;j>=0;j--){
		   nullForm.splice(startIndex,0,newArry[j]);
		}	
   }
   //创建新tabs分组
   function createNewTabsGroup(){
	  var tabsList =  parent.$.fn.module.getModule().dataSource.tabsList;
	  var rows = 0;
	 /*  for(var i=0;i<tabsList.length;i++){
		  rows = tabsList[i].rows>rows?tabsList[i].rows:rows;
	  } */
	  $.each(tabsList,function(k,v){
		  rows = v.rows>rows?v.rows:rows;
	  });
	  rows =rows+1;
	  var tabsId = "edit_tabs_"+rows;
	  parent.$.fn.module.addTabsList(rows);
	   $("#edit_page_tabs").append("<div><div id='"+tabsId+"'  border='false' ></div></div>");
	 // $("#"+tabsId).tabs({}); 
	   initCkeditor(tabsId);
	 
   }
   
   //删除tabs分组
   function deleteTabsGroup(obj){
	   var titleId = $(obj).closest("div[id]").attr("id");
	   var rows = titleId.replace(/[^0-9]/ig, "");
	   var selectTab = $("#"+titleId).tabs("getSelected");
	   if(selectTab){
		   alert("请先删除所有的表格后再删除！");
	   }else{
		   $.messager.confirm('警告', 'tabs分组一经删除不能恢复，确定删除？', function(r){
				if (r){
					 var flag = parent.$.fn.module.deleteTabsList(rows);
					  if(flag){
						  $("#"+titleId).remove();
					  }
				}
			});
		 
	   }
	  
   }
   //表格左移
   function moveTabLeft(){
	  var title =  $("#edit_page_tabs").data("title");
	  var tabsListId = titleObj[title];
	   var tabListId = titToCols[title];
	   var rows = tabsListId.replace(/[^0-9]/ig, "");
	   var colsArr = tabListId.split("_");
	   var cols = colsArr[colsArr.length-1];
	  // alert(rows+"  : "+cols);
	   var flag = parent.$.fn.module.moveTabPlace(rows,cols,"left");
	   if(!flag){
		   alert("已经是边界位置不能实现该操作!");
	   }else{
		   parent.doRefreshTab('编辑页面设计','${ctx}/jsp/edit.jsp', true);
	   }
	  // alert('${ctx}');
	  
   }
   //表格右移
   function moveTabRight(){
	   	   var title =  $("#edit_page_tabs").data("title");
		   var tabsListId = titleObj[title];
		   var tabListId = titToCols[title];
		   var rows = tabsListId.replace(/[^0-9]/ig, "");
		   var colsArr = tabListId.split("_");
		   var cols = colsArr[colsArr.length-1];
		  // alert(rows+"  : "+cols);
		   var flag = parent.$.fn.module.moveTabPlace(rows,cols,"right");
		   if(!flag){
			   alert("已经是边界位置不能实现该操作!");
		   }else{
			   parent.doRefreshTab('编辑页面设计','${ctx}/jsp/edit.jsp', true);
		   }
		  
   }
   //表格上移
   function moveTabUp(){
	   var title =  $("#edit_page_tabs").data("title");
	   var tabsListId = titleObj[title];
	   var tabListId = titToCols[title];
	   var rows = tabsListId.replace(/[^0-9]/ig, "");
	   var colsArr = tabListId.split("_");
	   var cols = colsArr[colsArr.length-1];
	   var flag = parent.$.fn.module.moveTabUpDown(rows,cols,"up");
	   if(!flag){
		   alert("已经是边界位置不能实现该操作!");
	   } else{
		  parent.doRefreshTab('编辑页面设计','${ctx}/jsp/edit.jsp', true);
	   }
	  
   }
   //表格下移
   function moveTabDown(){
	   var title =  $("#edit_page_tabs").data("title");
	   var tabsListId = titleObj[title];
	   var tabListId = titToCols[title];
	   var rows = tabsListId.replace(/[^0-9]/ig, "");
	   var colsArr = tabListId.split("_");
	   var cols = colsArr[colsArr.length-1];
	   var flag = parent.$.fn.module.moveTabUpDown(rows,cols,"down");
	   if(!flag){
		   alert("已经是边界位置不能实现该操作!");
	   }else{
		   parent.doRefreshTab('编辑页面设计','${ctx}/jsp/edit.jsp', true);
	   }
	  
   }
   /**
   *根据form生成对应的表格
   *@param rows 生成的表格的行数
   *@param cols 生成表格的列数
   *@param form 表格一个表格数据
   *@param tab 单个布局tab属性
   *@param tabId 生成的tab或者datagrid的id
   *@returns null*/
   function autoCreateTabs(rows,cols,form,tab,tabId){
	   	str = "";
		col_Count=0;
		row_Count=0;
		arr =[];
		for(var i=0;i<rows;i++){
			arr[i] = new Array();
			for(var j=0;j<cols;j++){
			arr[i][j]=0;
			}}
		 var data = form.fieldList;
		 if(form.isMaster){
			 for(var k=0;k<rows;k++){
					arr[k] = new Array();
					for(var v=0;v<cols;v++){
					arr[k][v]=0;
				}} 
				str="";
				col_Count = 0;
				row_Count = 0; 
				
				for(var m=0;m<data.length;m++){
					if(data[m].tabRows==tab.rows && data[m].tabCols==tab.cols){
						createTab(data[m],cols,form.index);
					}
					if(m == data.length-1){											
						var num =cols-col_Count;
						if(num != cols){
							var _index = parent.$.fn.module.getMaxIndex();
							for(var n=0;n<num;n++){												
								var datajson = {"caption":"","key":"","languageText":"","isVirtual":null,"index":_index++,"tabRows":tab.rows,"tabCols":tab.cols,"dataProperties":{"dataType":"dtString","size":50,"scale":0,"key":"CeShi","colName":"CeShi","notNull":false,"unique":false},"editProperties":{"editType":"","multiple":true,"visible":true,"readOnly":false,"charCase":"normal","defaultValue":"","isPassword":false,"align":"left","rows":1,"cols":1,"maxSize":2000,"dtFormat":"","formula":null,"width":null,"height":null,"top":null,"left":null},"queryProperties":{
									"align":"left",
									"width":"80",
									"showInGrid":false,
									"showInSearch":false,
									"dafaultValue":"",
									"sortable":false,
									"filterable":"",
									"showInTreeSearch":false
							}};
								parent.$.fn.module.setMaxIndex(_index);								
								createTab(datajson,cols,form.index);
							}
						}
						
					}
					
				}
				//$("#"+tabId).html("<div id='drag'><table id='ck_tab"+tab.cols+"' border='0' cellspacing='0' cellpadding='0' style='width:100%;height:100%;border-left: 1px solid #99BBE8; border-bottom: 1px solid #99BBE8;'>"+str+"</table></div>");
				return "<div id='drag'><table id='ck_tab"+tab.cols+"' border='0' cellspacing='0' cellpadding='0' style='width:100%;height:100%;'>"+str+"</table></div>"
		 }else{
			 	var field = {};
				var col=[[]];
				var dataObj = {};
				for(var i=0;i<form.fieldList.length;i++){
					field = form.fieldList[i];
					if(field.queryProperties.showInGrid) {
						col[0].push({field:field.key,width:field.queryProperties.width,title:field.caption});
						var value = "<div onclick='parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' tabIndex="+form.index+" id="+field.index+"><lable>"+field.dataProperties.colName+"</lable></div>"
						dataObj[field.key] = value;
					}
				}
				var hei = form.height || 330;
				$('#'+tabId).edatagrid({
					//width: 800,
					//fitColumns : true,
					nowrap : true,
					height: hei,
					columns:col
				});
				$('#'+tabId).edatagrid("addRow",dataObj); 
		 }
   }
  
   /**
   */
   function initTableDrag(dragId, win){
	   var	rd = REDIPS.drag;
		// initialization
		rd.init("drag", win);
		// set hover color
		rd.hover.color_td = '#E7C7B2';
		// set drop option to 'shift'
		rd.drop_option = 'switch';
		// set shift mode to vertical2
		rd.shift_option = 'vertical2';
		// enable animation on shifted elements
		rd.animation_shift = true;
		// set animation loop pause
		rd.animation_pause = 20;
   }
 //创建新tab页
	var arr =[]; 
	var str;
	var col_Count;
	var row_Count; 
   function createTabs(obj){
	   var cols = 0;
	   var tab={};
	  // alert(obj.id);
	 /*  var totleId= $(obj).closest("div[id]").attr("id");
	  var titleId =$("#"+totleId).parents("div[id]").attr("id"); */
	  var titleId = $(obj).closest("div[id]").attr("id");
	  var rows = titleId.replace(/[^0-9]/ig, "");
	// var rowArr = titleId.split("_");
	// var cols = rowArr[]
	  // var tabsList = parent.$.fn.module.getTabsList();
	   var tabList = parent.$.fn.module.getTabsList(rows).tabList;
	   var MasterKey = "";
		 var form = parent.$.fn.module.getModule().dataSource.formList;
	 	$.each(form,function(k,v){
	 		if(v.isMaster){
	 			MasterKey = v.key;
	 		}
	 	});
	   if(tabList){
		   for(var i =0;i<tabList.length;i++){
				   cols = cols>tabList[i].cols?cols:tabList[i].cols;
		   }
	   }
	  
	  	cols =cols+1;
	  	var autoId = rows+""+cols;
		tab = $.extend({},{"rows":rows,"cols":cols,"form":MasterKey,"caption":"补充信息"+autoId,"languageText":""});
		var editId = "editor"+rows+"_"+cols;
		titToCols['补充信息'+autoId] = editId;
		titleObj['补充信息'+autoId] = titleId;
	   parent.$.fn.module.setTabList(tab); 
	   $('#'+titleId).tabs('add',{  
		    title:'补充信息'+autoId,  
		    content:"<textarea id="+editId+" name="+editId+" ><div id='drag'><table table id='ck_tab"+cols+"' border='0' cellspacing='0' cellpadding='0' style='width:100%;height:100% ;'></table></div></textarea>"  
		}); 
	 //  autoIndex++;
	   /*var editors = "editor"+cols;
	    CKEDITOR.replace( editors, {
			extraPlugins : 'tableresize'
		});  */
	   
   }
   /**
   	*判断ckeditor内容是否为空来执行删除tabs分页方法
   */
   function delele_tabs(){
	   var title = $("#edit_page_tabs").data("title");
	   var tableId = titleObj[title];
	   var tab = $("#"+tableId).tabs("getTab",title);
	   var editorId = $(tab[0]).find("textarea")[0].name;
	   var isEmpty = true;
	   var editor=CKEDITOR.instances[editorId];
	   if(editor) {
		   var imgs = $(editor.document.$).find("img");
		   var labels = $(editor.document.$).find("label");
		   isEmpty = (imgs.length == 0) && (labels.length == 0);
	   }	
	   var rows = tableId.replace(/[^0-9]/ig, "");
	 // var rowsArr = tableId.split("_");
	 // var rows = rowsArr[rowsArr.length-1];
	   //var cols = editorId.charAt(editorId.length-1);
	   var colsArr = editorId.split("_");
	   var cols = colsArr[colsArr.length-1];
	   if(isEmpty){
		  // alert("功能研发中.......");
		   $("#"+tableId).tabs("close",title);
		   delete titToCols[title];
		   delete titleObj[title];
		   parent.$.fn.module.deleteTabList(rows,cols);
	   }else{
		   $.messager.alert("提示","tab分页中还存在内容请移动到其他tab分页中再删除!",'warning');
	   } 
	   
   }
   /**
   *动态修改表头
   */
   function changeTitle(oldTitle,newTitle){
	   var titValue = titleObj[oldTitle];
	   var gridValue = titToCols[oldTitle];
	   delete titleObj[oldTitle];
	   delete titToCols[oldTitle];
	   titleObj[newTitle] = titValue;
	   titToCols[newTitle] = gridValue;
		var tab = $("#"+titValue).tabs('getSelected');
		tab.panel("setTitle", newTitle);
		$("#"+titValue).find("li.tabs-selected span.tabs-title").text(newTitle);
	}
   /**
   	*根据传过来的数据自动生成相应的表格 
   	*@param rows 表格行数
   	*@param cols 表格列数
   	*@param isMaster 是否是主表
   	*@param form 所有表的值
   	*@param id 
   	*@param tabList  布局格式表格属性
   	*@returns null
   */
   function autoCreateTab(rows,cols,isMaster,form,id,tabsList){
	  	str = "";
		col_Count=0;
		row_Count=0;
	//	 arr = new Array();
		arr =[];
		for(var i=0;i<rows;i++){
			arr[i] = new Array();
			for(var j=0;j<cols;j++){
			arr[i][j]=0;
			}}
	   var data = form.fieldList;
	   //把生成的tab放到指定的div下面
		if(isMaster){
			if(tabsList && tabsList.length>0){
				for(var i=0;i<tabsList.length;i++){
					if(tabsList[i].rows=="1"){
						var tabList = tabsList[i].tabList;
						for(var j=0;j<tabList.length;j++){
						//	初始化布局数组及布局变量
							for(var k=0;k<rows;k++){
								arr[k] = new Array();
								for(var v=0;v<cols;v++){
								arr[k][v]=0;
									}} 
								str="";
								col_Count = 0;
								row_Count = 0; 
							if(tabList[j].cols=="1"){		//主要信息展示生成
								for(var m=0;m<data.length;m++){
									if(data[m].tabRows=="1" && data[m].tabCols=="1"){
										createTab(data[m],cols,form.index);
										if(m==data.length-1){
											var datajson = {"caption":"","key":"","languageText":"","isVirtual":null,"index":"","tabRows":1,"tabCols":1,"dataProperties":{"dataType":"dtString","size":50,"scale":0,"key":"CeShi","colName":"CeShi","notNull":false,"unique":false},"editProperties":{"editType":"","multiple":true,"visible":true,"readOnly":false,"charCase":"normal","defaultValue":"","isPassword":false,"align":"left","rows":1,"cols":1,"maxSize":2000,"dtFormat":"","formula":null,"width":null,"height":null,"top":null,"left":null},"queryProperties":""};
											var num =cols-col_Count;
											if(num!=cols){
												for(var n=0;n<num;n++){
													createTab(datajson,cols,form.index);
												}
											}
											
										}
									}
									
								}
								var editors = "editor"+tabList[j].cols;
								$('#edit_tabs').tabs('add',{  
								     title:tabList[j].caption,  
								 	 content:"<textarea id='"+editors+"' name='"+editors+"' ></textarea>"
								 	
								    
								} ); 
								$("#"+editors).html("<div id='drag'><table id='ck_tab' border='0' cellspacing='0' cellpadding='0' style='width:100%;height:100% ;'>"+str+"</table></div>");	
								
								
								/*  CKEDITOR.replace( editors, {
									extraPlugins : 'tabledrag,tableresize'
								}); */
								 //初始化布局数组
								/*   for(var k=0;k<rows;k++){
										arr[k] = new Array();
										for(var v=0;v<cols;v++){
										arr[k][v]=0;
										}} 
								 str=""; */
							}else{				//补充信息展示生成
							//	alert(str);
								 var editors = "editor"+tabList[j].cols;
								//	alert(cols);
								 for(var m=0;m<data.length;m++){
										if(data[m].tabRows=="1" && data[m].tabCols==j+1){
											createTab(data[m],cols,form.index);
											if(m==data.length-1){
												var datajson = {"caption":"","key":"","languageText":"","isVirtual":null,"index":"","tabRows":1,"tabCols":j+1,"dataProperties":{"dataType":"dtString","size":50,"scale":0,"key":"CeShi","colName":"CeShi","notNull":false,"unique":false},"editProperties":{"editType":"","multiple":true,"visible":true,"readOnly":false,"charCase":"normal","defaultValue":"","isPassword":false,"align":"left","rows":1,"cols":1,"maxSize":2000,"dtFormat":"","formula":null,"width":null,"height":null,"top":null,"left":null},"queryProperties":""};
												var num =cols-col_Count;
												if(num!=cols){
													for(var n=0;n<num;n++){
														createTab(datajson,cols,form.index);
													}
												}
											}
										}
									}
								$('#edit_tabs').tabs('add',{  
									   title:tabList[j].caption,    
									   content:"<textarea id='"+editors+"' name='"+editors+"' ></textarea>"
									}); 
								$("#"+editors).html("<div id='drag'><table id='ck_tab"+tabList[j].cols+"' border='0' cellspacing='0' cellpadding='0' style='width:100%;height:100%;border-left: 1px solid #99BBE8; border-bottom: 1px solid #99BBE8;'>"+str+"</table></div>");

								/* //初始化布局数组								
								 for(var k=0;k<rows;k++){
									arr[k] = new Array();
									for(var v=0;v<cols;v++){
									arr[k][v]=0;
									}} 
								 str="";  */
								
							  
							}
						}
					}
					
				}
				
			}else{
				$('#edit_tabs').tabs('add',{  
				    title:form.caption,  
				 	 content:"<textarea id='editor' name='editor' ></textarea>"
				    
				} );
				for(var i=0;i<data.length;i++){
						createTab(data[i],cols,form.index);
					}
					$("#editor").html("<div id='drag'><table style='width:100%;height:100%;'>"+str+"</table></div>");	
					
			}		
		}else{
			//var tabDiv = document.createElement("div");
			//var table=document.createElement("table");
			//tabDiv.setAttribute("id","tabsDIV"+id);
			//tabDiv.setAttribute("title","子表信息"+id);
			var field = {};
			var col=[[]];
			//table.setAttribute("id","tab"+form.index);
			var dataObj = {};
			for(var i=0;i<form.fieldList.length;i++){
				field = form.fieldList[i];
				if(field.queryProperties.showInGrid) {
					col[0].push({field:field.key,width:field.queryProperties.width,title:field.caption});
					var value = "<div onclick='parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' tabIndex="+form.index+" id="+field.index+"><lable>"+field.dataProperties.colName+"</lable></div>"
					dataObj[field.key] = value;
				}
			}
			var dgId = "tab_dg_"+form.index;
			$('#childTabInfo').tabs('add',{
			     title:form.caption,  
			 	 content:"<table id='" + dgId + "'></table>"
			});
			//document.getElementById("childTabInfo").appendChild(tabDiv);
			//document.getElementById(tabDiv.id).appendChild(table); 
			var hei = form.height || 330;
			$('#'+dgId).edatagrid({
				//width: 800,
				//fitColumns : true,
				nowrap : true,
				height: hei,
				columns:col
			});
			$('#'+dgId).edatagrid("addRow",dataObj);
			 
		 
		}
   }
   
   /**
   *根据传入字段生成对应表格
   *@param field	字段
   *@param  cols 	占用列
   *@param tabId    表编号
   *@returns str表格字符串*/
   function createTab(field,cols,tabId){
			if(field.editProperties.visible===true || field.editProperties.visible == "true"){
				var row_temp = parseInt(field.editProperties.rows);
				var col_temp = parseInt(field.editProperties.cols);
				if(arr[row_Count][col_Count]=="0"){
					 var flag = -1;
					 for(var j =col_Count;j<cols;j++){
						if(arr[row_Count][j]=="1"){
							flag = j-col_Count;
							break;
						}
					}
					if(flag == "-1"){
						if(col_Count==0){
							if(field.key){
								str +="<tr><td style='width:33%;' rowspan="+row_temp+" colspan="+col_temp+">"+covInputType(field,tabId);+"</td>";
								copyFiledToNewForm(field);
							}else{
								str +="<tr><td style='width:33%;' rowspan="+row_temp+" colspan="+col_temp+">"+covInputType(field,tabId);+"</td>";
								copyFiledToNewForm(field);
							}
						//	var row_num = parseInt(row_Count)+parseInt(row_temp);
						//	var col_num = parseInt(col_Count)+parseInt(col_temp);
							for(var r=row_Count;r<row_Count+row_temp;r++){
								for(var c=col_Count;c<col_Count+col_temp;c++){	
										arr[r][c]=1;
								}
							}
							col_Count += col_temp;
							if(col_Count>=cols){
								col_Count = 0;
								row_Count++;
								str +="</tr>";
							}
						}else{
							if(col_Count+col_temp>cols){
							//	alert(col_Count+col_temp);
								for(var i=0;i<col_Count+col_temp-cols;i++){
									if(field.key){
										/*  nullFieldRows;
										   var nullFieldCols;
										   var maxFieldIndex */
										var maxFieldIndex = parseInt(parent.$.fn.module.getMaxIndex());
										var newField = $.extend({},nullfield,{"index":maxFieldIndex,"tabRows":nullFieldRows,"tabCols":nullFieldCols});
										str +="<td style='width:33%'>"+covInputType(newField,tabId);+"</td>";
										maxFieldIndex++;
										parent.$.fn.module.setMaxIndex(maxFieldIndex);
										copyFiledToNewForm(newField);
									}else{
										var maxFieldIndex = parseInt(parent.$.fn.module.getMaxIndex());
										var newField = $.extend({},nullfield,{"index":maxFieldIndex,"tabRows":nullFieldRows,"tabCols":nullFieldCols});
										str +="<td style='width:33%'>"+covInputType(newField,tabId);+"</td>";
										maxFieldIndex++;
										parent.$.fn.module.setMaxIndex(maxFieldIndex);
										copyFiledToNewForm(newField);
									}
								}
								str +="</tr>";
								col_Count=0;
								row_Count++;
								createTab(field,cols,tabId); 
							}else if(col_Count+col_temp==cols){
								if(field.key){
									str +="<td style='width:33%' rowspan="+row_temp+" colspan="+col_temp+">"+covInputType(field,tabId);+"</td>";
									copyFiledToNewForm(field);
								}else{
									str +="<td style='width:33%' rowspan="+row_temp+" colspan="+col_temp+">"+covInputType(field,tabId);+"</td>";
									copyFiledToNewForm(field);
								}
								for(var r=row_Count;r<row_Count+row_temp;r++){
									for(var c=col_Count;c<col_Count+col_temp;c++){	
											arr[r][c]=1;
									}
								}
								col_Count = 0;
								row_Count++;
								str +="</tr><tr>";
								
							}else{
								if(field.key){
									str +="<td style='width:33%' rowspan="+row_temp+" colspan="+col_temp+">"+covInputType(field,tabId);+"</td>";
									copyFiledToNewForm(field);
								}else{
									str +="<td style='width:33%' rowspan="+row_temp+" colspan="+col_temp+">"+covInputType(field,tabId);+"</td>";
									copyFiledToNewForm(field);
								}
								for(var r=row_Count;r<row_Count+row_temp;r++){
									for(var c=col_Count;c<col_Count+col_temp;c++){	
											arr[r][c]=1;
									}
								}
								 col_Count += col_temp;
								 //if(col_Count)
								
							}
						
						}
					 } else{
						if(flag>=col_temp){
							if(field.key){
								str +="<td style='width:33%'>"+covInputType(field,tabId)+"</td>";
								copyFiledToNewForm(field);
							}else{
								str +="<td style='width:33%'>"+covInputType(field,tabId)+"</td>";
								copyFiledToNewForm(field);
							}
						} else {
							str +="<td style='width:33%'></td>";
							createTab(field,cols,tabId); 
						}
						col_Count++;
						if(col_Count>=cols){
							col_Count = 0;
							row_Count++;
							str +="</tr>";
						}
						
					}  
						
				}else {
					col_Count++;
					if(col_Count>=cols){
						str +="</tr>";
						row_Count++;
						col_Count=0;
					}
					createTab(field,cols,tabId); 
				}
		} else {
			copyFiledToNewForm(field);
		}
   }
   
   
   
   /**
   *拖动控件改变控件大小
   */
  /*  function resizeImage(evt,id){
	   newX=evt.x;
	   newY=evt.y;
	 //  alert(id);
	   var tab = $("#edit_tabs").tabs("getSelected");
	   var editorId = $(tab[0]).find("textarea")[0].name;
	   var editor=CKEDITOR.instances[editorId];
	 //  alert(editor);
	   if(editor) {
		   var imgs = $(editor.document.$).children("#"+id);
		  // $(imgs).css({"width":newX+"px","height":newY+"px"});
	   }	
	 //  $("#"+id).css({"width":newX+"px","height":newY+"px"});
	   }
   */
   /**
   *拖动结束后设置控件属性
   */
   
  /* function resizeImageEnd(cellObj,divObj){
	   newX=$(cellObj).width();
	   newY=$(cellObj).height();
	  // alert($(divObj).attr("id")+":"+$(divObj).attr("tabIndex"));
	  // alert(newX+":"+newY);
	 } */
	
	//点击行显示其详细信息
	  /*  function showLineInfo(tableIndex,fieldIndex){
		  data = $.fn.module.getFieldPropJson(tableIndex, fieldIndex);
		  $("#module_table_propGrid").propertygrid("loadData",data);
	   }  */  
   /**
   	*把不同的编辑类型转化为不同的标签
    *@param data tabId  字段  表编号
    *@returns tdstr 单元格内元素*/
   function covInputType(data,tabId){
	   return parent.$.fn.module.coverCellType(data, tabId);
   }
   
	   
	   
	   //编辑界面添加按钮
	   function addbutton(caption,key){
		   var data = {"caption":caption,"key":key};
	  		parent.$.fn.module.setButtonProp(mainTableIndex,key,'editPage',data);
		   var buttonStr = "<a href='#' class='addBt' plain='true' id='"+key+"' name='editPage'  onclick='parent.showButtonProp(mainTableIndex,this.id,this.name)' title='"+caption+"'>"+caption+"</a>";
	   		$("#editbuttons").append(buttonStr);
	   		$('#'+key).linkbutton({});
	   }
	   //编辑界面删除按钮
	     function deletebutton(){
	    	 var buttonObj = $("#delete_menu").data("obj");
	    	 var key = $(buttonObj).attr("id");
	    	 var name = $(buttonObj).attr("title");
	    	 var data = {"caption":name,"key":key};
	    	 parent.$.fn.module.deleteButton(mainTableIndex,key,'editPage');    	 
	    	 $('#'+key).remove();
	    }
	   function addMenu(mainKey,jsonData){
		  if( $("#menu"+mainKey).length != 0){
			  $("#menu"+mainKey).remove();
		  }
			var menu=$("<div id='menu"+mainKey+"' style='width:100px;'></div>");
  			var len = jsonData.length;
  			var div = [];
  				for(var i=0;i<len;i++){
  					div.push('<div id="');
  					div.push(jsonData[i].key);
  					div.push('">');
  					div.push(jsonData[i].caption);
  					div.push('</div>');
  				}
  		menu.append(div.join(""));
  		menu.appendTo("body");
  		$('#'+mainKey).html("");	
		$('#'+mainKey).menubutton({   //('#'+key,win.document.body)查找win的页面中的key元素
			menu: '#menu'+mainKey,
			text :"报表"
		}); 
		  
	   }
	   function changeButtonType(key){	   
		   var bt = parent.$.fn.module.getButtonProp(mainTableIndex,key,'editPage');
		   bt.buttonType="normal";
		   bt.buttonList= [];
		   parent.$.fn.module.setButtonProp(mainTableIndex,key,'editPage',bt);
		   $('#'+key).html("");	
		   $("#menu"+key).remove();
			$('#'+key).linkbutton({ 
				text : bt.caption
			}); 
	   }
function replaceTabs(tabObject,type){
	var rows = tabObject.rows;
	var cols = tabObject.cols; 
	var editors = "editor"+rows+"_"+cols;
	var formKey = tabObject.form;
	var tableCols = tabObject.tableCols;
	var tabsId = "edit_tabs_"+rows;
	var tab = $("#"+tabsId).tabs("getSelected");
	 if(type=="table"){						
		 var form = parent.$.fn.module.getFormByKey(formKey);
		 var rowNum = form.fieldList.length;
		 nullForm = [];
		 var tabStr = autoCreateTabs(rowNum,tableCols,form,tabObject,editors);
		 content ="<textarea id='"+editors+"' name='"+editors+"' >"+tabStr+"</textarea>";
		 $("#"+editors).replaceWith(content);
		 $("#"+editors).hide();
		 var editor = CKEDITOR.instances[editors];
			if(!editor) {
				 	CKEDITOR.replace( editors, {
						extraPlugins : 'tabledrag',
						width: 800,
						height:600
					});
			   } else {
				   $("#cke_"+editors).show();
					//initTableDrag("drag", editor.window.$);			
			 } 
	 }else if(type=="absolute"){
		var divHt =  tabObject.height;
		if(!divHt){
			divHt = 300;
		}
		 content="<div id='"+editors+"' style ='height:"+divHt+"px;'><iframe  src='${ctx}/jsp/absolute.jsp?tRows="+rows+"&tCols="+cols+"&editors="+editors+"' frameborder='0' style='width:100%;height:100%'></iframe></div>"
		 $("#"+editors).replaceWith(content);
		 $("#cke_"+editors).hide();
	 } else if(type=="excel"){
			var x = parent.$.fn.module.getTabList(rows,cols);
			if(x.content) {
				if(!x.content.html) {
					x.content = {html:""
						,paddingTop:0
						,paddingBottom:0
						,paddingLeft:0
						,paddingRight:0};
				}
			} else {
				x.content = {html:""
				,paddingTop:0
				,paddingBottom:0
				,paddingLeft:0
				,paddingRight:0};
			}
			html = "<div class='excelForm' id='" + editors + "' style='"
			+ "padding-top:" + x.content.paddingTop
			+ "px;padding-bottom:" + x.content.paddingBottom
			+ "px;padding-left:" + x.content.paddingLeft
			+ "px;padding-right:" + x.content.paddingRight
			+ "px' excel='true'>" + x.content.html + "</div>";
			$("#"+editors).replaceWith(html);
			$("#cke_"+editors).hide();
		}
	/*  $("#"+tabsId).tabs("update",{
			tab: tab,
			options:{
				"content":content
			}
	}); */

}
function changeHeight(tId,height){
	$("#"+tId).css("height",height);
} 
   </script>
</head> 
<body class="easyui-layout">
<!-- 	<div region="north" style="height:32px;overflow:hidden;" border="false">
		<div id="editbuttons" style="height:28px;padding:2px;background:#E0ECFF;border-bottom:1px solid #8DB2E3;border-top: 1px solid #8DB2E3;">
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-cancel"  title="关闭">关闭</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-first"  title="第一条">第一条</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-prev" title="上一条">上一条</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-next" title="下一条">下一条</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-last" title="最后一条">最后一条</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add" title="新增">新增</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-edit" title="修改">修改</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-copy" title="复制">复制</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-attach" title="附件">附件</a>
		</div>	
	</div> -->
	<div region="center" id="edit_page_tabs" border="false"> 
			
		<!-- <div>
			<div id="edit_tabs"  border="false"  tools="#tab-tool">
			</div> 
		</div>
		 <div width="100%">
			<div id="childTabInfo"   border="false">
			</div> 
		</div> -->
	</div>
	<!-- <div region="south" split="true" style="height:100px" >  
		<div id="childTabInfo" fit="true" border="false">
		</div> 
	</div> --> 

<div id="delete_menu" class="easyui-menu" style="width:100px;" border="false">
		<div onclick="deletebutton();">删除按钮</div>
</div>
<div id="del_tabs" class="easyui-menu" style="width:110px;" border="false">
		
		<div onclick="moveTabLeft();">左移tab分页</div>
		<div onclick="moveTabRight();">右移tab分页</div>
		<div onclick="moveTabUp();">上移tab分页</div>
		<div onclick="moveTabDown();">下移tab分页</div>
		<div onclick="delele_tabs();">删除tab分页</div>
</div>
<div id="grid_menu" class="easyui-menu" style="width:110px;" border="false">
		<div onclick="moveTabLeft();">左移tab分页</div>
		<div onclick="moveTabRight();">右移tab分页</div>
		<div onclick="moveTabUp();">上移tab分页</div>
		<div onclick="moveTabDown();">下移tab分页</div>
</div>
<div id="tab-tool" border="false" >
		<a href="#" onclick="createTabs(this)" class="easyui-linkbutton" plain="true" iconCls="icon-add" title="新增tab"></a>
</div>
</body>
</html>