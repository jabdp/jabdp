/*
 * iDesigner Module
 * Copyright 2012, GaoXJ
 */

$(function($) {
    var _MAX_FIELD_LEN_ = 64;
    var _index = 1;
    var _systemField = ["ID","VERSION","CREATE_USER","CREATE_TIME","LAST_UPDATE_USER","LAST_UPDATE_TIME","STATUS","SORT_ORDER_NO","ATM_ID","MASTER_ID","FLOW_INS_ID","CANCEL_REASON","AUTO_CODE","AUTO_PARENT_CODE","ENTITY_NAME","ENTITY_ID","STOCK_RATIO","DATA_SHARE_LOG"];
    var _systemFieldToSqlParamMap = {"ID":"id","MASTER_ID":"masterId","STATUS":"status","CREATE_TIME":"createTime","VERSION":"versions","CREATE_USER":"createUser","DATA_SHARE_LOG":"dataShareLog"};
    var _dataStatus_ = [
        {"key":"status0","caption":"所有","selected":true},
        {"key":"status1","caption":"草稿"},
        {"key":"status3","caption":"审批/审核通过"},
        {"key":"status13","caption":"草稿+审批/审核通过"}
    ];
    var _editTypeImgDic = {
        "TextBox":"TextBox.jpg",
        "RichTextBox":"RichTextBox.jpg",
        "ComboBox":"ComboBox.jpg",
        "ComboBoxSearch":"ComboBox.jpg",
        "ComboTree":"ComboBox.jpg",
        "DictTree":"ComboBox.jpg",
        "ComboGrid":"ComboBox.jpg",
        "DateBox":"DateBox.jpg",
        "RadioBox":"RadioBox.jpg",
        "CheckBox":"CheckBox.jpg",
        "ComboRadioBox":"ComboBox.jpg",
        "ComboCheckBox":"ComboBox.jpg",
        "Button":"Button.jpg",
        "HyperLink":"HyperLink.jpg",
        "ImageBox":"ImageBox.jpg",
        "BrowserBox":"BrowserBox.jpg",
        "DataGrid":"DataGrid.jpg",
        "TreeGrid":"TreeGrid.jpg",
        "Tree":"Tree.jpg",
        "Label":"Label.jpg",
        "PasswordBox":"PasswordBox.jpg",
        "ProgressBar":"ProgressBar.jpg",
        "zTree":"zTree.jpg",
        "Div":"div.jpg",
        "DialogueWindow":"DialogueWindow.jpg"
    };
    var _dataTypeDic = [
        {"key":"dtString","caption":"字符串"},
        {"key":"dtLong","caption":"整数"},
        {"key":"dtDouble","caption":"小数"},
        {"key":"dtDateTime","caption":"时间"},
        {"key":"dtText","caption":"文本"},
        {"key":"dtImage","caption":"图片"}
    ], _editTypeDic = [
        {"key":"TextBox","caption":"TextBox"},
        {"key":"RichTextBox","caption":"RichTextBox"},
        {"key":"ComboBox","caption":"ComboBox"},
        {"key":"ComboBoxSearch","caption":"ComboBoxSearch"},
        {"key":"ComboTree","caption":"ComboTree"},
        {"key":"DictTree","caption":"DictTree"},
        {"key":"ComboGrid","caption":"ComboGrid"},
        {"key":"DateBox","caption":"DateBox"},
        {"key":"RadioBox","caption":"RadioBox"},
        {"key":"CheckBox","caption":"CheckBox"},
        {"key":"ComboRadioBox","caption":"ComboRadioBox"},
        {"key":"ComboCheckBox","caption":"ComboCheckBox"},
        {"key":"Button","caption":"Button"},
        {"key":"HyperLink","caption":"HyperLink"},
        {"key":"ImageBox","caption":"ImageBox"},
        {"key":"BrowserBox","caption":"BrowserBox"},
        {"key":"DataGrid","caption":"DataGrid"},
        //{"key":"TreeGrid","caption":"TreeGrid"},
        {"key":"Div","caption":"Div"},
        {"key":"Tree","caption":"Tree"},
        {"key":"zTree","caption":"zTree"},
        {"key":"Label","caption":"Label"},
        {"key":"PasswordBox","caption":"PasswordBox"},
        {"key":"ProgressBar","caption":"ProgressBar"},
        {"key":"DialogueWindow","caption":"DialogueWindow"}
    ], _charCaseDic = [
        {"key":"normal","caption":"普通"},
        {"key":"upperCase","caption":"大写"},
        {"key":"lowerCase","caption":"小写"}
    ], _alignDic = [
        {"key":"left","caption":"左对齐"},
        {"key":"center","caption":"居中对齐"},
        {"key":"right","caption":"右对齐"}
    ],_buttonType = [
        {"key":"normal","caption":"普通按钮"},
        {"key":"menu","caption":"下拉菜单按钮"}
    ],_order = [
        {"key":"total","caption":"求和"},
        {"key":"average","caption":"求平均值"},
        {"key":"max","caption":"求最大值"},
        {"key":"min","caption":"求最小值"},
        {"key":"other","caption":"自定义规则"}
    ],_checkBoxDic = {
        "on":true,
        "off":false
    },_flagData = [{key:'sqlWhere',caption:"查询条件"},
        {key:'setValue',caption:"设置参数"},
        {key:'groupBy',caption:"分组查询"},
        {key:'orderBy',caption:"排序方式"}
    ],_orderByData = [{key:'ASC',caption:"升序"},
        {key:'DESC',caption:"降序"},
        {key:'',caption:"空值"},
        {key:'hidden',caption:"隐藏"},
        {key:'disquery',caption:"不作为对话窗查询条件"}
    ],_langType =[{key:'zh_CN',caption:'简体中文'},
        {key:'zh_TW',caption:'繁体中文'},
        {key:'en',caption:'英语'},
        {key:'ar',caption:'阿拉伯语'}
    ],_listTypeDic = [
        {"key":"DataGrid","caption":"DataGrid"},
        {"key":"TreeGrid","caption":"TreeGrid"},
        {"key":"Form","caption":"表单Form"}
    ];


    /**
     * 设置sql语句查询时连接条件*/
    var _sqlQueryWhere = [	/*{key:"=",caption:"="},
	 {key:"<>",caption:"<>"},
	 {key:"<",caption:"<"},
	 {key:"<=",caption:"<="},
	 {key:">",caption:">"},
	 {key:">=",caption:">="},
	 {key:"BETWEEN",caption:"BETWEEN"},
	 {key:"IN",caption:"IN"},
	 {key:"NOT IN",caption:"NOT IN"},
	 {key:"LIKE",caption:"LIKE"},
	 {key:"IS NULL",caption:"IS NULL"},
	 {key:"IS NOT NULL",caption:"IS NOT NULL"}*/
        {"key":'EQ',"caption":'等于'},
        {"key":'NE',"caption":'不等于'},
        {"key":'LT',"caption":'小于'},
        {"key":'LE',"caption":'小于等于'},
        {"key":'GT',"caption":'大于'},
        {"key":'GE',"caption":'大于等于'},
        {"key":'IN',"caption":'匹配多个值'},
        {"key":'LIKE',"caption":'模糊匹配'},
        {"key":'LIKEL',"caption":'开始于'},
        {"key":'LIKER',"caption":'结束于'},
        {"key":'ISNULL',"caption":'为空'},
        {"key":'ISNOTNULL',"caption":'不为空'},
        {"key":'FULLTEXT',"caption":'全文检索'},
        {"key":'NONE',"caption":'不检索'}
    ];

    var _rule = {
        "caption":"",
        "key":"",
        "type":"",
        "sql":null
    };
    var _column = {
        "source":"",
        "dest":"",
        "rule":""
    },_table = {
        "source":"",
        "dest":"",
        "sourceModule":"",
        "destModule":"",
        "columnList":[]
    },_selectInfo = {
        "tableItemList" : [],
        "whereItemList" :[]
    };
    //设置moduleProperties及其子节点属性
    var _ownerProperties = {
        "creator":"",
        "createTime":new Date().format(),
        //	"createTime":"",
        "lastModifiedBy":"",
        "lastModifiedTime":""
    }, _moduleProperties = {
        "caption":"",
        "key":"",
        "type":"module",
        "languageText":"",
        "version":"1.0.1",
        "relevanceModule":"",
        "ownerProperties":_ownerProperties
    };
    //设置dataProperties节点属性
    var _dataProperties={
        "dataType":"",
        "size":"",
        "scale":"",
        "colName":"",
        "key":"",
        "notNull":"",
        "unique":""
    };
    //设置queryProperties节点属性
    var _queryProperties={
        "align":"left",
        "width":"80",
        "showInGrid":false,
        "showInSearch":false,
        "dafaultValue":"",
        "sortable":false,
        "filterable":false,
        "showInTreeSearch":false,
        "showInTabSearch":false, //作为tab标签页查询
        "useAsShareUser":false, //作为共享用户属性
        "useAsAuthField":false,//作为权限控制字段
        "frozenColumn":false, //冻结列
        "mergeable":false //是否合并相同值的单元格
        ,"showInCommonSearch":false //作为常用查询条件
    };
    //设置tab属性
    var _tab={
        "rows":1,
        "cols":1,
        "form":"",
        "caption":"主要信息",
        "languageText":"",
        "content":{},//存放Excel布局html
        "tableCols":3,
        "layout":"table"//默认表格布局
    },_tabs={
        "rows":1,
        "tabList":[]
    };
    var _queryList={
        "type":"",
        "key":"",
        "formula":[]
    };

    var _editPage ={
        "buttonList":[],
        "eventList":[],
        "stsList":[]
    };

    var _event ={
        "key":"",
        "caption":"",
        "params":"",
        "content":""
    },_button = {
        "key":"",
        "caption":"",
        "languageText":"",
        "iconCls":"",
        "width":"",
        "height":"",
        "enableAuthCheck":false, //开启权限检查
        "eventList":[],
        "i18nKey":"",
        "buttonType":"normal", //normal--普通按钮；menu--下拉菜单按钮
        "buttonList":[]
    },_queryPage = {
        "buttonList":[],
        "eventList":[],
        "stsList":[],
        "querySql":null
    },_extension={
        "queryPage":_queryPage,
        "editPage":_editPage
    };
    //设置dataSource及其子节点属性
    var _itemProp={
        "key":"",
        "caption":"",
        "column":"",
        "order":"",
        "operator":"",
        "value":"",
        "columntype":"",
        "width":""
    },_sqlProp={
        "key":"",
        "caption":"",
        "content":"",
        "dataStatus":"",//数据状态
        "sqlSelect":"",
        "table":null,
        "isCheckAuth":false,//检查数据权限
        "isUseCache":false,//是否使用缓存
        "entityName":"",//实体名称
        "sqlWhereItemList":[],
        "setValueItemList":[],
        "groupByItemList":[],
        "orderByItemList":[],
        "selectInfo" : null
    },_procProp={
        "key":"",
        "caption":"",
        "content":""
    },_formulaProp={
        "type":"",
        "sql":null,
        "itemList":[],
        "content":"",
        "refKey":"",//查询数据源
        "refQsKey":""//编辑数据源
    }, _editProperties={
        "editType":"TextBox",
        "multiple":false,
        "visible":true,
        "visibleCondition" :"",
        "readOnly":false,
        "charCase":"normal",
        "defaultValue":"",
        "isPassword":false,
        "align":"left",
        "rows":"1",
        "cols":"1",
        "layer":"1",
        "rowspan":"1",
        "colspan":"1",
        "maxSize":"2000",
        "dtFormat":"",
        "minDate":null,
        "maxDate":null,
        "formulaInfo":"无内容",
        "eventList":[],
        "formula":null,
        "layoutTab":"",
        "width":"",
        "height":"",
        "left":"",
        "top":"",
        "expCode":"",
        "sqlList":[],
        "autoIdRule":"",
        "quickAdd":false,
        "limit":"",//数据源查询限制记录数
        "fieldSuffix":"",//字段后缀显示，放在控件之后，用于表示单位属性等
        "autoCollapse":false, //树形控件，是否自动折叠属性
        "validInView":false, //查看状态下是否可操作
        "autoComplete":false, //自动完成
        "uniqueFilterMaster":false, //子表唯一性过滤主表记录
        "style":"",//样式内容
        "className":"" //样式名称
    },_fieldProp = {
        "index":0,
        "tabRows":1,
        "tabCols":1,
        "caption":"",
        "key":"",
        "entityName":"",
        "syncEntityName":"",
        "syncKey":"",
        "fromEntityName":"",
        "dynamicProp":_dynamicProp,
        "languageText":"",
        "isVirtual":false,
        "isCaption":false,
        "enableImport":false,
        "enableExport":false,
        "checkImportUnique":false,
        "dataProperties":null,
        "editProperties":null,
        "queryProperties":null,
        "enableFieldAuthCheck":false, //开启字段数据权限
        "enableRevise":false //开启字段修订功能
    },_formProp={
        "flag":true,
        "index":0,
        "caption":"",
        "key":"",
        "tableName":"",
        "languageText":"",
        "visible":true,
        "visibleCondition" :"",
        "readOnly":true,
        "cols":"5",
        "height":"",
        "tabIndex":"1",
        "isMaster":false,
        "entityName":"",
        "lcKey":"",
        "fieldList":[],
        "queryType":"",
        "queryList":[],
        "extension":_extension,
        "isVirtual":false,
        "isListType":false,
        "listType":"",
        "treeField":"",
        "isCommon":false,
        "isReferCommon":false,
        "dynamicShow":false,
        "formType":"",
        "stockType":"",
        "queryStyle":"leftshow",
        "inOutVal":"",
        "enableImport":false,
        "enableExport":false,
        "showTools":false,
        "referEntityTableName":"",
        "enableFieldAuthCheck":false, //开启字段数据权限
        "showStatusInTab":false, //系统状态作为Tab查询
        "isHideStatus":false, //隐藏系统状态
        "pageSizeList":null,//显示记录数范围
        "pageSize":null,//默认显示记录数
        "sortName":null, //默认排序字段
        "sortOrder":null, //默认排序方式
        "isShowCreateUser":true, //所有者
        "isShowCreateTime":true, //创建时间
        "isShowLastUpdateUser":false, //最后修改者
        "isShowLastUpdateTime":false, //最后修改时间
        "enablePageSelect":false //开启分页选择模式
    },_dataSource={
        "formList":[],
        "tabsList":[]
    },_flow={
        "key":"",
        "caption":""
    };
    //设置主节点属性
    var _moduleProp = {
        "moduleProperties":_moduleProperties,
        "dataSource":_dataSource,
        "maxIndex":0,
        "flowList":[],
        "ruleList":[]
    };
    //设置动态显示属性
    var _dynamicProp ={
        "dynamicTable":false,
        "dynamicTableItem":_itemProp,
        "dynamicColumn":false,
        "dynamicColumnItems" :[]
    };
    var x=1,y=1;
    //var _newSqlProp = {"total":1,"rows":{"flag":_flagData,"key":"","caption":"","column":"","orderBy":_orderByData}};
    var _layoutData = [];
    //字段属性
    var _fieldPropJson = {"total":10,"rows":[
        {"code":"caption","name":"显示名","value":"","group":"基本属性","editor":"text"},
        {"code":"key","name":"属性名","value":"","group":"基本属性","editor":"label"},
        {"code":"languageText","name":"国际化设置","value":"","group":"基本属性","editor":"label"},
        {"code":"isVirtual","name":"虚拟字段","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"isCaption","name":"表头字段","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"entityName","name":"关联实体名","value":"","group":"基本属性","editor":"label"},
        {"code":"syncEntityName","name":"同步实体名","value":"","group":"基本属性","editor":"label"},
        {"code":"syncKey","name":"同步属性名","value":"","group":"基本属性","editor":"label"},
        {"code":"fromEntityName","name":"来源实体名","value":"","group":"基本属性","editor":"label"},
        {"code":"dynamicProp","name":"动态显示设置","value":"","group":"基本属性","editor":"label"},
        {"code":"enableFieldAuthCheck","name":"字段权限启用","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"enableRevise","name":"字段修订启用","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"colName","name":"字段名","value":"","group":"数据属性","editor":"text"},
        {"code":"dataType","name":"数据类型","value":"dtString","group":"数据属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption","data":_dataTypeDic
            }
        }},
        {"code":"size","name":"长度","value":"100","group":"数据属性","editor":"numberbox"},
        {"code":"scale","name":"精度","value":"0","group":"数据属性","editor":"numberbox"},
        {"code":"notNull","name":"非空","value":"false","group":"数据属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"unique","name":"唯一","value":"false","group":"数据属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"editType","name":"编辑类型","value":"TextBox","group":"编辑属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption","data":_editTypeDic
            }
        }},
        {"code":"multiple","name":"允许多选","value":"false","group":"编辑属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"visible","name":"可见","value":"true","group":"编辑属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"visibleCondition","name":"可见条件","value":"","group":"编辑属性","editor":"text"},
        {"code":"readOnly","name":"只读","value":"false","group":"编辑属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"validInView","name":"查看状态可操作","value":"false","group":"编辑属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"autoComplete","name":"自动完成","value":"false","group":"编辑属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"uniqueFilterMaster","name":"过滤主表唯一","value":"false","group":"编辑属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"charCase","name":"大小写","value":"normal","group":"编辑属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption",
                "data":_charCaseDic
            }
        }},
		/*{"code":"isPassword","name":"密码格式","value":"false","group":"编辑属性","editor":{
		 "type":"checkbox",
		 "options":_checkBoxDic
		 }},*/
        {"code":"align","name":"内容对齐方式","value":"left","group":"编辑属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption",
                "data":_alignDic
            }
        }},
        {"code":"style","name":"样式内容","value":"","group":"编辑属性","editor":"label"},
        {"code":"className","name":"样式名称","value":"","group":"编辑属性","editor":"text"},
        {"code":"rows","name":"占用行数","value":"1","group":"编辑属性","editor":"numberbox"},
        {"code":"cols","name":"占用列数","value":"1","group":"编辑属性","editor":"numberbox"},
        {"code":"layer","name":"表头层级","value":"1","group":"编辑属性","editor":"numberbox"},
        {"code":"rowspan","name":"表头跨行数","value":"1","group":"编辑属性","editor":"numberbox"},
        {"code":"colspan","name":"表头跨列数","value":"1","group":"编辑属性","editor":"numberbox"},
//	                                    	{"code":"maxSize","name":"上传大小限制(MB)","value":"2","group":"编辑属性","editor":"numberbox"},
        {"code":"dtFormat","name":"显示格式","value":"","group":"编辑属性","editor":"text"},
        {"code":"minDate","name":"最小日期","group":"编辑属性","editor":"text"},
        {"code":"maxDate","name":"最大日期","group":"编辑属性","editor":"text"},
        {"code":"defaultValue","name":"默认值","value":"","group":"编辑属性","editor":"text"},
        {"code":"autoIdRule","name":"自动编号规则","value":"","group":"编辑属性","editor":"text"},
        {"code":"fieldSuffix","name":"字段后缀","value":"","group":"编辑属性","editor":"text"},
        {"code":"formulaInfo","name":"数据源设置","value":"无内容","group":"编辑属性","editor":"label"},
        {"code":"limit","name":"限定记录数","value":"","group":"编辑属性","editor":"numberbox"},
        {"code":"quickAdd","name":"快速添加数据","value":"false","group":"编辑属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"autoCollapse","name":"树形自动折叠","value":"false","group":"编辑属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"expCode","name":"公式代码","value":"","group":"编辑属性","editor":"label"},
        {"code":"sqlList","name":"sql编辑","value":"","group":"编辑属性","editor":"label"},
        {"code":"eventList","name":"事件编辑","value":"","group":"编辑属性","editor":"label"},
        {"code":"layoutTab","name":"分组归属","value":"","group":"编辑属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption",
                "data":_layoutData
            }
        }},

        {"code":"width","name":"宽度","value":"0","group":"编辑属性","editor":"numberbox"},
        {"code":"height","name":"高度","value":"0","group":"编辑属性","editor":"numberbox"},
        {"code":"top","name":"上边距","value":"0","group":"编辑属性","editor":"numberbox"},
        {"code":"left","name":"左边距","value":"0","group":"编辑属性","editor":"numberbox"},

        {"code":"showInGrid","name":"列表显示","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"align","name":"表格内对齐方式","value":"left","group":"查询属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption",
                "data":_alignDic
            }
        }},
        {"code":"sortable","name":"是否排序字段","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"filterable","name":"是否页面过滤","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"width","name":"列宽","value":"80","group":"查询属性","editor":"numberbox"},
        {"code":"showInTreeSearch","name":"作为树形查询","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"showInTabSearch","name":"作为标签页查询","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"showInSearch","name":"作为查询条件","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"showInCommonSearch","name":"作为常用查询条件","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"useAsShareUser","name":"作为共享用户属性","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"useAsAuthField","name":"作为权限控制字段","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"frozenColumn","name":"作为冻结列","value":"false","group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"defaultValue","name":"查询默认值","value":"","group":"查询属性","editor":"text"},
        {"code":"mergeable","name":"合并相同值的单元格","value":false,"group":"查询属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }}
    ]};
    //表单属性
    var _formPropJson={"total":10,"index":0,"rows":[
        {"code":"caption","name":"显示名","value":"","group":"基本属性","editor":"text"},
        {"code":"tableName","name":"表名","value":"","group":"基本属性","editor":"text"},
        {"code":"key","name":"属性名","value":"","group":"基本属性","editor":"label"},
        {"code":"languageText","name":"国际化设置","value":"","group":"基本属性","editor":"label"},
        {"code":"visible","name":"可见","value":"true","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"visibleCondition","name":"可见条件","value":"","group":"基本属性","editor":"text"},
        {"code":"readOnly","name":"只读","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"cols","name":"列数","value":"","group":"基本属性","editor":"numberbox"},
        {"code":"height","name":"列表高度","value":"","group":"基本属性","editor":"numberbox"},
        {"code":"showTools","name":"显示工具条","value":"","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"tabIndex","name":"tab索引","value":"","group":"基本属性","editor":"numberbox"},
        {"code":"isMaster","name":"主表","value":"","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"isVirtual","name":"是否虚拟表","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"isListType","name":"是否列表显示","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"listType","name":"列表展示类型","value":"TextBox","group":"基本属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption","data":_listTypeDic
            }
        }},
        {"code":"enablePageSelect","name":"启用分页选择","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"treeField","name":"树形字段属性","value":"","group":"基本属性","editor":"text"},
        {"code":"isCommon","name":"公用表","value":"","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"isReferCommon","name":"关联公用表","value":"","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"formType","name":"公用表类型","value":"","group":"基本属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption","data":[{"key":" ","caption":"通用"},{"key":"stock","caption":"库存"}]
            }
        }},
        {"code":"stockType","name":"库存类型","value":"","group":"基本属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption","data":[{"key":" ","caption":"无"},{"key":"-1","caption":"出库"},{"key":"1","caption":"入库"},{"key":"0","caption":"出入库"}]
            }
        }},
        {"code":"inOutVal","name":"出入库默认值","value":"","group":"基本属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption","data":[{"key":" ","caption":"无"},{"key":"-1","caption":"出库"},{"key":"1","caption":"入库"}]
            }
        }},
        {"code":"dynamicShow","name":"动态显示","value":"","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"referEntityTableName","name":"关联实体表名","value":"","group":"基本属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption","data":[]
            }
        }},
        {"code":"enableFieldAuthCheck","name":"字段权限启用","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"showStatusInTab","name":"状态作为标签查询","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"isHideStatus","name":"隐藏系统状态","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"pageSizeList","name":"显示记录数范围","value":"","group":"基本属性","editor":"text"},
        {"code":"pageSize","name":"默认显示记录数","value":"","group":"基本属性","editor":"text"},
        {"code":"sortName","name":"默认排序字段","value":"","group":"基本属性","editor":"text"},
        {"code":"sortOrder","name":"默认排序方式","value":"","group":"基本属性","editor":"text"},
        {"code":"isShowCreateUser","name":"显示所有者","value":"true","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"isShowCreateTime","name":"显示创建时间","value":"true","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"isShowLastUpdateUser","name":"显示最后修改者","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"isShowLastUpdateTime","name":"显示最后修改时间","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"queryStyle","name":"查询样式","value":"leftshow","group":"基本属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption","data":[{"key":"blank","caption":"不显示"},{"key":"leftshow","caption":"左边显示"},{"key":"topshow","caption":"上边显示"}]
            }
        }},
		/*{"code":"parentTable","name":"父表key","value":"","group":"基本属性","editor":{
		 "type":"combobox",
		 "options":{
		 "valueField":"key","textField":"caption","data":[{"key":"MainTable","caption":"MainTable"}]
		 }
		 }},*/
        //{"code":"queryType","name":"查询方式","value":"","group":"基本属性","editor":"label"},
        {"code":"eventList","name":"事件编辑","value":"","group":"编辑属性","editor":"label"},
        //{"code":"stsList","name":"汇总统计","value":"","group":"编辑属性","editor":"label"},
        {"code":"eventList","name":"事件编辑","value":"","group":"查询属性","editor":"label"},
        {"code":"stsList","name":"汇总统计","value":"","group":"查询属性","editor":"label"},
        {"code":"querySql","name":"自定义查询","value":"","group":"查询属性","editor":"label"},
        {"code":"buttonList","name":"自定义按钮","value":"","group":"查询属性","editor":"label"}
    ]};
    //模块属性
    var _modulePropJson={"total":8,"index":0,"rows":[
        {"code":"caption","name":"模块名称","value":"","group":"基本属性","editor":{
            "type":"text",
            options:{"required":true}
        }},
        {"code":"key","name":"属性名","value":"","group":"基本属性","editor":"label"},
        {"code":"version","name":"版本","value":"","group":"基本属性","editor":"label"},
        {"code":"languageText","name":"国际化设置","value":"","group":"基本属性","editor":"label"},
        {"code":"relevanceModule","name":"关联模块属性名","value":"","group":"基本属性","editor":{
            "type":"text",
            options:{"required":true}
        }},
        {"code":"creator","name":"创建者","value":"gxj","group":"基本属性","editor":"label"},
        {"code":"createTime","name":"创建时间","value":"2012-05-25 15:00:00","group":"基本属性","editor":"label"},
        {"code":"lastModifiedBy","name":"最后修改者","value":"","group":"基本属性","editor":"label"},
        {"code":"lastModifiedTime","name":"最后修改时间","value":"","group":"基本属性","editor":"label"},
        {"code":"type","name":"模块类型","value":"","group":"基本属性","editor":"label"},
        {"code":"setImportExport","name":"导入导出设置","value":"设置","group":"基本属性","editor":"label"},
        {"code":"allLanguageText","name":"国际化设置","value":"","group":"基本属性","editor":"label"}
    ]};
    //布局属性
    var _tabPropJson ={"total":4,"rows":[
        {"code":"rows","name":"tabs分组","value":"","group":"基本属性","editor":"label"},
        {"code":"cols","name":"tab分组","value":"","group":"基本属性","editor":"label"},
        {"code":"form","name":"对应表key","value":"","group":"基本属性","editor":"label"},
        {"code":"tableCols","name":"列数","value":"","group":"基本属性","editor":"numberbox"},
        {"code":"caption","name":"显示名","value":"","group":"基本属性","editor":"text"},
        {"code":"languageText","name":"国际化设置","value":"","group":"基本属性","editor":"label"},
        {"code":"layout","name":"布局类型","value":"","group":"基本属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption","data":[{"key":"table","caption":"表单布局"},{"key":"absolute","caption":"绝对布局"},{"key":"excel","caption":"excel布局"}]
            }
        }},
        {"code":"height","name":"绝对定位高度","value":"","group":"基本属性","editor":"text"},
        {"code":"isShowInQueryList","name":"是否在查询列表显示","value":"","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }}
    ]};

    //子表布局属性
    var _tabChildPropJson ={"total":6,"rows":[
        {"code":"rows","name":"tabs分组","value":"","group":"基本属性","editor":"label"},
        {"code":"cols","name":"tab分组","value":"","group":"基本属性","editor":"label"},
        {"code":"form","name":"对应表key","value":"","group":"基本属性","editor":"label"},
        {"code":"tableCols","name":"列数","value":"","group":"基本属性","editor":"numberbox"},
        {"code":"caption","name":"显示名","value":"","group":"基本属性","editor":"text"},
        {"code":"languageText","name":"国际化设置","value":"","group":"基本属性","editor":"label"}
    ]};
    //添加按钮属性
    var _buttonPropJson = {"total":8,"rows":[
        {"code":"key","name":"按钮key","value":"","group":"基本属性","editor":"label"},
        {"code":"caption","name":"显示名","value":"","group":"基本属性","editor":"text"},
        {"code":"languageText","name":"国际化设置","value":"","group":"基本属性","editor":"label"},
        {"code":"iconCls","name":"按钮图片","value":"","group":"基本属性",
            "editor":{
                "type":"combobox",
                "options":{
                    "valueField":'id',
                    "textField":'text',
                    "url":"",
                    "formatter":function(row){
                        return "<span style='width: 16px;height:16px;display:inline-block;vertical-align:middle;' class='"+row.id+"'></span><span class='item-text'>"+row.text+"</span>";
                    }

                }
            }},
        {"code":"enableAuthCheck","name":"开启权限检查","value":"false","group":"基本属性","editor":{
            "type":"checkbox",
            "options":_checkBoxDic
        }},
        {"code":"width","name":"宽度","value":"","group":"基本属性","editor":"numberbox"},
        {"code":"height","name":"高度","value":"","group":"基本属性","editor":"numberbox"},
        {"code":"eventList","name":"事件编辑","value":"","group":"基本属性","editor":"label"},
        {"code":"buttonType","name":"按钮类型","value":"","group":"基本属性","editor":{
            "type":"combobox",
            "options":{
                "valueField":"key","textField":"caption",
                "data":_buttonType
            }
        }
        },
        {"code":"buttonList","name":"添加菜单","value":"","group":"基本属性","editor":"label"}

    ]};
    var convertBooleanToString = function(val) {
        if(typeof(val) == "boolean") {
            //console.log("log:" + new String(val));
            if(val) {
                return "true";
            } else {
                return "false";
            }
        } else {
            return val;
        }
    };
	/*	var copyRowData;
	 var copyTabJson;*/

    //把表格里面的值赋值给js
    $.fn.module={
        /**
         * 获得整个模块*/
        getModule:function() {
            _moduleProp["maxIndex"] = _index;
            return _moduleProp;
        },
        /**
         * 设置整个模块*/
        setModule:function(module) {
            $.extend(_moduleProp, module);
            _index = _moduleProp["maxIndex"];

        },
        /**
         * 获得最大值*/
        getMaxIndex:function(){
            return $.fn.module.getModule().maxIndex;
        },
        /**
         * 设置最大值*/
        setMaxIndex:function(index){
            _index = index;
            _moduleProp.maxIndex = index;

        },

        /**
         * 获得模块属性*/
        getModuleProp:function() {
            return _moduleProp.moduleProperties;
        },
        /**
         * 获得流程属性*/
        getFlowsProp:function() {
            var flows = _moduleProp.flowList;
            if(!flows) {
                flows = [];
                _moduleProp.flowList = flows;
            }
            return flows;
        },
        /**
         * 新增流程
         * @param flow*/
        addFlowsProp:function(flow) {
            var fl = $.extend(true, {}, _flow, flow);
            var flows = _moduleProp.flowList;
            for(var i=0;i<flows.length;i++) {
                var flow = flows[i];
                if(fl.key == flow.key) {
                    return false;
                }
            }
            flows.push(fl);
            return true;
        },
        /**
         * 删除流程
         * @param flowName*/
        delFlowsProp:function(flowName) {
            var flows = _moduleProp.flowList;
            var delIndex = -1;
            $.each(flows, function(i, flow) {
                if(flow.key == flowName) {
                    delIndex = i;
                }
            });
            if(delIndex!=-1) {
                flows.splice(delIndex, 1);
            }
        },
        /**
         * 获得表属性
         * @param index 表单索引
         * @return form*/
        getFormProp:function(index) {
            var forms =_moduleProp.dataSource.formList;
            //	var newForm = $.extend({},_formProp);
            //if(index) {
            for(var i=0;i<forms.length;i++){
                if(forms[i]){
                    if(forms[i].index==index){
                        $.each(forms[i].queryList,function(k,v){
                            if(v.type){
                                forms[i].queryType = "树形查询";
                            }});
                        return forms[i];
                    }
                }
            }
            //}
            var form = $.extend(true, {},_formProp, {"index":_index++,"flag":true});
            _moduleProp.dataSource.formList.push(form);
            return form;
        },
        /**
         * 获得表单行字段属性
         * @param tableIndex 表索引
         * @param index 字段索引
         * return obj*/
        getFieldProp:function(tableIndex, index) {
            var form = $.fn.module.getFormProp(tableIndex);
            var fields=form.fieldList;
            if(fields){
                for(var i=0;i<fields.length;i++){
                    if(fields[i].index==index){
                        //$.each(fields[i],function(k,v){alert(k+" : "+v)});
                        return fields[i];
                    }
                }
            }
            return $.extend(true, {},_fieldProp, {"index":_index++});
        },

        /**
         * 根据表key获得对应表中系统字段属性
         * @param tableIndex 表索引
         * @param index 字段索引
         * return obj*/
        getFieldPropByKey:function(tableIndex, index) {
            var form = $.fn.module.getFormProp(tableIndex);
            var fields=form.systemFields;
            if(fields){
                for(var i=0;i<fields.length;i++){
                    if(fields[i].columnName==index){
                        return fields[i];
                    }
                }
            }
            return null;
        },
        /**
         * 获得公式属性
         * @param tableIndex 表索引
         * @param index 字段索引
         * @returns formula
         * */
        getFormulaProp:function(tableIndex, index){
            var formula= $.fn.module.getFieldProp(tableIndex, index).editProperties.formula;
            return formula;
        },

        /**
         * 获得分页tabsList属性
         * @param rows 布局分组的行号
         * @returns tabsList
         * */
        getTabsList:function(rows){
            var tabsList =$.fn.module.getModule().dataSource.tabsList;
            if(tabsList){
                for(var i=0;i<tabsList.length;i++){
                    //alert(tabsList[i].rows);
                    //$.each(tabsList[i],function(k,v){alert(k+":"+v)});
                    if(rows==tabsList[i].rows){
                        return tabsList[i];
                    }
                }
                //return tabsList;
            }
            return null;
        },
        /**
         * 获得分页tabsList下单个tab属性
         * @param rows 布局分组行号
         * @param cols 布局分组的列号
         * @returns tabList
         * */
        getTabList:function(rows,cols){
            var tabList = $.fn.module.getTabsList(rows).tabList;
            if(tabList){
                for(var i=0;i<tabList.length;i++){
                    if(cols==tabList[i].cols){
                        return tabList[i];
                    }
                }
            }
            return null;
        },
        /**
         * 删除tab分页
         * @param rows 布局分组行号
         * @param cols 布局分组列号
         * @returns
         * */
        deleteTabList:function(rows,cols){
            var tabList = $.fn.module.getTabsList(rows).tabList;
            if(tabList){
                for(var i=0 ;i<tabList.length;i++){
                    if(tabList[i].cols==cols){
                        tabList.splice(i,1);
                        break;
                    }
                }
            }

        },
        /**
         * 删除子表布局属性
         * @param key*/
        deleteChildTabList:function(key){
            var tabsList = $.fn.module.getModule().dataSource.tabsList;
            if(tabsList){
                for(var i=0 ;i<tabsList.length;i++){
                    var tab = tabsList[i].tabList;
                    for(var j=0;j<tab.length;j++){
                        if(tab[j].form == key){
                            tab.splice(j,1);
                            break;
                        }
                    }
                }
            }
        },

        /**
         * 删除tabs分组
         * @param rows 布局分组行号*/
        deleteTabsList:function(rows){
            var tabsList = $.fn.module.getModule().dataSource.tabsList;
            for(var i=0;i<tabsList.length;i++){
                if(tabsList[i].rows==rows){
                    tabsList.splice(i,1);
                    return true;
                }
            }
        },
        /**
         * 添加新tabs分组
         * @param rows布局分组行号*/
        addTabsList:function(rows){
            var tabsList = $.fn.module.getModule().dataSource.tabsList;
            var tabs = $.extend(true, {},_tabs,{"rows":rows});
            tabsList.push(tabs);

        },
        /**
         * 设置分页tabsList属性
         * @param tabsJson 布局属性json对象*/
        setTabsList:function(tabsJson){
            var dataSource = $.fn.module.getModule().dataSource;
            var tabs = $.fn.module.getTabsList(tabsJson.rows);
            if(!tabs){
                tabs = $.extend(true, {},_tabs,tabsJson);
                dataSource.tabsList.push(tabs);
            }

        },
        /**
         * 设置分页tabsList下单个tab属性
         * @param tabJson 布局属性json对象*/
        setTabList:function(tabJson){
            //var tabJson = $.fn.module.removeBlank(tabJsonn);
            var tabs = $.fn.module.getTabsList(tabJson.rows);
            var tab = $.fn.module.getTabList(tabJson.rows,tabJson.cols);
            if(!tab){
                tab = $.extend(true, {},_tab,tabJson);
                tabs.tabList.push(tab);
            }else{
                $.extend(tab,tabJson);
            }
        },
        /**
         * 通过新值和旧值改变tab属性
         * @param oldKey
         * @param newKey
         * */
        setTabByKey:function(oldKey,newKey){
            var tabsList =$.fn.module.getModule().dataSource.tabsList;
            for(var i=0;i<tabsList.length;i++){
                var tab = tabsList[i].tabList;
                for(var j=0;j<tab.length;j++){
                    if(tab[j].form==oldKey){
                        $.extend(tab[j],{"form":newKey});
                    }
                }
            }
        },
        /**
         * 获得单个模块下所有的tab
         * */
        getAllTabList:function(){
            var form  = $.fn.module.getModule().dataSource.formList;
            var mainTabKey = "";
            var layoutObj = [];
            for(var m=0;m<form.length;m++){
                if(form[m].isMaster){
                    mainTabKey = form[m].key;
                }
            }
            var tabsList = $.fn.module.getModule().dataSource.tabsList;
            for(var i=0;i<tabsList.length;i++){
                var tab = tabsList[i].tabList;
                for(var j=0;j<tab.length;j++){
                    if(tab[j].form==mainTabKey){
                        layoutObj.push({"key":tab[j].rows+"."+tab[j].cols,"caption":tab[j].caption});
                    }
                }
            }
            return layoutObj;
        },
        /**
         * 获得布局tab,json展示属性
         * @param rows
         * @param cols
         * */
        getTabListJsonProp:function(rows,cols,isChild){
            var tab = $.fn.module.getTabList(rows,cols);
            var tabJson = {};
            if(isChild){
                tabJson =_tabChildPropJson;
            }else{
                tabJson =_tabPropJson;
            }
            var index = 0;
            $.each(tabJson.rows,function(k,v){
                v.value = tab[v.code];
            });
            return tabJson;
        },
        /**
         * 设置布局属性
         * @param json*/
        setTabListJsonProp:function(json){
            //var json = $.fn.module.removeBlank(jsonn);
            var rows = json.rows[0].value;
            var cols = json.rows[1].value;
            var tab = $.fn.module.getTabList(rows,cols);
            $.each(json.rows,function(k,v){
                //alert(v.code+":"+ v.value);
                //tab[v.code] = v.value;
                tab[v.code] = $.fn.module.removeBlank(v).value;
            });
            $.fn.module.setTabList(tab);
        },
        /**
         *设置字段归属分组
         *@param tableIndex
         *@param fieldIndex
         *@param rows
         *@param cols */
        setFieldLayoutTab:function(tableIndex,fieldIndex,rows,cols){
            //	alert("1111");
            var field = $.fn.module.getFieldProp(tableIndex, fieldIndex);
            $.extend(field,{"tabRows":rows,"tabCols":cols});
        },

        /**
         * 设置主表及子表行的值
         * @param index
         * @param rowDataa*/
        setTableProp:function(index,rowDataa){
            //	var dataProp = rowData;
            var rowData = $.fn.module.removeBlank(rowDataa);
            var checkArray = $.extend([],_systemField);
            var fields =$.fn.module.getFormProp(index);
            if(fields){
                if(fields.isVirtual) checkArray = [];//虚拟表，不检查系统字段
                var fl = fields.fieldList;
                if(fl){
                    for(var i=0;i<fl.length;i++){
                        if(fl[i].index != rowData.index) {
                            checkArray.push(fl[i].key.toUpperCase());
                        }
                    }
                    if(rowData["colName_"+index]){
                        if(rowData["colName_"+index].replace(/[ ]/g,"").length>0){
                            var col_name = $.fn.module.covToDBRule(rowData["colName_"+index]);
                            if($.inArray(col_name.toUpperCase(), checkArray)>=0){
                                rowData["colName_"+index] = col_name+"1";
                            }
                        }
                    }
                }
            }
            var dataProp = $.fn.module.convertRowData(rowData);
            var rowIndex = rowData["index"];
            var flag=true;
            if(rowIndex) {
                var fp = $.fn.module.getFieldProp(index, rowIndex);
                fp["key"] = dataProp["key"];
                fp["caption"] = dataProp["caption"];
                $.extend(fp.dataProperties,dataProp);
            } else {
                if(dataProp["key"]/*&&dataProp["caption"]*/){
                    var fp = $.extend(true, {},_fieldProp, {"index":_index++});
                    fp["key"] = dataProp["key"];
                    fp["caption"] = dataProp["caption"];
                    fp.dataProperties=$.extend(true, {},_dataProperties,dataProp);
                    fp.editProperties=$.extend(true, {},_editProperties);
                    fp.queryProperties=$.extend(true, {},_queryProperties);
                    rowData["index"] = fp.index;
                    var form = $.fn.module.getFormProp(index);
                    var mp = $.fn.module.getModuleProp();

                    //判断字段所在表是主表还是子表，定制个性化属性
                    if(form.isMaster) {
                        if(mp.type=="dict"){//设置默认值
                            fp.queryProperties.showInSearch = true;
                            fp.queryProperties.showInGrid = true;
                        }else{
                            fp.queryProperties.showInGrid = false;
                        }
                    } else {
                        fp.queryProperties.showInGrid = true;
                    }
                    form.fieldList.push(fp);
                }else{
                    flag = false;
                }
            }
            return flag;
        },
        /**
         * 把页面模块属性json赋值给js
         * @param rowDataa*/
        setModuleProp:function(rowDataa) {
            var filter=["creator","createTime","lastModifiedBy","lastModifiedTime"];
            var tabProp={};
            var rowData = $.fn.module.removeBlank(rowDataa);
            tabProp[rowData.code] = rowData.value;
            var module = $.fn.module.getModuleProp();
            if(module){
                if($.inArray(rowData.code, filter) >= 0 ){
                    $.extend(module.ownerProperties,tabProp);
                }else{
                    $.extend(module,tabProp);
                }
            }else{
                $.extend(_moduleProperties,tabProp);
            }
        },
        /**
         * 给元素设置设置宽度高度等属性
         * @param tableIndex  表单的编号
         * @param fieldIndex 对应行的编号
         * @parma data 传入的控件宽高等属性*/
        setEditProp:function(tableIndex,fieldIndex,data){
            var field = $.fn.module.getFieldProp(tableIndex,fieldIndex);
            //var editProp  = field.editProperties;
            $.extend(field.editProperties,data);
            //	alert(tableIndex+" : "+fieldIndex);
            //$.each(field,function(k,v){alert(k+" : "+v)});
            //alert(editProp.width+" : "+editProp.height);
        },
        /**
         * 把页面表格行属性json赋值给js
         * @param tableIndex 表编号
         * @param fieldIndex 行编号
         * @param rowDataa 行json数据*/
        setFieldProp:function(tableIndex, fieldIndex, rowDataa) {
            var fieldProp={};
            var rowData = $.fn.module.removeBlank(rowDataa);
            fieldProp[rowData.code] = $.fn.module.convertRowData(rowData).value;
            //	fieldProp[rowData.code] = rowData.value;
            var field = $.fn.module.getFieldProp(tableIndex,fieldIndex);
            if(field){
                var group = rowData.group;
                switch(group) {
                    case "编辑属性":
                        $.extend(field.editProperties,fieldProp);
                        break;
                    case "查询属性":
                        $.extend(field.queryProperties,fieldProp);
                        break;
                    case "数据属性":
                        if(rowData.code == "colName") {
                            var v = fieldProp["colName"];//.toUpperCase();
                            fieldProp["colName"] = v;
                            field["key"] = $.fn.module.convertProp(v);
                        }
                        $.extend(field.dataProperties,fieldProp);
                        break;
                    default:
                        $.extend(field,fieldProp);
                }
            }else{
                $.extend(_fieldProp,fieldProp);
                _fieldProp.index=index;
            }
        },
        /**
         * 给field里面设置event事件值
         * @param tableIndex 表编号
         * @param fieldIndex 行编号
         * @param data 事件json数据*/
        setFieldEventProp:function(tableIndex,fieldIndex,data){
            var eventVal = $.extend(true, {},_event,data);
            var field = $.fn.module.getFieldProp(tableIndex,fieldIndex);
            var flag =$.fn.module.findEvent(tableIndex,fieldIndex,data.key);
            if(flag>=0){
                $.extend(field.editProperties.eventList[flag],eventVal);
            }else{
                var prop =field.editProperties;
                if(!$.isArray(prop.eventList)){
                    prop.eventList=[];
                }
                prop.eventList.push(eventVal);
            }

        },
        /**
         * 查找事件位置
         * @param tableIndex
         * @param fieldIndex
         * @param eventKey 事件的key*/
        findEvent:function(tableIndex,fieldIndex,eventKey){
            var field = $.fn.module.getFieldProp(tableIndex,fieldIndex);
            if(field){
                var eventList = field.editProperties.eventList;
                if(eventList.length>0){
                    for(var i=0;i<eventList.length;i++){
                        if(eventList[i].key==eventKey){
                            return i;
                        }
                    }
                }
                return -1;

            }
        },
        /**
         * 删除控件绑定的事件
         * @param tableIndex
         * @param feildIndex
         * @param eventName 事件类型
         * @returns null*/
        deleteEvent:function(tableIndex,fieldIndex,eventName){
            var field = $.fn.module.getFieldProp(tableIndex,fieldIndex);
            if(field){
                var editProp = field.editProperties;
                if(editProp){
                    var eventList = editProp.eventList;
                    for(var i=0;i<eventList.length;i++){
                        if(eventList[i].key == eventName){
                            eventList.splice(i,1);
                            break;
                        }
                    }
                }
            }
        },
        /**
         * 把页面获得的公式属性赋值给js
         * @param tableIndex
         * @param index
         * @param data
         * @param id
         * @param sqls
         * @param content 保存的sql语句
         * @param refQsKey 查询数据源
         * */
        setFormulaProp:function(tableIndex, index,data,id,sqls,content, refQsKey){
            var fp = $.fn.module.getFormulaProp(tableIndex, index);
            var field = $.fn.module.getFieldProp(tableIndex, index).editProperties;
            var len = data.length;
            var temp={};
            if(id=="2"){
                if(!fp ||!fp.sql){
                    fp = $.extend(true, {},_formulaProp);
                }
                fp.refKey = data;
                fp.refQsKey = refQsKey;
                fp.type = id;
                $.extend(field,{"formula":fp});
            }else if(id=="1"){
                if(!fp){
                    //$.extend(fp,_formulaProp);
                    fp = $.extend(true, {},_formulaProp,{"sql":null});
                }
                fp.type=id;
                fp["itemList"] = [];
                for(var i=0;i<len;i++){
                    var rowdata = $.fn.module.removeBlank(data[i]);
                    if(rowdata.key || rowdata.caption || rowdata.column){
                        temp = $.extend(true, {},_itemProp,rowdata);
                        fp.itemList.push(temp);
                    }
                }

                $.extend(field,{"formula":fp});
            }else if(id=="5"){
                if(!fp){
                    fp = $.extend(true, {},_formulaProp,{"sql":null});
                }
                fp.type=id;
                var sql = $.extend(true, {}, _sqlProp);
                sql["key"] = "module";
                sql["entityName"] = data["entityName"];
                sql["isCheckAuth"] = data["isCheckAuth"];
                sql["isUseCache"] = data["isUseCache"];
                sql["dataStatus"] = data["dataStatus"];
                sql.setValueItemList = data.rows;
                fp.sql = sql;
                $.extend(field,{"formula":fp});
            } else{
                field.formula=null;
            }

        },
        /**
         * 获取公式代码
         * @param tableIndex
         * @param index
         * @returns
         */
        getExpCode:function(tableIndex, index) {
            var ep = $.fn.module.getFieldProp(tableIndex, index).editProperties;
            return ep.expCode;
        },
        /**
         * 设置公式代码
         * @param tableIndex
         * @param index
         * @param content
         */
        setExpCode:function(tableIndex, index, content) {
            var ep = $.fn.module.getFieldProp(tableIndex, index).editProperties;
            ep.expCode = content;
        },
        /**
         * 设置查询方式
         * @param tableIndex
         * @param type
         * @param key
         * @param sql
         * @param data*/
        setQueryType:function(tableIndex,type,key,sql,data){
            var form = $.fn.module.getFormProp(tableIndex);
            var formula={};
			/*
			 var sqlProp = $.extend({},_sqlProp);
			 sqlProp.sqlWhereItemList=[];
			 sqlProp.content = sql;
			 for(var i=0;i<data.length;i++){
			 if(data[i]["flag"]=="sqlWhere"){
			 if(data[i].key || data[i].caption || data[i].column){
			 temp = $.extend({},_itemProp,data[i]);
			 sqlProp.sqlWhereItemList.push(temp);
			 }

			 }
			 else if(data[i]["flag"]=="setValue"){
			 if(data[i].key || data[i].caption || data[i].column){
			 temp = $.extend({},_itemProp,data[i]);
			 sqlProp.setValueItemList.push(temp);
			 }

			 }
			 else if(data[i]["flag"]=="groupBy"){
			 if(data[i].key || data[i].caption || data[i].column){
			 temp = $.extend({},_itemProp,data[i]);
			 sqlProp.groupByItemList.push(temp);
			 }

			 }
			 else if(data[i]["flag"]=="orderBy"){
			 if(data[i].key || data[i].caption || data[i].column){
			 temp = $.extend({},_itemProp,data[i]);
			 sqlProp.orderByItemList.push(temp);
			 }

			 }
			 }*/
            formula = $.extend(true, {},_formulaProp,{"type":"2","refKey":data});
            var queryList = $.extend(true, {},_queryList,{"type":type,"key":key,"formula":formula});
            form.queryList=[];
            form.queryList.push(queryList);
        },
        /**
         * 把页面表格属性json赋值给js
         * @param index 表索引
         * @param rowData 每一行的数据
         * @returns null
         */
        setFormProp:function(index,rowDataa){
            var rowData = $.fn.module.removeBlank(rowDataa);
            var form = $.fn.module.getFormProp(index);
            var formProp=$.fn.module.convertTableProp(rowData);
            //formProp[rowData.code] = rowData.value;

            //	alert(form.key+" : "+formProp.key);
            //	var newForm = $.extend(_formProp,formProp,{"index":index++});
            //	rowData[index] = newForm.index;
            if(rowData.code=="tableName"){
                var oldKey = form.key;
                var newKey = formProp.key;
                $.fn.module.setTabByKey(oldKey,newKey);
                //	console.log("old:"+oldKey+" new ;"+newKey);
            }else if(rowData.code=="referEntityTableName"){
                if(form.referEntityTableName){
                    formProp.isVirtual = true;
                }
            }
            if(form){
                $.extend(form,formProp);
            }else{
                form = $.extend(true, {}, _formProp,formProp,{"_formProp.index":index});
            }
        },

        /**
         * 删除表格单行属性
         * @param tableIndex 表索引
         * @param rowIndex 表格行索引
         * @returns null
         */
        deleteRowProp:function(tableIndex,rowIndex){
            var form = $.fn.module.getFormProp(tableIndex);
            var fieldList = form.fieldList;
            for(var i=0;i<fieldList.length;i++){
                if(fieldList[i].index == rowIndex){
                    fieldList.splice(i,1);
                    break;
                }
            }
            //form.fieldList.splice(rowIndex,1);
        },
        /**
         * 删除表格*
         * @param tableIndex*/
        deleteTable:function(tableIndex){
            var module = $.fn.module.getModule();
            var tab = module.dataSource.formList;
            for(var i=0;i<tab.length;i++){
                if(tab[i]["index"]==tableIndex){
                    if(tab[i].isMaster == false){
                        //delete module.dataSource.formList[i];
                        $.fn.module.deleteChildTabList(tab[i].key);
                        module.dataSource.formList.splice(i,1);
                        return true;
                    }else{
                        alert("主表不允许删除！");
                        return false;
                    }

                }
            }


        },
        /**
         * 获得赋值后的FieldPropJson
         * @param tableIndex
         * @param fieldIndex*/
        getFieldPropJson:function(tableIndex, fieldIndex) {
            //	alert(tableIndex+" : "+fieldIndex);
            var fp = $.fn.module.getFieldProp(tableIndex, fieldIndex);
            var tj = $.extend(true, {},_fieldPropJson);
            $.each(tj.rows, function(k, v) {
                var group = v.group;
                switch(group) {
                    case "编辑属性":
                        if(v.code=="formulaInfo"){
                            var formula=fp.editProperties.formula;
                            if(formula){
                                var type = formula.type;
                                if(type=="1"){
                                    v.value="固定值";
                                }else if(type=="2"){
                                    v.value="SQL语句";
                                }else if(type=="0"){
                                    v.value="无内容";
                                }else if(type=="5"){
                                    v.value="模块";
                                }
                            }else{
                                v.value="无内容";
                            }
                        }else if(v.code == "layoutTab"){
                            var tabList = $.fn.module.getTabList(fp.tabRows,fp.tabCols);
                            if(tabList){
                                v.value = tabList.caption;
                            }

                        }else{
                            v.value = convertBooleanToString(fp.editProperties[v.code]);
                        }
                        break;
                    case "查询属性":
                        v.value = convertBooleanToString(fp.queryProperties[v.code]);
                        break;
                    case "数据属性":
                        v.value = convertBooleanToString(fp.dataProperties[v.code]);
                        //v.value = fp.dataProperties[v.code];
                        break;
                    default:
                        v.value = convertBooleanToString(fp[v.code]);
                }
            });
            return tj;
        },
        /**
         * 获得赋值后的FormPropJson
         * @param index*/
        getFormPropJson:function(index){
            var formJson = $.fn.module.getFormProp(index);
            var fj = $.extend(true, {},_formPropJson);
            $.each(fj.rows,function(k,v){
                var group = v.group;
                var ext = $.fn.module.getExtension(index);
                switch(group) {
                    case "编辑属性":
                        if(v.code=="eventList"){
                            v.value = ext["editPage"][v.code];
                        }else if(v.code=="stsList"){
                            v.value = ext["editPage"][v.code];
                        }else{
                            v.value = convertBooleanToString(formJson[v.code]);
                        }
                        break;
                    case "查询属性":
                        if(v.code=="eventList"){
                            v.value = ext["queryPage"][v.code];
                        }else if(v.code=="stsList"){
                            v.value = ext["queryPage"][v.code];
                        }else if(v.code=="querySql"){
                            v.value = ext["queryPage"][v.code];
                        }else if(v.code=="buttonList"){
                            v.value = ext["queryPage"][v.code];
                        }else{
                            v.value = convertBooleanToString(formJson[v.code]);
                        }
                        break;
                    default:
                        v.value = convertBooleanToString(formJson[v.code]);
                }

            });
            fj.index=formJson.index;
            return fj;
        },
        /**
         * 获得赋值后的ModulePropJson
         * */
        getModulePropJson:function(){
            var mp = $.fn.module.getModuleProp();
            var mj = $.extend(true, {},_modulePropJson);
            var filter = ["caption","key","version","languageText","type","relevanceModule"];
            $.each(mj.rows,function(k,v){
                if($.inArray(v.code,filter) >= 0 ){
                    v.value = mp[v.code];
					/*if(v.code=="version"){
					 alert(v.value);
					 }*/
                }else{
                    if(v.code=="createTime"||v.code=="lastModifiedTime"){
                        v.value = mp.ownerProperties[v.code];
                    }else{
                        v.value = mp.ownerProperties[v.code];
                    }

                }

            });
            mj.index=mp.index;
            return mj;
        },
        /**
         *  获得转换后的表格json数据
         *  @param tableIndex*/
        getTableFieldsJson:function(tableIndex) {
            var data = {"total":0,"rows":[]};
            //alert(tableIndex);
            var frm = $.fn.module.getFormProp(tableIndex);
            if(frm) {
                if(frm.flag){
                    frm.fieldList=[];
                    return {"total":0,"rows":[]};
                }else{
                    if(!frm.fieldList){
                        frm.fieldList=[];
                    }
                    var fl = frm.fieldList;
                    data.total = fl.length;

                    $.each(fl, function(k, v) {
                        if(v.key){
                            var tv = $.fn.module.convertFieldToDgJson(v,tableIndex);
                            data.rows.push(tv);
                        }
                    });
                    return data;
                }
            }else{
                return {"total":0,"rows":[]};
            }

        },
        /**
         * 获得转换后的SqlPropJson
         * @param tableIndex
         * @param fieldIndex*/
        getSqlPropJson:function(tableIndex,fieldIndex){
            var data = {"total":0,"rows":[]};
            var formula = $.fn.module.getFormulaProp(tableIndex,fieldIndex);
			/*if(!formula){
			 formula = $.extend({},_formulaProp);
			 }*/
            //	alert(formula);
            var type = "2";
            var plSql = "";
            if(formula && formula.type == type){
                if(!formula.sql){
                    formula.sql=[];
                }
                var sqlProp = formula.sql;
                plSql = sqlProp.content;
                //	alert(plSql);
                data.total = sqlProp.sqlWhereItemList.length+sqlProp.setValueItemList.length+sqlProp.groupByItemList.length+sqlProp.orderByItemList.length;
                $.each(sqlProp.sqlWhereItemList,function(k,v){
                    var sql = $.extend(true, {},v,{"flag":"sqlWhere"});
                    data.rows.push(sql);
                });
                $.each(sqlProp.setValueItemList,function(k,v){
                    var sql = $.extend(true, {},v,{"flag":"setValue"});
                    data.rows.push(sql);
                });
                $.each(sqlProp.groupByItemList,function(k,v){
                    var sql = $.extend(true, {},v,{"flag":"groupBy"});
                    data.rows.push(sql);
                });
                $.each(sqlProp.orderByItemList,function(k,v){
                    var sql = $.extend(true, {},v,{"flag":"orderBy"});
                    data.rows.push(sql);
                });
            }
            return {"type":type,"sql":plSql,"data":data};
        },
        /**
         * 得到item属性
         * @param tableIndex
         * @param fieldIndex*/
        getFixtabJson:function(tableIndex,fieldIndex){
            var data = {"total":0,"rows":[]};
            var formula = $.fn.module.getFormulaProp(tableIndex,fieldIndex);
			/*if(!formula){
			 formula = $.extend({},_formulaProp);
			 }*/
            //	alert(formula);
            var type = "1";
            if(formula && formula.type == type){
                if(formula.itemList){
                    data.total = formula.itemList.length;
                    $.each(formula.itemList,function(k,v){
                        //	alert(v.key+":"+v.caption);
                        var item = $.extend(true, {},{"key":v.key,"caption":v.caption});
                        data.rows.push(item);
                    });
                }
            }
            return {"type":type,"data":data};
        },
        /**
         * 获得查询方式
         * @param tableIndex*/
        getQueryType:function(tableIndex){
            var form = $.fn.module.getFormProp(tableIndex);
            var data={"total":0,"rows":[]};
            var sql="";
            var type="";
            var key="";
            var formula=[];
            var sqlProp=[];
            if(form){
                if(form.queryList){
                    $.each(form.queryList,function(k,v){
                        type =v.type;
                        key = v.key;
                        formula = v.formula;
                    });
                    if(formula){
                        $.each(formula,function(ck,cv){
                            if(ck=="sql"){
                                sqlProp = cv;
                            }
                        });
                        if(sqlProp){
                            sql = sqlProp.content;
							/*	data.total = sqlProp.sqlWhereItemList.length+sqlProp.setValueItemList.length+
							 sqlProp.groupByItemList.length+sqlProp.orderByItemList.length;*/
                            if(sqlProp.sqlWhereItemList){
                                $.each(sqlProp.sqlWhereItemList,function(k,v){
                                    var sql = $.extend(true,{},v,{"flag":"sqlWhere"});
                                    data.rows.push(sql);
                                });
                            }
                            if(sqlProp.setValueItemList){
                                $.each(sqlProp.setValueItemList,function(k,v){
                                    var sql = $.extend(true,{},v,{"flag":"setValue"});
                                    data.rows.push(sql);
                                });
                            }
                            if(sqlProp.groupByItemList){
                                $.each(sqlProp.groupByItemList,function(k,v){
                                    var sql = $.extend(true,{},v,{"flag":"groupBy"});
                                    data.rows.push(sql);
                                });
                            }
                            if(sqlProp.orderByItemList){
                                $.each(sqlProp.orderByItemList,function(k,v){
                                    var sql = $.extend(true,{},v,{"flag":"orderBy"});
                                    data.rows.push(sql);
                                });
                            }

                        }
                    }
                }
            }
            return {"key":key,"type":type,"sql":sql,"data":data};
        },
        /**
         * 转换字段名格式为全部大写
         * @param row*/
        convertRowData:function(row) {
            var newRow = {};
            $.each(row, function(k, v) {
                var tmp = k.split("_");
                var key = tmp[0];
                //屏蔽大小写转换规则
                if(key == "colName") {
                    var prop = $.fn.module.convertProp(v);
                    //v = v.toUpperCase();
                    //	row[k] = v;
                    //	alert(v);
                    newRow["key"]=$.fn.module.covToDBRule(prop);
                    //console.log(prop);
                }
                newRow[key] = v;
                //console.log(v);
            });
            return newRow;
        },


        /**
         * 转换表名格式为首字母大写
         * @param row*/
        convertTableProp:function(row){
            var newRow = {};
            if(row.code == "tableName"){
                var tmp = $.fn.module.covToDBRule(row.value.toUpperCase());
                //var v = tmp;
                row.value = tmp;
                var prop  = $.fn.module.convertProp(tmp,true);
                newRow["key"] = prop;
                //console.log("prop:"+prop+" tmp ;"+tmp);
            }
            newRow[row.code] = row.value;
            return newRow;
        },
        /**
         * 判断是表名还是字段名去除字符串中“_”并且把转换后的字符串拼接起来
         * @param str
         * @param flag*/
        convertProp:function(str,flag){
            var ch=str.split("_");
            var strs = [];
            for(var i=0;i<ch.length;i++){
                if(i>0) {
                    var temp=ch[i].toLowerCase();
                    temp=temp.replace(temp.charAt(0),temp.charAt(0).toUpperCase());
                    strs.push(temp);
                } else {
                    if(flag){
                        var temp=ch[i].toLowerCase();
                        temp=temp.replace(temp.charAt(0),temp.charAt(0).toUpperCase());
                        strs.push(temp);
                    }else{
                        strs.push(ch[i].toLowerCase());
                    }
                }
            }
            return strs.join("");
        },
        /**
         * 转换不含“_”的json到也页面对应的含“_”的json
         * @param fd
         * @param tableIndex*/
        convertFieldToDgJson:function(fd, tableIndex) {
            var newFd = {};
            $.each(fd.dataProperties, function(k, v) {
                var tk = k + "_" + tableIndex;
                newFd[tk] = convertBooleanToString(v);
            });
            newFd["caption" + "_" + tableIndex] = fd["caption"];
            newFd["index"]=fd["index"];
            return newFd;
        },
        /**
         * 刷新页面表格数据
         * @param tableIndex
         * @param fieldIndex
         * @param rowData
         * @param callFunc*/
        refreshTableGrid:function(tableIndex, fieldIndex, rowData, callFunc) {
            if(rowData) {
                var flag = false;
                var group = rowData.group;
                var filter = ["caption","colName","dataType","size","scale","notNull","unique"];
                switch(group) {
                    case "基本属性":
                    case "数据属性":
                        if($.inArray(rowData.code, filter) >= 0 ) {
                            flag = true;
                        }
                        break;
                    default:
                        break;
                }
                if(flag && callFunc) {
                    var fd = $.fn.module.getFieldProp(tableIndex, fieldIndex);
                    var newRow = $.fn.module.convertFieldToDgJson(fd, tableIndex);
                    callFunc(newRow);
                }
            }
        },
		/*//复制表格行
		 copyRow:function(rowData){
		 copyRowData = $.extend({},rowData);
		 //	return rowData;
		 },*/
        /**
         * 粘贴行数据转换
         * @param tableIndex
         * @param rowData*/
        pasteRow:function(tableIndex, rowData){
            var rowJson =$.extend(true, {},rowData["dataProperties"],{"caption":rowData["caption"]});
            return rowJson;

        },
        /**
         * 点击行显示其详细信息
         * @param tableIndex
         * @param fieldIndex*/
        showLineInfo:function (tableIndex,fieldIndex){
            data = $.fn.module.getFieldPropJson(tableIndex, fieldIndex);
            var layoutData = $.fn.module.getAllTabList();
            var drows = data.rows;
            for(var i=0,len=drows.length;i<len;i++) {
                if(drows[i].code == "layoutTab") {
                    drows[i].editor.options.data = layoutData;
                    break;
                }
            }
            $("#module_table_propGrid").data("tableIndex", tableIndex);
            $("#module_table_propGrid").data("fieldIndex", fieldIndex);
            $("#module_table_propGrid").data("type", "字段属性");
            $("#module_table_propGrid").propertygrid({"title":"字段属性"});
            $("#module_table_propGrid").propertygrid("loadData",data);
        },
		/*//复制表格
		 copyTabs:function(forms,tabsIndex){
		 for(var i=0;i<forms.length;i++){
		 //	$.each(forms[i],function(k,v){alert(k+" : "+v)});
		 if(forms[i].index==tabsIndex){
		 copyTabJson = $.extend({},forms[i]);
		 //	return tabJson;
		 }
		 }
		 },*/
        /**
         * 拖动交换field在内存中的位置
         * @param tableIndex
         * @param fieldIndex*/
        swapPosition:function(tableIndex,fieldIndex){
            var form = $.fn.module.getFormProp(tableIndex);
            var fields=form.fieldList;
            if(fields){
                for(var i=0;i<fields.length;i++){
                    if(fields[i].index==fieldIndex){
                        return i;
                    }
                }
            }
            return null;
        },
        //通过传入的data参数返回相应的x,y位置
		/*dataToXy:function(data,tab_rows,tab_cols){
		 var temp_x = data.editProperties.cols;
		 var temp_y = data.editProperties.rows;
		 var tab_flag = new Array[3][tableCols];
		 alert(tab_flag[1][0]);
		 if(x>tableCols){
		 y++;
		 x=1+temp_x;
		 return {"x":x-temp_x,"y":y};
		 }else{
		 x=x+temp_x;
		 return {"x":x-temp_x,"y":y};
		 }
		 var rowCount = 0;
		 var colCount = 0;
		 var arr  = new Array();
		 for(var i=0;i<tab_rows;i++){
		 arr[i] = new Array();
		 for(var j=0;j<tab_cols;j++){
		 arr[i][j]=0;
		 }
		 }

		 },*/


        /**
         * 根据传入信息移动tab分组左右位置
         * @param rows
         * @param cols
         * @param site*/
        moveTabPlace:function(rows,cols,site){
            var tabsList = $.fn.module.getTabsList(rows);
            if(tabsList){
                var tabList = tabsList.tabList;
                var temp = {};
                for(var i=0;i<tabList.length;i++){
                    if(tabList[i].cols == cols){
                        temp = tabList[i];
                        if(site =="left"){
                            if(i==0){
                                return false;
                            }else{
                                tabList[i] = tabList[i-1];
                                tabList[i-1] = temp;
                                return true;
                            }

                        }else if(site == "right"){
                            if(i==tabList.length-1){
                                return false;
                            }else{
                                tabList[i] = tabList[i+1];
                                tabList[i+1] = temp;
                                return true;
                            }

                        }
                    }
                }

            }

        },

        /**
         *根据传入信息移动tab分组上下位置位置
         * @param rows
         * @param cols
         * @param site*/
        moveTabUpDown:function(rows,cols,site){
            var tabsList = $.fn.module.getModule().dataSource.tabsList;
            for(var i=0;i<tabsList.length;i++){
                if(tabsList[i].rows == rows){
                    if(site=="up"){
                        if(i==0){
                            return false;
                        }else{
                            var newRows = tabsList[i-1].rows;
                            var newCols = 0;
                            $.each(tabsList[i-1].tabList,function(k,v){
                                newCols = v.cols>newCols?v.cols:newCols;
                            });
                            newCols +=1;

                            var oldTab =$.fn.module.getTabList(rows,cols);
                            var formkey = oldTab.form;
                            var newTab = $.extend(true,{},oldTab,{"rows":newRows,"cols":newCols});
                            tabsList[i-1].tabList.push(newTab);


                            var form = $.fn.module.getFormByKey(formkey);
                            for(var m=0;m<form.fieldList.length;m++){
                                if(form.fieldList[m].tabRows == rows && form.fieldList[m].tabCols == cols){
                                    $.extend(form.fieldList[m],{"tabRows":newRows,"tabCols":newCols});
                                }
                            }
                            for(var n=0;tabsList[i].tabList.length;n++){
                                if(tabsList[i].tabList[n].cols == cols){
                                    tabsList[i].tabList.splice(n,1);
                                    return true;
                                }
                            }

                        }
                    }else if(site == "down"){
                        // alert(i);
                        if(i==tabsList.length-1){
                            return false;
                        }else{
                            var newRows = tabsList[i+1].rows;
                            var newCols = 0;
                            $.each(tabsList[i+1].tabList,function(k,v){
                                newCols = v.cols>newCols?v.cols:newCols;
                            });
                            newCols +=1;

                            var oldTab =$.fn.module.getTabList(rows,cols);
                            var formkey = oldTab.form;
                            var newTab = $.extend(true,{},oldTab,{"rows":newRows,"cols":newCols});
                            tabsList[i+1].tabList.push(newTab);


                            var form = $.fn.module.getFormByKey(formkey);
                            for(var m=0;m<form.fieldList.length;m++){
                                if(form.fieldList[m].tabRows == rows && form.fieldList[m].tabCols == cols){
                                    $.extend(form.fieldList[m],{"tabRows":newRows,"tabCols":newCols});
                                }
                            }
                            for(var n=0;tabsList[i].tabList.length;n++){
                                if(tabsList[i].tabList[n].cols == cols){
                                    tabsList[i].tabList.splice(n,1);
                                    return true;
                                }
                            }


                        }
                    }
                }
            }
        },

        /**
         * 删除下推规则
         * @param data*/
        deleteRule:function(data){
            var module = $.fn.module.getModule();
            var ruleList = module.ruleList;
            var flag = $.fn.module.findRuleList(ruleList,data.key);
            ruleList.splice(flag,1);
        },
        /**
         * 设置sql语句生成的下推规则
         * @param data
         * @param sqlStr
         * @return null*/
        setSqlRule:function(data,sqlStr,oldKey){
            var module = $.fn.module.getModule();
            var ruleList = module.ruleList;
            var flag = $.fn.module.findRuleList(ruleList,data.key);
            if(oldKey){
                var oldFlag = $.fn.module.findRuleList(ruleList,oldKey);
                var sql = $.extend(true,{},_sqlProp,{"content":sqlStr});
                $.extend(ruleList[oldFlag],{"key":data.key,"caption":data.caption,"type":data.type,"sql":sql});
                return true;
            }else{
                if(flag&&flag>=0){
                    return false;
                }else{
                    var sql = $.extend(true,{},_sqlProp,{"content":sqlStr});
                    var rule = $.extend(true,{},_rule,{"key":data.key,"caption":data.caption,"type":data.type,"sql":sql});
                    ruleList.push(rule);
                    return true;
                }
            }

        },
        /**
         * 设置通过字段生成的映射生成的下推规则
         *@param data
         *@param oldKey */
        setFieldRule:function(data,oldKey,type){
            var module = $.fn.module.getModule();
            var ruleList = module.ruleList;
            var flag = $.fn.module.findRuleList(ruleList,data.key);
            // alert(oldKey);
            if(oldKey && type=="sql"){
                var oldFlag = $.fn.module.findRuleList(ruleList,oldKey);
                //	ruleListsplice(oldFlag,1);;
                var columnList = [];
                var columns = data.data;
                $.each(columns,function(k,v){
                    var column = $.extend(true,{},_column,{"source":v.source,"dest":v.dest,"rule":v.rule});
                    columnList.push(column);
                });
                var whereRows = data.whereRows;
                var sqlProp = $.extend(true,{},_sqlProp);
                sqlProp.sqlWhereItemList = [];
                sqlProp.orderByItemList = [];
                $.each(whereRows,function(k,v){
                    if(v.flag=="sqlWhere"){
                        var item = $.extend(true,{},_itemProp,v);
                        sqlProp.sqlWhereItemList.push(item);
                    }else if(v.flag=="orderBy"){
                        var item = $.extend(true,{},_itemProp,v);
                        sqlProp.orderByItemList.push(item);
                    }
                });
                var table = $.extend(true,{},_table,{"source":data.source,"dest":data.dest,"sourceModule":"","destModule":"","columnList":columnList});
                var sql = $.extend(true,{},sqlProp,{"content":data.content,"table":table});
                var rule = $.extend(true,{},_rule,{"key":data.key,"caption":data.caption,"type":data.type,"sql":sql});
                //ruleList[oldFlag].sql.

                //ruleList[oldFlag].sql.sqlWhereItemList = [];
                //ruleList[oldFlag].sql.orderByItemList = [];
                //ruleList[oldFlag] = rule;
                $.extend(ruleList[oldFlag],rule);
                //alert("111");
                return true;
            }else if(oldKey && type=="proc"){
                var procProp = $.extend(true,{},_procProp);
                var oldFlag = $.fn.module.findRuleList(ruleList,oldKey);
                var proc = $.extend(true,{},procProp,{"content":data.content});
                var rule = $.extend(true,{},_rule,{"key":data.key,"caption":data.caption,"type":data.type,"sql":proc});
                $.extend(ruleList[oldFlag],rule);
                return true;
            }else{
                if(flag&&flag>=0){
                    return false;
                }else if(type=="sql"){
                    var columnList = [];
                    var columns = data.data;
                    $.each(columns,function(k,v){
                        var column = $.extend(true,{},_column,{"source":v.source,"dest":v.dest,"rule":v.rule});
                        columnList.push(column);
                    });
                    var whereRows = data.whereRows;
                    var sqlProp = $.extend(true,{},_sqlProp);
                    $.each(whereRows,function(k,v){
                        if(v.flag=="sqlWhere"){
                            var item = $.extend(true,{},_itemProp,v);
                            sqlProp.sqlWhereItemList.push(item);
                        }else if(v.flag=="orderBy"){
                            var item = $.extend(true,{},_itemProp,v);
                            sqlProp.orderByItemList.push(item);
                        }
                    });
                    var table = $.extend(true,{},_table,{"source":data.source,"dest":data.dest,"sourceModule":"","destModule":"","columnList":columnList});
                    var sql = $.extend(true,{},sqlProp,{"content":data.content,"table":table});
                    var rule = $.extend(true,{},_rule,{"key":data.key,"caption":data.caption,"type":data.type,"sql":sql});
                    ruleList.push(rule);
                    return true;
                }else if(type=="proc"){
                    var procProp = $.extend(true,{},_procProp);
                    var proc = $.extend(true,{},procProp,{"content":data.content});
                    var rule = $.extend(true,{},_rule,{"key":data.key,"caption":data.caption,"type":data.type,"sql":proc});
                    ruleList.push(rule);
                    return true;
                }
            }
        },
        /**
         * 查找下推规则位置若找到返回相应位置否则返回null*/
        findRuleList:function(ruleList,key){
            for(var i=0;i<ruleList.length;i++){
                if(ruleList[i].key == key){
                    return i;
                }
            }
            return null;
        },
        /**
         * 获得sql语句生成的下推规则*/
        getSqlRule:function(){
            var module = $.fn.module.getModule();
            var ruleList = module.ruleList;
            var rowsList = [];
            $.each(ruleList,function(k,v){
                if(v.type=="sql"){
                    var table = null;
                    var whereData=[];
                    if(v.sql.table){
                        table = v.sql.table;
                    }
					/*  if(v.sql.sqlWhereItemList){
					 "sqlWhereItemList":[],
					 "setValueItemList":[],
					 "groupByItemList":[],
					 "orderByItemList":[]
					 }*/
                    $.each(v.sql.sqlWhereItemList,function(ck,cv){
                        var item = $.extend(true,cv,{"flag":"sqlWhere"});
                        whereData.push(item);
                    });
                    $.each(v.sql.orderByItemList,function(ck,cv){
                        var item = $.extend(true,cv,{"flag":"orderBy"});
                        whereData.push(item);
                    });
                    var rule = {"key":v.key,"caption":v.caption,"type":v.type,"sqlStr":v.sql.content,"table":table,"whereData":whereData};
                    rowsList.push(rule);
                }else if(v.type=="proc"){
                    var rule = {"key":v.key,"caption":v.caption,"type":v.type,"sqlStr":v.sql.content};
                    rowsList.push(rule);
                }
            });
            return {"total":0,"rows":rowsList};
        },
        /**粘贴表格数据格式转换
         * @param index
         * @param tab*/
        pasteTabs:function(index,tab){
            var tabIndex = tab.index;
            var tabsJson = tab;
            var newTab = $.extend(true,{},tabsJson,{"index":_index++});
            var field = newTab.fieldList;
            if(field){
                for(var i=0;i<field.length;i++){
                    field[i].index = _index++;
                }
                $.fn.module.getModule().dataSource.formList.push(newTab);
                return newTab;
            }
        },
        /**删除查询方式行
         * @param index
         * @param rowData*/
        delQueryRow:function(index,rowData){
            var form =$.fn.module.getFormProp(index);
            var sql = form.queryList[0].formula.sql;
            //	$.each(sql,function(k,v){alert(k+":"+v)});
            if(sql){
                if(rowData.flag=="sqlWhere"){
                    for(var i=0;i<sql.sqlWhereItemList.length;i++){
                        if(sql.sqlWhereItemList[i].key==rowData.key){
                            sql.sqlWhereItemList.splice(i,1);
                        }
                    }
                }else if(rowData.flag=="setValue"){
                    for(var i=0;i<sql.setValueItemList.length;i++){
                        if(sql.setValueItemList[i].key==rowData.key){
                            sql.setValueItemList.splice(i,1);
                        }
                    }
                }else if(rowData.flag=="groupBy"){
                    for(var i=0;i<sql.groupByItemList.length;i++){
                        if(sql.groupByItemList[i].key==rowData.key){
                            sql.groupByItemList.splice(i,1);
                        }
                    }
                }else if(rowData.flag=="orderBy"){
                    for(var i=0;i<sql.orderByItemList.length;i++){
                        if(sql.orderByItemList[i].key==rowData.key){
                            sql.orderByItemList.splice(i,1);
                        }
                    }
                }
            }

        },
        /**删除公式属性SQl语句
         * @param tableInex
         * @param fieldIndex
         * @param rowData*/
        delSqlRow:function(tableIndex,fieldIndex,rowData){
            var formula=$.fn.module.getFormulaProp(tableIndex, fieldIndex);
            if(formula){
                var sql = formula.sql;
                if(sql){
                    if(rowData.flag=="sqlWhere"){
                        for(var i=0;i<sql.sqlWhereItemList.length;i++){
                            if(sql.sqlWhereItemList[i].key==rowData.key){
                                sql.sqlWhereItemList.splice(i,1);
                            }
                        }
                    }else if(rowData.flag=="setValue"){
                        for(var i=0;i<sql.setValueItemList.length;i++){
                            if(sql.setValueItemList[i].key==rowData.key){
                                sql.setValueItemList.splice(i,1);
                            }
                        }
                    }else if(rowData.flag=="groupBy"){
                        for(var i=0;i<sql.groupByItemList.length;i++){
                            if(sql.groupByItemList[i].key==rowData.key){
                                sql.groupByItemList.splice(i,1);
                            }
                        }
                    }else if(rowData.flag=="orderBy"){
                        for(var i=0;i<sql.orderByItemList.length;i++){
                            if(sql.orderByItemList[i].key==rowData.key){
                                sql.orderByItemList.splice(i,1);
                            }
                        }
                    }
                }
            }
        },
        /**删除公式属性固定值方式
         * @param tableIndex
         * @param fiedlIndex
         * @param rowData 行数据*/
        delFixRow:function(tableIndex,fieldIndex,rowData){
            var formula=$.fn.module.getFormulaProp(tableIndex, fieldIndex);
            var itemList = formula.itemList;
            if(itemList){
                for(var i=0;i<itemList.length;i++){
                    if(itemList[i].key==rowData.key){
                        formula.itemList.splice(i,1);
                    }
                }
            }

        },
        /**根据传入参数生成对应表格
         * @param field 字段的json对象
         * @param cols 占用的列数
         * @param tabId 表的id
         * @param arr 布局的数据
         * @param row_Count 行的标识
         * @param col_Count 列的标识
         * @parm str 生成的tab的字符串*/
        autoCreateTable:function(field,cols,tabId,arr,row_Count,col_Count,str){
            if(field.editProperties.visible){
                var row_temp = parseInt(field.editProperties.rows);
                var col_temp = parseInt(field.editProperties.cols);
                //	alert(row_Count+":"+col_Count);
                if(arr[row_Count][col_Count]=="0"){
                    var flag = -1;
                    for(var j =col_Count;j<cols;j++){
                        if(arr[row_Count][j]=="1"){
                            flag = j-col_Count;
                            break;
                        }
                    }
                    if(flag == "-1"){
                        if(col_Count==0){
                            if(field.key){
                                str +="<tr><td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;' rowspan="+row_temp+" colspan="+col_temp+">"+$.fn.module.coverCellType(field,tabId);+"</td>";
                            }else{
                                str +="<tr><td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;' rowspan="+row_temp+" colspan="+col_temp+">"+$.fn.module.coverCellType(field,tabId);+"</td>";
                            }
                            //	var row_num = parseInt(row_Count)+parseInt(row_temp);
                            //	var col_num = parseInt(col_Count)+parseInt(col_temp);
                            for(var r=row_Count;r<row_Count+row_temp;r++){
                                for(var c=col_Count;c<col_Count+col_temp;c++){
                                    arr[r][c]=1;
                                }
                            }
                            col_Count += col_temp;
                            if(col_Count>=cols){
                                col_Count = 0;
                                row_Count++;
                                str +="</tr>";
                            }
                        }else{
                            if(col_Count+col_temp>cols){
                                //	alert(col_Count+col_temp);
                                for(var i=0;i<col_Count+col_temp-cols;i++){
                                    if(field.key){
                                        str +="<td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;'></td>";
                                    }else{
                                        str +="<td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;'></td>";
                                    }
                                }
                                str +="</tr>";
                                col_Count=0;
                                row_Count++;
                                $.fn.module.autoCreateTable(field,cols,tabId);
                            }else if(col_Count+col_temp==cols){
                                if(field.key){
                                    str +="<td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;' rowspan="+row_temp+" colspan="+col_temp+">"+$.fn.module.coverCellType(field,tabId);+"</td>";
                                }else{
                                    str +="<td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;' rowspan="+row_temp+" colspan="+col_temp+">"+$.fn.module.coverCellType(field,tabId);+"</td>";
                                }
                                for(var r=row_Count;r<row_Count+row_temp;r++){
                                    for(var c=col_Count;c<col_Count+col_temp;c++){
                                        arr[r][c]=1;
                                    }
                                }
                                col_Count = 0;
                                row_Count++;
                                str +="</tr><tr>";

                            }else{
                                if(field.key){
                                    str +="<td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;' rowspan="+row_temp+" colspan="+col_temp+">"+$.fn.module.coverCellType(field,tabId);+"</td>";
                                }else{
                                    str +="<td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;' rowspan="+row_temp+" colspan="+col_temp+">"+$.fn.module.coverCellType(field,tabId);+"</td>";
                                }
                                for(var r=row_Count;r<row_Count+row_temp;r++){
                                    for(var c=col_Count;c<col_Count+col_temp;c++){
                                        arr[r][c]=1;
                                    }
                                }
                                col_Count += col_temp;
                                //if(col_Count)

                            }

                        }
                    } else{
                        if(flag>=col_temp){
                            if(field.key){
                                str +="<td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;'>"+$.fn.module.coverCellType(field,tabId)+"</td>";
                            }else{
                                str +="<td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;'>"+$.fn.module.coverCellType(field,tabId)+"</td>";
                            }
                        } else {
                            str +="<td style='border-right:1px solid #99BBE8;border-top: 1px solid #99BBE8;'></td>";
                            $.fn.module.autoCreateTable(field,cols,tabId);
                        }
                        col_Count++;
                        if(col_Count>=cols){
                            col_Count = 0;
                            row_Count++;
                            str +="</tr>";
                        }

                    }

                }else {
                    col_Count++;
                    if(col_Count>=cols){
                        str +="</tr>";
                        row_Count++;
                        col_Count=0;
                    }
                    $.fn.module.autoCreateTable(field,cols,tabId);
                }

            }
            return {"str":str,"arr":arr,"row_Count":row_Count,"col_Count":col_Count};
        },
        /**绝对定位初始化生成Div元素
         * @param data
         * @param tabId
         * */
        createDiv:function(data,tabId){
            var t = data.editProperties.top;
            var l = data.editProperties.left;
            var editType = data.editProperties.editType;
            var cellWidth = data.editProperties.width;
            var cellHeight = data.editProperties.height;
            var caption = "";
            if(data.caption){
                caption = data.caption+":";
            }
            if(!cellWidth){
                cellWidth = 149;
                data.editProperties.width= 149;
            }
            if(!cellHeight){
                cellHeight =20;
                data.editProperties.height=20;
            }
            if(_editTypeImgDic[editType]) {
                var newArr = [];
                newArr.push("<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' style='position:absolute;top:");
                newArr.push(t);
                newArr.push("px;left:");
                newArr.push(l);
                newArr.push("px; width:");
                newArr.push(cellWidth+80);
                newArr.push("px;height:");
                newArr.push(cellHeight);
                newArr.push("px;border:1px dotted #ccc;white-space:nowrap;' tabIndex=");
                newArr.push(tabId);
                newArr.push(" id=");
                newArr.push(data.index);
                newArr.push("><span style='font-size:12px;width:80px;height:auto;display:inline-block;text-align:right;vertical-align:middle '><lable style='border-bottom: 1px solid gray;' >");
                newArr.push(caption);
                newArr.push("</lable></span><img align='bottom' style='width:auto;height:20px;border:0px;vertical-align:middle;'");
                newArr.push(" src='editTypeImages/");
                newArr.push(_editTypeImgDic[editType]);
                newArr.push("'/></div>");
                return newArr.join("");
            }
        },
        /**根具字段类型返回对应的元素
         * @param data
         * @param tabId*/
        coverCellType:function(data,tabId){
            var editType = data.editProperties.editType;
            var cellWidth = data.editProperties.width;
            var cellHeight = data.editProperties.height;
            var caption = "";
            if(data.caption){
                caption = data.caption+":";
            }
            if(!cellWidth){
                cellWidth = "auto";
            }else{
                cellWidth = cellWidth+"px";
            }
            if(!cellHeight){
                cellHeight = "auto";
            }else{
                cellHeight = cellHeight+"px";
            }
            if(_editTypeImgDic[editType]) {
                var newArr = [];
                newArr.push("<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)'  class='drag' style='border-color:#FFFFFF; white-space:nowrap;' tabIndex=");
                newArr.push(tabId);
                newArr.push(" id=");
                newArr.push(data.index);
                newArr.push("><span style='width:80px;display:inline-block;text-align:right;vertical-align:middle '><lable style='border-bottom: 1px solid gray;' >");
                newArr.push(caption);
                newArr.push("</lable></span><img align='bottom' width='");
                newArr.push(cellWidth);
                newArr.push("' height='");
                newArr.push(cellHeight);
                newArr.push("' src='editTypeImages/");
                newArr.push(_editTypeImgDic[editType]);
                newArr.push("'/></div>");
                return newArr.join("");
            } else {
                var newArr = [];
                newArr.push("<div class='drag' style='border-color:#FFFFFF' tabIndex=");
                newArr.push(tabId);
                newArr.push(" id=");
                newArr.push(data.index);
                if(data.index){
                    newArr.push("><span style='width:80px;display:inline-block;text-align:right;vertical-align:middle '><lable>[占位符]&nbsp;&nbsp;&nbsp;</lable></span><div>");
                }else{
                    newArr.push("><span style='width:80px;display:inline-block;text-align:right;vertical-align:middle '><lable>[无占位符]&nbsp;&nbsp;&nbsp;</lable></span><div>");
                }
                return newArr.join("");
            }
			/*white-space:nowrap;
			 if(true){
			 switch(edType){
			 case "TextBox":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><input type='text' readOnly='true' value="+data.dataProperties.colName+"></input></div>";
			 var tdstr="<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)'  class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img id='TextBox"+data.index+"'  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/text.jpg'></div>";
			 //var tdstr="<lable>"+caption+"</lable><img id='TextBox"+data.index+"'  width='"+cellWidth+"px' height='"+cellHeight+"px' ondragover='parent.resizeImage(event,this.id)' ondragend='parent.resizeImageEnd(this,this.parentNode)' src='editTypeImages/text.jpg'>";

			 return tdstr;
			 }
			 case "RichTextBox":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/RichTextBox.jpg'></img></div>";
			 var tdstr="<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)'  class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/RichTextBox.jpg'></div>";
			 return tdstr;
			 }
			 case "ComboBox":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/Tree.jpg'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag ' style='border-color:#FFFFFF'  tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/ComboBox.jpg'></div>";
			 return tdstr;
			 }
			 case "ComboTree":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/ComboTree.jpg'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag ' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/ComboBox.jpg'></div>";
			 return tdstr;
			 }
			 case "ComboGrid":{
			 //str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/ComboGrid.jpg'></img></div>";
			 var  tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag ' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/ComboBox.jpg'></div>";
			 return tdstr;
			 }
			 case "DateBox":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/DataBox.jpg'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/DataBox.jpg'></div>";
			 return tdstr;
			 }
			 case "RadioBox":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/RadioBox.jpg'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/RadioBox.jpg'></div>";
			 return tdstr;
			 }
			 case "CheckBox":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/CheckBox.jpg'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/CheckBox.jpg'></div>";
			 return tdstr;
			 }
			 case "Button":{
			 //str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><a href='#' class='easyui-linkbutton'>"+data.dataProperties.colName+"</a></div>"
			 var tdstr="<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/Button.jpg'></div>";
			 return tdstr;
			 }
			 case "HyperLink":{
			 //str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><a href='#' >"+data.dataProperties.colName+"</a></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/HyperLink.jpg'></div>";
			 return tdstr;
			 }
			 case "ImageBox":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/imageBox.gif'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF'  tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/imageBox.png'></div>";
			 return tdstr;
			 }
			 case "BrowserBox":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/BrowserBox.jpg'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)'  class='drag'  style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  width='200px' height='30px' src='editTypeImages/BrowserBox.jpg'></div>";
			 return tdstr;
			 }
			 case "DataGrid":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/ComboBox.jpg'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"' src='editTypeImages/ComboBox.jpg'></div>";
			 return tdstr;
			 }
			 case "TreeGrid":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/TreeGrid.jpg'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag'  style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/Tree.jpg'></div>";
			 return tdstr;
			 }
			 case "Tree":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><img src='editTypeImages/Tree.jpg'></img></div>";
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/Tree.jpg'></div>";
			 return tdstr;
			 }
			 case "Label":{
			 //  str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><lable>"+data.dataProperties.colName+"</lable></div>";
			 var labelVal ="label 默认值为空" ;
			 var defaultVal = data.editProperties.defaultValue;
			 if(defaultVal){
			 labelVal = defaultVal;
			 }
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)'  class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><label>"+labelVal+"</label></div>";
			 return tdstr;
			 }
			 case "PasswordBox":{
			 // str = "<div class='item' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+"><lable>"+data.caption+":</lable><input type='password' value='**********'></input></div>";

			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/password.jpg'></div>";
			 return tdstr;
			 }
			 case "ProgressBar":
			 var tdstr = "<div onclick='parent.parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' class='drag' style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>"+caption+"</lable><img  width='"+cellWidth+"' height='"+cellHeight+"'  src='editTypeImages/ProgressBar.jpg'></div>";
			 return tdstr;
			 default :{
			 var tdstr = "<div class='drag'  style='border-color:#FFFFFF' tabIndex="+tabId+" id="+data.index+"><lable>&nbsp;&nbsp;&nbsp;</lable><div>";
			 return tdstr;
			 }
			 }
			 }*/
        },
        /**
         * 获取查询属性
         * @param data
         * @param tabId
         * @param flag
         * @returns
         */
        covQueryType:function(data,tabId,flag){
            var editType = data.editProperties.editType;
            if(flag) {
                if(_editTypeImgDic[editType]) {
                    var newArr = [];
                    newArr.push("<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title=");
                    newArr.push(data.caption);
                    newArr.push("><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex=");
                    newArr.push(tabId);
                    newArr.push(" id=");
                    newArr.push(data.index);
                    newArr.push(" src='editTypeImages/");
                    newArr.push(_editTypeImgDic[editType]);
                    newArr.push("'></img></div>");
                    return newArr.join("");
                } else {
                    var newArr = [];
                    newArr.push("<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title=");
                    newArr.push(data.caption);
                    newArr.push("><input onclick='showLineInfo(this.tabIndex,this.id)' tabIndex=");
                    newArr.push(tabId);
                    newArr.push(" id=");
                    newArr.push(data.index);
                    newArr.push(" readOnly='true' value='");
                    newArr.push(data.dataProperties.colName);
                    newArr.push("'></input></div>");
                    return newArr.join("");
                }
            } else {
                var newArr = [];
                newArr.push("<div onclick='showLineInfo(this.tabIndex,this.id)' tabIndex=");
                newArr.push(tabId);
                newArr.push(" id=");
                newArr.push(data.index);
                newArr.push(">");
                newArr.push(editType);
                newArr.push("</div>");
                return newArr.join("");
            }
			/*
			 if(flag){
			 switch(edType){
			 case "TextBox":{
			 str = "<div class='easyui-panel  xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><input type='text' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" readOnly='true' value="+data.dataProperties.colName+"></input></div>";
			 break;
			 }
			 case "RichTextBox":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/RichTextBox.jpg'></img></div>";
			 break;
			 }
			 case "ComboBox":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/Tree.jpg'></img></div>";
			 break;
			 }
			 case "ComboTree":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/ComboTree.jpg'></img></div>";
			 break;
			 }
			 case "ComboGrid":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/ComboGrid.jpg'></img></div>";
			 break;
			 }
			 case "DateBox":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/DataBox.jpg'></img>----<br/>"+
			 "<img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/DataBox.jpg'></img></div>";
			 break;
			 }
			 case "RadioBox":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/RadioBox.jpg'></img></div>";
			 break;
			 }
			 case "CheckBox":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/CheckBox.jpg'></img></div>";
			 break;
			 }
			 case "Button":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><a onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" href='#' class='easyui-linkbutton'>"+data.dataProperties.colName+"</a></div>"
			 break;
			 }
			 case "HyperLink":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><a onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" href='#' >"+data.dataProperties.colName+"</a></div>"
			 break;
			 }
			 case "ImageBox":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/imageBox.gif'></img></div>";
			 break;
			 }
			 case "BrowserBox":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/BrowserBox.jpg'></img></div>";
			 break;
			 }
			 case "DataGrid":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/ComboBox.jpg'></img></div>";
			 break;
			 }
			 case "TreeGrid":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/TreeGrid.jpg'></img></div>";
			 break;
			 }
			 case "Tree":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><img onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" src='editTypeImages/Tree.jpg'></img></div>";
			 break;
			 }
			 case "Label":{
			 str = "<div  class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><lableonclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+">"+data.dataProperties.colName+"</lable></div>";
			 break;
			 }
			 case "Password":{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><input type='text' onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" readOnly='true' value='***********'></input></div>";
			 break;
			 }
			 default :{
			 str = "<div class='easyui-panel xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+data.caption+"><input  onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+" type='text' readOnly='true' value="+data.dataProperties.colName+"></input></div>";
			 }
			 }
			 }else{
			 str = "<div onclick='showLineInfo(this.tabIndex,this.id)' tabIndex="+tabId+" id="+data.index+">"+edType+"</div>";
			 }
			 return str;*/
        },
        /**把表名字段名等转化为合格的数据库属性名称
         * @param  illegal_name 不合法的规格名称
         * @returns licit_name  合法的规格名称*/
        covToDBRule:function(illegal_name){
            var k1 = new RegExp(/^[_a-zA-Z]$/);
            var k2 = new RegExp(/^[_a-zA-Z0-9]$/);
            var str="";
            var flag=false;
            for(var i=0;i<illegal_name.length;i++){
                if(!flag){
                    if(k1.test(illegal_name[i])){
                        str +=illegal_name[i];
                        flag=true;
                    }
                }else{
                    if(k2.test(illegal_name[i]))
                        str +=illegal_name[i];
                }
            }
            var len = str.length;
            if(len > _MAX_FIELD_LEN_){
                len = _MAX_FIELD_LEN_;
            }
            var licit_name = str.substring(0,len);
            return licit_name;
        },
        /**
         * 通过表key获得改表
         * @param tabKey 表的唯一key
         * @returns form 返回表对象*/
        getFormByKey:function(tabKey){
            var form = $.fn.module.getModule().dataSource.formList;
            for(var i=0;i<form.length;i++){
                if(form[i].key==tabKey){
                    return form[i];
                }
            }
        },
        /**
         * 根据值获得查询连接条件key
         * @param value 下拉框的key
         * @returns value 下拉框对应值的显示值*/
		/* sqlQueryType:function(value){
		 for(var i=0; i<_sqlQueryWhere.length; i++){
		 if (_sqlQueryWhere[i].key == value) return _sqlQueryWhere[i].caption;
		 }
		 return value;
		 },*/
        /**去掉单个对象中字符串两边的空格
         * @param obj*/
        removeBlank:function(obj){
            //var jsonObj = $.parseJSON(obj);
            $.each(obj,function(k,v){
				/*if($.isArray()){
				 removeBlank(v);
				 }else{*/
                //var temp = v.replace().replace(/[ ]/g,"");
                if($.type(v) === "string"){
                    obj[k] = $.trim(v);
                }
				/*	}*/
            });
            return obj;
        },
        /**校验表名，字段名，及模块名是否合法
         * @param flag  判断表，字段或者模块的标识
         * @param allKey 获得所有的字段名表名，或者模块名的集合
         * @param key*/
        checkKey:function(flag,allKey,key){
            if(flag=="moduless"){
                //var =false;
                if(key){
                    var checkArray=[];
                    if(allKey){
                        for(var i=0;i<allKey.length;i++){
                            checkArray.push(allKey[i].toUpperCase());
                        }
                    }
                    if($.inArray(key.toUpperCase(), checkArray)>=0){
                        //	alert("模块集合key已经存在");
                        $.messager.alert("警告","模块集合key已经存在!",'warning');
                        return true;
                    }
                    return false;
                }

            }else if(flag=="module"){
                if(key){
                    var checkArray=[];
                    if(allKey){
                        for(var i=0;i<allKey.length;i++){
                            checkArray.push(allKey[i].toUpperCase());
                        }
                    }
                    if($.inArray(key.toUpperCase(), checkArray)>=0){
                        //alert("模块key已经存在");
                        $.messager.alert("警告","模块key已经存在!",'warning');
                        return true;
                    }
                    return false;
                }
            }else if(flag=="table"){
                var checkArray=[];
                var forms =_moduleProp.dataSource.formList;
                var pop = 0;
                if(key){
                    if(key.substr(0,3).toUpperCase()=="SYS"){
                        //	alert("表名不能以SYS开头");
                        $.messager.alert("警告","表名不能以SYS开头!",'warning');
                        return true;
                    }else{
                        if(forms){
                            for(var i=0;i<forms.length;i++){
                                checkArray.push(forms[i].tableName.toUpperCase());
                            }
                            $.each(checkArray,function(k,v){
                                if(v==key.toUpperCase()){
                                    pop++;
                                }
                            });
                            if(pop>=2){
                                $.messager.alert("警告","表名已经存在,请更换表名!",'warning');
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
        },
        /**获得extension属性
         * @param tableIndex*/
        getExtension:function(tableIndex){
            var form = $.fn.module.getFormProp(tableIndex);
            var extension = form.extension;
            var queryPage = {
                "buttonList":[],
                "eventList":[],
                "stsList":[]
            },editPage ={
                "buttonList":[],
                "eventList":[],
                "stsList":[]
            };
            if(!extension){
                form.extension={
                    "queryPage":queryPage,
                    "editPage":editPage
                };
                return form.extension;
            }else{
                if(!extension.editPage){
                    extension.editPage = editPage;
                }else if(!extension.queryPage){
                    extension.queryPage = queryPage;
                }
                return extension;
            }
        },
        /**获得编辑界面按钮
         * @param tableIndex*/
        getEditPage:function(tableIndex){
            var editPage = $.fn.module.getExtension(tableIndex).editPage;
            if(!editPage){
                editPage = _editPage;
            }
            return editPage;
        },

        /**获得查询界面按钮
         * @param tableIndex*/
        getQueryPage:function(tableIndex){
            var queryPage = $.fn.module.getExtension(tableIndex).queryPage;
            if(!queryPage){
                queryPage = _queryPage;
            }
            return queryPage;
        },
        /**获得按钮属性
         * @param tableIndex
         * @param key  按钮唯一key
         * @param pageFlag 分辨页面标识editPage（编辑页面设计）queryPage（查询页面设计）
         * */
        getButtonProp:function(tableIndex,key,pageFlag){
            if(pageFlag=="editPage"){
                var buttonList = $.fn.module.getEditPage(tableIndex).buttonList;
                for(var i=0;i<buttonList.length;i++){
                    if(buttonList[i].key == key){
                        return buttonList[i];
                    }
                }
            }else if(pageFlag=="queryPage"){
                var buttonList = $.fn.module.getQueryPage(tableIndex).buttonList;
                for(var i=0;i<buttonList.length;i++){
                    if(buttonList[i].key == key){
                        return buttonList[i];
                    }
                }
            }
        },
        /**删除按钮事件
         * @param tableIndex
         * @param key 按钮唯一key
         * @param pageFlag 分辨页面标识editPage（编辑页面设计）queryPage（查询页面设计）
         * @param data*/
        delButtonEvents:function(tableIndex,key,pageFlag,data){
            var button = $.fn.module.getButtonProp(tableIndex,key,pageFlag);
            if(button){
                var eventList = button.eventList;
                var flag = $.fn.module.findButtonEvents(eventList,data.key);
                if(flag>=0){
                    button.eventList.splice(flag,1);
                }
            }
        },
        /**设置按钮事件
         * @param tableIndex
         * @param key 按钮唯一key
         * @param pageFlag  分辨页面标识editPage（编辑页面设计）queryPage（查询页面设计）
         * @param data*/
        setButtonEvents:function(tableIndex,key,pageFlag,data){
            var bt = $.fn.module.getButtonProp(tableIndex,key,pageFlag);
            if(bt){
                var eventList = bt.eventList;
                var flag = $.fn.module.findButtonEvents(eventList,data.key);
                if(flag>=0){
                    $.extend(eventList[flag],data);
                }else{
                    eventList.push(data);
                }
            }


        },
        /**设置菜单按钮事件
         * @param tableIndex
         * @param key 按钮唯一key
         * @param pageFlag  分辨页面标识editPage（编辑页面设计）queryPage（查询页面设计）
         * @param data 菜单按钮数据
         * */
        setMenuButton:function(tableIndex,key,pageFlag,data){
            var parentBt = $.fn.module.getButtonProp(tableIndex,key,pageFlag);
            if(parentBt){
                parentBt.buttonType ="menu";
                //var buttonList = parentBt.buttonList;
                parentBt.buttonList=data;
				/*	for(var i=0;i<data.length;i++){
				 var newButton  = {
				 "key":data[i].key,
				 "caption":data[i].caption,
				 "languageText":data[i].languageText,
				 "iconCls":data[i].iconCls,
				 "width":data[i].width,
				 "height":data[i].height,
				 "eventList":data[i].eventList,
				 "i18nKey":data[i].i18nKey,
				 "buttonType":"normal",
				 "buttonList":[]
				 }
				 buttonList.push(newButton);
				 }*/
            }
        },
        /**
         * 设置页面按钮
         * @param tableIndex
         * @param pageFlag
         * @param data
         */
        setPageButtons:function(tableIndex, pageFlag, data) {
            if(pageFlag=="editPage"){
                var page = $.fn.module.getEditPage(tableIndex);
                page.buttonList = $.extend(true, [], data);
            } else if(pageFlag=="queryPage"){
                var page = $.fn.module.getQueryPage(tableIndex);
                page.buttonList = $.extend(true, [], data);
            }
        },
        /**
         * 查找自定义按钮事件是否存在
         * @param eventList
         * @param eventKey
         */
        findButtonEvents:function(eventList,eventKey){
            var flag = -1;
            for(var i=0;i<eventList.length;i++){
                if(eventList[i].key == eventKey){
                    flag = i;
                    break;
                }
            }
            return flag;
        },
        /**获得按钮属性
         * @param tableIndex
         * @param key 按钮唯一key
         * @param pageFlag 分辨页面标识editPage（编辑页面设计）queryPage（查询页面设计）
         * */
        getButtonPropJson:function(tableIndex,key,pageFlag){
            var button = $.fn.module.getButtonProp(tableIndex,key,pageFlag);
            var buttonPropJson = $.extend(true,{},_buttonPropJson);
            $.each(buttonPropJson.rows,function(k,v){
                v.value = convertBooleanToString(button[v.code]);
            });
            return buttonPropJson;
        },

        /**设置按钮属性
         * @param tableIndex 表索引
         * @param key 按钮唯一key
         * @param pageFlag 分辨页面标识editPage（编辑页面设计）queryPage（查询页面设计）
         * @param buttonJson */
        setButtonProp:function(tableIndex,key,pageFlag,buttonJson){
            var button = $.fn.module.getButtonProp(tableIndex,key,pageFlag);
            if(button){
                var code = buttonJson["code"];
                var value = buttonJson["value"];
                button[code] = value;
                //$.extend(button,{[code]:value});
            }else{
                var form  = $.fn.module.getFormProp(tableIndex);
                if(form){
                    var extension = form.extension;
                    if(extension){
                        if(extension[pageFlag]){
                            //push
                            _button.eventList = [];
                            var newButton = $.extend(true,{},_button);
                            $.extend(newButton,{"key":buttonJson.key,"caption":buttonJson.caption,"iconCls":buttonJson.iconCls});
                            extension[pageFlag].buttonList.push(newButton);
                        }else{
                            //add
                            _button.eventList = [];
                            var extension = $.fn.module.getExtension(tableIndex);
                            var newButton = $.extend(true,{},_button);
                            $.extend(newButton,{"key":buttonJson.key,"caption":buttonJson.caption,"iconCls":buttonJson.iconCls});
                            if(pageFlag == "editPage"){
                                var editPage = $.extend(true,{},_editPage);
                                editPage.buttonList.push(newButton);
                                extension.editPage = editPage;
                            }else if(pageFlag == "queryPage"){
                                var queryPage = $.extend(true,{},_queryPage);
                                queryPage.buttonList.push(newButton);
                                extension.queryPage = queryPage;
                            }
                        }
                    }else{
                        _button.eventList = [];
                        var newButton = $.extend(true,{},_button,{"key":buttonJson.key,"caption":buttonJson.caption,"iconCls":buttonJson.iconCls});
                        var extension = $.extend(true,{},_extension);
                        if(pageFlag == "editPage"){
                            var editPage = $.extend(true,{},_editPage);
                            editPage.buttonList.push(newButton);
                            extension.editPage = editPage;

                        }else if(pageFlag == "queryPage"){
                            var queryPage = $.extend(true,{},_queryPage);
                            queryPage.buttonList.push(newButton);
                            extension.queryPage = queryPage;
                        }
                        $.extend(form,{"extension":extension});
                    }
                }
            }
        },
        /**
         * 查找自定义按钮的下标
         */
        findButtonPlace:function(buttonList,key){
            var flag = -1;
            for(var i=0;i<buttonList.length;i++){
                if(buttonList[i].key == key){
                    flag = i;
                    break;
                }
            }
            return flag;
        },
        /**
         * 删除按钮事件
         */
        deleteButton:function(tableIndex,key,pageFlag){
            var button = $.fn.module.getButtonProp(tableIndex,key,pageFlag);
            var form  = $.fn.module.getFormProp(tableIndex);
            if(form){
                var extension = form.extension;
                if(extension){
                    if(extension[pageFlag]){
                        var btList = extension[pageFlag].buttonList;
                        var flag = $.fn.module.findButtonPlace(btList,button.key);
                        if(flag>=0){
                            btList.splice(flag,1);
                        }
                    }
                }
            }
        },
        /**
         * 获得表中所有字段的属性
         */
        getAllField:function(index){
            var form = $.fn.module.getFormProp(index);
            var fieldList = form.fieldList;
            var resList = [];
            $.each(fieldList, function(i,field) {
                if(field.key && field.caption) {
                    resList.push({"key":field.key,"caption":field.caption});
                }
            });
            return resList;
        },
        getFieldOrder:function() {
            return _order;
        },
        /**
         * 返回where操作符数组
         * @returns {Array}
         */
        getSqlQueryWhere:function() {
            return _sqlQueryWhere;
        },
        getLangType:function() {
            return _langType;
        },
        getFlagData:function() {
            return _flagData;
        },
        getOrderByData:function() {
            return _orderByData;
        },
        getDataType:function() {
            return _dataTypeDic;
        },
        getSqlList:function(tableIndex, fieldIndex) {
            var sl = $.fn.module.getFieldProp(tableIndex, fieldIndex).editProperties.sqlList;
            return sl;
        },
        /**
         * 将sql对象保存到内存中
         */
        setSqlList:function(tableIndex, fieldIndex,sqlData) {
            var sl = $.fn.module.getFieldProp(tableIndex, fieldIndex).editProperties.sqlList;
            if(sqlData){
                sl.push(sqlData);
            }
        },
        saveSql:function(tableIndex, fieldIndex, data) {
            var ep = $.fn.module.getFieldProp(tableIndex, fieldIndex).editProperties;
            if(!ep.sqlList) {
                ep.sqlList = [];
            }
            var sl = ep.sqlList;
            var sql = null;
            var sqlKey = data["key"];
            var len=sl.length;
            if(sqlKey) { //修改
                for(var i=0;i<len;i++) {
                    if(sl[i]["key"] == sqlKey) {
                        sql = sl[i];
                        break;
                    }
                }
            }
            if(sql == null) { //新增，自动生成sqlkey
                var orderNo = 1;
                if(len > 0) {
                    var key = sl[len-1]["key"];
                    key = key.replace("sql","");
                    orderNo = parseInt(key)+1;
                }
                sql = {"key":"sql"+orderNo};
                sl.push(sql);
            }
            sql["caption"] = data["caption"];
            sql["content"] = data["content"];
            sql["sqlSelect"] = data["sqlSelect"];
            sql["isCheckAuth"] = data["isCheckAuth"];
            sql["isUseCache"] = data["isUseCache"];
            sql["entityName"] = data["entityName"];
            sql["selectInfo"] = data["selectInfo"];
            sql["dataStatus"] = data["dataStatus"];
            sql.setValueItemList= [];
            sql.sqlWhereItemList = [];
            sql.orderByItemList = [];
            sql.groupByItemList = [];
            var rows = data["rows"];
            for(var i=0,len=rows.length;i<len;i++){
                var rowdata = $.fn.module.removeBlank(rows[i]);
                if(rowdata["flag"]=="sqlWhere"){
                    if(rowdata.key || rowdata.caption || rowdata.column){
                        temp = $.extend(true,{},_itemProp,rowdata);
                        sql.sqlWhereItemList.push(temp);
                    }

                }
                else if(rowdata["flag"]=="setValue"){
                    if(rowdata.key || rowdata.caption || rowdata.column){
                        temp = $.extend(true,{},_itemProp,rowdata);
                        sql.setValueItemList.push(temp);
                    }

                }
                else if(rowdata["flag"]=="groupBy"){
                    if(rowdata.key || rowdata.caption || rowdata.column){
                        temp = $.extend(true,{},_itemProp,rowdata);
                        sql.groupByItemList.push(temp);
                    }

                }
                else if(rowdata["flag"]=="orderBy"){
                    if(rowdata.key || rowdata.caption || rowdata.column){
                        temp = $.extend(true,{},_itemProp,rowdata);
                        sql.orderByItemList.push(temp);
                    }

                }
            }
        },
        convertSqlToData:function(sqlData) {
            var sqlProp = sqlData;
            var rows = [];
            $.each(sqlProp.sqlWhereItemList,function(k,v){
                var sql = $.extend(true,{},v,{"flag":"sqlWhere"});
                rows.push(sql);
            });
            $.each(sqlProp.setValueItemList,function(k,v){
                var sql = $.extend(true,{},v,{"flag":"setValue"});
                rows.push(sql);
            });
            $.each(sqlProp.groupByItemList,function(k,v){
                var sql = $.extend(true,{},v,{"flag":"groupBy"});
                rows.push(sql);
            });
            $.each(sqlProp.orderByItemList,function(k,v){
                var sql = $.extend(true,{},v,{"flag":"orderBy"});
                rows.push(sql);
            });
            sqlData["rows"] = rows;
            return sqlData;
        },
        delSql:function(tableIndex, fieldIndex, data) {
            var sl = $.fn.module.getFieldProp(tableIndex, fieldIndex).editProperties.sqlList;
            for(var i=0,len=sl.length;i<len;i++) {
                if(sl[i]["key"] == data["key"]) {
                    sl.splice(i,1);
                    break;
                }
            }
        },
        getSqlComboData:function(tableIndex, fieldIndex) {
            var sl = $.fn.module.getFieldProp(tableIndex, fieldIndex).editProperties.sqlList;
            var data = [];
            if(sl) {
                $.each(sl, function(i, sql) {
                    data.push({"key":sql["key"],"caption":sql["caption"]});
                });
            }
            return data;
        },
        getEntityName:function(tableIndex, fieldIndex) {
            var mp = $.fn.module.getModuleProp();
            var val = mp["key"].toLowerCase();
            if(tableIndex) {
                var frp = $.fn.module.getFormProp(tableIndex);
                val = val + "." + frp["key"];
                if(fieldIndex) {
                    var flp = $.fn.module.getFieldProp(tableIndex, fieldIndex);
                    val = val + "." + flp["key"];
                }
            }
            return val;
        },
        /**
         * 表字段位置互换，将sourceFieldIndex和destFieldIndex对应的字段交换位置
         * @param tableIndex
         * @param sourceFieldIndex
         * @param destFieldIndex
         */
        swapTableField:function(tableIndex, sourceFieldIndex, destFieldIndex) {
            var form  = $.fn.module.getFormProp(tableIndex);
            var fl = form.fieldList;
            var sIndex=0,dIndex=0;
            for(var i=0;i<fl.length;i++) {
                if(fl[i].index == sourceFieldIndex) {
                    sIndex = i;
                } else if(fl[i].index == destFieldIndex) {
                    dIndex = i;
                    break;
                }
            }
            var tmpField = fl[dIndex];
            fl[dIndex] = fl[sIndex];
            fl[sIndex] = tmpField;
        },
        /**
         * 获取字段sql参数映射关系
         * @returns {___anonymous282_294}
         */
        getSystemFieldToSqlParamMap:function() {
            return _systemFieldToSqlParamMap;
        },
        /**
         * 根据字段类型改变字段长度和精度
         */
        doChangeSize:function(obj,changeVal,fieldKey,fieldKey2,tableKey){
            if(changeVal) {
                try {
                    var td = $(obj).closest("td[field]");
                    if(td) {
                        var tr = td.parent();
                        var rowIndex =tr.attr("datagrid-row-index");
                        if(rowIndex) {
                            var rows = $("#"+tableKey).datagrid("getRows");
                            if(changeVal=="dtString"){
                                tr.find("div.datagrid-editable").each(function(){
                                    var ed=$.data(this,"datagrid.editor");
                                    if(ed.field==fieldKey){
                                        ed.actions.setValue(ed.target,"100");
                                    }else if(ed.field==fieldKey2){
                                        ed.actions.setValue(ed.target,"0");
                                    }
                                });
                            }else if(changeVal=="dtLong"){
                                tr.find("div.datagrid-editable").each(function(){
                                    var ed=$.data(this,"datagrid.editor");
                                    if(ed.field==fieldKey){
                                        ed.actions.setValue(ed.target,"38");
                                    }else if(ed.field==fieldKey2){
                                        ed.actions.setValue(ed.target,"0");
                                    }
                                });
                            }else if(changeVal=="dtDouble"){
                                tr.find("div.datagrid-editable").each(function(){
                                    var ed=$.data(this,"datagrid.editor");
                                    if(ed.field==fieldKey){
                                        ed.actions.setValue(ed.target,"38");
                                    }else if(ed.field==fieldKey2){
                                        ed.actions.setValue(ed.target,"2");
                                    }
                                });
                            }
                        }
                    }
                } catch(ex) {
                    alert(ex);
                }
            }
        },
        /**
         * 添加公用字段
         * @param tableIndex 表索引
         * @param fieldsData 添加的字段数据
         * @param entityName 公用的模块名
         */
        setCommonFields:function(fieldsData,tableIndex,entityName){
            var form = $.fn.module.getFormProp(tableIndex);
            var fl=form.fieldList;
            for(var j=0;j<fieldsData.length;j++){
                fieldsData[j]["entityName"] = entityName;
                fieldsData[j]["index"] = _index++;
                fl.push(fieldsData[j]);
            }
            form["isReferCommon"] = true;
        },
        /**
         * 添加字段
         * @param tableIndex 表索引
         * @param fieldsData 添加的字段数据
         * @param entityName 模块名
         */
        setFields:function(fieldsData,tableIndex,entityName){
            var form = $.fn.module.getFormProp(tableIndex);
            var fl=form.fieldList;
            for(var j=0;j<fieldsData.length;j++){
                fieldsData[j]["entityName"] = entityName;
                fieldsData[j]["index"] = _index++;
                fl.push(fieldsData[j]);
            }
        },

        /**
         * 添加自定义查询语句
         * @param tableIndex 表索引
         * @param content 语句内容
         */
        setQuerySql:function(tableIndex,content){
            var queryPage = $.fn.module.getQueryPage(tableIndex);
            var sql = $.extend(true,{}, _sqlProp);
            sql["content"] = content;
            queryPage["querySql"]=sql;
        },
        /**
         * 添加同步字段
         * @param tableIndex 表索引
         * @param fieldsData 添加的字段数据
         * @param tableName 同步的表名
         */
        setSyncFields:function(fieldsData,tableIndex,tableName){
            var form = $.fn.module.getFormProp(tableIndex);
            var fl=form.fieldList;
            for(var j=0;j<fieldsData.length;j++){
                if(fieldsData[j]["entityName"]){
                    fieldsData[j]["syncEntityName"] = fieldsData[j]["entityName"];
                    fieldsData[j]["entityName"]="";
                    fieldsData[j]["fromEntityName"] =tableName;
                }else{
                    fieldsData[j]["syncEntityName"] =tableName;
                    fieldsData[j]["fromEntityName"] =tableName;
                }
                fieldsData[j]["index"] = _index++;
                fl.push(fieldsData[j]);
            }
        },
        /**
         * 获取模块数据树，用于导入导出设置使用
         * @param checkKey
         */
        getModuleDataTree:function(checkKey) {
            var treeData = [];
            var node = {"iconSkin":"icon01"};
            node["id"] = null;
            node["key"] = _moduleProp.moduleProperties.key;
            var caption = _moduleProp.moduleProperties.caption;
            caption += "（选中即可启用";
            if(checkKey == "enableImport") {
                caption += "，右键设置是否检查字段重复";
            }
            caption += "）";
            node["name"] = caption;
            node["nocheck"] = true;
            node["open"] = true;
            treeData.push(node);
            var fl = _moduleProp.dataSource.formList;
            if(fl && fl.length) {
                for(var i=0,len=fl.length;i<len;i++) {
                    var fr = fl[i];
                    var fNode = {"iconSkin":"icon04"};
                    fNode["id"] = fr.index;
                    fNode["pid"] = null;
                    fNode["key"] = fr.key;
                    fNode["name"] = fr.caption;
                    if(fr[checkKey]) {
                        fNode["checked"] = true;
                    } else {
                        fNode["checked"] = false;
                    }
                    treeData.push(fNode);
                    var fieldList = fr.fieldList;
                    if(fieldList && fieldList.length) {
                        for(var j=0,jLen=fieldList.length;j<jLen;j++) {
                            var field = fieldList[j];
                            if(!field.isVirtual && field.key) {//非虚拟字段
                                var pNode = {"iconSkin":"icon03"};
                                pNode["id"] = field.index;
                                pNode["pid"] = fr.index;
                                pNode["key"] = field.key;
                                pNode["name"] = field.caption;
                                if(field[checkKey]) {
                                    pNode["checked"] = true;
                                } else {
                                    pNode["checked"] = false;
                                }
                                if(checkKey == "enableImport") {
                                    pNode["checkImportUnique"] = field.checkImportUnique;
                                    if(field.checkImportUnique) {
                                        pNode["iconSkin"] = "icon05";
                                    }
                                }
                                treeData.push(pNode);
                            }
                        }
                    }
                    if(checkKey == "enableExport" && fr.isMaster) {
                        //如果是主表的excle导出设置树数据，将系统字段加上。
                        /*2018/8/10*/
                        // var systemFields = fr.exportSystemFieldList;
                        var systemFields = fr.systemFields;
                        /*2018/8/10*/
                        if(systemFields && systemFields.length) {
                            for(var j=0,jLen=systemFields.length;j<jLen;j++) {
                                var field = systemFields[j];
                                var pNode = {"iconSkin":"icon09"};
                                pNode["pid"] = fr.index;
                                pNode["key"] = field.columnName;
                                pNode["name"] = field.caption;
                                pNode["checked"] = field.enableExport;
                                treeData.push(pNode);
                            }
                        }
                    }

                }
            }
            return treeData;
        },
        /**
         * 将导入导出设置更新至相关对象
         * @param treeData
         * @param checkKey
         */
        setImportExportData:function(treeData, checkKey) {
            if(treeData && treeData.length) {
                var fl = treeData[0].children;
                if(fl && fl.length) {
                    for(var i=0,len=fl.length;i<len;i++) {
                        var fr = fl[i];
                        var frm = $.fn.module.getFormProp(fr.id);
                        if(fr["checked"]) {
                            frm[checkKey] = true;
                        } else {
                            frm[checkKey] = false;
                        }
                        var fieldList = fr.children;
                        if(fieldList && fieldList.length) {
                            for(var j=0,jLen=fieldList.length;j<jLen;j++) {
                                var field = fieldList[j];
                                if(field.id!==0 && !field.id) {
                                    //如果叶子中id属性不存在，说明是表示系统字段的叶子，修改系统字段中是否导出设置（目前没有使用系统字段中的enableexport）
                                    var fld = $.fn.module.getFieldPropByKey(field.pid, field.key);
                                    if(field["checked"]) {
                                        fld[checkKey] = true;
                                    } else {
                                        fld[checkKey] = false;
                                    }
                                    //修改新增系统字段中是否导出设置，因为是后期添加系统字段导出的保存工作，所以出现的冗余字段
                                    var form = $.fn.module.getFormProp(field.pid);
                                    var exportSystemFieldList = form.exportSystemFieldList;
                                    if(field["checked"]) {
                                        for(var ii in exportSystemFieldList) {
                                            var exportSystemField = exportSystemFieldList[ii];
                                            if(exportSystemField.columnName == field.key) {
                                                exportSystemField.enableExport = true;
                                                break;
                                            }
                                        }
                                    } else {
                                        for(var ii in exportSystemFieldList) {
                                            var exportSystemField = exportSystemFieldList[ii];
                                            if(exportSystemField.columnName == field.key) {
                                                exportSystemField.enableExport = false;
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    //非系统字段，修改方式不变
                                    var fld = $.fn.module.getFieldProp(field.pid, field.id);
                                    if(field["checked"]) {
                                        fld[checkKey] = true;
                                    } else {
                                        fld[checkKey] = false;
                                    }
                                    if(checkKey == "enableImport") {
                                        fld["checkImportUnique"] = field.checkImportUnique;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },//返回数据状态列表
        getDataStatus:function() {
            return _dataStatus_;
        },
        //返回空的字段属性对象
        setNewField:function(index,rowData){
            var fp = $.extend(true,{},_fieldProp, {"index":_index++});
            fp.dataProperties=$.extend(true,{},_dataProperties,rowData);
            fp.editProperties=$.extend(true,{},_editProperties);
            fp.queryProperties=$.extend(true,{},_queryProperties);
            rowData["index"] = fp.index;
            var form = $.fn.module.getFormProp(index);
            var mp = $.fn.module.getModuleProp();
            if(form.isMaster) {
                if(mp.type=="dict"){
                    fp.queryProperties.showInSearch = true;
                    fp.queryProperties.showInGrid = true;
                }else{
                    fp.queryProperties.showInGrid = false;
                }
            } else {
                fp.queryProperties.showInGrid = true;
            }
            return fp;
        },
        getEditTypeImgDic:function() {
            return _editTypeImgDic;
        },//生成新的占位符字段
        getNullField:function(opt) {
            var editProp = {"editType":"" ,"multiple":"false" ,"visible":"true" ,"readOnly":"false" ,"charCase":"normal" ,"defaultValue":"" ,
                "isPassword":"false" ,"align":"left", "rows":opt.rows||"1" ,"cols":opt.cols||"1", "maxSize":"2000" ,"dtFormat":"" ,"width":"","height":"" ,
                "top":"" ,"left":""};
            var nullfield = {"caption":"", "key":"" ,"languageText":"" ,"isVirtual":"false" ,"index":"" ,"tabRows":"" ,"tabCols":"",
                "dataProperties":{}, "queryProperties":{}, "editProperties":editProp};
            var maxFieldIndex = _index++;
            var newField = $.extend({},nullfield,{"index":maxFieldIndex,"tabRows":opt.tabRows,"tabCols":opt.tabCols});
            return newField;
        }
    };
});