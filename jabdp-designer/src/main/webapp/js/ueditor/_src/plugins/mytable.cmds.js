/* 
 * 对表格的新的插入，创建，修改操作
 * */

//根据传入的宽高，插入对应表格
UE.plugin.register('mycreatetable', function() {
    return {
        commands: {
            'mycreatetable': {
            	queryCommandState: function () {
                    return UE.UETable.getTableItemsByRange(this).table ? -1 : 0;
                },
            	execCommand: function(cmd, opt) {
            		var me = this;
            		//若没有定表格行数和列数数量
                    if (!opt) {
                        opt = utils.extend({}, {
                            numCols: me.options.defaultCols,
                            numRows: me.options.defaultRows,
                            tdvalign: me.options.tdvalign
                        });
                    }
                    var range = me.selection.getRange(),
                        start = range.startContainer,
                        firstParentBlock = domUtils.findParent(start, function (node) {
                            return domUtils.isBlockElm(node);
                        }, true) || me.body;
                    var defaultValue = UE.UETable.getDefaultValue(me),
                    	tdWidth = 68,
                    	tdHeight = 28,
                        numcol = Math.round(opt.width/tdWidth),
                    	numrow = Math.round(opt.height/tdHeight);
                    if(opt.numcol!=''||opt.numrow!='') {
                    	if(opt.numcol!='') {
                    		numcol = opt.numcol;
                    		tdWidth = Math.round(opt.width/numcol);
                    	}
                    	if(opt.numrow!='') {
                    		numrow = opt.numrow;
                    		tdHeight = Math.round(opt.height/numrow);
                    	}
                    }
                    else if(opt.tdwidth!=''||opt.tdheight!='') {
                    	if(opt.tdwidth!='') {
                        	tdWidth = opt.tdwidth;
                        	numcol = Math.round(opt.width/tdWidth);
                        }
                        if(opt.tdheight!='') {
                        	tdHeight = opt.tdheight;
                        	numrow = Math.round(opt.height/tdHeight);
                        }
                    }
                    firstParentBlock.offsetWidth = tdWidth * numcol;
                    firstParentBlock.offsetHeight = tdHeight * numrow;
                    //todo其他属性
                    //如果opt没有新的valign赋值，valign=预设的值
                    !opt.tdvalign && (opt.tdvalign = me.options.tdvalign);
                    function createTable (opt, tdWidth, tdHeight) {
                    	var html = [];
                    	for(var r = 0; r < numrow; r++) {
                    		if(r==0) {
                    			//html.push('<tr style="padding:0px">');
                        		for (var c = 0; c <= numcol; c++) {
                        			if(c===0) {
                        				//html.push('<th width=20 height=20 vAlign="top" style="padding:0px"></th>');
                        			}
                        			else {
                        				//html.push('<th width=' + tdWidth + ' vAlign="top" style="padding:0px"></th>');
                        			}
                        		}
                        		//html.push('</tr>');
                    		}
                    		html.push('<tr style="padding:0px">');
                    		for(var c = 0; c < numcol; c++) {
//                    			if(c===0) {
//                    				//html.push('<th height="' + tdHeight + '" width="0" vAlign="top" style="padding:0px;border-width:0px"></th>');
//                    			}
                    			html.push('<td width="' + (tdWidth-9) + '" height="' + (tdHeight-9) 
                    					+ '" valign="middle" align="center" rowspan="1" colspan="1" style="padding:4px"></td>');
                    		}
                    		html.push('</tr>');
                    	}
                    	return '<table style="border-collapse:collapse;padding:0px"><tbody>' + html.join('') + '</tbody></table>';
                    }
                    //插入表格
                    me.execCommand("inserthtml", createTable(opt, tdWidth, tdHeight));
            	}
            }
        }
    }
});

/*自动插入主表所有字段*/
UE.plugin.register('myautocreatetable', function() {
    return {
        commands: {
            'myautocreatetable': {
            	execCommand: function(cmd, opt) {
            		var me = this;
            		var numcol = opt.numcol;
            		var numrow = opt.numrow;
                    var html = [];
                    for(var r = 0; r < numrow; r++) {
                    	if(r==0) {
                    			//html.push('<tr style="padding:0px">');
                        	for (var c = 0; c <= numcol; c++) {
                        		if(c==0) {
                        				//html.push('<th width=20 height=20 vAlign="top" style="padding:0px"></th>');
                        		}
                        		else {
                        				//html.push('<th width=' + tdWidth + ' vAlign="top" style="padding:0px"></th>');
                        		}
                        	}
                        		//html.push('</tr>');
                    	}
                    	html.push('<tr style="padding:0px">');
                    	for (var c = 0; c <= numcol; c++) {
                    		if(c==0) {
                    				//html.push('<th height="' + tdHeight + '" width="0" vAlign="top" style="padding:0px;border-width:0px"></th>');
                    		}
                    		else if(c%2==1) {
                    			html.push('<td width="68" height="22"  valign="bottom" align="right" rowspan="1" colspan="1" style="padding:4px"></td>');
                    		}
                    		else {
                    			html.push('<td width="163" height="22"  valign="bottom" align="left" rowspan="1" colspan="1" style="padding:4px"></td>');
                    		}
                    	}
                    	html.push('</tr>');
                    }
                    var htmlstr = '<table style="border-collapse:collapse;padding:0px"><tbody>' + html.join('') + '</tbody></table>';
                    //插入表格
                    me.execCommand("inserthtml", htmlstr);
            	}
            }
        }
    }
});

/*插入行*/
UE.plugin.register('myinsertrow', function() {
	return {
        commands: {
            'myinsertrow': {
            	queryCommandState: function () {
            		var tableItems = UE.UETable.getTableItemsByRange(this),
            		cell = tableItems.cell;
            		return cell && (cell.tagName == "TD" || (cell.tagName == 'TH' && tableItems.tr !== tableItems.table.rows[0])) ? 0 : -1;
            	},
            	execCommand: function () {
            		var rng = this.selection.getRange(),
                    bk = rng.createBookmark(true);
            		var tableItems = UE.UETable.getTableItemsByRange(this),
                    cell = tableItems.cell,
                    table = tableItems.table,
                    ut = UE.UETable.getUETable(table),
                    cellInfo = ut.getCellInfo(cell);
            		UE.dom.domUtils.removeAttributes(cell,"width");
            		UE.dom.domUtils.setAttributes(cell,{height:22});
            		if (!ut.selectedTds.length) {
            			ut.insertRow(cellInfo.rowIndex, cell);
            		}
            		else {
            			var range = ut.cellsRange;
            			for (var i = 0, len = range.endRowIndex - range.beginRowIndex + 1; i < len; i++) {
            				ut.insertRow(range.beginRowIndex, cell);
            			}
            		}
            		rng.moveToBookmark(bk).select();
            		if (table.getAttribute("interlaced") === "enabled") {
            			this.fireEvent("interlacetable", table);
            		}
            	}
            }
        }
	}
});

//最理想的插入行高度计算方式，因为有余数处理问题，没有最终实现
//UE.plugin.register('myinsertrow', function() {
//    return {
//        commands: {
//            'myinsertrow': {
//            	execCommand: function(cmd) {
//            		me = this;//ueditor
//            		var rng = me.selection.getRange(),
//                    bk = rng.createBookmark(true);
//            		var tableItems = UE.UETable.getTableItemsByRange(this),
//                    cell = tableItems.cell,
//                    table = tableItems.table,
//                    ut = UE.UETable.getUETable(table),
//                    cellInfo = ut.getCellInfo(cell);
//            		//重新设置行的高度所需的参数
//            		var table_height = ut.table.clientHeight;//插入前的高度
//            		var table_rownum = ut.rowsNum;//插入行前的表格行数
//            		var table_colnum = ut.colsNum;//表格列数
//            		var rowIndex = cellInfo.rowIndex;//焦点位置
//            		var insert_rownum;//插入的行数
//            		if(!ut.selectedTds.length) {
//            			insert_rownum = 1;
//            		}
//            		else {
//            			var range = ut.cellsRange;
//            			insert_rownum = range.endRowIndex - range.beginRowIndex+1;
//            		}
//            		var newtd_height = Math.round(table_height/(ut.rowsNum+insert_rownum));//新插入的td高度
//                    //插一行
//            		if(!ut.selectedTds.length) {
//            			ut.insertRow(cellInfo.rowIndex, cell);
//            		}
//            		//插多行
//            		else {
//            			var range = ut.cellsRange;
//            			for(var i = 0, len = range.endRowIndex - range.beginRowIndex + 1; i < len; i++) {
//            				ut.insertRow(range.beginRowIndex, cell);
//            			}
//            		}
//            		rng.moveToBookmark(bk).select();
//            		//重新设置插入后td的height,以保持表格整体的宽高不变
//            		var tb = ut.table.children[0];
//            		for(var i=0; i<tb.childElementCount; i++) {
//            			var trs = tb.children[i];
//            			//新插入的列，高度为Math.round(table_height/ut.rowsNum)
//            			if(i < rowIndex + insert_rownum && i >= rowIndex) {
//            				for(var j=0; j<trs.childElementCount; j++) {
//            					UE.dom.domUtils.setAttributes(trs.children[j],{height:newtd_height});
//            					trs.children[j].style.padding = 0;
//            				}
//            			}
            			//原先的列，高度减少Math.round(node.height/ut.rowsNum)，目的是按自身比例抽取高度
//            			else {
//            				for(var j=0; j<trs.childElementCount; j++) {
//            					UE.dom.domUtils.setAttributes(trs.children[j],
//            							{height:Math.round(trs.children[j].height*(1-insert_rownum/ut.rowsNum))});
//            					trs.children[j].style.padding = 0;
//            				}
//            			}
//            		}
            		//处理按自身比例抽取高度，余数问题，余数将以1为单位顺序分配
//            		short = table_height-ut.table.clientHeight;
//            		if(short>0) {
//            			for(var i=0; i<tb.childElementCount; i++) {
//                			var trs = tb.children[i];
//                			//新插入的列，高度为Math.round(table_height/ut.rowsNum)
//                			if(i < rowIndex + insert_rownum && i >= rowIndex) {
//                				for(var j=0; j<trs.childElementCount; j++) {
//                					UE.dom.domUtils.setAttributes(trs.children[j],{height:(trs.children[j].height+1)});
//                					
//                				}
//                			}
//                			//原先的列，高度减少Math.round(node.height/ut.rowsNum)，目的是按自身比例抽取高度
//                			else {
//                				for(var j=0; j<trs.childElementCount; j++) {
//                					UE.dom.domUtils.setAttributes(trs.children[j],
//                							{height:Math.round(trs.children[j].height*(1-insert_rownum/ut.rowsNum))});
//                					trs.children[j].style.padding = 0;
//                				}
//                			}
//                		}
//            		}
            		//else if(short<0)
//                }
//            }
//        }
//    }
//});

//插入列
UE.plugin.register('myinsertcol', function() {
	return {
        commands: {
            'myinsertcol': {
            	queryCommandState: function (cmd) {
                    var tableItems = UE.UETable.getTableItemsByRange(this),
                        cell = tableItems.cell;
                    return cell && (cell.tagName == "TD" || (cell.tagName == 'TH' && cell !== tableItems.tr.cells[0])) ? 0 : -1;
                },
                execCommand: function (cmd) {
                    var rng = this.selection.getRange(),
                        bk = rng.createBookmark(true);
                    if(this.queryCommandState(cmd) == -1) {
                    	return;
                    }
                    var cell = UE.UETable.getTableItemsByRange(this).cell,
                        ut = UE.UETable.getUETable(cell),
                        cellInfo = ut.getCellInfo(cell);
                    var table_width = ut.table.clientWidth;//插入前的宽度
            		var table_colnum = ut.colsNum;//插入行前的表格列数
            		var table_rownum = ut.rowsNum;//表格行数
            		var colIndex = cellInfo.colIndex;//焦点位置
            		var insert_colnum;//插入的行数
            		if(!ut.selectedTds.length) {
            			insert_colnum = 1;
            		}
            		else {
            			var range = ut.cellsRange;
            			insert_colnum = range.endColIndex - range.beginColIndex + 1;
            		}
                    if (!ut.selectedTds.length) {
                        ut.insertCol(cellInfo.colIndex, cell);
                    }
                    else {
                        var range = ut.cellsRange;
                        for(var i = 0, len = range.endColIndex - range.beginColIndex + 1; i < len; i++) {
                            ut.insertCol(range.beginColIndex, cell);
                        }
                    }
                    rng.moveToBookmark(bk).select(true);
                    var average_width = table_width/(ut.colsNum);//算数平均列宽，一般有小数
            		var newtd_width = 0;//新插入的td宽度
            		while(newtd_width < average_width) {
            			newtd_width_before = newtd_width;
            			newtd_width = newtd_width + ut.colsNum;
            		}
            		//取与平均列宽绝对值小的width为新插入的td宽度
            		newtd_width = newtd_width_before;
            		//重新设置插入后td的width
            		var tb = ut.table.children[0];
            		var indexTable = ut.indexTable;
            		for(var i=0; i<tb.childElementCount; i++) {
            			var trs = tb.children[i];
            			var c = 0;//该列插入td之前的td个数
            			for(var j=0,l=0; j<indexTable[i].length && l<colIndex; j++) {
            				//<td>的代码位置在本行的单元格
            				if(indexTable[i][j].rowIndex==i) {
            					c++;
            					l = l + indexTable[i][j].colSpan;
            				}
            				//<td>的代码位置不在本行的单元格
            				else {
            					l = l + indexTable[i][j].colSpan;
            				}
            			}
            			//新插入的td的宽设为newtd_width
            			for(var j=c; j<c+insert_colnum; j++) {
            				UE.dom.domUtils.setAttributes(trs.children[j],{width:newtd_width-1});
            			}
            		}
            		//去除table宽度
            		domUtils.removeAttributes(ut.table,'width height');
            		//余数等于插入列宽度，且刚好被最小单元格总数整除
            		var short = newtd_width/ut.colsNum;
            		for(var i=0; i<tb.childElementCount; i++) {
            			var trs = tb.children[i];
            			var c = 0;//td节点列标
            			for(var j=0; j<indexTable[i].length; j++) {
            				//<td>的代码位置在本行的单元格
            				if(indexTable[i][j].rowIndex==i) {
            					UE.dom.domUtils.setAttributes(trs.children[c],{width:trs.children[c].width-short*insert_colnum});
            					if(indexTable[i][j].colSpan>1) {
            						var x = indexTable[i][j].colSpan;
            						for(var l=1; l<x; l++) {
            							j++;
            						}
            					}
            					c++;
            					l = l + indexTable[i][j].colSpan;
            				}
            			}
            		}
                }
            }
        }
	}
});

/*单元格属性设置*/
UE.plugin.register('mysetpadding', function() {
	return {
        commands: {
            'mysetpadding': {
                execCommand: function (cmd, padding) {
                	var ut = UE.UETable.getUETableBySelected(this);
                	//若有多个单元格被同时选中
                    if (ut && ut.selectedTds.length) {
                    	for(var i=0; i<ut.selectedTds.length; i++) {
                    		var cell = ut.selectedTds[i];
                    		if(padding.top!='') {
                    			$(cell).css("padding-top",padding.top);
                    		}
                    		if(padding.bottom!='') {
                    			$(cell).css("padding-bottom",padding.bottom);
                    		}
                    		if(padding.left!='') {
                    			$(cell).css("padding-left",padding.left);
                    		}
                    		if(padding.right!='') {
                    			$(cell).css("padding-right",padding.right);
                    		}
                    	}
                    }
                    //若只有单个单元格被同时选中
                    else {
                    	var cell = UE.UETable.getTableItemsByRange(this).cell;
                    	if(padding.top!='') {
                			$(cell).css("padding-top",padding.top);
                		}
                		if(padding.bottom!='') {
                			$(cell).css("padding-bottom",padding.bottom);
                		}
                		if(padding.left!='') {
                			$(cell).css("padding-left",padding.left);
                		}
                		if(padding.right!='') {
                			$(cell).css("padding-right",padding.right);
                		}
                    }
                }
            }
        }
	}
});

/*合并单元格*/
UE.plugin.register('mymergecells', function() {
	return {
        commands: {
            'mymergecells': {
            	queryCommandState: function () {
                    return UE.UETable.getUETableBySelected(this) ? 0 : -1;
                },
                execCommand: function () {
                	var ut = UE.UETable.getUETableBySelected(this);
                	//选区左起位置
                	var colstar = ut.selectedTds[0].offsetLeft;
                	//选区右末位置，末单元格不一定在最右位置
                	var colend = 0;
      				for(var i=0; i<ut.selectedTds.length; i++) {
      					if(colend < ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth) {
      						colend = ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth;
      					}
      				}
      				//选区上起位置
      				var rowstar = ut.selectedTds[0].offsetTop;
      				//选区下末位置
      				var rowend = ut.selectedTds[ut.selectedTds.length-1].offsetTop + ut.selectedTds[ut.selectedTds.length-1].offsetHeight;
      				
                    if (ut && ut.selectedTds.length) {
                    	//获取选区中第一个td，其属性作为合并后单元格的样板
                        var cell = ut.selectedTds[0];
                        ut.mergeRange();
                        var rng = this.selection.getRange();
                        //第一个单元格中是否有模块
                        if (domUtils.isEmptyBlock(cell)) {
                            rng.setStart(cell, 0).collapse(true);
                        } else {
                            rng.selectNodeContents(cell);
                        }
                        rng.select();
                    }
                    var cell = UE.UETable.getTableItemsByRange(this).cell;
                    //默认padding设为4px
                    $(cell).css({padding:'4px'});
                    UE.dom.domUtils.setAttributes(cell,{width:$(cell).width()+1,height:$(cell).height()+1});
                }
            }
        }
	}
});

/*拆分单元格*/
UE.plugin.register('mysplittocells', function() {
	return {
        commands: {
            'mysplittocells': {
            	queryCommandState: function () {
            		var tableItems = UE.UETable.getTableItemsByRange(this),
	                    cell = tableItems.cell;
	                if (!cell) return -1;
	                var ut = UE.UETable.getUETable(tableItems.table);
	                if (ut.selectedTds.length > 0) return -1;
	                return cell && (cell.colSpan > 1 || cell.rowSpan > 1) ? 0 : -1;
                },
                execCommand: function () {
                	var rng = this.selection.getRange(),
	                    bk = rng.createBookmark(true);
	                var cell = UE.UETable.getTableItemsByRange(this).cell,
	                    ut = UE.UETable.getUETable(cell),
	                    tb = ut.table.children[0];
	                UE.dom.domUtils.setAttributes(cell,{height:-1});
	                ut.splitToCells(cell);
	                $(cell).attr("height",$(cell).height()+1);
	                $(cell).attr("width",$(cell).width()+1);
	                rng.moveToBookmark(bk).select();
	                $(tb).children().each(function(i){
	                	$(this).children().each(function(j){
	                		var _this = $(this);
	                		if(!_this.attr("height")){
	                			_this.attr("height",_this.height()+1);
	                			_this.attr("width",_this.width()+1);
	                		}
	                	});
	                });
                }
            }
        }
	}
});

/*修改单元格宽*/
UE.plugin.register('mychangecellwidth', function() {
	return {
        commands: {
            'mychangecellwidth': {
                execCommand: function (cmd, width) {
                	var ut = UE.UETable.getUETableBySelected(this);
                	//若有多个单元格被同时选中
                    if (ut && ut.selectedTds.length) {
                    	for(var i=0; i<ut.selectedTds.length; i++) {
                    		var cell = ut.selectedTds[i];
                    		UE.dom.domUtils.setAttributes(cell,{width:width});
                    	}
                    }
                    //若只有单个单元格被同时选中
                    else {
                    	var cell = UE.UETable.getTableItemsByRange(this).cell;
                    	//ut包括选中td,所在tr与table
                    	var ut = UE.UETable.getTableItemsByRange(this);
                    	var tb = ut.table.children[0];
                    	//修改本单元格上边框
                    	UE.dom.domUtils.setAttributes(cell,{width:width});
                    }
                }
            }
        }
	}
});

/*修改单元格高*/
UE.plugin.register('mychangecellheight', function() {
	return {
        commands: {
            'mychangecellheight': {
                execCommand: function (cmd, height) {
                	var ut = UE.UETable.getUETableBySelected(this);
                	//若有多个单元格被同时选中
                    if (ut && ut.selectedTds.length) {
                    	for(var i=0; i<ut.selectedTds.length; i++) {
                    		var cell = ut.selectedTds[i];
                    		UE.dom.domUtils.setAttributes(cell,{height:height});
                    	}
                    }
                    //若只有单个单元格被同时选中
                    else {
                    	var cell = UE.UETable.getTableItemsByRange(this).cell;
                    	//ut包括选中td,所在tr与table
                    	var ut = UE.UETable.getTableItemsByRange(this);
                    	var tb = ut.table.children[0];
                    	//修改本单元格上边框
                    	UE.dom.domUtils.setAttributes(cell,{height:height});
                    }
                }
            }
        }
	}
});



