﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	config.language = 'zh-cn';
	config.uiColor = '#D2E0F2';
	config.width = "600";
    config.height = "50";
	config.toolbar =[ ['Maximize']]; 
	config.toolbarCanCollapse = false;
	config.width = "98%";    
	config.height = "200";
};
