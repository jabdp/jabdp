<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<meta http-equiv="Cache-Control" content="no-store"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="icon" type="image/x-icon" href="${ctx}/favicon.ico"></link> 
<link rel="shortcut icon" type="image/x-icon" href="${ctx}/favicon.ico"></link>
<!-- <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> -->
  <script type="text/javascript">
  var __ctx='${ctx}';
  </script>
<!-- jquery js -->
<script src="${ctx}/js/jquery/jquery-1.7.2.min.js" type="text/javascript"></script>
<!-- easyui -->
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/themes/${themeColor}/easyui.css" colorTitle="${themeColor}"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/themes/icon.css"/>
<link href="${ctx}/js/easyui/themes/icon-add.css" rel="stylesheet" type="text/css"/>
<link href="${ctx}/js/easyui/themes/icon-ztree.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${ctx}/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui/jquery.edatagrid.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui/locale/easyui-lang-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui/easyui.extend.js"></script>
<!-- ztree -->
<link href="${ctx}/js/ztree/themes/blue/zTreeStyle.css" rel="stylesheet" type="text/css"/>
<link href="${ctx}/js/ztree/themes/blue/icon.css" rel="stylesheet" type="text/css"/>
<script src="${ctx}/js/ztree/jquery.ztree.all-3.2.min.js" type="text/javascript"></script>
<!-- datepicker -->
<script type="text/javascript" src="${ctx}/js/datepicker/WdatePicker.js"></script>
<!-- hotkey -->
<script type="text/javascript" src="${ctx}/js/hotkey/jquery.hotkeys.js"></script>
<!-- json -->
<script type="text/javascript" src="${ctx}/js/json/json2.js"></script>
<!-- jquery loadmask -->
<link href="${ctx}/js/loadmask/jquery.loadmask.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${ctx}/js/loadmask/jquery.loadmask.min.js"></script>
<!-- toastr -->
<link rel="stylesheet" href="${ctx}/js/toastr/toastr.min.css">
<script src="${ctx}/js/toastr/toastr.min.js"></script>
<!-- common -->
<script type="text/javascript" src="${ctx}/js/common/common.js"></script>
<link href="${cssPath}/main.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="${ctx}/js/module/idesigner.module.js"></script>
<script type="text/javascript" src="${ctx}/js/Hz2Py/jQuery.Hz2Py-min.js"></script>

<!-- codemirror -->
<link rel="stylesheet" href="${ctx}/js/codeMirror/theme/idea.css">
<link rel="stylesheet" href="${ctx}/js/codeMirror/addon/hint/show-hint.css">
<link rel="stylesheet" href="${ctx}/js/codeMirror/lib/codemirror.css">
<link rel="stylesheet" href="${ctx}/js/codeMirror/addon/search/matchesonscrollbar.css">
<link rel="stylesheet" href="${ctx}/js/codeMirror/addon/scroll/simplescrollbars.css">
<link rel="stylesheet" href="${ctx}/js/codeMirror/addon/fold/foldgutter.css">
<link rel="stylesheet" href="${ctx}/js/codeMirror/addon/dialog/dialog.css">
<script src="${ctx}/js/codeMirror/lib/codemirror.js"></script>
<script src="${ctx}/js/codeMirror/mode/javascript/javascript.js"></script>
<script src="${ctx}/js/codeMirror/mode/markdown/markdown.js"></script>
<script src="${ctx}/js/codeMirror/mode/xml/xml.js"></script>
<script src="${ctx}/js/codeMirror/addon/hint/show-hint.js"></script>
<script src="${ctx}/js/codeMirror/addon/hint/javascript-hint.js"></script>
<script src="${ctx}/js/codeMirror/addon/search/search.js"></script>
<script src="${ctx}/js/codeMirror/addon/search/searchcursor.js"></script>
<script src="${ctx}/js/codeMirror/addon/search/jump-to-line.js"></script>
<script src="${ctx}/js/codeMirror/addon/search/match-highlighter.js"></script>
<script src="${ctx}/js/codeMirror/addon/search/matchesonscrollbar.js"></script>
<script src="${ctx}/js/codeMirror/addon/scroll/simplescrollbars.js"></script>
<script src="${ctx}/js/codeMirror/addon/edit/matchbrackets.js"></script>
<script src="${ctx}/js/codeMirror/addon/fold/foldcode.js"></script>
<script src="${ctx}/js/codeMirror/addon/fold/foldgutter.js"></script>
<script src="${ctx}/js/codeMirror/addon/fold/comment-fold.js"></script>
<script src="${ctx}/js/codeMirror/addon/fold/brace-fold.js"></script>
<script src="${ctx}/js/codeMirror/addon/dialog/dialog.js"></script>
<script src="${ctx}/js/codeMirror/addon/display/placeholder.js"></script>