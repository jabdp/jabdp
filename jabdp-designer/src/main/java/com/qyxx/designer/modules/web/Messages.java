/**
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.designer.modules.web;


/**
 *  消息常量类
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-28 下午09:49:14
 */

public class Messages {
	/**
	 * 失败值-0
	 */
	public static final String FAIL = "0";
	
	/**
	 * 成功值-1
	 */
	public static final String SUCCESS = "1";
	
	/**
	 * 错误值-2
	 */
	public static final String ERROR = "2";
	
	/**
	 * 业务异常信息
	 */
	public static final String LOGIC_ERROR_MESSAGE = "业务逻辑错误！";
	
	/**
	 * 系统异常信息
	 */
	public static final String SYSTEM_ERROR_MESSAGE = "服务器内部错误！";
	
}
