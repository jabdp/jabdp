package com.qyxx.designer.modules.utils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.qyxx.designer.modules.mapper.JaxbMapper;
import com.qyxx.jwp.bean.Module;
import  com.qyxx.designer.modules.utils.EncryptAndDecryptUtils;



public class PathUtils {
	public static String pathSeparator = "/";

	public static final String ENCODING = "UTF-8";

	private static JaxbMapper jaxbBinder = new JaxbMapper(Module.class);
	
	
	/**
	 * 根据path建立相应路径，返回规范可用的path
	 * 
	 * @param path
	 * @return
	 */
	public static File createPath(String path) {
		path = formatePath(path);
		File file = new File(getFolder(path));
		if (!file.exists())
			file.mkdirs();
		return file;
	}
	

	/**
	 * 格式化path
	 * 
	 * @param path
	 * @return
	 */
	private static String formatePath(String path) {
		if (!path.startsWith(pathSeparator)) {
			return pathSeparator + path;
		}
		return path;
	}
	
	/**
	 * 取路径中的文件夹
	 * 
	 * @param path
	 * @return
	 */
	public static String getFolder(String path) {
		String folder = StringUtils.substringBeforeLast(path,
				pathSeparator);
		return StringUtils.isEmpty(folder) ? pathSeparator : folder;
	}
	
	
	/**
	 * 根据文件夹路径获取指定后缀名的子文件列表
	 * 
	 * @param dirPath
	 * @param extName
	 * @return
	 */
	public static String[] getDirFilePathList(String dirPath,
			String extName) {
		File dir = new File(dirPath);
		return dir.list(new DefaultFilenameFilter(extName));
	}
	
	
	/**
	 * 迭代取某一個路徑下的所有xml文件
	 * 
	 * @param dirPath
	 * @param sourceList
	 * @return
	 * @throws Exception
	 */
	public static List<File> getSourceList(String dirPath,
			List<File> sourceList) throws Exception {
		File dir = new File(dirPath);
		File[] files = dir.listFiles(new DefaultFilenameFilter("xml",
				false));
		for (File f : files) {
			if (f.isDirectory()) {// 如果是文件夹则迭代
				getSourceList(dirPath + File.separator + f.getName(),
						sourceList);
			} else {
				//System.out.println(f.getName());
				if(!f.getName().equals("menu.xml")&&!f.getName().equals(".xml")){
					sourceList.add(f);
				}
			}
		}
		return sourceList;
	}
	
	/**
	 * 根据模块路径封装成一个Map<String, Root>
	 */
	public static Map<String, Module> getModuleMaps(String dirPath)
																		throws Exception {
		File dir = new File(dirPath);
		File[] files = dir
				.listFiles(new DefaultFilenameFilter("xml"));
		Map<String, Module> moduleMap = new HashMap<String, Module>();
		for (File f : files) {
			if (f.isFile()) {
				Module root = getRootByFile(dirPath + File.separator
						+ f.getName());
				moduleMap.put(root.getModuleProperties().getKey(),
						root);
			}
		}
		return moduleMap;
	}
	
	/**
	 * 通过xml文件生成一个root
	 * 
	 * @param fileName
	 */
	public static Module getRootByFile(String fileName)	throws Exception {
		String str = FileUtils.readFileToString(new File(fileName),ENCODING);
		str =EncryptAndDecryptUtils.aesDecrypt(str, "");
		Module root = (Module) jaxbBinder.fromXml(str);
		return root;
	}
	
	
}
