package com.qyxx.designer.miniweb.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qyxx.designer.miniweb.service.AllModuleManager;
import com.qyxx.designer.modules.mapper.JsonMapper;
import com.qyxx.designer.modules.web.FormatMessage;
import com.qyxx.jwp.bean.Field;
import com.qyxx.jwp.bean.Item;







/**
 * 获得单个帐套下面所有的文件*/

@Controller
@RequestMapping(value = "/design")
public class AllModuleController {
	@Autowired
	private AllModuleManager allModuleManager;
	private JsonMapper jm = new JsonMapper();
	
	/**
	 * 获得对应路径下所有的模块的表名
	 * @param treeId 帐套的名称id
	 * @param urlType
	 * @param request 
	 * @return renMsg*/
	@RequestMapping(value = "doXml/getTabName", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String getFilePath(@RequestParam("treeId") String treeId,@RequestParam("urlType") String urlType,HttpServletRequest request){
		String path = DesignController.getRealPath(request)+urlType+"/"+treeId + "/config/module";
		allModuleManager.readModule(path,treeId);
		Map<String,String> mp =  allModuleManager.getTabName(treeId);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(mp);
		//System.out.println(rtnMsg.getMsg());
		return jm.toJson(rtnMsg);
	}
	
	/**
	 * 获得对应路径下对应表的所有字段名
	 * @param treeId 帐套名称id
	 * @param formKey 表名
	 * @return renMsg*/
	@RequestMapping(value = "doXml/getFileName", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String getFieldName(@RequestParam("treeId") String treeId,@RequestParam("formKey") String formKey){
		Map<String,String> mp =  allModuleManager.getFieldName(treeId, formKey);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(mp);
		return jm.toJson(rtnMsg);
		
	}
	
	/**
	 * 读取表字段
	 * 
	 * @param treeId
	 * @param entityName
	 * @return
	 */
	@RequestMapping(value = "doXml/getTableFields", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String getTableFields(@RequestParam("treeId") String treeId,@RequestParam("entityName") String entityName){
		List<Item> list =  allModuleManager.getTableFields(treeId, entityName);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(list);
		return jm.toJson(rtnMsg);
	}
	/**
	 * 读取所有模块的公用表
	 * 
	 * @param treeId
	 * @param urlType
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "doXml/getCommonTables", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String getCommonTables(@RequestParam("treeId") String treeId,@RequestParam("urlType") String urlType,HttpServletRequest request){
		String path = DesignController.getRealPath(request)+urlType+"/"+treeId + "/config/module";
		allModuleManager.readModule(path,treeId);
		List<Item> list =  allModuleManager.getCommonTables(treeId);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(list);
		return jm.toJson(rtnMsg);
	}
	/**
	 * 读取所有模块表
	 * 
	 * @param treeId
	 * @param urlType
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "doXml/getModuleTables", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String getModuleTables(@RequestParam("treeId") String treeId,@RequestParam("urlType") String urlType,HttpServletRequest request){
		String path = DesignController.getRealPath(request)+urlType+"/"+treeId + "/config/module";
		allModuleManager.readModule(path,treeId);
		List<Item> list =  allModuleManager.getModuleTables(treeId);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(list);
		return jm.toJson(rtnMsg);
	}

	/**
	 * 读取公用表字段
	 * 
	 * @param treeId
	 * @param entityName
	 * @return
	 */
	@RequestMapping(value = "doXml/getCommonTableFields", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String getCommonTableFields(@RequestParam("treeId") String treeId,@RequestParam("entityName") String entityName){
		List<Field> list  =  allModuleManager.getCommonTableFields(treeId, entityName);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(list);
		return jm.toJson(rtnMsg);
	}
	/**
	 * 获取所有模块的表
	 * 
	 * @param treeId
	 * @param urlType
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "doXml/getFormTables", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String getFormTables(@RequestParam("treeId") String treeId,@RequestParam("urlType") String urlType,HttpServletRequest request){
		String path = DesignController.getRealPath(request)+urlType+"/"+treeId + "/config/module";
		allModuleManager.readModule(path,treeId);
		List<Item> list =  allModuleManager.getFormsTables(treeId);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(list);
		return jm.toJson(rtnMsg);
	}
	/**
	 * 读取默认查询语句
	 * 
	 * @param treeId
	 * @param entityName
	 * @return
	 */
	@RequestMapping(value = "doXml/getDefaultSql", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String getDefaultSql(@RequestParam("treeId") String treeId,@RequestParam("urlType") String urlType,@RequestParam("formKey") String formKey,HttpServletRequest request){
		String path = DesignController.getRealPath(request)+urlType+"/"+treeId + "/config/module";
		allModuleManager.readModule(path,treeId);
		String content  =  allModuleManager.getDefaultSql(treeId,formKey);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(content);
		return jm.toJson(rtnMsg);
	}
}
