package com.qyxx.designer.miniweb.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.qyxx.designer.miniweb.service.ZtreeMenuManager;
import com.qyxx.designer.modules.mapper.JsonMapper;
import com.qyxx.designer.modules.web.FormatMessage;
import com.qyxx.jwp.menu.Menu;


/**
 * Urls:
 * List   page        : GET  /account/user/
 * Create page        : GET  /account/user/create
 * Create action      : POST /account/user/save
 * Update page        : GET  /account/user/update/{id}
 * Update action      : POST /account/user/save/{id}
 * Delete action      : POST /account/user/delete/{id}
 * CheckLoginName ajax: GET  /account/user/checkLoginName?oldLoginName=a&loginName=b
 * 
 * @author calvin
 *
 */
@Controller
@SessionAttributes({"loginUrl","loginName","configPath"})
@RequestMapping(value = "/design")
public class ZtreeMenuController {

	@Autowired
	private ZtreeMenuManager ztreeMenuManager;
	
	private JsonMapper jm = new JsonMapper();

	//public final static String MODULE_PATH = "E:\\space\\eclipseSpace\\idesigner\\src\\main\\webapp\\config\\";
	@RequestMapping(value = "config/getMenu",produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String getMenu(@ModelAttribute("configPath") Map<String,String> configPath,@RequestParam("urlType") String urlType) {
		 Set<Map.Entry<String, String>> set = configPath.entrySet();
	//	 String jsonStr[] = new String[configPath.size()];
		 Map<String,Menu> json = new HashMap<String,Menu>();
		 FormatMessage rtnMsg = new FormatMessage();
		// int i=0;
		 for (Iterator<Map.Entry<String, String>> it = set.iterator(); it.hasNext();) {
	            Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
	            Menu menus = ztreeMenuManager.getMenu(entry.getValue());
	    		if(entry.getValue().indexOf(urlType)!=-1){
	    			String key = entry.getKey();
		    		json.put(key, menus);
	    		}
	    		
	    	//	String jsonStr  =jm.toJson(rtnMsg);
	    	//	return jsonStr;
	    	//	jsonStr[i] = jm.toJson(rtnMsg);
	    	//	i++;
	        }	
		rtnMsg.setMsg(json);
		return jm.toJson(rtnMsg);
		
	}
	/**
	 * 保存帐套树形结构
	 * @param menuJson 帐套树形节点json字符串
	 * @param treeId	帐套名称
	 * @param configPath 所有帐套路径键值对
	 * @param request
	 * @return rtnMsg*/
	@RequestMapping(value = "config/saveMenu", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String saveMenu(@RequestParam("menuJson") String menuJson,@RequestParam("treeId") String treeId,@ModelAttribute("configPath")  Map<String,String> configPath,HttpServletRequest request) {
		Menu menus= jm.fromJson(menuJson, Menu.class);
		String path = configPath.get(treeId);
		if(path==null){
		//	DesignController design = new DesignController();
			path=DesignController.getRealPath(request)+"local/"+treeId+"/config/";
			configPath.put(treeId, path);
		}
		ztreeMenuManager.saveMenu(menus, path);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(true);
		return jm.toJson(rtnMsg);
	}
	/**
	 * 修改帐套树形结构
	 * @param newPath 新的帐套路径
	 * @param oldPath 旧的帐套路径
	 * @param treeId	帐套名称
	 * @param configPath 所有帐套路径键值对
	 * @param request
	 * @return rtnMsg*/
	@RequestMapping(value = "config/changeMenu", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String changeMenu(@RequestParam("treeId") String treeId,@RequestParam("newPath") String newPath,@RequestParam("oldPath") String oldPath,@ModelAttribute("configPath")  Map<String,String> configPath,HttpServletRequest request) {
		String path = configPath.get(treeId);
		if(path==null){
			path=DesignController.getRealPath(request)+"local/"+treeId+"/config/";
			configPath.put(treeId, path);
		}
		String np = path+newPath;
		String op = path+oldPath;
		ztreeMenuManager.changeMenu(np,op);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(true);
		return jm.toJson(rtnMsg);
	}
	
	/**
	 * 删除帐套树形节点文件单个模块
	 * @param configPath 所有帐套路径键值对
	 * @param treeId 帐套名称id
	 * @param sPath 
	 * @return rtnMsg*/
	@RequestMapping(value = "config/deleteXml", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String deleteXml(@ModelAttribute("configPath") Map<String,String> configPath,@RequestParam("treeId") String treeId, @RequestParam("sPath") String sPath){
		String path = configPath.get(treeId)+sPath;
		Boolean flag = ztreeMenuManager.deleteFolder(path);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(flag);
		return jm.toJson(rtnMsg);
	}
	
	/**
	 * 删除帐套树形节点文件多个模块
	 * @param configPath 所有帐套路径键值对
	 * @param treeId 帐套名称id
	 * @param request
	 * @return rtnMsg*/
	@RequestMapping(value = "config/deleteAccount", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public String deleteAccount(HttpServletRequest request,@ModelAttribute("configPath") Map<String,String> configPath,@RequestParam("treeId") String treeId){
		configPath.remove(treeId);
		String path = DesignController.getRealPath(request)+"local/"+treeId;
		Boolean flag = ztreeMenuManager.deleteFolder(path);
		FormatMessage rtnMsg = new FormatMessage();
		rtnMsg.setMsg(flag);
		return jm.toJson(rtnMsg);
	}
}
