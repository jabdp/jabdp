<?xml version="1.0" encoding="utf-8"?>
<#assign formList=root.dataSource.formList/>
<workbook>
<#list formList as form>
	<#if form.enableImport>
    <worksheet name="${form.caption}">
   		<section startRow="0" endRow="0"/>
        <loop startRow="1" endRow="1" items="sheetData" var="field" varType="java.util.HashMap">
            <section startRow="1" endRow="1">       
            <#if form.isMaster>    
            	<mapping row="1" col="0">field.systemCode</mapping>
            <#else>
            	<mapping row="1" col="0">field.refSystemCode</mapping>
            </#if>     	
            <#assign i=1>
            <#list form.fieldList as fields>
            	<#if fields.enableImport>
                <mapping row="1" col="${i}">field.${fields.key}</mapping>
                <#assign i=i+1>
                </#if>
			</#list>
            </section>
            <loopbreakcondition>
                <rowcheck offset="0">
                    <cellcheck offset="0"></cellcheck>
                </rowcheck>
            </loopbreakcondition>
        </loop>
    </worksheet>
    </#if>
 </#list>
</workbook>