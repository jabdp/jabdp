<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
<#import "common/field.ftl" as fieldftl>
<#--include "common/field.ftl"-->
<#assign entityName=root.entityName />
<#assign titleName=root.moduleProperties.caption/>
<#assign moduleKey=root.moduleProperties.key/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign formList=root.dataSource.formList/>
<#assign tabsList=root.dataSource.tabsList/>
<#assign flowList= root.flowList />
<#assign dynamicShowTable = false /><#-- 控制是否动态显示子表 -->
<#-- 关联表表 -->
<#if root.moduleProperties.relevanceModule??>
<#assign relevanceModule=root.moduleProperties.relevanceModule/>
<#else>
<#assign relevanceModule=""/>
</#if>
<link rel="stylesheet" type="text/css" href="${r"${ctx}"}/js/easyui/gray/easyui-module.css" colorTitle="gray"/>
<link rel="stylesheet" type="text/css" href="${r"${ctx}"}/js/easyui/blue/easyui-module.css" colorTitle="blue"/>
<style type="text/css"><#-- 初始化样式 -->
.tabs-tool {border:0;}
body {margin:0px}
<@fieldftl.initFormCssOrJavascript formList=formList eventKey="addCssStyle"/>
</style>
<script type="text/javascript">
  //定义一个全局变量保存操作状态
    var operMethod="${r"${operMethod}"}";
	var __tabTitle = "";
	var _loadSubCount_ = 0;
	var _subTabCount_ = 0;
	var _wait_ = null;
	var _autoIdMap_ = {};
	var _isCloseAfterSave_ = false;<#-- 保存后关闭 -->
	<#list formList as form><#-- 用于将隐藏子表数量去掉 -->
		<#if !form.isMaster>
			<#if form.visible>
				<@fieldftl.initCheckFormAuth form=form isEnd=false/>  
	_subTabCount_ ++ ;            
	            <@fieldftl.initCheckFormAuth form=form isEnd=true/>
			</#if>   
		<#else>
			<#assign dynamicShowTable = (form.dynamicShow)!false />
		</#if>
	</#list>
	<c:if test="${r"${operMethod=='view'}"}">
		__tabTitle = '<s:text name="system.button.view.title"/><s:text name="${i18nKey!titleName}"/>-${r"${param.id}"}';
	</c:if>
	<c:if test="${r"${operMethod=='add'}"}">
        __tabTitle = '<s:text name="system.button.add.title"/><s:text name="${i18nKey!titleName}"/>';
    </c:if>
    <c:if test="${r"${operMethod=='edit'}"}">
        __tabTitle = '<s:text name="system.button.copy.title"/><s:text name="${i18nKey!titleName}"/>-${r"${param.id}"}';
    </c:if> 
    
    <#-- 初始化JS脚本 -->
    <@fieldftl.initFormCssOrJavascript formList=formList eventKey="addJavaScript"/>
    
    	<#-- 关闭窗口前检查是否保存方法，供首页调用 -->
    	function checkIsSaveBeforeClose() {
    		if(operMethod!='view') {
    			if(confirm("模块（<s:text name="${i18nKey!titleName}"/>）数据尚未保存，确定要在关闭前保存数据吗？")) {
    				_isCloseAfterSave_ = true;
    				doSaveObj();
    				doRefreshModuleData();
    			}
    		}
    		return true;
    	}
    
    	function setViewTitle() {
			<c:if test="${r"${param.isEdit!='1' && param.isChangeTitle!='false'}"}">
    		__tabTitle = '<s:text name="system.button.view.title"/><s:text name="${i18nKey!titleName}"/>-' + $("#id").val();
    		if(top.doModifyTabTitle) {
    			top.doModifyTabTitle(__tabTitle);
    		}
    		</c:if>
    	}
    	
    	function setAddTitle() {
    		<c:if test="${r"${param.isEdit!='1' && param.isChangeTitle!='false'}"}">
    		__tabTitle = '<s:text name="system.button.add.title"/><s:text name="${i18nKey!titleName}"/>';
    		if(top.doModifyTabTitle) {
    			top.doModifyTabTitle(__tabTitle);
    		}
    		</c:if>
    	}
    	
    	function setModifyTitle() {
    		<c:if test="${r"${param.isEdit!='1' && param.isChangeTitle!='false'}"}">
    		__tabTitle = '<s:text name="system.button.modify.title"/><s:text name="${i18nKey!titleName}"/>-' + $("#id").val();
    		if(top.doModifyTabTitle) {
    			top.doModifyTabTitle(__tabTitle);
    		}
    		</c:if>
    	}
        
       //得到子表一个行(json0对象)
		function getObject(rowIndex,ChildTableIndex){
			var rows = $("#"+ChildTableIndex).edatagrid("getRows");
			return rows[rowIndex];
		}

       //得到子表里所有行数
		function getAllrowsLength(ChildTableIndex){
			var rows = $("#"+ChildTableIndex).edatagrid("getRows");
			var len=rows.length-1;
			return len;
		}
		
		//将修改后的一行(json对象)设置到dategrid里面去
		function setObject(rowIndex,ChildTableIndex,row){
			var rows = $("#"+ChildTableIndex).edatagrid("getRows");
			var newRow=rows[rowIndex];
			$.extend(newRow,row);
			$("#"+ChildTableIndex).edatagrid('refreshRow',rowIndex);
		}
		
		//关闭子表名字窗口
		function closeDetailDialog(){
			$('#subTableDetailInfo').dialog("close");
		}
		
		function getQueryParams() {
				var strParam =$("#hid_queryParams").val();
				return "&queryParams=" + strParam;
		}

		function doPrepareForm(jsonData) {
			<#list formList as form>
			     <#if form.isMaster>
			     	<@fieldftl.prepareForm form=form />
			     <#else>
			     	<@fieldftl.prepareSubForm form=form />
			     </#if>
			</#list>
		}
		<#-- isChange 是否变更，变更时保留附件 -->
		function clearAddField(isChange) {
			$("#id").val("");
			if(isChange!="1") {
				$("#atmId").val("");	
			}
			$("#flowInsId").val("");
			$("#status").val("10");
			$("#_dv_approved_").hide();
			clearAutoIdFieldValue();
		}
		
		function doInitForm(data, callFunc) {
			if (data.msg) {
                  var jsonData = data.msg;
                  doPrepareForm(jsonData);
                  $('#viewForm').form('load',jsonData);
                  //在第二次载入数据后，使textarea不出现滚动条
                  /*$(".module_form textarea").each(function(){
					  var height = $(this).height();
					  var innerHeight = $(this).innerHeight();
				      $(this).height(this.scrollHeight - innerHeight + height);
			      });*/
                  if(window.onAfterLoadData) {
                  	window.onAfterLoadData(jsonData);
                  }
                  <@fieldftl.initDynamicShowSubTable formList=formList isAdd=false />
                  if(operMethod == "view") {
                  	<#-- initAutoIdFieldValue(true);查看时，去除自动编号提示文本 -->
                  	setViewTitle();
                  }
                  if(operMethod == "edit") {
                  	clearAddField("${r"${param.isChange}"}");
                  	<c:if test="${r"${param.isChange=='1'}"}"> <#-- 变更时，处理自动编号 -->
                  	saveOldAutoIdFieldValue(jsonData);
                  	</c:if>
					if(window.onAfterCopyData) {  <#-- 变更时，处理自动编号 -->
						window.onAfterCopyData(jsonData <c:if test="${r"${param.isChange=='1'}"}">,true</c:if>);
					}
                  }
            }
            if(callFunc) {
            	callFunc(data.msg);
            }
		}
		
		function doHideFlowButton() {
			<c:choose>
			<c:when test="${r"${param.isEdit=='1'}"}">		
				$('#tprocess').show();
				$('#tedit').show();
    	  		$("#tdel").show();
    	  		$("#tcancelprocess").hide();
    	  		$("#tinvalidData").hide();
			</c:when>
			<c:otherwise>
			    var flowStatus = $("#status").val();
			    jwpf.doCancelSetFormToRevise();
			    <security:authorize url="/sys/log/revise-log.action?mn=${entityName}"> <#-- 查看修订日志 -->
				jwpf.doCancelReviseView();
				</security:authorize>
				if(flowStatus == "10") {
					$('#tprocess').show();
					$('#tedit').show();
	    	  		$("#tdel").show();
	    	  		$("#tcancelprocess").hide();
	    	  		$("#tinvalidData").hide();
	    	  		$("#_dv_approved_").hide();
	    	  		$('#tcopyChange').hide();
	    	  		$("#trevise").hide();
	    	  		$("#treviseview").hide();
	    	  		$("#tcancelrevise").hide();
				} else {
					$('#tprocess').hide();
					$('#tedit').hide();
	    	  		$("#tdel").hide();
	    	  		$("#tcancelprocess").hide();
	    	  		$("#tinvalidData").hide();
	    	  		$("#_dv_approved_").hide();
	    	  		$('#tcopyChange').hide();
	    	  		$("#tcancelrevise").hide();
	    	  		if(flowStatus == "35" || flowStatus == "20") {
	    	  			$("#tcancelprocess").show();
	    	  		}
	    	  		if(flowStatus == "30" || flowStatus == "31" || flowStatus == "35") {
	    	  			$("#tinvalidData").show();
	    	  			$("#_dv_approved_").show();
	    	  			$("#trevise").show();
	    	  			$("#treviseview").show();
	    	  			<security:authorize url="/sys/log/revise-log.action?mn=${entityName}"> <#-- 查看修订日志 -->
				    	jwpf.doInitReviseView("${entityName}","${moduleKey}");
				    	</security:authorize>
	    	  		}
	    	  		if(flowStatus >= 30) {
	    	  			$('#tcopyChange').show();
	    	  		}
	    	  		<s:if test='#session.USER.isSuperAdmin=="1"'>
	    	  		if(flowStatus == "30" || flowStatus == "31" || flowStatus == "32") {
	    	  			$('#tcancelApproved').show();
	    	  		}
	    	  		</s:if>
				}
		   	</c:otherwise>
		    </c:choose>
		    if(window.onAfterDisplayButton) { <#-- 控制按钮显隐 -->
    	  	   window.onAfterDisplayButton();
    	    }
		}
			
		 function doView(type) {
		  	var reqData = {};
		  	reqData["id"] = $("#id").val();
		  	reqData["findType"] = type;
		  	var hidParam = $("#hid_queryParams").val();
		  	if(hidParam) {
			  	var reqParam = JSON.parse(hidParam);
			  	$.extend(reqData, reqParam);
		  	}
		  	var options = {
                  url : '${r"${ctx}"}/gs/gs-mng!queryEntityByType.action?entityName=${entityName}',
                  data : reqData,
                  success : function(data) {
                  	if(data.msg) {
                  		$("#id").val(data.msg["id"]);
                  	}
                    _loadSubCount_ = 0;
                  	_initTimer_();
                  	doInitForm(data, doHideFlowButton);
                  }
            };
            fnFormAjaxWithJson(options);  
	  }
	  
	  function initRichTextBox() {
	  	var config = {
					/*toolbar:
					[
						['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
						['UIColor', 'Preview']
					],
					customConfig : 'config_en.js'*/
		};
		$('.jquery_ckeditor').ckeditor(config);
	  }
		
	  function initTabs() {
	  	<#if tabsList??&&tabsList?size gt 0>
             <#--分tab页面 -->    
             <#list tabsList as tabs>
             $("#form_tabs_${tabs.rows}").tabs({
		  		onSelect:function(title) {
		  			/*if(operMethod != "view") {
		  				jwpf.focusFirstInputOnFormTabs("form_tabs_${tabs.rows}");
		  			}*/
		  		},
				tools:[{  
					iconCls:'icon-expand',  
					handler:function(){  
						$("#form_tabs_${tabs.rows}").tabs("getSelected").panel("body").toggle();
						<#-- 调整tabs列宽 -->
						$("div.tabs-panels div.panel-body:visible").panel("resize");
					}  
				}]
		  	 });
             </#list>
             <#-- 收缩在列表页面显示的tab所在tabs -->
             <#list tabsList as tabs>
		  		<#list tabs.tabList as tab>
		  		<#if tab.isShowInQueryList?? && tab.isShowInQueryList>
             		$("#form_tabs_${tabs.rows}").tabs("getSelected").panel("body").hide();
             		<#break>
             	</#if>
             	</#list>
             </#list>
             <#-- 调整tools宽度 -->
			 var panel_body = $("div.tabs-panels div.panel-body:visible");
			 if(panel_body.length > 0) {
				 $("div.tabs-header").width(panel_body.width());
			 }
        </#if>
	  }
		  
		  $(document).ready(function() {
		  			hideTool();
		  			comboboxInit();
    	            initCombo();
    	            initRichTextBox();
                    if(operMethod == "add") {
                    	//新增
                    	<@fieldftl.initDynamicShowSubTable formList=formList isAdd=true />
                        //autoRow();
                        doSetFormFieldReadOnly();
                        showSave();
                    } else if(operMethod == "edit") {
                    	//复制
                    	showSave();
					    doModify("${r"${param.id}"}", focusFirstElement);
                    }else if(operMethod == "view"){
                       //查看
                       hideSave();
 					   doModify("${r"${param.id}"}", doDisabled);
                    }
                    initSubGridHotKey();
                    initEvent();
                    _initTimer_();
                    jwpf.bindKeydownOnForm("viewForm");
                    jwpf.bindHotKeysOnViewPage();
                    //initBody();
                    initTabs();
        });
	  
	  <#-- js方式初始化layout控件，解决多行tools高度自适应 -->
	  function initBody() {
	      var height = $('div#main_tabs_div > div.tabs-panels:first', parent.document).height(); 
	      $("body").height(height).layout();//body元素自动添加fit：true
	      <#-- 因为跨了一个iframe，chrome浏览器中fit属性的作用不太理想，
	      	未撑满iframe的body容器导致center块自适应外部容器，暂时高度不足使内部元素撑出滚动条
	      	layout会在0.1秒后再次根据，body的宽高执行resize，此时fit方才体现效果，
	      	不过因为滚动条的消失，之前计算所得的宽度就会产生空白
	      	可通过layout前确定body高度解决此问题-->
	  }
	  
	  function showSave(){
	  	  $("#tclose").show();
    	  $("#tsave").show();
      }
      
      function hideSave(){
          $("#tclose").show();
          $("#trefresh").show();
          $("#tadd").show();
    	  $('#tedit').show();
    	  $("#tdel").show();
    	  $('#tcopy').show();
    	  //$('#tcopyChange').show();
    	  $('#tfirst').show();
    	  $('#tprev').show();
    	  $('#tnext').show();
    	  $('#tlast').show();
    	  $('#tattach').show();
    	  $('#bt_report').show();
    	  $('#tprocess').show();
    	  $('#tviewprocess').show();
    	  $("#trevise").show();
	      $("#treviseview").show();
    	  $("a.userAddBt").show();
      }
      
      //隐藏工具条
      function hideTool(){
    	  $("div.datagrid-toolbar a", "#toolbar_layout").hide();
      }
    
	function comboboxInit(){
			   <#list formList as form>
			     <#if form.isMaster>
			         <#assign fields=form.fieldList!>
		                <#if fields??&&fields?size gt 0>
		                  <#list fields as field>
			                <@fieldftl.initControlsData field=field type="query" prefix="" entityName=form.entityName formKey="" moduleTitle=i18nKey!titleName />
		             	  </#list>
		          		</#if>
		          	<@fieldftl.initDynamicProp form = form />
		        </#if>
		     </#list>
       }
		
		//设置焦点
		function focusFirstElement() {
			if(operMethod != "view") {
	  			jwpf.focusFirstInputOnFormTabs("form_tabs_1");
	  		}
		}	
 	
      //将控件设置成可编辑状态
	    function doSetFormToEdit(){
	    	setModifyTitle();
	    	hideTool();
    	    showSave();
    	    $('#tcancel').show();
	    	operMethod="modify";
	    	doEnable();
           	autoRow();
            focusFirstElement();
            if(window.onAfterModifyInit) {<#-- 启用修改后触发 -->
            	window.onAfterModifyInit();
            }
	    }
	    
	    //将控件设置成可修订状态
	    function doSetFormToRevise(){
	    	jwpf.doSetFormToRevise("${entityName}","${moduleKey}","<s:text name="${i18nKey!titleName}"/>");
	   		<#list formList as form>
	            <#if !form.isMaster && !((form.isVirtual)!false)>
	        jwpf.doSetTableToRevise("${form.key}","${form.entityName}","${moduleKey}","<s:text name="${i18nKey!titleName}"/>");
	            </#if>    
	    	</#list>
	    	$("#trevise").hide();
	    	$("#tcancelrevise").show();
	    }
	    
	    function doCancelSetFormToRevise() {
	    	jwpf.doCancelSetFormToRevise();
	    	doCancelEdit();
	    }
	    
	    function doViewRevise() {
	    	jwpf.doViewReviseLog(jwpf.getId(),"${moduleKey}");
	    }
	    
	    function initFormDefaultValue() {
	    	<#list formList as form>
	            <#if form.isMaster>
	                <@fieldftl.initFormFieldDefaultValue form=form />
	            </#if>    
	    	</#list>
	    }
      
      //初始化一个可编辑行
       function autoRow(){
        <#list formList as form>
	            <#if !form.isMaster && !((form.showTools)!false) && !((form.isVirtual)!false)>
	                $("#${form.key}").edatagrid('addRow',false);
	            </#if>    
	    </#list>
      }
                
     function initSubGridHotKey(){
     <#--list formList as form>
	            <#if !form.isMaster>
	                    $('#dv_tab_${form.key} input').live('keydown', 'return',function (evt){ evt.preventDefault();$("#${form.key}").edatagrid('addRow'); return false; });
	            </#if>    
	    </#list-->
      }
   
       //主表复制一条记录  
	function doCopyMainTab(isChange, closeTitle){
            var id=$("#id").val();
			var chgStr = "";
			var title = "<s:text name="system.button.copy.title"/>";
            if(isChange) {
            	chgStr = "isChange=1&";
            	title = "<s:text name="变更"/>";
            }
           	top.addTab(title+'<s:text name="${i18nKey!titleName}"/>-'+id, '${r"${ctx}"}/gs/gs-mng!edit.action?' + chgStr +'entityName=${entityName}&id=' + id+getQueryParams());
			if(closeTitle && top.closeTab) {
				top.closeTab(closeTitle);
			}
	}
	
	
   //页面数据初始化
   function doModify(id, callFunc) {
       	var options = {
       		 <#-- 关联表表 -->
             url : '${r"${ctx}"}/gs/gs-mng!queryEntity.action?entityName=${entityName}&relevanceModule=${relevanceModule}',
             data : {
                 "id" : id
             },
             async: !_isCloseAfterSave_,<#-- 当自动关闭时，需要使用同步方法，可以调用保存后方法 -->
             success : function(data) {
             	if(!_isCloseAfterSave_) {
                  doInitForm(data, callFunc);
                } else {
                  if(window.onAfterSave) {
				     window.onAfterSave(data.msg);
				  }
                }
             }
        };
        fnFormAjaxWithJson(options);  
   }

   function getChangeRows(updateRows, deleteRows) {
   		var changesRows = {
			inserted : [],
			updated : [],
			deleted : []
		};
		if (operMethod == "edit" || operMethod == "add") {
			if (updateRows && updateRows.length > 0) {
				//复制时删除id
				for ( var k = 0; k < updateRows.length; k++) {
					//if (!jwpf.isEmptyRow(updateRows[k])) {
						updateRows[k].id = null;
						changesRows.updated.push(updateRows[k]);
					//}
				}
			}

		} else {
			if (updateRows && updateRows.length > 0) {
				for ( var k = 0; k < updateRows.length; k++) {
					//if (!jwpf.isEmptyRow(updateRows[k])) {
						changesRows.updated.push(updateRows[k]);
					//}
				}
			}
			if (deleteRows && deleteRows.length > 0) {
				for ( var j = 0; j < deleteRows.length; j++) {
					changesRows.deleted.push(deleteRows[j]);
				}
			}
		}
		return changesRows;
   }
   
	function cSaveObj(childTableIndex) {
		var updateRows = $('#' + childTableIndex).edatagrid('saveRow');
		//var updateRows = $('#' + childTableIndex).edatagrid('getRows');
		if(updateRows) {
			var deleteRows = $('#' + childTableIndex).edatagrid('getChanges',
				'deleted');
			return getChangeRows(updateRows, deleteRows);
		} else {
			return false;
		}
	}
	
	function cSaveObjTree(childTableIndex) {
		var updateRows = $('#' + childTableIndex).edatagrid('saveRow');
		//var updateRows = $('#' + childTableIndex).edatagrid('getAllNodes');
		if(updateRows) {
			var deleteRows = $('#' + childTableIndex).edatagrid('getDeleteNodes');
			return getChangeRows(updateRows, deleteRows);
		} else {
			return false;
		}
	}

	<#list formList as form>
	      <#if form.isMaster>
	      	<#if form.extension?? && form.extension.editPage??>
	      		<#assign eventList=form.extension.editPage.eventList />
	      		<@fieldftl.initFormEvent eventList=eventList/>
	      	</#if>          
	      	<@fieldftl.initFieldReviseEvent form=form formKey="form" /><#-- 初始化主表修订事件 -->
	      <#else>
	      	<@fieldftl.initFieldReviseEvent form=form /><#-- 初始化子表修订事件 -->	
	      </#if>    
	</#list>
	<#-- 生成自动编号参数对象 -->
	function buildAutoIdParam() {
		var params = [];
		var val = "";
		<#list formList as form>
	      <#if form.isMaster>
	      	 <#assign fields=form.fieldList/>
                   <#if fields??&&fields?size gt 0>
                         <#list fields as field>
                              <#assign autoIdRule = (field.editProperties.autoIdRule)!""/>
                              <#if autoIdRule != "">
                                    	try {
                                    		<c:choose>
											<c:when test="${r"${param.isChange=='1'}"}"><#-- 变更修改 -->
											if(operMethod == "edit") {
												val = _autoIdMap_["${field.key}"] + "-";
												var autoIdRule = ${autoIdRule};
												var changeBitNum = "3";
												if($.isPlainObject(autoIdRule)) {
													changeBitNum = autoIdRule["changeBitNum"] || "3";
												}
												params.push({"key":"${field.key}","busVal":val,"bitNum":changeBitNum,"excludeDate":"1"});		
											} else {
												val = ${autoIdRule};
	                                    		if($.isPlainObject(val)) {
	                                    			params.push({"key":"${field.key}","busVal":val["busVal"],"bitNum":val["bitNum"],"dateFmt":val["dateFmt"],"excludeDate":val["excludeDate"],"dateFirst":val["dateFirst"]});
	                                    		} else {
	                                    			params.push({"key":"${field.key}","busVal":val});
	                                    		}
											}
											</c:when>
											<c:otherwise><#-- 其他情况 -->
											val = ${autoIdRule};
                                    		if($.isPlainObject(val)) {
                                    			params.push({"key":"${field.key}","busVal":val["busVal"],"bitNum":val["bitNum"],"dateFmt":val["dateFmt"],"excludeDate":val["excludeDate"],"dateFirst":val["dateFirst"]});
                                    		} else {
                                    			params.push({"key":"${field.key}","busVal":val});
                                    		}
											</c:otherwise>
											</c:choose>
                                    	} catch(e) {alert("为${field.caption}(${field.key})属性配置自动编号规则时出现异常：" + e);}
                              </#if>
                         </#list>
                  </#if> 
	      </#if>    
		</#list>
		return params;
	}
	<#-- 保存旧的自动编号值 -->
	function saveOldAutoIdFieldValue(data) {
		<#list formList as form>
	      <#if form.isMaster>
	      	 <#assign fields=form.fieldList/>
                   <#if fields??&&fields?size gt 0>
                         <#list fields as field>
                              <#assign autoIdRule = (field.editProperties.autoIdRule)!""/>
                              <#if autoIdRule != "">
         _autoIdMap_["${field.key}"] = data["${field.key}"];                          
                              </#if>
                         </#list>
                  </#if> 
	      </#if>    
		</#list>
	}
	<#-- 自动编号框提示
	function initAutoIdFieldValue(isHide) {
		<#list formList as form>
	      <#if form.isMaster>
	      	 <#assign fields=form.fieldList/>
                   <#if fields??&&fields?size gt 0>
                         <#list fields as field>
                              <#assign autoIdRule = (field.editProperties.autoIdRule)!""/>
                              <#if autoIdRule != "">
         $("#${field.key}").span_tip({text:"(<s:text name="system.autoGen.title"/>)", hide: isHide});
                              </#if>
                         </#list>
                  </#if> 
	      </#if>    
		</#list>
	} -->
	<#-- 清除自动编号值 -->
	function clearAutoIdFieldValue() {
		<#list formList as form>
		      <#if form.isMaster>
		      	 <#assign fields=form.fieldList/>
	                   <#if fields??&&fields?size gt 0>
	                         <#list fields as field>
	                              <#assign autoIdRule = (field.editProperties.autoIdRule)!""/>
	                              <#if autoIdRule != "">
	    jwpf.setFormVal("${field.key}", "");
	                              </#if>
	                         </#list>
	                  </#if> 
		      </#if>    
		</#list>
	}
	
	function doSaveData(sd) {
		   if(operMethod == "view") { <#-- 修复修订模式下，关联字段不会保存到数据库bug -->
		   		$("input,textarea,select").removeAttr("disabled");
		   }
		   var sendData = $.extend({"businessMessage":[]}, sd);
			//key,busVal
		   sendData["autoIdRule"] = buildAutoIdParam();
	   
		   var jsons = formToJSON('viewForm');
		   <#list formList as form>
	            <#if !form.isMaster && !((form.isVirtual)!false)>
	       if($("#${form.key}").length) {
	            	<#if form.listType == "TreeGrid">
	           var rows = cSaveObjTree("${form.key}");
	            	<#else>
	           var rows = cSaveObj("${form.key}");
	            	</#if>
	           if(rows === false) {return;}<#-- 校验子表必输项 -->
	           jsons['${form.lcKey}s'] = rows; 	
	       }     
	            </#if>    
	       </#list>
			<#--if(operMethod == "edit" || operMethod == "add"){
				jsons["id"]=null;
				jsons["atmId"]=null;
				jsons["flowInsId"]=null;
				jsons["status"]="10";
			}-->
			
			var jsonSendData = JSON.stringify(sendData);
			var jsonSavedata = JSON.stringify(jsons);
			var options = {
				<#-- 关联表表 -->
				url : '${r"${ctx}"}/gs/gs-mng!save.action?entityName=${entityName}&relevanceModule=${relevanceModule}',
				async : false,
				data : {
					"jsonSaveData" : jsonSavedata,
					"jsonSenddata": jsonSendData
				},
				success : function(data) {
						operMethod="view";
						<#-- 保存后操作函数判断，用于快速添加数据等操作 -->
						<c:if test="${r"${not empty param.afterSaveFunc}"}">
						var asFunc = eval("parent.${r"${param.afterSaveFunc}"}");
						if(asFunc) {
							asFunc(data.msg);
						}
						</c:if>
						formClear();
		                $("#id").val(data.msg);<#-- 用于加载子表 -->
						doModify(data.msg, doAfterSave);
				}
			};
			<#--fnAjaxSubmitWithJson('viewForm', options);-->
			fnFormAjaxWithJson(options);
	}

	function doSaveObj() {
	   var isValidate= $("#viewform").form("validate");
	   if(isValidate){
	   	   var sendData = {"businessMessage":[]};
	   	   var bsFlag = true;
		   if(window.onBeforeSave) {
				bsFlag = window.onBeforeSave(sendData);
		   }
		   if(bsFlag === false) {
		   		return false;
		   }
		   doSaveData(sendData);
	   }
	}
	
	function doRefreshModuleData() {
		if (top.refreshTabData) {
			top.refreshTabData("<s:text name="${i18nKey!titleName}"/>");
		}
	}
	
	function doAfterSave(data) {
		doRefreshModuleData();
		hideTool();
		hideSave();
		doDisabled();
		//operMethod="view";
		//setViewTitle();
		if(window.onAfterSave) {
			window.onAfterSave(data);
		}
	}
	
	function doSetFormFieldReadOnly() {
		<#list formList as form>
	       <#if form.isMaster>
	       	<@fieldftl.setFormFieldReadOnly form=form />	      
	       </#if>    
	    </#list>
	}
	
	function doEnable() {
	    $("div.file_upload_button").show();
		//$("input[type!=hidden],textarea,select,button").removeAttr("disabled");
		$("button,input.Idate").removeAttr("disabled");
		$("textarea").removeAttr("readonly");
		$("input.combo-f,select.combo-f").combo('readonly',false);
		$("input.easyui-numberbox").numberbox('readonly',false);
		$("input.easyui-textbox").numberbox('readonly',false);
		$("input.easyui-processbarSpinner").processbarSpinner('enable');
		//$("input.textbox-f").textbox('enable');
		//$('input.Idate').addClass("Wdate");
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(false);
			} catch(e) {
				$(this).ckeditorGet().setReadOnly(false);
			}
		});
		//hide start process
		$('#tprocess').hide();
		$("a.bt-extra").linkbutton('enable');
		jwpf.enableLinkButton($("a.hyperlink").not(".validInView"));
		$("input[type=text].validInView").removeAttr("readonly");
		<#--设置该只读的字段只读-->
		doSetFormFieldReadOnly();
		<#list formList as form>
	            <#if !form.isMaster && !((form.isVirtual)!false)>
	                   $("#${form.key}").edatagrid('enableEditing');
	            </#if>    
	    </#list>
	}
	
	//将页面上的控件置为不可编辑状态
	function doDisabled() {
	    $("div.file_upload_button").hide();
		//$("input[type!=hidden],textarea,select,button").attr("disabled", "disabled");
		$("button,input.Idate").attr("disabled", "disabled");
		$("textarea").attr("readonly","readonly");
		$("input.combo-f,select.combo-f").combo('readonly',true);
		$("input.easyui-numberbox").numberbox('readonly',true);
		$("input.easyui-textbox").numberbox('readonly',true);
		$("input.easyui-processbarSpinner").processbarSpinner('disable');
		//$("input.textbox-f").textbox('disable');
		//$('input.Idate').removeClass("Wdate");
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(true);
			} catch(e) {
				$(this).ckeditorGet().setReadOnly(true);
			}
		});
		$("a.bt-extra").linkbutton('disable');
		jwpf.disableLinkButton($("a.hyperlink").not(".validInView"));
		$("button.validInView").removeAttr("disabled");<#-- 查看状态下可操作开启 -->
		$("input[type=text].validInView").removeAttr("disabled").attr("readonly","readonly");
		<#list formList as form>
	            <#if !form.isMaster && !((form.isVirtual)!false)>
	                   $("#${form.key}").edatagrid('disableEditing');
	            </#if>    
	    </#list>
	    doHideFlowButton();
	}
	
	function doDisableSubDatagrid(tableKey) {
		if(operMethod=="view") {
			var subTabId = "#dv_tab_" + tableKey;
			$("input[type!=hidden],textarea,select,button", $(subTabId)).attr("disabled", "disabled");
			$("input:checkbox", $(subTabId)).removeAttr("disabled");
			$("button.validInView", $(subTabId)).removeAttr("disabled");<#-- 查看状态下可操作开启 -->
			jwpf.disableLinkButton($("a.hyperlink", $(subTabId)).not(".validInView"));
		}
	}


<#--
	//刷新子表
	function doRefreshSubtable(id) {
		 <#list formList as form>
	            <#if !form.isMaster>
	                   var ${form.key}="#"+"${form.key}";
	                   $(${form.key}).edatagrid({url:getSubUrl("${form.entityName}", id)});
	            </#if>    
	    </#list>
		
	}-->

	function formClear() {
		$('#viewForm').form('clear');
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setData("");
			} catch(e) {
				$(this).ckeditorGet().setData("");
			}
		});
		$("img.previewImageCls").attr("src", "");
	}
	
	//查看里的新增
	function doAdd() {
		setAddTitle();
	    hideTool();
	    showSave();
		operMethod="add";
		doEnable();
		formClear();
		clearAddField();
		<@fieldftl.initDynamicShowSubTable formList=formList isAdd=true isDoAdd=true/>
		<#if dynamicShowTable > <#-- 清空子表 -->
		$("#form_tabs_dynamic").html("");
		focusFirstElement();
		</#if>
	}
	
	//取消修改
	function doCancelEdit() {
		setViewTitle();
		hideTool();
		hideSave();
		var id = jwpf.getId();
		formClear();
		operMethod="view";
		doModify(id, doDisabled);
	}
	//刪除
	function doDel() {
                var ids = [];
                ids.push($("#id").val());
                if(window.onBeforeDelete) {
			           var flag = window.onBeforeDelete();
			           if(flag === false) {
			           	   return;
			           }
			    }
                    $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
                        if (r) {
                            var options = {
                            	<#-- 关联表表 -->
                                url : '${r"${ctx}"}/gs/gs-mng!delete.action?entityName=${entityName}&relevanceModule=${relevanceModule}',
                                data : {
                                    "ids" : ids
                                },
                                success : function(data) {
                                    if (data.msg) {
                                    	doRefreshModuleData();
                                    	doCloseCurrentTab();
                                    }
                                },
                                traditional:true
                            };
                            fnFormAjaxWithJson(options);
                        }
                    });
    }
    
    function initDynamicTab(tableList) {
    	$("#form_tabs_dynamic").html("");
    	if(tableList && tableList.length) {
    		var tabsDiv = $("<div id='tabs_dynamic_sub'></div>");
    		tabsDiv.appendTo("#form_tabs_dynamic");
    		tabsDiv.tabs({border:false});
    		for(var i=0,len=tableList.length;i<len;i++) {
    			var tab = tableList[i];
    			var title = tab.caption;
    			var content = "<table id='" + tab.key + "'></table>";
    			tabsDiv.tabs("add", {
    				title:title,
    				content:content
    			});
    		}	
    	}
    }
	
	function initAddSub(tableList, columnListMap) { 
   <#list formList as form>
        <#if !form.isMaster>
        	<#if form.visible>
        	<@fieldftl.initCheckFormAuth form=form isEnd=false/>
        	if(!tableList || $.inArray("${form.key}", tableList)>= 0) {
             <#assign methodName='get'+'${form.key}'>
             <#if form.listType=="TreeGrid">
             	initTreeEdatagrid("${form.entityName}","${form.key}", {
             		<#if (form.isVirtual)!false >
             		editing:false,
             		pagination : true,
             		pageList:[<#if ((form.pageSizeList)!"") != "">${form.pageSizeList}<#else>10,20,30,40,50,100,200,500</#if>],
					pageSize:<#if ((form.pageSize)!"") != "">${form.pageSize}<#else>20</#if>,
					sortName:"",
             		</#if>
					treeField:"${form.treeField}",
					iconCls : 'icon-edit',
					<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
					<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
					defaultRow : ${methodName}DefaultRow()
					<@fieldftl.initDatagridParam form=form />
					<@fieldftl.initDatagridToolBar form=form />
					<#-- 是否存在列，其需要根据值合并单元格 -->
					<@fieldftl.initMergeable form=form />
				}, ${methodName}(columnListMap));
             <#else>
                initEdatagrid("${form.entityName}","${form.key}", {
                    <#if (form.isVirtual)!false >
             		editing:false,
             		pagination : true,
             		pageList:[<#if ((form.pageSizeList)!"") != "">${form.pageSizeList}<#else>10,20,30,40,50,100,200,500</#if>],
					pageSize:<#if ((form.pageSize)!"") != "">${form.pageSize}<#else>20</#if>,
					sortName:"",
             		</#if>
					iconCls : 'icon-edit',
					"isVirtual" :${((form.isVirtual)!false)?string},
					<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
					<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
					defaultRow : ${methodName}DefaultRow()
					<@fieldftl.initDatagridParam form=form />
					<@fieldftl.initDatagridToolBar form=form />
					<#-- 是否存在列，其需要根据值合并单元格 -->
					<@fieldftl.initMergeable form=form />
				}, ${methodName}(columnListMap));
				<#-- 项目人员用js自定义合并单元格的事件 -->
				<#if form.extension??>
					<#if form.extension.queryPage??>
						<#assign eventList=form.extension.queryPage.eventList />
						<#if eventList?? && eventList?size gt 0>
			                <#list eventList as event>
			                     <#if event.key == "onAfterInitEdatagrid">
             	try {
             		var target = $("#${form.key}");
                	${event.content}
                } catch(e) {
                	alert("执行表单${form.caption}(${form.key})事件${event.key}出现异常:" + e);
                }
			                     	<#break/>
			                     </#if>
			                </#list>
			            </#if>
					</#if>          
				</#if>
             </#if>
				var dataObj${form.key} = {
					'total' : 0,
					'rows' : []
				};
				$("#${form.key}").edatagrid("loadData", dataObj${form.key});
				_loadSubCount_++;
			}
			<@fieldftl.initCheckFormAuth form=form isEnd=true/>
			</#if>	
		</#if>
   </#list>
		ctrlD();
	  }  
	  
	<#-- 初始化新增操作，供外部链接使用 -->  
	function _initAddParam_() {
		var hidParam = $("#hid_addParams").val();
		if(hidParam) {
			try {
				var paramObj = JSON.parse(hidParam);
				if(paramObj) {
					$.each(paramObj, function(k, v) {
						jwpf.setFormVal(k, v);
					});
				}
			}catch(e) {alert("初始化新增参数时出现异常：" + e);}
		}
	}
	  
	function _initTimer_() {
		if(_wait_) {
			clearInterval(_wait_);
		}
	    _wait_ = setInterval(function(){    
                    if(_loadSubCount_ >= _subTabCount_){    
                        clearInterval(_wait_);    
						setTimeout(initExpression, 0);
						if(operMethod=="add") {
							initFormDefaultValue();
							_initAddParam_();<#-- 初始化新增参数 -->
							if(window.onAfterAddInit) {
								window.onAfterAddInit();
							}
							autoRow();
							//$("#viewform").form("validate");
							focusFirstElement();
						} else if(operMethod=="edit" || operMethod=="modify"){
							autoRow();
						}
						<#--
						if(operMethod=="add" || operMethod=="edit") { 
							initAutoIdFieldValue();
						}新增或复制 -->
						initBody();
                    }
       },300);
	}	
	  
	//初始化所有事件及表达式支持
	function initEvent() {
		<#list formList as form>
			<#if form.isMaster>
		         <#assign fields=form.fieldList!>
	                <#if fields??&&fields?size gt 0>
	                    <#list fields as field>
	                    	<@fieldftl.initFieldEvent field=field/>
	                    </#list>
	               </#if>
	        </#if>       
	   </#list>                     
	}
	//初始化公式代码及汇总统计
	function initExpression() {
	   <#list formList as form>
		         <#assign fields=form.fieldList!>
	                <#if fields??&&fields?size gt 0>
	                    <#list fields as field>
	                    	<@fieldftl.initFieldExpression field=field formKey=form.key isMaster=form.isMaster />	
	                    </#list>
	               </#if>
	        <#if !form.isMaster>    
	            <@fieldftl.initStatistics form=form isInit="true"/>
				<@fieldftl.initStatistics form=form isInit="false"/>
			</#if>	
	   </#list>
	}
	
	<#--子表初始化相关combo -->
	function initCombo(){
		   <#list formList as form>
		     <#if !form.isMaster>
		         <#assign fields=form.fieldList!>
	                <#if fields??&&fields?size gt 0>
	                    <#list fields as field>
		                  <@fieldftl.initControlsData field=field type="subQuery" prefix=("sub_"+form.key+"_") entityName=form.entityName formKey=form.key />
	             		</#list>
	          		</#if>
	        </#if>
	     </#list>
  
  }
  
  
		<#list formList as form>
        <#if !form.isMaster>
             <#assign methodName='get'+'${form.key}'>
             function ${methodName}(columnListMap){
             	<#assign fields=form.fieldList/>
             	var frozenFields = [[ <#-- 冻结列 -->
	                 <#if fields??&&fields?size gt 0>
         			    <#assign layerend = 0>
         			    <#assign nohasfield = 1>
                        <#list 1..fields?size as layer>
                        	<#assign i=0>
                        	<#assign layerend = 1>
                            <#list fields as field>
                            	<#if field.key?? && field.key != "">
                            	<#if (!field.editProperties.layer?? && layer = 1) || (field.editProperties.layer?? && field.editProperties.layer = layer)>
                                <#if field.queryProperties.showInGrid && ((field.queryProperties.frozenColumn)!false)>
                                  <#if layer gt 1 && i = 0>,[</#if>
                            	  <#assign layerend = 0>
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>     
								  <#if i gt 0>,</#if>
                                	<@fieldftl.initSubQuery field=field prefix=("sub_"+form.key+"_") form=form />                                  
                                  <#assign i=i+1>	
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>    
                                </#if>
                                </#if>
                                </#if>
                            </#list>
                            <#if layerend = 0 && i gt 0>]<#assign nohasfield = 0></#if><#if layerend = 1><#break></#if>
                        </#list>
                        <#if nohasfield = 1>]</#if>
                    </#if>
	                ];
                handleFilterFields(columnListMap, frozenFields, "${form.key}");
             	var fields = [[
         			<#if fields??&&fields?size gt 0>
         			    <#assign layerend = 0>
         			    <#assign nohasfield = 1>
                        <#list 1..fields?size as layer>
                        	<#assign i=0>
                        	<#assign layerend = 1>
                            <#list fields as field>
                            	<#if field.key?? && field.key != "">
                            	<#if (!field.editProperties.layer?? && layer = 1) || (field.editProperties.layer?? && field.editProperties.layer = layer)>
                                <#if field.queryProperties.showInGrid && !((field.queryProperties.frozenColumn)!false)>
                                  <#if layer gt 1 && i = 0>,[</#if>
                            	  <#assign layerend = 0>
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>     
								  <#if i gt 0>,</#if>
                                	<@fieldftl.initSubQuery field=field prefix=("sub_"+form.key+"_") form=form />                                  
                                  <#assign i=i+1>	
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>    
                                </#if>
                                </#if>
                                </#if>
                            </#list>
                            <#if layerend = 0 && i gt 0>]<#assign nohasfield = 0></#if><#if layerend = 1><#break></#if>
                        </#list>
                        <#if nohasfield = 1>]</#if>
                    </#if>
                ];
               handleFilterFields(columnListMap, fields, "${form.key}");  
		       return {"frozenFields":frozenFields,"fields":fields};
		    }
		    <@fieldftl.initFormFieldDefaultValue form=form />
		    <@fieldftl.initFormFieldOnListEvent form=form />
        </#if>
    </#list>
    
    function handleFilterFields(columnListMap, fields, tableKey) {
    			if(columnListMap && columnListMap[tableKey]) {
               		var clms = columnListMap[tableKey];
               		for(var i=0,len=fields.length; i<len; i++) {
               			for(var j=0; j<fields[i].length; j++) {
               				var fld = fields[i][j];
	               			if(fld.field && $.inArray(fld.field, clms) == -1) {
	               				fld["hidden"] = true;
	               			}
               			}
               		}
               }      
    }

    function getSubUrl(entityName, id) {
		return "${r"${ctx}"}/gs/gs-mng!querySubList.action?entityName=" + entityName
				+ "&id=" + id;
	}
	
	function getFormUrl(entityName, id) {
		return "${r"${ctx}"}/gs/gs-mng!queryFormList.action?entityName=" + entityName
				+ "&id=" + id;
	}

	function initEdatagrid(entityName,id, options, columns) {
		var fcs1 =  [{
				field : 'ck',
				checkbox : true,
				rowspan : 1,<#-- 新版easyui不能大于field数组length -->
				colspan : 1
		}];
		var fcs = [];
		for(var i = 0; i < columns.frozenFields.length; i++){
			if(i===0) {
				fcs.push(fcs1.concat(columns.frozenFields[0]));
			} else {
				fcs.push(columns.frozenFields[i]);
			}
		}
		var opt = {
			iconCls : 'icon-edit',
			//height:330,<#-- 已可自适应高度 -->
			nowrap : false,
			striped : true,
			//singleSelect:true,
			//idField:'itemid',
			url : null,
			remoteSort : false,
			sortName : "sortOrderNo",
			rownumbers : true,
			frozenColumns : fcs,
			columns : columns.fields,
			onRowContextMenu : function(e, rowIndex, rowData) {
				//查看状态下没有右键功能
                if(operMethod!="view" && options.editing !== false){
                	e.preventDefault();
    				$('#mm').data("rowIndex", rowIndex);
    				$('#mm').data('ChildTableIndex', id);
    				$('#mm').menu('show', {
    					left : e.pageX,
    					top : e.pageY - 8
    				});
                }
				
			},
			//双击查看一条记录详细信息
			onDblClickRow : function(rowIndex, rowData) {
				 /*if(operMethod!="view"){
					openDialog(entityName,rowIndex, id);
					$('#' + id).edatagrid('endEdit', rowIndex);
				 }*/
			},
			onLoadError:function() {
    			$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="数据加载异常"/>', 'info');
    		}
		};
		var opts = $.extend(true, {}, opt, options);
		$('#' + id).edatagrid(opts);
	}
	
	function initTreeEdatagrid(entityName,id, options, columns) {
		var fcs1 =  [{
				field : 'ck',
				checkbox : true,
				rowspan : 1,
				colspan : 1
		}];
		var fcs = [];
		for(var i = 0; i < columns.frozenFields.length; i++){
			if(i===0) {
				fcs.push(fcs1.concat(columns.frozenFields[0]));
			} else {
				fcs.push(columns.frozenFields[i]);
			}
		}
		var opt = {
			height:330,
			nowrap : false,
			striped : true,
			tree:true,
			lines:true,
			isShowLevel:true,
			iconCls : 'icon-edit',
			idField:'autoCode',
			url : null,
			remoteSort : false,
			sortName : "sortOrderNo",
			rownumbers : true,
			frozenColumns : fcs,
			columns : columns.fields,
			onContextMenu : function(e, rowData) {
				//查看状态下没有右键功能
                if(operMethod!="view"  && options.editing !== false ){
                	e.preventDefault();
    				$('#mm_treeGrid').data("rowIndex", rowData);
    				$('#mm_treeGrid').data('ChildTableIndex', id);
    				$('#mm_treeGrid').menu('show', {
    					left : e.pageX,
    					top : e.pageY - 8
    				});
                }
				
			},
			//双击查看一条记录详细信息
			onDblClickRow : function(rowData) {
				 /*if(operMethod!="view"){
					openDialog(entityName,rowIndex, id);
					$('#' + id).edatagrid('endEdit', rowData);
				 }*/
			},
			onLoadError:function() {
    			$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="数据加载异常"/>', 'info');
    		}
		};
		var opts = $.extend(true, {}, opt, options);
		$('#' + id).edatagrid(opts);
	}
	
	function initSub(id, tableList, columnListMap) {
		if(!id){
		id="${r"${param.id}"}";
		}
	<#list formList as form>
        <#if !form.isMaster>
        	<#if form.visible>
        		<@fieldftl.initCheckFormAuth form=form isEnd=false/>
        	if((!tableList || $.inArray("${form.key}", tableList)>= 0)<#if (form.dynamicShow)!false> && columnListMap</#if>) { 
         	<#assign methodName='get'+'${form.key}'>
         	<#if form.listType=="TreeGrid">
         	initTreeEdatagrid("${form.entityName}","${form.key}", {
         		<#if (form.isVirtual)!false >
             	editing:false,
             	pagination : true,
             	pageList:[<#if ((form.pageSizeList)!"") != "">${form.pageSizeList}<#else>10,20,30,40,50,100,200,500</#if>],
				pageSize:<#if ((form.pageSize)!"") != "">${form.pageSize}<#else>20</#if>,
				"url" : getFormUrl("${form.entityName}", id),	
				sortName:"",
				<#else>
				"url" : getSubUrl("${form.entityName}", id),	
             	</#if>
         		"treeField":"${form.treeField}",
				"remoteSort" : true,
				<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
				<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
				defaultRow : ${methodName}DefaultRow(),
				onLoadSuccess :function(data){
					_loadSubCount_++;
					doDisableSubDatagrid("${form.key}");
					<@fieldftl.initDatagridOnLoadSuccess form=form />
				}
				<@fieldftl.initDatagridParam form=form />
				<@fieldftl.initDatagridToolBar form=form />
				<#-- 是否存在列，其需要根据值合并单元格 -->
				<@fieldftl.initMergeable form=form />
			}, ${methodName}(columnListMap));
         	<#else>
         	initEdatagrid("${form.entityName}","${form.key}", {
				<#if (form.isVirtual)!false >
             	editing:false,
             	pagination : true,
             	pageList:[<#if ((form.pageSizeList)!"") != "">${form.pageSizeList}<#else>10,20,30,40,50,100,200,500</#if>],
				pageSize:<#if ((form.pageSize)!"") != "">${form.pageSize}<#else>20</#if>,
				"url" : getFormUrl("${form.entityName}", id),	
				sortName:"",
				<#else>
				"url" : getSubUrl("${form.entityName}", id),	
             	</#if>
				"remoteSort" : true,
				<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
				<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
				defaultRow : ${methodName}DefaultRow(),
				onLoadSuccess :function(data){
					_loadSubCount_++;
					doDisableSubDatagrid("${form.key}");
					<@fieldftl.initDatagridOnLoadSuccess form=form />
				}
				<@fieldftl.initDatagridParam form=form />
				<@fieldftl.initDatagridToolBar form=form />
				<#-- 是否存在列，其需要根据值合并单元格 -->
				<@fieldftl.initMergeable form=form />
			}, ${methodName}(columnListMap));
			</#if>
			}	
				<@fieldftl.initCheckFormAuth form=form isEnd=true/>
         	</#if>
        </#if>
    </#list>
    	$('#subTableDetailInfo')
				.dialog(
						{
							onOpen : function() {
								var openUrl = $('#subTableDetailInfo').data(
										"url");
								$("#ifr_subtable")[0].contentWindow.window.document.location
										.replace(openUrl);
							},
							onClose : function() {
								$("#ifr_subtable")[0].contentWindow.window.document.location
										.replace("${r"${loadUrl}"}");
							}
						});
		ctrlD();

	}
	
	//Ctrl+D删除(支持多行一起删除)
	function ctrlD() {
	   <#list formList as form>
	            <#if !form.isMaster>
	            $('#dv_tab_${form.key}').live('keydown', 'Ctrl+D', function(evt) {
	                   $('#mm').data('ChildTableIndex','${form.key}');
						evt.preventDefault();
						doDelete();
						return false;
					});
	            </#if>    
	    </#list>
	}

	//子表最新复制方法
	function doCopy() {
		var rowIndex = $('#mm').data('rowIndex');
		var ChildTableIndex = $('#mm').data('ChildTableIndex');
		var rows = $("#" + ChildTableIndex).edatagrid("getRows");
		var newRow = $.extend({}, rows[rowIndex]);
		newRow.id = null;
		$("#" + ChildTableIndex).edatagrid('addRow', newRow);
		jwpf.doFormExpression(ChildTableIndex);
	}
	//子表中插入一条记录
	function doInsert() {
		var rowIndex = $("#mm").data("rowIndex");
		var ChildTableIndex = $('#mm').data('ChildTableIndex');
		var dg = $("#" + ChildTableIndex);
		var opts = dg.edatagrid("options");
		dg.edatagrid('insertNewRow', parseInt(rowIndex));
		<#-- 是否存在列，其需要根据值合并单元格，第0列插入不带值 -->
		if(opts.mergeable && rowIndex>0){
			var rows = dg.edatagrid("getRows");
			var frozenColumnFields = dg.edatagrid("getColumnFields", true);
			var columnFields = dg.edatagrid("getColumnFields");
			$.each(frozenColumnFields, function(l, v){
				if(dg.datagrid("getColumnOption", v).mergeable){
					rows[rowIndex][v] = rows[rowIndex-1][v];
				}
			});
			$.each(columnFields, function(l, v){
				if(dg.datagrid("getColumnOption", v).mergeable){
					rows[rowIndex][v] = rows[rowIndex-1][v];
				}
			});
			dg.edatagrid("refreshRow", rowIndex);
			dg.edatagrid("mergeEqualsCells");
		}
	}
	//插入一个子节点
	function doTreeInsert() {
		var rowData = $("#mm_treeGrid").data("rowIndex");
		var ChildTableIndex = $('#mm_treeGrid').data('ChildTableIndex');
		$("#" + ChildTableIndex).edatagrid('insertNewRow', rowData);
	}

	//上移
	function doMoveUp() {
		var rowIndex = parseInt($('#mm').data('rowIndex'));
		if (rowIndex > 0) {
			var ChildTableIndex = $('#mm').data('ChildTableIndex');
			var rows = $("#" + ChildTableIndex).edatagrid("getRows");
			var currentRow = $.extend({}, rows[rowIndex]);
			var lastRow = $.extend({}, rows[rowIndex - 1]);
			$('#' + ChildTableIndex).edatagrid('updateRow', {
				index : rowIndex - 1,
				row : currentRow
			});
			$('#' + ChildTableIndex).edatagrid('updateRow', {
				index : rowIndex,
				row : lastRow
			});
		}
		$('#' + ChildTableIndex).datagrid('unselectRow', parseInt(rowIndex));
		$('#' + ChildTableIndex).datagrid('selectRow', parseInt(rowIndex - 1));
	}
	
	function doTreeMoveUp() {
		var rowIndex = $('#mm_treeGrid').data('rowIndex');
		var ChildTableIndex = $('#mm_treeGrid').data('ChildTableIndex');
		var currentCode = rowIndex.autoCode;
		var pCode = rowIndex.autoParentCode;
		var arr = [];
		if(pCode){
			var p =$('#' + ChildTableIndex).treegrid('getParent',currentCode);
			arr = p.children;
		}else{
			arr = $('#' + ChildTableIndex).treegrid('getRoots');
		}
		var len = arr.length;
		for(var i=0;i<len;i++){
			if(arr[i].autoCode==currentCode){
				if(i>0){
					var a = $.extend({},arr[i-1]);
					var b = $.extend({},arr[i]);
					var c1 = a.autoCode;
					var c2 = b.autoCode;
					setTreeNodeCode(b, c1);
					setTreeNodeCode(a, c2);
					$('#' + ChildTableIndex).edatagrid("update",  {
						id: c1,
						row : b,
						update : false
					});
		 			$('#' + ChildTableIndex).edatagrid("update",  {
						id: c2,
						row : a,
						update : false
					});  
					break;
				}
			}
		}  	
	}
	
	function doSetDataGridParam(tableKey) {
		$('#mm').data('ChildTableIndex', tableKey);
		var rows = $('#'+tableKey).edatagrid('getSelections');
		if(rows && rows.length) {
			var index = $('#'+tableKey).edatagrid('getRowIndex', rows[rows.length-1]);
			$('#mm').data('rowIndex', index);
			return true;
		} else {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.recordInfo"/>',
							'info');
			return false;
		}
	}
	
	function doSetTreeGridParam(tableKey) {
		$('#mm_treeGrid').data('ChildTableIndex', tableKey);
		var rows = $('#'+tableKey).edatagrid('getSelections');
		if(rows && rows.length) {
			$('#mm_treeGrid').data('rowIndex', rows[rows.length-1]);
			return true;
		} else {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.recordInfo"/>',
							'info');
			return false;
		}
	}
	
	function setTreeNodeCode(node, code) {
		node.autoCode = code;
		var clds = node.children;
		if(clds && clds.length) {
			for(var i=0,len=clds.length;i<len;i++) {
				clds[i].autoParentCode = code;
				clds[i]._parentId = code;
			}
		}
	}

	//下移
	function doMoveDown() {
		var rowIndex = parseInt($('#mm').data('rowIndex'));
		var ChildTableIndex = $('#mm').data('ChildTableIndex');
		var rows = $("#" + ChildTableIndex).edatagrid("getRows");
		if (rowIndex < rows.length - 1) {
			var currentRow = $.extend({}, rows[rowIndex]);
			var nextRow = $.extend({}, rows[rowIndex + 1]);
			$('#' + ChildTableIndex).edatagrid('updateRow', {
				index : rowIndex + 1,
				row : currentRow
			});
			$('#' + ChildTableIndex).edatagrid('updateRow', {
				index : rowIndex,
				row : nextRow
			});
		}
		$('#' + ChildTableIndex).datagrid('unselectRow', parseInt(rowIndex));
		$('#' + ChildTableIndex).datagrid('selectRow', parseInt(rowIndex + 1));

	}
	
	function doTreeMoveDown() {
		var rowIndex = $('#mm_treeGrid').data('rowIndex');
		var ChildTableIndex = $('#mm_treeGrid').data('ChildTableIndex');
		var currentCode = rowIndex.autoCode;
		var pCode = rowIndex.autoParentCode;
		var arr = [];
		if(pCode){
			var p =$('#' + ChildTableIndex).treegrid('getParent',currentCode);
			arr = p.children;
		}else{
			arr = $('#' + ChildTableIndex).treegrid('getRoots');
		}
		var len = arr.length;
		for(var i=0;i<len;i++){
			if(arr[i].autoCode==currentCode){
				if(i<len-1){
					var a = $.extend({},arr[i]);
					var b = $.extend({},arr[i+1]);
					var c1 = a.autoCode;
					var c2 = b.autoCode;
					setTreeNodeCode(b, c1);
					setTreeNodeCode(a, c2);
					$('#' + ChildTableIndex).edatagrid("update",  {
						id: c1,
						row : b,
						update : false
					});
		 			$('#' + ChildTableIndex).edatagrid("update",  {
						'id': c2,
						'row' : a,
						update : false
					});  
					break;
				}
			}
		}  		
	}

	//右键批量删除
	function doDelete() {
     	var ChildTableIndex = $('#mm').data('ChildTableIndex');
		var rows = $('#'+ChildTableIndex).edatagrid('getSelections');
		var opts = $('#'+ChildTableIndex).edatagrid("options");
		if (rows.length < 1) {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.question"/>',
							'info');
		} else {
			$.messager.confirm(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.info"/>',
					function(r) {
					if(r){
						$("#"+ChildTableIndex).edatagrid("deleteRows", rows);
						<#-- 刷新td的rowspan属性 -->
						if(opts.mergeable){
							$('#'+ChildTableIndex).edatagrid("mergeEqualsCells");
						}
						jwpf.doFormExpression(ChildTableIndex);				
					}
			});
		}
	}
	
	//右键批量删除
	function doTreeDelete() {
			$.messager.confirm(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.info"/>',
					function(r) {
					if(r){
						var rowData = $("#mm_treeGrid").data("rowIndex");
						var ChildTableIndex = $('#mm_treeGrid').data('ChildTableIndex');
						$("#" + ChildTableIndex).edatagrid('removeNodes', rowData);
					}
			});
	}

	//子表明细
	function openDialog(entityName,RowIndex, ChildTableIndex) {
		var openUrl = "${r"${ctx}"}/gs/gs-mng!viewSub.action?entityName="+entityName+"&RowIndex="
				+ RowIndex + "&ChildTableIndex=" + ChildTableIndex;
		$('#subTableDetailInfo').data("url", openUrl);
		$('#subTableDetailInfo').dialog('open');
	}
     
      
 	//添加附件
    function doAccessory(key, stat) {	
		var attachId=$("#"+key).val();
		var id=$("#id").val();
		var status = stat || $("#status").val();
		 var opts ={"attachId":attachId,
   				 "id":id,
   				 "key":key,
   				 "entityName":"${entityName}",
   				 "dgId":"queryList",
   				 "flag":"mainEdit",
   				 "readOnly" : false
   				};
   	<c:choose>
	<c:when test="${r"${param.isEdit=='1'}"}">		
		opts.readOnly = false;
       	doAttach(opts);
	</c:when>
	<c:when test="${r"${param.isEdit=='2'}"}">
		opts.readOnly = true;
       	doAttach(opts);
	</c:when>
	<c:otherwise>
		if(status == "10") {
			opts.readOnly =false;
       	 	//doAttach(opts);
    	} else {
    		opts.readOnly =true;
       	 	//doAttach(opts);
    	}
    	doAttach(opts);
   	</c:otherwise>
    </c:choose>
    }
    
    //列表添加附件
    function doAccessoryOnList(fieldVal, fieldKey, tableKey, rowIndex, entityName, stat) {
		var opts ={"attachId":fieldVal,
   				 "id":rowIndex,
   				 "key":fieldKey,
   				 "entityName":entityName||"",
   				 "dgId":tableKey,
   				 "flag":"subQuery",
   				 "readOnly" : false
   		};
		   	<c:choose>
			<c:when test="${r"${param.isEdit=='1'}"}">
				opts.readOnly = false;
		       	doAttach(opts);
			</c:when>
			<c:when test="${r"${param.isEdit=='2'}"}">
				opts.readOnly = true;
		       	doAttach(opts);
			</c:when>
			<c:otherwise>
				var status = stat || $("#status").val();
				if(status == "10") {
					opts.readOnly =false;
		       	 	doAttach(opts);
		    	} else {
		    		opts.readOnly =true;
		       	 	doAttach(opts);
		    	}
		   	</c:otherwise>
		    </c:choose>
    }

	//更新附件ID flag: mainQuery-主实体查询，mainEdit-主实体编辑，subQuery-子实体查询，subEdit-子实体编辑
    function doUpdateAttach(opts) {
        if(opts.flag == "subQuery") {
        	var rowIndex = parseInt(opts.id);
        	var tableKey = opts.dgId;
        	var row = getObject(rowIndex, tableKey);
        	if(row) {
				if(operMethod == "view") {<#-- 查看状态下上传附件 -->
					if(row[opts.key] != opts.attachId) {
						var options = {
			    			url : '${r"${ctx}"}/gs/gs-mng!updateAttach.action',
			                data : {
			                	"entityName":opts.entityName,
			                    "id" : row["id"],
			                    "attachId": opts.attachId,
			                    "field" : opts.key
			                },
							async: false,
							success : function(data) {
								if(data.msg) {
									$("#" + tableKey).edatagrid("load");//刷新子表
								}
							}
						};
						fnFormAjaxWithJson(options);
					}
				} else {
        			row[opts.key] = opts.attachId;
        			$("#"+tableKey).edatagrid("refreshRow", rowIndex);
        		}
        	}
        } else {
        	if(opts.id) { <#-- 非新增操作时 -->
		        var options = {
		    			url : '${r"${ctx}"}/gs/gs-mng!updateAttach.action',
		                data : {
		                	"entityName":opts.entityName,
		                    "id" : opts.id,
		                    "attachId": opts.attachId,
		                    "field" : opts.key
		                },
						async: false,
						success : function(data) {
							if(data.msg) {
								$("#"+opts.key).val(opts.attachId);
								if(opts.key=="atmId"){		
		        					doRefreshModuleData();
		        				}
							}
						}
				};
				fnFormAjaxWithJson(options);
			} else {
				$("#"+opts.key).val(opts.attachId);
			}
        }
    }
	function doProcess(){
		if(window.onBeforeStartProcess) {
           var flag = window.onBeforeStartProcess();
           if(flag === false) {
           	   return;
           }
        }
	 	$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.operSure.title"/>',
			function(r) {
           		if(r) {
           			var tableName = "${entityName}";
	 				var id = $("#id").val();
           			doStartProcess(id,tableName, doAfterStartProcess);
           		}
           	}
        );
	}
	
	function doSelfProcess() {
		if(window.onBeforeStartProcess) {
           var flag = window.onBeforeStartProcess();
           if(flag === false) {
           	   return;
           }
        }
		$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.selfApproveSure"/>',
			function(r) {
           		if(r) {
                 	jwpf.doStartOrCancelApprove("${entityName}", $("#id").val(), "startApprove", function() {doAfterSelfProcess("35");});
					if(window.onAfterStartProcess) {
					   window.onAfterStartProcess();
					}
                } 
        	}
        );
	}
	
	function doAfterSelfProcess(status) {
		$("#status").val(status);
		doHideFlowButton();
		doRefreshModuleData();
	}
	
	//打开作废原因窗口
	function openCancelWin(){
		$('#cancelReasonWin').window('open');
		$('#cancelReason').removeAttr("disabled");
	}
	//作废
	function doInvalidData() {
		var text = $('#cancelReason').val();
		var reason = $.trim(text);
		if(reason){
			doCancelProcess("invalidProcess", reason, doAfterInvalidData);
		}else{
			$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>','<s:text name="system.javascript.alertinfo.notNull.title"/>','info');
		}
	}
	//作废后执行
	function doAfterInvalidData() {
		//$("#status").val(status);
		if(window.onAfterInvalidData) {
			window.onAfterInvalidData();
		}
		var isChange = $('#cancelReasonWin').data("isChange");
		if(isChange) {
			doCopyMainTab(true, __tabTitle);
		} 
		$('#cancelReasonWin').window('close');
		//doHideFlowButton();
		//doRefreshModuleData();
	}
	
	<#-- 取消审核 -->
	function doCancelSelfProcess() {
		$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.cancelSelfApproveSure"/>',
			function(r) {
           		if(r) {
           			if(!window.onBeforeCancelProcess || window.onBeforeCancelProcess()!=false) {
           				jwpf.doStartOrCancelApprove("${entityName}", $("#id").val(), "cancelApprove", function() {
                 			doAfterSelfProcess("10");
                 			if(window.onAfterCancelProcess) {
                 				window.onAfterCancelProcess();          			
                 			}
                 		});
           			}
                } 
        	}
        );
	}

	function doCancelProcess(operFlag, reason, callFunc, callFunc1) {<#-- 取消审批，对于审批中的单据进行操作，operFlag：cancelModels--取消审批， cancelProcess--撤销审批，-->
		var operFlag = operFlag || "cancelModels";
		var reason = reason || "";
		$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.operSure.title"/>',
			function(r) {
	           	if(r) {
	           		if((callFunc1 && callFunc1()!=false) || (!callFunc1 && (!window.onBeforeCancelProcess || window.onBeforeCancelProcess()!=false))) {
	           			var id = $("#id").val();
	                 	var options = {
				    			url : '${r"${ctx}"}/gs/process!' + operFlag + '.action?entityName=${entityName}',
				                data : {
				                	//"entityName":"${entityName}",
				                    "id" : id,
				                    "description":reason
				                },
								async: false,
								success : function(data) {
									if(data.msg) {
										if(callFunc) {
										    callFunc();
										} 
										if(operFlag == "cancelModels" && window.onAfterCancelProcess) {
											window.onAfterCancelProcess();<#-- 取消审批 -->
										}
										doRefreshModuleData();
										doCancelEdit();
									}
								}
						};
						fnFormAjaxWithJson(options); 
	           		}
                } 
        	}
        );
	}
	
	<s:if test='#session.USER.isSuperAdmin=="1"'><#-- 撤销审批通过的单据，恢复为草稿状态 -->
	function doCancelApproved() {
		doCancelProcess("cancelProcess","",window.onAfterCancelProcess,window.onBeforeCancelProcess);
	}
	</s:if>
	
	function doAfterStartProcess() {
		doRefreshModuleData();
		if(window.onAfterStartProcess) {
           window.onAfterStartProcess();
        }
		doCloseCurrentTab();
	}
	
	function doViewProcess(isSelf){
		var id = $("#id").val();
		if(isSelf) {
			var title = "<s:text name="${i18nKey!titleName}"/>-" + id + "-<s:text name="审核操作历史"/>";
			top.addTab(title,'${r"${ctx}"}/sys/workflow/all-approve-his-log.action?id=' + id + '&entityName=${entityName}');
		} else {
			var pId = $("#flowInsId").val();
			var title = "<s:text name="${i18nKey!titleName}"/>-" + id + "-<s:text name="system.module.flowDiagram.title"/>";
			top.addTab(title,'${r"${ctx}"}/sys/workflow/process-info.action?processInstanceId=' + pId + '&entityName=${entityName}');
		}
	}

	function doCloseCurrentTab() {
		if(window.top.closeCurrentTab) {
			window.top.closeCurrentTab();
		}
    }
    
	function doViewReport(tptName, type, callFunc) {
		if(callFunc) {
			callFunc();
			return true;
		}
		var id = $("#id").val();
		tptName = tptName.toLowerCase();
		top.addTab("<s:text name="${i18nKey!titleName}"/>-<s:text name="system.button.report.title"/>-" + tptName + "-" + id,'${r"${ctx}"}/sys/report/report.action?rptName=' + tptName + "&id=" + id + "&type=" + (type?type:""));
	}
	
	function __doListButtonClick(fieldVal, fieldKey, tableKey, rowIndex, callFunc) {
		if(callFunc) {
			callFunc(fieldVal, fieldKey, tableKey, rowIndex);
		}
	}
	
	function __getListButton(fieldVal, fieldKey, tableKey, rowIndex, title, validInView) {
		var astr = [];
		astr.push('<button type="button" class="button_big ' + ((validInView)?"validInView":"") + '"');
		astr.push(' onclick="__doListButtonClick(\'');
		astr.push(fieldVal);
		astr.push('\',\'');
		astr.push(fieldKey);
		astr.push('\',\'');
		astr.push(tableKey);
		astr.push('\',\'');
		astr.push(rowIndex);
		astr.push('\',');
		astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
		astr.push(')"');
		astr.push('>');
		astr.push(title);
		astr.push('</button>');
		return astr.join("");
	}
	
	function __getListHyperLink(fieldVal, fieldKey, tableKey, rowIndex, title, validInView) {
					var astr = [];
					astr.push('<a href="javascript:void(0);" style="text-decoration: underline;"');
					astr.push(' class="hyperlink ' + ((validInView)?"validInView":"") + '"');
					astr.push(' onclick="__doListButtonClick(\'');
					astr.push(fieldVal);
					astr.push('\',\'');
					astr.push(fieldKey);
					astr.push('\',\'');
					astr.push(tableKey);
					astr.push('\',\'');
					astr.push(rowIndex);
					astr.push('\',');
					astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
					astr.push(')"');
					astr.push('>');
					astr.push(title);
					astr.push('</a>');
					return astr.join("");
	}
	
	function __doListChange(obj, fieldVal, fieldKey, tableKey) {
		if(fieldVal) {
	 			try {
	 				var ftd = $(obj).closest("td[field]");
	 				if(ftd) {
	 					var ftr = ftd.parent();
						var rowIndex = ftr.attr("datagrid-row-index");
						if(rowIndex != undefined && rowIndex != null) {
							var callFunc = eval('onListChangeFor_'+tableKey+'_'+fieldKey);
							if(callFunc) {
								callFunc(fieldVal, fieldKey, tableKey, parseInt(rowIndex));
							}
						}
	 				}
		 		} catch(ex) {
		 			alert(ex);
		 		}
	 	}
	}
	
	<@fieldftl.initButtonEvent formList=formList />
	
	<#-- 单据变更 -->
	function doChange() {
		var stat = $("#status").val();
		if(stat!="40") { //还没有作废
			$.messager.confirm(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="变更操作将自动作废本单据（作废时需要填写作废原因），确定要作废并变更吗？"/>',
				function(r) {
					if(r){
						$('#cancelReasonWin').data("isChange","1");
						openCancelWin();	
					}
			});	
		} else {
			doCopyMainTab(true, __tabTitle);
		}
	}
	
	$(function() {
		$("#cancelReasonWin").window({
				width:350,
				height:205,
				onOpen:function() {
					$(this).window("move", {
						top:($(window).height()-205)*0.5,
						left:($(window).width()-350)*0.5
					});
				},
				onClose:function() {
					$("#cancelReasonWin").removeData("isChange");
				}
		});
	});	
</script>
</head>
<body>
<c:choose>
	<c:when test="${r"${param.isEdit=='1'}"}"> <#-- 驳回时使用 -->
	<div region="north" style="overflow:hidden;" border="false" id="toolbar_layout">
	    <div class="datagrid-toolbar">
								<a id="tcancel" href="javascript:void(0);" onclick="doCancelEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><s:text name="system.button.cancel.title"/>(U)</a>
	                             <security:authorize
	                             			<#-- 关联表 -->
											url="/gs/gs-mng!save.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
	                             	<a id="tsave" href="javascript:void(0);" onclick="doSaveObj()" class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/>(S)</a>
	                             </security:authorize>    
	                             <security:authorize
											url="/gs/gs-mng!modify.action?entityName=${entityName}">
											<a id="tedit" href="javascript:void(0);" onclick="doSetFormToEdit()"
												class="easyui-linkbutton" plain="true" iconCls="icon-edit"
												><s:text name="system.button.modify.title"/>(M)</a>
								 </security:authorize>
								 <security:authorize
													url="/sys/attach/attach.action?entityName=${entityName}">
			                            <a id="tattach" href="javascript:void(0);" onclick="doAccessory('atmId')" class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name="system.button.accessory.title"/>(Y)</a>
			                     </security:authorize>
	    </div>
	</div>                        	
	</c:when>
	<c:when test="${r"${param.isEdit=='2'}"}"> <#-- 只允许查看时使用 -->
	<div region="north" style="overflow:hidden;" border="false" id="toolbar_layout">
	    <div class="datagrid-toolbar">
	    <security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
		<a id="tattach" href="javascript:void(0);" onclick="doAccessory('atmId')" class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name="system.button.accessory.title"/>(Y)</a>
	    </security:authorize>
		<@fieldftl.initButtonOnViewPage entityName=entityName moduleKey=moduleKey formList=formList isDisplayReport=true isReportOnly=true />
	    </div>
	</div>                        	
	</c:when>
	<c:when test="${r"${param.isEdit=='3'}"}"> <#-- 快捷新增时使用，只有保存按钮 -->
	<div region="north" style="overflow:hidden;" border="false" id="toolbar_layout">
	    <div class="datagrid-toolbar">
	                             <security:authorize
	                             			<#-- 关联表 -->
											url="/gs/gs-mng!save.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
	                             	<a id="tsave" href="javascript:void(0);" onclick="doSaveObj()" class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/>(S)</a>
	                             </security:authorize>
	    </div>
	</div>                        	
	</c:when>	
	<c:otherwise>
<div region="north" style="overflow:hidden;" border="false" id="toolbar_layout">
                        <div class="datagrid-toolbar">
                             <a id="tclose" href="javascript:void(0);" onclick="doCloseCurrentTab()" class="easyui-linkbutton" plain="true" iconCls="icon-cancel" title="<s:text name="system.button.close.title"/>">(Q)</a>
                             <a id="tcancel" href="javascript:void(0);" onclick="doCancelEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><s:text name="system.button.cancel.title"/>(U)</a>
                             <a id="trefresh" href="javascript:void(0);" onclick="doCancelEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-reload" title="<s:text name="刷新"/>"></a>
                             <security:authorize
                             			<#-- 关联表 -->
										url="/gs/gs-mng!save.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
                             	<a id="tsave" href="javascript:void(0);" onclick="doSaveObj()" class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/>(S)</a>
                             </security:authorize>
		                           <security:authorize
												url="/gs/gs-mng!view.action?entityName=${entityName}">
		                            <a id="tfirst" href="javascript:void(0);" onclick="doView('first')" class="easyui-linkbutton" plain="true" iconCls="icon-first" title="<s:text name="system.button.first.title"/>">(F)</a>
		                            <a id="tprev" href="javascript:void(0);" onclick="doView('pre')" class="easyui-linkbutton" plain="true" iconCls="icon-prev" title="<s:text name="system.button.prev.title"/>">(←)</a>
		                            <a id="tnext" href="javascript:void(0);" onclick="doView('next')" class="easyui-linkbutton" plain="true" iconCls="icon-next" title="<s:text name="system.button.next.title"/>">(→)</a>
		                            <a id="tlast" href="javascript:void(0);" onclick="doView('last')" class="easyui-linkbutton" plain="true" iconCls="icon-last" title="<s:text name="system.button.last.title"/>">(L)</a>
		                           </security:authorize>
		                           <security:authorize
										url="/gs/gs-mng!add.action?entityName=${entityName}">
										<a id="tadd" href="javascript:void(0);" onclick="doAdd()" class="easyui-linkbutton"
											plain="true" iconCls="icon-add"><s:text name="system.button.add.title"/>(I)</a>
									</security:authorize>
									<security:authorize
										url="/gs/gs-mng!modify.action?entityName=${entityName}">
										<a id="tedit" href="javascript:void(0);" onclick="doSetFormToEdit()"
											class="easyui-linkbutton" plain="true" iconCls="icon-edit"
											><s:text name="system.button.modify.title"/>(M)</a>
									</security:authorize>
									<security:authorize
										url="/gs/gs-mng!revise.action?entityName=${entityName}"> <#-- 修订 -->
										<a id="trevise" href="javascript:void(0);" onclick="doSetFormToRevise()"
											class="easyui-linkbutton" plain="true" iconCls="icon-edit"
											><s:text name="启用修订"/></a>
										<a id="tcancelrevise" href="javascript:void(0);" onclick="doCancelSetFormToRevise()"
											class="easyui-linkbutton" plain="true" iconCls="icon-edit"
											><s:text name="退出修订"/></a>	
									</security:authorize>
									<security:authorize
										url="/sys/log/revise-log.action?mn=${entityName}"> <#-- 查看修订日志 -->
										<a id="treviseview" href="javascript:void(0);" onclick="doViewRevise()"
											class="easyui-linkbutton" plain="true" iconCls="icon-new-doc-ico"
											><s:text name="修订日志"/></a>
									</security:authorize>
									<#-- 关联表 -->
									<security:authorize url="/gs/gs-mng!delete.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
										<a id="tdel" href="javascript:void(0);" onclick="doDel()"
											class="easyui-linkbutton" plain="true" iconCls="icon-remove"
											><s:text name="system.button.delete.title"/>(D)</a>
									</security:authorize>
									<security:authorize
										url="/gs/gs-mng!edit.action?entityName=${entityName}">
										<a id="tcopy" href="javascript:void(0);" onclick="doCopyMainTab()"
											class="easyui-linkbutton" plain="true" iconCls="icon-copy"><s:text name="system.button.copy.title"/>(G)</a>
									</security:authorize>
									<security:authorize
										url="/gs/gs-mng!edit.action?isChange=1&entityName=${entityName}">		
										<a id="tcopyChange" href="javascript:void(0);" onclick="doChange()"
											class="easyui-linkbutton" plain="true" iconCls="icon-copy"><s:text name="system.button.copyChange.title"/></a>	
									</security:authorize>
		                            <security:authorize
												url="/sys/attach/attach.action?entityName=${entityName}">
		                            <a id="tattach" href="javascript:void(0);" onclick="doAccessory('atmId')" class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name="system.button.accessory.title"/>(Y)</a>
		                           </security:authorize>
		                        <#if flowList??&&flowList?size gt 0>
		                        <#assign processBtName="system.button.startFlow.title" />
		                        <#list formList as form>
									      <#if form.isMaster>
									      	<#if form.extension?? && form.extension.editPage??>
									      		<#assign buttonList=form.extension.editPage.buttonList />
									      		<#if buttonList?? && buttonList?size gt 0>
									      			<#list buttonList as button>
									      				<#if button.key == "bt_process">
									      					<#assign processBtName=button.caption />
									      				</#if>
									      			</#list>
									      		</#if>
									      	</#if>
									      </#if>
								</#list>
		                        <security:authorize
												url="/gs/process!startApprove.action?entityName=${entityName}">
		                        <a id="tprocess" href="javascript:void(0);" onclick="doProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-flowStart"><s:text name="${processBtName}"/>(P)</a>
		                        </security:authorize>
		                        <security:authorize
												url="/gs/process!cancelApprove.action?entityName=${entityName}">
		                        <a id="tcancelprocess" href="javascript:void(0);" onclick="doCancelProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-user"><s:text name="system.button.cancelProcess.title"/>(J)</a>
		                        </security:authorize>
		                        <a id="tviewprocess" href="javascript:void(0);" onclick="doViewProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-flowDetail"><s:text name="system.button.viewFlow.title"/>(E)</a>
                        		<#else>
                        		<security:authorize
												url="/gs/process!startApprove.action?entityName=${entityName}">
                        		<a id="tprocess" href="javascript:void(0);" onclick="doSelfProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-flowStart"><s:text name="system.button.selfApprove.title"/>(P)</a>
                        		</security:authorize>
                        		<security:authorize
												url="/gs/process!cancelApprove.action?entityName=${entityName}">
                        		<a id="tcancelprocess" href="javascript:void(0);" onclick="doCancelSelfProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-user"><s:text name="system.button.cancelSelfApprove.title"/>(J)</a>
                        		</security:authorize>
                        		<a id="tviewprocess" href="javascript:void(0);" onclick="doViewProcess(true)" class="easyui-linkbutton" plain="true" iconCls="icon-flowDetail"><s:text name="审核历史"/>(E)</a>
                        		</#if>
                        		<security:authorize
												url="/gs/process!invalidProcess.action?entityName=${entityName}">
                        		<a id="tinvalidData" href="javascript:void(0);" onclick="openCancelWin()" class="easyui-linkbutton" plain="true" iconCls="icon-invalid"><s:text name="system.button.invalid.title"/></a>                     		
                        		</security:authorize>
                        		<@fieldftl.initButtonOnViewPage entityName=entityName moduleKey=moduleKey formList=formList isDisplayReport=true isReportOnly=false />
							<s:if test='#session.USER.isSuperAdmin=="1"'><#-- 对于已经审批结束的单据进行操作，将其设为草稿状态，可以重新发起流程，只有超级管理员可以执行该操作 -->
								<a id="tcancelApproved" href="javascript:void(0);" onclick="doCancelApproved()" class="easyui-linkbutton" plain="true" iconCls="icon-user"><s:text name="撤销审批"/></a>
							</s:if>	
                        </div>
</div>     
	</c:otherwise>
</c:choose>           
<div region="center" style="+position:relative;overflow:auto;" class="module-bg" border="false">
                <form
                        action=""
                        name="viewForm" id="viewForm" style="margin:0px;">
                        <input type="hidden" id="id" name="id"/>
                        <input type="hidden" id="atmId" name="atmId" />
                        <input type="hidden" id="flowInsId" name="flowInsId" />
                        <input type="hidden" id="status" name="status" value="10" />
                      <#list formList as form>
                      		<#if form.isMaster>
                      			<div style="display:none;">
                      			<#list form.fieldList as field>
                      				<#if !field.editProperties.visible>
                      					<@fieldftl.getFiledView field=field form=form/>
                      				</#if>
                      			</#list>
                      			</div>
                      		</#if>
                      </#list>  
                      <#if !dynamicShowTable && tabsList?? && tabsList?size gt 0>
                            <#--分tab页面 -->    
                            <#list tabsList as tabs>
                               <#assign tabList= tabs.tabList>
                               <#if tabList?size gt 0>
	                               <#if tabs_index gt 0>
	                               <div style="height:10px;"></div>
	                               </#if>
	                           <#-- 避免二次，二次初始化时不会更新panel面板 -->
                               <div id="form_tabs_${tabs.rows}">
                               <#list tabList as tab >
                                     <#list formList as form>
	                                        <#if form.key==tab.form>
		                                         <#if form.isMaster><#--主表分tab页面-->
									                 <div title="<s:text name="${tab.i18nKey!tab.caption}"/>"  style="padding:0;overflow:auto;">
									                <#if tab.layout == "absolute"><#-- 绝对布局 -->
		                                         	  <div class="module_div" style="height:${(tab.height)!200}px">
		                                         	  	<#assign fields=form.fieldList>
		                                         	  	<#list fields as field>
														   <#if field.editProperties.visible && !(field.isCaption?? && field.isCaption)>
														   	<#if field.tabRows==tab.rows && field.tabCols==tab.cols>
															   	<#if field.key?? && field.key!="" >
															   		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>  
															   		<div style="position:absolute;left:${((field.editProperties.left)!10)?c}px;top:${((field.editProperties.top)!10)?c}px;">
																   		<span class="module_th">
																   		<#if field.caption?? && field.caption!="">	
													                        <label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
													                    <#else>
													                        &nbsp;
													                    </#if>
													                    </span>
													                    <div class="module_td">
												                    	<@fieldftl.getFiledView field=field form=form/>
												                    	</div>
												                    </div>
												                    <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
												                </#if>
												             </#if>
														   </#if>
														</#list>
		                                         	  </div>
		                                         	<#elseif tab.layout == "excel"><#-- excel布局 -->
		                                         	  <#-- 避免因padding属性不存在时的更新报错 -->
		                                         	  <div style="padding-top:<#if tab.content.paddingTop??>${tab.content.paddingTop}<#else>0</#if>px;padding-bottom:<#if tab.content.paddingBottom??>${tab.content.paddingBottom}<#else>0</#if>px;padding-left:<#if tab.content.paddingLeft??>${tab.content.paddingLeft}<#else>0</#if>px;padding-right:<#if tab.content.paddingRight??>${tab.content.paddingRight}<#else>0</#if>px">
		                                         	  	<#assign fields=form.fieldList>
		                                         	  	<#assign html=tab.content.html>
		                                         	  	<#list fields as field>
		                                         	  		<#if field.key?? && field.key!="" && !(field.isCaption?? && field.isCaption)>
		                                         	  			<#assign control><@fieldftl.getFiledView field=field form=form/></#assign>
		                                         	  			<#assign editType = field.editProperties.editType/>
		                                         	  			<#assign myfieldkey = field.key/>
		                                         	  			<#assign html = freeMarkerExcel(html,control,editType,myfieldkey)/>
		                                         	  		</#if>
		                                         	  	</#list>
		                                         	  	${html}
		                                         	  </div>
		                                         	<#else><#-- 表格布局 -->
									                  <table border="0" cellpadding="0" cellspacing="0"  class="module_form">
												                       	<#assign colCount=1>
												                       	<#assign rowCount=0>
																		<#assign fields=form.fieldList>
																		<#assign columns=tab.tableCols!(form.cols!3)>
																		<#assign column=columns>
																		<#assign rows=newWritableNumberArray(fields?size)><#--每一行被占的列数-->
														               <#list fields as field>
														                <#if field.editProperties.visible && !(field.isCaption?? && field.isCaption)>														                 
											                                 <#if field.tabRows==tab.rows && field.tabCols==tab.cols>
																			    <#-- 一行首列 -->
																			    <#if colCount == 1>
																			    	<tr>
																			    	<#assign column=column-rows[rowCount]>
																			    </#if>
																			    <#assign oldColumn=column>
																			    <#assign column=column-field.editProperties.cols>
											                                    <#if column gte 0>
											                                    	 <#assign rowspan=field.editProperties.rows>
											                                    	 <#assign colspan=field.editProperties.cols*2-1>
											                                         <#-- 判断行是不是满了-->
											                                       <#if field.key?? && field.key!="">
											                                         <#if field.editProperties.editType != 'Label' && field.editProperties.editType != 'BrowserBox'>
											                                         <th rowspan="${rowspan}">
											                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>
											                                         		<#if field.caption?? && field.caption!="">	
											                                         			<label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
											                                                <#else>
											                                                	&nbsp;
											                                                </#if>
														          						 <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
											                                         </th>
											                                         <td rowspan="${rowspan}" colspan="${colspan}">
											                                            <@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>   
											                                                   <@fieldftl.getFiledView field=field form=form/>
											                                     		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>      
											                                         </td>
											                                         <#else>
											                                         <td rowspan="${rowspan}" colspan="${colspan+1}">
											                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>   
											                                                   <@fieldftl.getFiledView field=field form=form/>
											                                     		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>   
											                                         </td>
											                                         </#if>
											                                       <#else>
											                                       	 <th rowspan="${rowspan}">&nbsp;</th>
											                                         <td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
											                                       </#if>
												                                         <#if rowspan gt 1>
													                                         <#assign rowStart = rowCount+1>
													                                         <#assign rowEnd = rowCount+rowspan-1>
													                                         <#list rowStart..rowEnd as i>
													                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
													                                         </#list>
													                                      </#if>
											                                      	 <#if column==0>
											                                      	 	<#-- 一行中最后一列 -->
											                                            </tr>
											                                            <#assign rowCount=rowCount+1>
											                                            <#assign colCount=1>
											                                            <#assign column=columns>
											                                         <#else>
											                                         	<#assign colCount=colCount + 1>
											                                         </#if>
											                                     <#else>
												                                     	<th>&nbsp;</th>
														                                <td colspan="${oldColumn*2-1}">&nbsp;</td>
														                                </tr>
														                                <#assign rowCount=rowCount+1>
											                                            <#assign column=columns>
											                                            <#assign column=column-field.editProperties.cols-rows[rowCount]>
														                                <tr>
														                                <#if column gte 0>
														                                 <#assign rowspan=field.editProperties.rows>
												                                    	 <#assign colspan=field.editProperties.cols*2-1>
												                                         <#-- 判断行是不是满了-->
												                                       <#if field.key?? && field.key!="">
												                                         <#if field.editProperties.editType != 'Label' && field.editProperties.editType != 'BrowserBox'>
												                                         <th rowspan="${rowspan}">
												                                       		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>    
												                                         	<#if field.caption?? && field.caption!="">	
											                                         			<label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
											                                                <#else>
											                                                	&nbsp;
											                                                </#if> 
											                                        		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>    
												                                         </th>
												                                         <td rowspan="${rowspan}" colspan="${colspan}">
												                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>    
												                                                   <@fieldftl.getFiledView field=field form=form/>
												                                      		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
												                                         </td>
												                                         <#else>
												                                         <td rowspan="${rowspan}" colspan="${colspan+1}">
												                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>   
												                                                   <@fieldftl.getFiledView field=field form=form/>
												                                     		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>   
												                                         </td>
												                                         </#if>
												                                        <#else>
												                                        	<th rowspan="${rowspan}">&nbsp;</th>
												                                         	<td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
												                                        </#if>
												                                         <#if rowspan gt 1>
													                                         <#assign rowStart = rowCount+1>
													                                         <#assign rowEnd = rowCount+rowspan-1>
													                                         <#list rowStart..rowEnd as i>
													                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
													                                         </#list>
													                                     </#if>
														                                 <#if column==0>
												                                      	 	<#-- 一行中最后一列 -->
												                                            </tr>
												                                               <#assign rowCount=rowCount+1>
													                                            <#assign colCount=1>
													                                            <#assign column=columns>
												                                         </#if>
												                                      </#if>
		   																	 </#if>
		   																  </#if>
		   																
		   																 </#if>
										                                </#list>
										                                <#if column != columns>
										                                	<th>&nbsp;</th>
											                                <td colspan="${column*2-1}">&nbsp;</td>
											                                </tr>
										                                </#if>
		                                               </table>
		                                               </#if>
		                                             </div>
		                                        <#else>
		                                        <#-- 子表  -->
		                                            <@fieldftl.initCheckFormAuth form=form isEnd=false/>        
		                                               <div id="dv_tab_${form.key}" title="<s:text name="${tab.i18nKey!tab.caption}"/>" closable="false">
									                        <table id="${form.key}" >
									                        </table>
									                   </div>
									                <@fieldftl.initCheckFormAuth form=form isEnd=true/>
		                                        </#if>
		                                   </#if>
		                              </#list>
		                        </#list>
		                 </div>	
		                 </#if>
		           	</#list> 
		         <#else> <#-- 默认布局，主表在上，子表在下 -->
					<#list formList as form>
		                                <#if form.isMaster>
		                                	<div id="form_tabs_1">
		                                         <#--主表分tab页面-->
									                 <div title="<s:text name="${form.i18nKey!form.caption}"/>"  style="padding:0;overflow:hidden;">
									                 <#if ((form.layout)!"") == "absolute"><#-- 绝对布局 -->
		                                         	  <div class="module_div" style="height:${(form.height)!200}px">
		                                         	  	<#assign fields=form.fieldList>
		                                         	  	<#list fields as field>
														   <#if field.editProperties.visible && !(field.isCaption?? && field.isCaption)>
														   	 <#if field.key?? && field.key!="">
														   	 	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>    
														   		<div style="position:absolute;left:${((field.editProperties.left)!10)?c}px;top:${((field.editProperties.top)!10)?c}px;">
															   		<span class="module_th">
															   		<#if field.caption?? && field.caption!="">	
												                        <label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
												                    <#else>
												                        &nbsp;
												                    </#if>
												                    </span>
												                    <div class="module_td">
											                    	<@fieldftl.getFiledView field=field form=form/>
											                    	</div>
											                    </div>
											                 	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>      
											                  </#if>
														   </#if>
														</#list>
		                                         	  </div>
		                                         	<#else><#-- 表格布局 -->
									                  <table border="0" cellpadding="0" cellspacing="0"  class="module_form">
												                       	<#assign colCount=1>
												                       	<#assign rowCount=0>
																		<#assign fields=form.fieldList>
																		<#assign columns=form.cols!3>
																		<#assign column=columns>
																		<#assign rows=newWritableNumberArray(fields?size)><#--每一行被占的列数-->
														               <#list fields as field>
														                <#if field.editProperties.visible && !(field.isCaption?? && field.isCaption)>
																			    <#-- 一行首列 -->
																			    <#if colCount == 1>
																			    	<tr>
																			    	<#assign column=column-rows[rowCount]>
																			    </#if>
																			    <#assign oldColumn=column>
																			    <#assign column=column-field.editProperties.cols>
											                                    <#if column gte 0>
											                                    	 <#assign rowspan=field.editProperties.rows>
											                                    	 <#assign colspan=field.editProperties.cols*2-1>
											                                         <#-- 判断行是不是满了-->
											                                        <#if field.key?? && field.key!="">
												                                         <th rowspan="${rowspan}">
												                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>    
												                                         	<#if field.caption?? && field.caption!="">
											                                         			<label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
											                                                <#else>
											                                                	&nbsp;
											                                                </#if> 
											                                                <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
												                                         </th>
												                                         <td rowspan="${rowspan}" colspan="${colspan}">
												                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>   
												                                                   <@fieldftl.getFiledView field=field form=form/>
												                                           	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>          
												                                         </td>
											                                        <#else>
											                                        	 <th rowspan="${rowspan}">&nbsp;</th>
												                                         <td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
												                                     </#if>
												                                         <#if rowspan gt 1>
													                                         <#assign rowStart = rowCount+1>
													                                         <#assign rowEnd = rowCount+rowspan-1>
													                                         <#list rowStart..rowEnd as i>
													                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
													                                         </#list>
													                                      </#if>
											                                      	 <#if column==0>
											                                      	 	<#-- 一行中最后一列 -->
											                                            </tr>
											                                            <#assign rowCount=rowCount+1>
											                                            <#assign colCount=1>
											                                            <#assign column=columns>
											                                         <#else>
											                                         	<#assign colCount=colCount + 1>
											                                         </#if>
											                                     <#else>
												                                     	<th>&nbsp;</th>
														                                <td colspan="${oldColumn*2-1}">&nbsp;</td>
														                                </tr>
														                                <#assign rowCount=rowCount+1>
											                                            <#assign column=columns>
											                                            <#assign column=column-field.editProperties.cols-rows[rowCount]>
														                                <tr>
														                                <#if column gte 0>
														                                 <#assign rowspan=field.editProperties.rows>
												                                    	 <#assign colspan=field.editProperties.cols*2-1>
												                                         <#-- 判断行是不是满了-->
												                                       <#if field.key?? && field.key!="">  
												                                         <th rowspan="${rowspan}">
												                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>  
												                                         	<#if field.caption?? && field.caption!="">	
											                                         			<label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
											                                                <#else>
											                                                	&nbsp;
											                                                </#if> 
											                                                <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
												                                         </th>
												                                         <td rowspan="${rowspan}" colspan="${colspan}">
												                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>  	  
												                                                 <@fieldftl.getFiledView field=field form=form/>
												                                            <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
												                                         </td>
												                                       <#else>
												                                       	 <th rowspan="${rowspan}">&nbsp;</th>
												                                         <td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
												                                       </#if>
												                                         <#if rowspan gt 1>
													                                         <#assign rowStart = rowCount+1>
													                                         <#assign rowEnd = rowCount+rowspan-1>
													                                         <#list rowStart..rowEnd as i>
													                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
													                                         </#list>
													                                     </#if>
														                                 <#if column==0>
												                                      	 	<#-- 一行中最后一列 -->
												                                            </tr>
												                                               <#assign rowCount=rowCount+1>
													                                            <#assign colCount=1>
													                                            <#assign column=columns>
												                                         </#if>
												                                      </#if>
		   																	 </#if>
		   																  </#if>
										                                </#list>
										                                <#if column != columns>
										                                	<th>&nbsp;</th>
											                                <td colspan="${column*2-1}">&nbsp;</td>
											                                </tr>
										                                </#if>
		                                               </table>
		                                               </#if>
		                                               </div>
		                                            </div>   
		                                        </#if>
		                              </#list>
		           <#if !dynamicShowTable > <#-- 非动态显示子表 -->
			           <#if formList?size gt 1>
			            <div style="height:10px;"></div>
					    <div id="form_tabs_2" border="false">
					    <#list formList as form>
					        <#if !form.isMaster>
					        	<#if form.visible>
					        		 <@fieldftl.initCheckFormAuth form=form isEnd=false/>    
					                    <div id="dv_tab_${form.key}" title="<s:text name="${form.i18nKey!form.caption}"/>" closable="false">
					                        <table id="${form.key}" >
					                        </table>
					                    </div>
					          		 <@fieldftl.initCheckFormAuth form=form isEnd=true/> 
								</#if>   
					        </#if>
					    </#list>
					    </div>
				       </#if>
				   <#else> <#-- 动态显示子表 -->
				   		<div style="height:10px;"></div>
				    	<div id="form_tabs_dynamic"></div>    
			       </#if>
		       </#if>
        </form>
</div>
         	<div id="mm" class="easyui-menu" style="width:120px;">
				<div id="stInsert" onclick="doInsert()"><s:text name="system.button.insert.title"/></div>
				<div id="stCopy" onclick="doCopy()"><s:text name="system.button.copy.title"/></div>
				<div id="stMoveUp" onclick="doMoveUp()"><s:text name="system.button.moveUp.title"/></div>
				<div id="stMoveDown" onclick="doMoveDown()"><s:text name="system.button.moveDown.title"/></div>
				<div id="stDelete" onclick="doDelete()"><s:text name="system.button.delete.title"/></div>
			</div>
			<div id="mm_treeGrid" class="easyui-menu" style="width:120px;">
				<div id="stInsert_tree" onclick="doTreeInsert()"><s:text name="system.button.insert.title"/></div>
				<div id="stMoveUp_tree" onclick="doTreeMoveUp()"><s:text name="system.button.moveUp.title"/></div>
				<div id="stMoveDown_tree" onclick="doTreeMoveDown()"><s:text name="system.button.moveDown.title"/></div>
				<div id="stDelete_tree" onclick="doTreeDelete()"><s:text name="system.button.delete.title"/></div>
			</div>
			<@fieldftl.initButtonMenuOnViewPage formList=formList /> 	
			
     <div id="subTableDetailInfo" class="easyui-dialog" title="<s:text name="system.button.ctabledetail.title"/>" style="width:800px;height:450px;"
			closed="true">
		<iframe id="ifr_subtable" name="ifr_subtable" scrolling="auto" frameborder="0"  src="" style="width:100%;height:100%;overflow:hidden;"></iframe>
	</div>
	<div  id="cancelReasonWin" closed="true" modal="true" title="<s:text name="system.module.invalidReason.title"/>"  style=" width: 350px; height:205px; padding: 0px;">							
								<div class="easyui-layout" fit="true">	
									<div region="center" border="false" style="padding:1px;">
									<textarea style="text-align: left; width: 98%;height:73%;" name="cancelReason" id ="cancelReason" ></textarea>				
									</div>
									<div  region="south" border="false" style="text-align:right;padding:7px 0;">
										<a class="easyui-linkbutton" iconCls="icon-ok" href="javascript:void(0)" onclick="doInvalidData()"><s:text name="system.button.ok.title"/></a> 
										<a class="easyui-linkbutton" iconCls="icon-cancel" href="javascript:void(0)" onclick="$('#cancelReasonWin').window('close')"><s:text name="system.button.cancel.title"/></a>
									</div>
								</div>	
	</div> 
	<#-- 新版easyui在初始化layout时，将会查询children中的form元素，如存在则初始化对form兄弟元素无效 -->
	<div style="display:none;">
	<form method="post" name="hidFrm" id="hidFrm" >
		<textarea style="display:none;" name="hid_queryParams" id="hid_queryParams">${r"${param.queryParams}"}</textarea><#-- 查询参数 -->
		<textarea style="display:none;" name="hid_addParams" id="hid_addParams">${r"${param.addParams}"}</textarea><#-- 新增参数 -->
	</form>
	</div>
	<div id="_dv_approved_" style="position: absolute;top:30px;right:35px;display:none;z-index:99999;">
		<img src="${r"${imgPath}"}/approve-001.png" alt="已审" />
	</div>
	<@fieldftl.initFormCssOrJavascript formList=formList eventKey="referJsOrCss"/>
</body>
</html>
