<#if jsVersion??>
<#assign jsVersion=jsVersion/>
</#if>
{
  "schema":"js",
  "debug":true,
  "loglevel":"info",
  "logfiler":"",
  "showerror":true,
  "isPortrait":true,
  "jsVersion":${jsVersion?c},
  "debugIp":"127.0.0.1",
  "debugEntry":"app/entry.js",
  "releaseEntry":"app/entry.js",
  "wechatEntry":"app/demo/index.js",
  "appBoard":"root:common.js",
  "notifyEntry":"http://172.20.10.3:8890/js/notifyEntry.js"
}

