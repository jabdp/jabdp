/*
 * @(#)DataShareConfigDao.java
 * 2015-3-11 下午02:36:08
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsmng.common.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.gsmng.common.entity.DataShareConfig;


/**
 *  数据共享设置 
 *  @author gxj
 *  @version 1.0 2015-3-11 下午02:36:08
 *  @since jdk1.6 
 */
@Component
public class DataShareConfigDao extends HibernateDao<DataShareConfig, Long> {

	/**
	 * 更新业务模块数据共享日志
	 * 
	 * @param entityName
	 * @param entityId
	 * @param log
	 */
	public void saveDataShareLog(String entityName, String entityId, String log) {
		String hql = "update " + entityName + " set dataShareLog = ? where id = ? ";
		super.batchExecute(hql, log, Long.valueOf(entityId));
	}
	
}
