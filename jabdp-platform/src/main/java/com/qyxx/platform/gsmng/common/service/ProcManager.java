package com.qyxx.platform.gsmng.common.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.collect.Maps;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.gsmng.common.dao.GsMngDao;






/**
 * 存储过程管理类
 * 
 * @author xk
 * @version 1.0
 * @since 1.6 2013-2-26 下午16:36:23
 */
@Service
@Transactional
public class ProcManager {
	
	private GsMngDao gsMngDao;

	@Autowired
	public void setGsMngDao(GsMngDao gsMngDao) {
		this.gsMngDao = gsMngDao;
	}
	
	/**
	 * 创建存储过程方法
	 *           
	 */
	public void createProcedure(String procedureContent) {
		gsMngDao.createProcedure(procedureContent);
	}
	/**
	 * 调用存储过程方法
	 *           
	 */
	public void callProcedure(String procKey,final List<PropertyFilter> filters) {
		Map<String, Object> map = Maps.newHashMap();
		if(null!=filters && !filters.isEmpty()) {
			for(PropertyFilter pf : filters) {
				String key = pf.getPropertyName(); 
				Object value = pf.getMatchValue();
				map.put(key, value);				
			}
		}
		gsMngDao.callProcedure(procKey, map);
	}
	
	/**
	 * 调用存储过程方法，返回结果
	 *           
	 */
	public String callProcedureWithResult(String procKey,final List<PropertyFilter> filters) {
		Map<String, Object> map = Maps.newHashMap();
		if(null!=filters && !filters.isEmpty()) {
			for(PropertyFilter pf : filters) {
				String key = pf.getPropertyName(); 
				Object value = pf.getMatchValue();
				map.put(key, value);				
			}
		}
		return gsMngDao.callProcedureWithResult(procKey, map);
	}
}
