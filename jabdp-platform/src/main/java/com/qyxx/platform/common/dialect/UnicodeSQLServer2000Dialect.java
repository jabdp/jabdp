/*
 * @(#)UnicodeSQLServerDialect.java
 * 2011-11-19 下午09:51:28
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.dialect;

import java.sql.Types;

import org.hibernate.dialect.SQLServerDialect;

/**
 * 国际化语言类，将varchar等转为nvarchar
 * 
 * 
 * @author gxj
 * @version 1.0
 * @since 1.6 2011-11-19 下午09:51:28
 */

public class UnicodeSQLServer2000Dialect extends SQLServerDialect {

	public UnicodeSQLServer2000Dialect() {
		super();
        registerColumnType(Types.BIGINT, "bigint");
        registerColumnType(Types.BIT, "bit");
        registerColumnType(Types.CHAR, "nchar(1)");
        registerColumnType(Types.VARCHAR, 4000, "nvarchar($l)");
        registerColumnType(Types.VARCHAR, "ntext");
        registerColumnType(Types.VARBINARY, 4000, "varbinary($1)");
        registerColumnType(Types.VARBINARY, "image");
        registerColumnType(Types.BLOB, "image");
        registerColumnType(Types.CLOB, "ntext");
        registerColumnType(Types.LONGVARCHAR, "ntext" );
	}
	
}
