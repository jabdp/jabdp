/*
 * @(#)FormatMessage.java
 * 2011-5-28 下午09:45:51
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.module.web;


/**
 * 消息封装类
 * 
 * @author gxj
 * @version 1.0
 * @since 1.6 2011-5-28 下午09:45:51
 */
public class FormatMessage {
	
	private String flag = Messages.SUCCESS;

	private Object msg;

	/**
	 * @return flag
	 */
	public String getFlag() {
		return flag;
	}

	/**
	 * @param flag
	 */
	public void setFlag(String flag) {
		this.flag = flag;
	}

	/**
	 * @return msg
	 */
	public Object getMsg() {
		return msg;
	}

	/**
	 * @param msg
	 */
	public void setMsg(Object msg) {
		this.msg = msg;
	}

}
