package com.qyxx.platform.common.freemarker;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;
import freemarker.template.TemplateSequenceModel;

public class FreeMarkerExcel implements TemplateMethodModel {

//	@Override
//	public void execute(Environment env, Map params, TemplateModel[] loopVars,
//			TemplateDirectiveBody body) throws TemplateException, IOException {
//		// TODO Auto-generated method stub
//		if (params.isEmpty()) {
//			throw new TemplateModelException(
//					"This directive parameters must be set.");
//		}
//		if (loopVars.length != 0) {
//			throw new TemplateModelException(
//					"This directive doesn't allow loop variables.");
//		}
//		
//		//NumberArraySequence seq = null;
//		int index = 0;
//		int value = 0;
//		Iterator it = params.entrySet().iterator();
//        while(it.hasNext()) {
//            Map.Entry e = (Map.Entry)it.next();
//            String pname = (String)e.getKey();
//            TemplateScalarModel pvalue = (TemplateScalarModel)e.getValue();
//            Writer out = env.getOut();
//            String str = pvalue.getAsString();
//            str = str + "x";
//            e.setValue(str);
//            out.write(str);
//        }
////        Writer out = env.getOut();
////        out.write("egg");
//        //System.out.print("a");
//	}

	@Override
	public Object exec(List arguments) throws TemplateModelException {
		String html = (String)arguments.get(0);
		String control = (String)arguments.get(1);
		String editType = (String)arguments.get(2);
		String key = (String)arguments.get(3);
		String regex = "<img [^>]*" + "_" + key + "_" + editType + ".*?(?:>|/>)";
		html = html.replaceAll(regex, control);
		return html;
	}
}
