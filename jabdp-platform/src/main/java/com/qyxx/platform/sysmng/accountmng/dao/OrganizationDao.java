/*
 * @(#)OrganizationDao.java
 * 2011-5-22 下午04:57:01
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.accountmng.entity.Organization;

/**
 * 用户组织泛型DAO
 * 
 * @author YB
 * @version 1.0
 * @since 1.6 2011-5-22 下午04:57:01
 */
@Component
public class OrganizationDao extends HibernateDao<Organization, Long> {
	
	private static final String FIND_ORG_BY_PARENTCODE_HQL = "select a from Organization a where a.organizationCode like ? order by a.organizationNo asc, a.id asc";
	
	/**
	 * 根据组织编码查找该组织及该组织下其他组织数据，根据序号排序
	 * 
	 * @param orgCode
	 * @return
	 */
	public List<Organization> findOrgList(String orgCode) {
		return find(FIND_ORG_BY_PARENTCODE_HQL, orgCode + "%");
	}
	
}
