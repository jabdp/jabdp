/*
 * @(#)DictionaryType.java
 * 2011-5-22 下午07:59:55
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.dictmng.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.common.collect.Lists;
import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  数据字典类型
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-22 下午07:59:55
 */
@Entity
@Table(name = "SYS_DICTIONARY_TYPE")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DictionaryType extends BaseEntity {

	private Long id;
	
	private String dictTypeName;
	
	private String dictTypeDesc;
	
	private String type;
	
	private String dictTypeKey;
	
	private List<Dictionary> dictList = Lists.newArrayList();

	
	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_DICTIONARY_TYPE_ID")
	@SequenceGenerator(name="SEQ_SYS_DICTIONARY_TYPE_ID", sequenceName="SEQ_SYS_DICTIONARY_TYPE_ID")
	public Long getId() {
		return id;
	}

	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return dictTypeName
	 */
	public String getDictTypeName() {
		return dictTypeName;
	}

	
	/**
	 * @param dictTypeName
	 */
	public void setDictTypeName(String dictTypeName) {
		this.dictTypeName = dictTypeName;
	}

	
	/**
	 * @return dictTypeDesc
	 */
	public String getDictTypeDesc() {
		return dictTypeDesc;
	}

	
	/**
	 * @param dictTypeDesc
	 */
	public void setDictTypeDesc(String dictTypeDesc) {
		this.dictTypeDesc = dictTypeDesc;
	}

	
	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	
	/**
	 * @return dictTypeKey
	 */
	public String getDictTypeKey() {
		return dictTypeKey;
	}

	
	/**
	 * @param dictTypeKey
	 */
	public void setDictTypeKey(String dictTypeKey) {
		this.dictTypeKey = dictTypeKey;
	}


	
	/**
	 * @return dictList
	 */
	@OneToMany(mappedBy="dictType")
	@OrderBy("id")
	public List<Dictionary> getDictList() {
		return dictList;
	}


	
	/**
	 * @param dictList
	 */
	public void setDictList(List<Dictionary> dictList) {
		this.dictList = dictList;
	}
	
}
