/*
 * @(#)UserVo.java
 * 2012-8-2 下午03:18:21
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;


/**
 *  user视图
 *  @author ly
 *  @version 1.0 2012-8-2 下午03:18:21
 *  @since jdk1.6 
 */

public class UserVo implements Serializable {
	
	/**
	 * long
	 */
	private static final long serialVersionUID = 8279577960757696111L;
	private Long id;
	private String loginName;
	private String realName;
	private String nickName;
	private String orgName;
	
	/**
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return loginName
	 */
	public String getLoginName() {
		return loginName;
	}
	
	/**
	 * @param loginName
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	/**
	 * @return realName
	 */
	public String getRealName() {
		return realName;
	}
	
	/**
	 * @param realName
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	
	/**
	 * @return nickName
	 */
	public String getNickName() {
		return nickName;
	}
	
	/**
	 * @param nickName
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	/**
	 * @return orgName
	 */
	public String getOrgName() {
		return orgName;
	}
	
	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	

}
