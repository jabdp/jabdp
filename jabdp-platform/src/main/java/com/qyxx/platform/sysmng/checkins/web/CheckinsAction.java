package com.qyxx.platform.sysmng.checkins.web;

import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.checkins.entity.Checkins;
import com.qyxx.platform.sysmng.checkins.service.CheckinsManager;
import com.qyxx.platform.sysmng.utils.Constants;

@Component
@Transactional
@Namespace("/sys/checkins")
public class CheckinsAction extends CrudActionSupport<Checkins> {

	private static final long serialVersionUID = 1139523735106408826L;
	
	private CheckinsManager checkinsManager;

	
	/**
	 * @param checkinsManager
	 */
	@Autowired
	public void setCheckinsManager(CheckinsManager checkinsManager) {
		this.checkinsManager = checkinsManager;
	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	}
	
	@JsonOutput
	public void queryList() throws Exception {
		// TODO Auto-generated method stub
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		if(!Constants.IS_SUPER_USERR.equals(user.getIsSuperAdmin())) {
			PropertyFilter filter = new PropertyFilter(
					"EQL_createUser", String.valueOf(user.getId()));
			filters.add(filter);
		}
		pager = checkinsManager.findPage(pager, filters);
		Struts2Utils.renderJson(pager);
	}
	
	@JsonOutput
	public void queryListMobile() throws Exception {
		// TODO Auto-generated method stub
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		pager = checkinsManager.findPage(pager, filters);
		Struts2Utils.renderJson(pager);
	}
	
	@JsonOutput
	public void queryListGroupByUserId() throws Exception {
		// TODO Auto-generated method stub
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		pager = checkinsManager.findPageGroupByUserId(pager, filters);
		Struts2Utils.renderJson(pager);
	}
	
	@JsonOutput
	public void queryMobile() throws Exception {
		// TODO Auto-generated method stub
		Page<Checkins> p = new Page<Checkins>();
		p.setOrder(Page.DESC);
		p.setOrderBy("id");
		p.setPageNo(1);
		p.setPageSize(10);
		p = checkinsManager.findPage(p);
		Struts2Utils.renderJson(p.getResult());
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonOutput
	public String save() throws Exception {
		// TODO Auto-generated method stub
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		entity.setUserId(user.getId());
		entity.setCheckinsTime(new Date());
		checkinsManager.save(entity);
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	@Override
	public void prepareSave() throws Exception {
		// TODO Auto-generated method stub
		entity = new Checkins();
	}
	
	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		checkinsManager.delete(ids);
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
	}

}
