package com.qyxx.platform.sysmng.desktop.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;



public class DesktopJson implements Serializable{
	
	/**
	 * long
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String desktopName;
	private Long parentId;
	private Boolean checked;
	private String iconSkin;
	private String  resourceStatus;
	private List<DesktopJson> childs = Lists.newArrayList();
	
	
	public DesktopJson(Desktop d) {
		super();
		this.id = d.getId();
		this.desktopName = d.getTitle();
		this.parentId = d.getParentId();
		 if(d.getLevel()==1){
			this.iconSkin = "iconMenu";
		}else if (d.getLevel()==2){
			this.iconSkin = "iconButton";
		}else if (d.getLevel()==3){
			this.iconSkin = "iconColumn";
		}else if(d.getLevel()==4){
			this.iconSkin = "iconData";
		}
		 this.resourceStatus = d.getSource();
	}


	
	public Long getId() {
		return id;
	}


	
	public void setId(Long id) {
		this.id = id;
	}


	@JsonProperty(value = "name")
	public String getDesktopName() {
		return desktopName;
	}


	
	public void setDesktopName(String desktopName) {
		this.desktopName = desktopName;
	}


	@JsonProperty(value = "pId")
	public Long getParentId() {
		return parentId;
	}


	
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}


	
	public Boolean getChecked() {
		return checked;
	}


	
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}


	
	public String getIconSkin() {
		return iconSkin;
	}


	
	public void setIconSkin(String iconSkin) {
		this.iconSkin = iconSkin;
	}


	
	public String getResourceStatus() {
		return resourceStatus;
	}


	
	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}


	
	public List<DesktopJson> getChilds() {
		return childs;
	}


	
	public void setChilds(List<DesktopJson> childs) {
		this.childs = childs;
	}
	
	
	
	
}
