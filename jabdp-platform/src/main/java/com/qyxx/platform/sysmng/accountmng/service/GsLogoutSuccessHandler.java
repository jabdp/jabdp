/*
 * @(#)GSLogoutSuccessHandler.java
 * 2011-5-15 下午12:21:37
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.service;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.module.web.FormatMessage;
import com.qyxx.platform.common.utils.web.ServletUtils;
import com.qyxx.platform.sysmng.accountmng.dao.OnlineUserDao;
import com.qyxx.platform.sysmng.accountmng.dao.UserDao;
import com.qyxx.platform.sysmng.accountmng.entity.OnlineUser;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.logmng.entity.Log;
import com.qyxx.platform.sysmng.logmng.web.LogInfoThreadPool;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  注销处理器
 *  
 *  移除用户session，将用户从在线用户列表移除
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-15 下午12:21:37
 */
@Transactional
public class GsLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler{
	
	private OnlineUserDao onlineUserDao;
	
	private LogInfoThreadPool logInfoThreadPool;
	
	/**
	 * @param onlineUserDao
	 */
	@Autowired
	public void setOnlineUserDao(OnlineUserDao onlineUserDao) {
		this.onlineUserDao = onlineUserDao;
	}
	
	/**
	 * @param logInfoThreadPool
	 */
	@Autowired
	public void setLogInfoThreadPool(LogInfoThreadPool logInfoThreadPool) {
		this.logInfoThreadPool = logInfoThreadPool;
	}
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute(Constants.USER_SESSION_ID);
/*		//TODO 注销操作
		logger.info("sessionId：" + session.getId());
		OnlineUser ou = onlineUserDao.findUniqueBy("sessionId", session.getId());
		if(ou != null) {
			onlineUserDao.delete(ou);
		}
		logger.info("移除1次");*/
		session.removeAttribute(Constants.USER_SESSION_ID);
		session.removeAttribute(Constants.LOCALE_SESSION_ID);
		session.removeAttribute(Constants.THEME_STYLE_SESSION_ID);
		session.removeAttribute(Constants.THEME_COLOR_SESSION_ID);
		session.invalidate();
		
		if(null != user) {
			//记录日志
			String url = UrlUtils.buildRequestUrl(request);
			logger.info("日志拦截器拦截url为：" + url);
			String operName = Constants.LOGOUT_MESSAGE;
			String operDesc = Constants.LOGOUT_MESSAGE;
			Log log = new Log();
			log.setOperationUrl(url);
			log.setLoginName(user.getLoginName());
			log.setRealName(user.getRealName());
			log.setIp(ServletUtils.getIpAddr(request));
			log.setMachineName(request.getRemoteHost());
			String method = "logout";
			log.setOperationMethod(method);
			log.setOperationName(operName);
			log.setOperationDesc(operDesc);
			//添加组织名称
			log.setOrgName(user.getOrganizationName());
			log.setOperationTime(new Date());
			log.setRemark(request.getHeader("User-Agent"));
			logInfoThreadPool.addLog(log);
		}
		//自定义重定向策略，支持ajax请求
		AjaxRedirectStrategy art = new AjaxRedirectStrategy();
		FormatMessage fm = new FormatMessage();
		fm.setMsg("成功推出当前账号！");
		art.setFm(fm);
		this.setRedirectStrategy(art);
		super.onLogoutSuccess(request, response, authentication);
	}

}
