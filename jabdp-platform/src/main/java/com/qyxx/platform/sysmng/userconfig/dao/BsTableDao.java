/*
 * @(#)BsTableDao.java
 * 2016年10月19日 下午4:40:32
 * 
 * Copyright (c) 2016 QiYunInfoTech - All Rights Reserved.
 * ====================================================================
 * The QiYunInfoTech License, Version 1.0
 *
 * This software is the confidential and proprietary information of QiYunInfoTech.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with QiYunInfoTech.
 */
package com.qyxx.platform.sysmng.userconfig.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.userconfig.entity.BsTable;

/**
 *  业务表dao
 *  @author bobgao
 *  @version 1.0 2016年10月19日 下午4:40:32
 *  @since jdk1.7 
 */
@Component
public class BsTableDao extends HibernateDao<BsTable, Long>{

}
