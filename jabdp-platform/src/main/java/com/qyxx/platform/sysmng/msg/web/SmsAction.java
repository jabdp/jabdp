/*
 * @(#)SmsAction.java
 * 2014-4-1 下午07:21:26
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.msg.web;

import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.msg.entity.SmsMsg;
import com.qyxx.platform.sysmng.msg.service.SmsFactory;
import com.qyxx.platform.sysmng.msg.service.SmsManager;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  短信action
 *  @author gxj
 *  @version 1.0 2014-4-1 下午07:21:26
 *  @since jdk1.6 
 */
@Namespace("/sys/msg")
@Results({})
public class SmsAction extends CrudActionSupport<SmsMsg> {

	/**
	 * long
	 */
	private static final long serialVersionUID = 2511878788590780721L;
	
	private SmsManager smsManager;
	private String status;
	private Long atmId;

	/**
	 * @param smsManager
	 */
	@Autowired
	public void setSmsManager(SmsManager smsManager) {
		this.smsManager = smsManager;
	}


	/**
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @return atmId
	 */
	public Long getAtmId() {
		return atmId;
	}

	
	/**
	 * @param atmId
	 */
	public void setAtmId(Long atmId) {
		this.atmId = atmId;
	}

	/**
	 * 跳转至列表页面
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#list()
	 */
	@Override
	public String list() throws Exception {
		return SUCCESS;
	}
	
	/**
	 * 跳转至查看页面
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#view()
	 */
	@Override
	public String view() throws Exception {
		return VIEW;
	}
	
	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 查询列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter
		.buildFromHttpRequest(Struts2Utils.getRequest());
		pager = smsManager.queryList(pager, filters);
		Struts2Utils.renderJson(pager);
		return null;
	}
	
	/**
	 * 保存数据
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#save()
	 */
	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User) Struts2Utils
		.getSessionAttribute(Constants.USER_SESSION_ID);
		smsManager.setUser(user);
		smsManager.save(entity);
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	/**
	 * 查看数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryView() throws Exception {
		entity = smsManager.get(id);
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	/**
	 * 删除数据
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#delete()
	 */
	@Override
	@JsonOutput
	public String delete() throws Exception {
		smsManager.delete(ids);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 更新数据状态
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String updateStatus() throws Exception {
		User user = (User) Struts2Utils
		.getSessionAttribute(Constants.USER_SESSION_ID);
		smsManager.setUser(user);
		smsManager.updateStatus(id, status);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	/**
	 * 预处理
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#prepareModel()
	 */
	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		if (id != null) {
			entity = smsManager.get(id);
		} else {
			entity = new SmsMsg();
		}
	}
	
	/**
	 * 重置Sms设置
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String resetSms() throws Exception {
		SmsFactory.resetServiceSms();
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

}
