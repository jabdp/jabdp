/*
 * @(#)OrganizationAction.java
 * 2011-5-29 上午10:17:03
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.web;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.service.ServiceException;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.Organization;
import com.qyxx.platform.sysmng.accountmng.entity.OrganizationJson;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.accountmng.service.AccountManager;
import com.qyxx.platform.sysmng.accountmng.service.OrganizationManager;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 * 
 * @author YB
 * @version 1.0
 * @since 1.6 2011-5-29 上午10:17:03
 */
@Namespace("/sys/account")
// 定义名为reload的result重定向到organization.action, 其他result则按照convention默认.
@Results({ @Result(name = CrudActionSupport.RELOAD, location = "organization.action", type = "redirect") })
public class OrganizationAction extends
		CrudActionSupport<Organization> {

	/**
	 * long
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long parentId;
	private OrganizationManager organizationManager;
	private Organization entity;
    private String method;
	private AccountManager accountManager;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}
	@Autowired
	public void setAccountManager(AccountManager accountManager) {
		this.accountManager = accountManager;
	}
	
	public String getMethod() {
		return method;
	}

	
	public void setMethod(String method) {
		this.method = method;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	@Autowired
	public void setOrganizationManager(
			OrganizationManager organizationManager) {
		this.organizationManager = organizationManager;
	}
	
	@Override
	public Organization getModel() {
		// TODO Auto-generated method stub
		return entity;
	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonOutput
	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		this.organizationManager.setUser(user);
		/*if ("add".equals(method)) {
			Organization organization = new Organization();
			organization = organizationManager
					.getOrganizationById(parentId);
			entity.setOrganization(organization);

		} else {
			if (parentId != null) {
				Organization organization = new Organization();
				organization = organizationManager
						.getOrganizationById(parentId);
				entity.setOrganization(organization);
			} else {
				entity.setOrganization(null);
			}
		}*/
		this.organizationManager.saveOrganization(entity, parentId);

		if ("add".equals(method)) {
			List<Organization> list = new ArrayList<Organization>();
			list.add(entity);
			formatMessage.setMsg(list);
		} else {
			formatMessage.setMsg(entity);
		}
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@JsonOutput
	public String modifyInfo() throws Exception {
		// TODO Auto-generated method stub
		entity = organizationManager.getOrganizationById(id);
		OrganizationJson organizationJson = new OrganizationJson();
		if (entity.getOrganization() != null) {
			organizationJson.setParentId(entity.getOrganization()
					.getId());
			organizationJson.setParentName(entity.getOrganization()
					.getOrganizationName());

		}
		organizationJson.setId(id);
		organizationJson.setOrganizationName(entity.getOrganizationName());
		organizationJson.setOrganizationNo(entity.getOrganizationNo());
		organizationJson.setOrganizationDesc(entity.getOrganizationDesc());
		organizationJson.setOrganizationCode(entity.getOrganizationCode());
		organizationJson.setParentOrganizationCode(entity.getParentOrganizationCode());
		formatMessage.setMsg(organizationJson);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@JsonOutput
	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		if (parentId == null) {
			throw new ServiceException("你要添加的组织为空", new Exception(
					"你要添加的组织为空"));
		}
		entity = organizationManager.getOrganizationById(parentId);
		if (entity == null) {
			throw new ServiceException("你要添加的组织为空", new Exception(
					"你要添加的组织为空"));
		}
		OrganizationJson organizationJson = new OrganizationJson();
		organizationJson.setParentId(parentId);
		organizationJson.setParentName(entity.getOrganizationName());
		formatMessage.setMsg(organizationJson);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@JsonOutput
	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		List<PropertyFilter> filters = PropertyFilter
		.buildFromHttpRequest(Struts2Utils.getRequest());
		if (parentId != null) {
			String ids = this.accountManager.getStringOrgSonId(parentId);
			PropertyFilter propertyFilterParentOrg = new PropertyFilter(
					"INL_organization.id", ids);
			filters.add(propertyFilterParentOrg);
		}
		Page<User> pagers = new Page<User>(10);
		pagers = accountManager.searchUser(pagers, filters);
		if(pagers.getTotalCount()>0)
		{
			throw new ServiceException("你要删除的组织下面有用户,不能删除!", new Exception(
			"你要删除的组织下面有用户,不能删除!"));
		}
		entity=organizationManager.getOrganizationById(parentId);
		if(entity!=null&&entity.getOrganizationList().size()>0)
		{
			
				throw new ServiceException("你要删除的组织下面有子组织,不能删除!", new Exception(
				"你要删除的组织下面有组织,不能删除!"));
	
		}
		organizationManager.removeOrganization(parentId);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	//TODO 需要优化，在2W条数据下很慢
	@JsonOutput
	public String queryList() throws Exception {
		// TODO Auto-generated method stub
		entity = this.organizationManager
				.getOrganizationById(Constants.USER_SUPER_ORGANIZATION);
		List<Organization> list = new ArrayList<Organization>();
		list.add(entity);
		Struts2Utils.renderJson(list);
		return null;
	}
	
	//TODO 需要优化，在2W条数据下很慢
	@JsonOutput
	public String userOrgList() throws Exception {
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		Long orgId = user.getOrganization().getId();
		entity = this.organizationManager.getOrganizationById(orgId);
		List<Organization> list = new ArrayList<Organization>();
		list.add(entity);
		Struts2Utils.renderJson(list);
		return null;
	}
	
	@JsonOutput
	public String ztreeList() throws Exception {	
		List<OrganizationJson> list = this.organizationManager.getOrganizationJsonList(id);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		if (id != null) {
			entity = organizationManager.getOrganizationById(id);
		} else {
			entity = new Organization();
		}

	}
	
	@JsonOutput
	public String queryListToselect() throws Exception {
		// TODO Auto-generated method stub
		entity = this.organizationManager
				.getOrganizationById(Constants.USER_SUPER_ORGANIZATION);
		List<Organization> list = new ArrayList<Organization>();
		list.add(entity);
		Struts2Utils.renderJson(list);
		return null;
	}
	
	@JsonOutput
	public String getOrganizationEntity() throws Exception {
		entity = organizationManager.getOrganizationById(id);
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	@JsonOutput
	public String findOrgList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		List<Organization> orgList = organizationManager.getOrganizationList(filters);
		List<OrganizationJson> ojList = new ArrayList<OrganizationJson>();
		if(orgList != null && !orgList.isEmpty()) {
			for(Organization org : orgList) {
				OrganizationJson oj = new OrganizationJson();
				oj.setId(org.getId());
				oj.setName(org.getOrganizationName());
				ojList.add(oj);
			}
		}
		formatMessage.setMsg(ojList);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 根据当前用户组织获取下级组织
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String getOrgList() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		formatMessage.setMsg(organizationManager.getOrgChildList(user.getOrgCode()));
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	
}
