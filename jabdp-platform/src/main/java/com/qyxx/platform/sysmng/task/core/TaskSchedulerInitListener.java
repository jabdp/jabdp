/*
 * @(#)TaskSchedulerInitListener.java
 * 2013-2-26 下午12:08:46
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.task.core;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.commons.lang.StringUtils;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qyxx.platform.common.utils.PropertiesUtils;


/**
 *  任务调度启动类
 *  @author gxj
 *  @version 1.0 2013-2-26 下午12:08:46
 *  @since jdk1.6 
 */

public class TaskSchedulerInitListener extends QuartzInitializerListener {

	private static Logger logger = LoggerFactory.getLogger(TaskSchedulerInitListener.class);
	
	/** 
	 * @see org.quartz.ee.servlet.QuartzInitializerListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		super.contextInitialized(arg0);
		
		logger.info("quartz组件已准备就绪！");
		
		//获取quartz.properties判断是否分布式部署方式
		ServletContext sc = arg0.getServletContext();
		String configFile = sc.getInitParameter("quartz:config-file");
		Properties pp = new Properties();
		try {
			pp = PropertiesUtils.loadProperties(configFile);
		} catch (IOException e) {
			logger.error("加载quartz配置文件出现异常",e);
		}
		String js = pp.getProperty(StdSchedulerFactory.PROP_JOB_STORE_CLASS, "");
		if(StringUtils.contains(js, "RAMJobStore")) {//在RAMJobStore下启动所有任务
			QuartzManager.startAllTask(sc);
			logger.info("quartz单机调度已启动！");
		} else {
			logger.info("quartz分布式调度已启动！");
		}
	}

}
