package com.qyxx.platform.sysmng.memo.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;
import com.qyxx.platform.common.module.entity.BaseEntity;

/**
 *  @author tsd
 *  @version 1.0 2015-3-16 下午04:09:37
 *  @since jdk1.6 
 *  用户备忘录表
 */

@Entity
@Table(name = "SYS_MEMO")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Memo extends BaseEntity {
	
	public static final String STATUS_0 = "0";// 0.草稿 1.已发布
	public static final String STATUS_1 = "1";
	
	private Long id; //备忘录id
	private String title; //主题
	private String content; //内容
	private String remindType; //定时提醒类型：1.每天同一时刻 2.每周提醒 3.指定时刻
	private String remindTime; //提醒时间
	private Date remindDate; //提醒日期
	private String remindWeek; //一周提醒，复选框
	private String status; //0.草稿 1.已发布
	private List<MemoToUser> memoToUserList = Lists.newArrayList();
	
	private String mtuStatus; //执行状态
    private Long mtuId; //查看通知编号
    private Long mtuUserId; //分享用户
    
	private String memoToUserIds; //备忘录被分享者ID
	
	public Memo() {}
	
    public Memo(Memo memo, MemoToUser memoToUser) {
		this.id = memo.id;
		this.title = memo.title;
		this.content = memo.content;
		this.remindType = memo.remindType;
		this.remindTime = memo.remindTime;
		this.remindDate = memo.remindDate;
		this.remindWeek = memo.remindWeek;
		this.mtuStatus = memoToUser.getStatus();
		this.mtuId = memoToUser.getId();
		this.mtuUserId = memoToUser.getUserId();
		this.createTime = memo.createTime;
		this.createUser = memo.createUser;
		this.lastUpdateTime = memo.createTime;
		this.lastUpdateUser = memo.createUser;
		this.version = memo.version;
	}

	@Transient
	public String getMtuStatus() {
		return mtuStatus;
	}

	public void setMtuStatus(String mtuStatus) {
		this.mtuStatus = mtuStatus;
	}

	@Transient
	public Long getMtuId() {
		return mtuId;
	}

	public void setMtuId(Long mtuId) {
		this.mtuId = mtuId;
	}
	
	@Transient
	public Long getMtuUserId() {
		return mtuUserId;
	}

	public void setMtuUserId(Long mtuUserId) {
		this.mtuUserId = mtuUserId;
	}

	@Transient
	public String getMemoToUserIds() {
		return memoToUserIds;
	}

	public void setMemoToUserIds(String memoToUserIds) {
		this.memoToUserIds = memoToUserIds;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_MEMO_ID")
	@SequenceGenerator(name = "SEQ_SYS_MEMO_ID", sequenceName = "SEQ_SYS_MEMO_ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "Title", length = 100)
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Lob
	@Column(name = "CONTENT_VAL", columnDefinition = "ntext")
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	@Column(name = "REMIND_TYPE", length = 20)
	public String getRemindType() {
		return remindType;
	}
	
	public void setRemindType(String remindType) {
		this.remindType = remindType;
	}
	
	@Column(name = "REMIND_TIME", length = 10)
	public String getRemindTime() {
		return remindTime;
	}
	
	public void setRemindTime(String remindTime) {
		this.remindTime = remindTime;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REMIND_DATE")
	public Date getRemindDate() {
		return remindDate;
	}
	
	public void setRemindDate(Date remindDate) {
		this.remindDate = remindDate;
	}
	
	@Column(name = "REMIND_WEEK", length = 100)
	public String getRemindWeek() {
		return remindWeek;
	}
	
	public void setRemindWeek(String remindWeek) {
		this.remindWeek = remindWeek;
	}
	
	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	@OneToMany(mappedBy = "memo")
	@OrderBy("id")
	@JsonIgnore
	public List<MemoToUser> getMemoToUserList() {
		return memoToUserList;
	}

	public void setMemoToUserList(List<MemoToUser> memoToUserList) {
		this.memoToUserList = memoToUserList;
	}
	
}
