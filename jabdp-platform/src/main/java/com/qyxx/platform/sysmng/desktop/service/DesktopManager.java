/*
 * @(#)DsckTopAcManager.java
 * 2012-9-5 下午06:17:56
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.desktop.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.accountmng.dao.RoleDao;
import com.qyxx.platform.sysmng.accountmng.dao.UserDao;
import com.qyxx.platform.sysmng.accountmng.entity.Organization;
import com.qyxx.platform.sysmng.accountmng.entity.Role;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.desktop.dao.DesktopDao;
import com.qyxx.platform.sysmng.desktop.entity.Desktop;
import com.qyxx.platform.sysmng.desktop.entity.DesktopJson;
import com.qyxx.platform.sysmng.desktop.entity.DesktopToUser;
import com.qyxx.platform.sysmng.notice.entity.Notice;



/**
 * 
 * @author ly
 * @version 1.0 2012-9-5 下午06:17:56
 * @since jdk1.6
 */
@Component
@Transactional
public class DesktopManager {
	private static final String FIND_DESKTOP_BY_USERID="select a from Desktop a ";
	private static final String FIND_LEVEL1_DESKTOP = "select distinct(a) from Desktop a inner join  a.roleList b where b.id in(select e.id from User d inner join d.roleList e where d.id = ?) and a.level = ?";
	private static final String FIND_LEVEL1_DESKTOP_BYPID = "select distinct(a) from Desktop a inner join  a.roleList b where b.id in(select e.id from User d inner join d.roleList e where d.id = ?) and a.parentId = ? and a.level = ?";
	private DesktopDao desktopDao;
    private RoleDao roleDao;
    private UserDao userDao;
    
	public DesktopDao getDesktopDao() {
		return desktopDao;
	}
	@Autowired
	public void setDesktopDao(DesktopDao desktopDao) {
		this.desktopDao = desktopDao;
	}
	
	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	public RoleDao getRoleDao() {
		return roleDao;
	}
	
	@Autowired
	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}
	public void findDesktopByRoleId(Long roleId){
	}
	
	
	public static void main(String[] args) {
		String[] strType=StringUtils.split("p5:p6:",":");
		System.out.println(strType.length);
	}
	
	

	/**
	 * 查询当前用户创建的系统个人桌面列表
	 * @param page
	 * @param userId
	 * @return
	 */
	@Transactional(readOnly = true)
	public Page<Desktop> findDeskTopList(
			Page<Desktop> page, List<PropertyFilter> filters) {
		return preHandlePage(desktopDao.findPage(page, filters));
	}

	/**
	 * 角色管理中查询个人桌面列表
	 *  * @param roleId
	 */
	@Transactional(readOnly = true)
	public List<DesktopJson> getDesktopList(Long roleId){
		List<DesktopJson> list = new ArrayList<DesktopJson>();
		
			List<Desktop> listDesktop= desktopDao.getAll();		
			for (int j = 0; j < listDesktop.size(); j++) {
				Desktop desktop = listDesktop.get(j);
				DesktopJson dj = new DesktopJson(desktop);
				list.add(dj);
			}
			
			if(roleId!= null){
				Role role = roleDao.get(roleId);
				List<Desktop> selectList = role.getDesktopList();
				Long b = 0L;
				for (int i = 0; i < selectList.size(); i++) {
					b = selectList.get(i).getId();
					for (int j = 0; j < list.size(); j++) {
						if (b == list.get(j).getId()) {
							list.get(j).setChecked(true);
						}
					}
				}
				return list;
			}
		return list;
	}

		

	
	/**
	 * 根据LEVEL查询系统个人桌面 展示出一颗树
	 * @param userId
	 * @return
	 */
	
	public List<Desktop> desktopTree(Long userId){
		return desktopDao.find(FIND_DESKTOP_BY_USERID);
	}

	/**
	 * 根据id查找系统个人桌面
	 * @param userId
	 * @return
	 */
	public Desktop findById(Long id){
		return desktopDao.get(id);
	}
	/**
	 * 查询用户个人桌面中LEVEL=1的集合
	 * 
	 * @param userId
	 * @return
	 */
	public List<Desktop> findLevel1Desktop(Long userId){
		List<Desktop> desktop=desktopDao.find(FIND_LEVEL1_DESKTOP,userId,DesktopToUser.DESKTOPTOUSER_LEVEL1); 
	    return desktop;
	}
	/**
	 * 查询用户个人桌面每一个Level1下的LEVEL2
	 * @param parentId
	 * @return
	 */
	public List<Desktop> findLevel2DesktopByParentId(Long parentId,Long userId){
		List<Desktop> desktop=desktopDao.find(FIND_LEVEL1_DESKTOP_BYPID,userId,parentId,DesktopToUser.DESKTOPTOUSER_LEVEL2); 
	    return desktop;
	}
	
	public Desktop queryDesktopSource(Long desktopId,User user){
		Desktop desktop=desktopDao.get(desktopId);
		String sourceType=desktop.getSourceType();
		String source=desktop.getSource();
		Long displayNum=desktop.getDisplayNum();
		String orgCode = user.getOrganization().getOrganizationCode();
		List<String> codeList = Organization.getSplitCodeList(orgCode);
		if(sourceType.equals("sql")){
			Map<String, Object> param=new HashMap<String, Object>();
			param.put("userId", user.getId());
			param.put("desktopId", desktopId);
			param.put("parentOrgCodes", codeList);
			List<Map<String, Object>> list=desktopDao.findDataList(source, displayNum.intValue(), param);
			desktop.setList(list);			
		}
		return desktop;
	}
	
	/**
	 * 处理Page对象，翻译字段
	 * 
	 * @param page
	 * @return
	 */
	public Page<Desktop> preHandlePage(Page<Desktop> page) {
		List<Desktop> list = page.getResult();
		if(list != null) {
			for(Desktop n : list) {
				n.setCreateUserCaption(userDao.getUserNameById(n.getCreateUser()));
			}
		}
		return page;
	}
}
