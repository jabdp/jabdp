/*
 * @(#)TaskJobImpl.java
 * 2013-2-26 下午01:59:51
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.task.core;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qyxx.platform.common.utils.encode.JsonBinder;
import com.qyxx.platform.common.utils.spring.SpringContextHolder;
import com.qyxx.platform.sysmng.exception.GsException;
import com.qyxx.platform.sysmng.task.entity.Task;
import com.qyxx.platform.sysmng.task.entity.TaskContentJson;
import com.qyxx.platform.sysmng.task.service.TaskManager;


/**
 *  任务执行器--Job
 *  @author gxj
 *  @version 1.0 2013-2-26 下午01:59:51
 *  @since jdk1.6 
 */
@DisallowConcurrentExecution
public class TaskJobImpl implements Job{

	public static final String MAP_KEY = "param";
	
	private static Logger logger = LoggerFactory.getLogger(TaskJobImpl.class);
	
	private JsonBinder jsonBinder = JsonBinder.getAlwaysMapper();
	
	private TaskManager taskManager = SpringContextHolder.getBean("taskManager");

	@Override
	public void execute(JobExecutionContext context)
													throws JobExecutionException {
		try {
			Task task = (Task)context.getJobDetail().getJobDataMap().get(MAP_KEY);
			//logger.info("测试任务执行:" + task);
			if(null!=task) {
				TaskContentJson tcj = jsonBinder.fromJson(task.getTaskContent(), TaskContentJson.class);
				if(Task.TASK_TYPE_NORMAL.equalsIgnoreCase(task.getTaskType())) {
					//普通任务
					logger.info("开始执行普通任务（" + task.getName() + "）");
					excuteNormalTask(tcj, context);
					logger.info("结束执行普通任务（" + task.getName() + "）");
				} else if(Task.TASK_TYPE_PREWARNING.equalsIgnoreCase(task.getTaskType())) {
					//预警通知任务
					logger.info("开始执行预警通知任务（" + task.getName() + "）");
					excuteNoticeTask(tcj, context);
					logger.info("结束执行预警通知任务（" + task.getName() + "）");
				} else if(Task.TASK_TYPE_UPDATE_GPS.equalsIgnoreCase(task.getTaskType())) {
					//GPS更新任务
					logger.info("开始执行GPS更新任务（" + task.getName() + "）");
					excuteGpsUpdateTask(tcj, context);
					logger.info("结束执行GPS更新任务（" + task.getName() + "）");
				} else {
					logger.info("开始执行普通任务（" + task.getName() + "）");
					excuteNormalTask(tcj, context);
					logger.info("结束执行普通任务（" + task.getName() + "）");
				}
			}
		} catch(Exception e) {
			logger.error("执行任务时出现异常", e);
		}
	}
	
	/**
	 * 执行普通任务
	 * 
	 * @param tcj
	 * @param context
	 */
	private void excuteNormalTask(TaskContentJson tcj, JobExecutionContext context) {
		if(TaskContentJson.EXECUTE_TYPE_CLASS.equalsIgnoreCase(tcj.getExecuteType())) {
			//执行class方法
			try {
				Job job = (Job)Class.forName(tcj.getExecuteContent()).newInstance();
				context.put(MAP_KEY, tcj.getExecuteParam());
				job.execute(context);
			} catch (Exception e) {
				logger.error("执行class任务时出现异常", e);
			}
		} else if(TaskContentJson.EXECUTE_TYPE_SQL.equalsIgnoreCase(tcj.getExecuteType())) {
			//TODO 执行sql语句
		} else if(TaskContentJson.EXECUTE_TYPE_PROC.equalsIgnoreCase(tcj.getExecuteType())) {
			//TODO 执行存储过程
		}
	}
	
	/**
	 * 执行预警通知任务
	 * 
	 * @param tcj
	 * @param context
	 */
	private void excuteNoticeTask(TaskContentJson tcj, JobExecutionContext context) {
		try {
			taskManager.runNoticeTask(tcj);
		} catch(Exception e) {
			throw new GsException("执行预警通知任务时出现异常", e);
		}
	}
	
	/**
	 * 执行GPS更新任务
	 * 
	 * @param tcj
	 * @param context
	 */
	private void excuteGpsUpdateTask(TaskContentJson tcj, JobExecutionContext context) {
		try {
			taskManager.runGpsUpdateTask(tcj);
		} catch(Exception e) {
			throw new GsException("执行GPS更新任务时出现异常", e);
		}
	}

}
