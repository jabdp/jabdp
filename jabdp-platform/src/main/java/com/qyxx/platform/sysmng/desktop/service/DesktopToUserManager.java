/*
 * @(#)DeskTopToUserService.java
 * 2012-9-5 下午06:18:50
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.desktop.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.accountmng.entity.Organization;
import com.qyxx.platform.sysmng.accountmng.entity.Role;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.desktop.dao.DesktopDao;
import com.qyxx.platform.sysmng.desktop.dao.DesktopToUserDao;
import com.qyxx.platform.sysmng.desktop.entity.Desktop;
import com.qyxx.platform.sysmng.desktop.entity.DesktopToUser;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 * 
 * @author ly
 * @version 1.0 2012-9-5 下午06:18:50
 * @since jdk1.6
 */
@Component
@Transactional
public class DesktopToUserManager {
    private static final String FIND_LEVEL1_DESKTOPTOUSER="select a from DesktopToUser a where a.userId = ? and a.level = ? ";
    private static final String FIND_LEVEL_DESKTOP="select a from Desktop a inner join  a.roleList b where b.id in(select e.id from User d inner join d.roleList e where d.id = ?)";
	private static final String FIND_LEVEL2_DESKTOPTOUSER_BY_PARENTID="select a from DesktopToUser a where a.level = ? and a.parentId = ? and a.userId = ?";
	private static final String FIND_LEVEL2_DESKTOP_BY_PARENTID="select a from Desktop a inner join  a.roleList b where b.id in(select e.id from User d inner join d.roleList e where d.id = ?) and a.level = ? and a.parentId = ? and a.id not in(select f.desktop.id from DesktopToUser f where f.userId = ?)";
	private static final String FIND_DESKTOPTOUSER_BY_USERID="select a from DesktopToUser a inner join  a.desktop b where b.id = ? and a.userId = ?";
    private static final String FIND_DESKTOP_BY_PARENTID="select a from Desktop a where a.parentId = ?";
	private static final String FIND_DESKTOPTOUSER_BY_DESKTOPID="select a from DesktopToUser a inner join  a.desktop b where b.id = ?";
	private static final String FIND_DESKTOPTOUSER_BY_PARENTID="select a from DesktopToUser a inner join  a.desktop b where b.parentId = ?";
	
	private DesktopToUserDao desktopToUserDao;
    private DesktopDao desktopDao;

	public DesktopToUserDao getDesktopToUserDao() {
		return desktopToUserDao;
	}
	
	@Autowired
	public void setDesktopToUserDao(DesktopToUserDao desktopToUserDao) {
		this.desktopToUserDao = desktopToUserDao;
	}

	public DesktopDao getDesktopDao() {
		return desktopDao;
	}
	
	@Autowired
	public void setDesktopDao(DesktopDao desktopDao) {
		this.desktopDao = desktopDao;
	}

	
	/**
	 * 查询用户个人桌面中LEVEL=1的集合
	 * 
	 * @param userId
	 * @return
	 */
	public List<DesktopToUser> findLevel1DesktopToUser(Long userId,Long desktopId){
		//定义一个集合保存用户个人桌面中level=1的数据
		List<DesktopToUser> desktopToUsers=desktopToUserDao.find(FIND_LEVEL1_DESKTOPTOUSER,userId,DesktopToUser.DESKTOPTOUSER_LEVEL1);
		 //如果集合不为空，将其赋值给集合desktopToUser
		if(desktopToUsers==null || desktopToUsers.isEmpty()){
	    	//查询系统个人桌面赋值给用户个人桌面
			List<Desktop> desktops=findDesktop(userId);
			for(Desktop desktop:desktops){
				DesktopToUser dtu=copyToDesktopToUser(desktop, userId);
				desktopToUserDao.save(dtu);
			}
			desktopToUsers = desktopToUserDao.find(FIND_LEVEL1_DESKTOPTOUSER,userId,DesktopToUser.DESKTOPTOUSER_LEVEL1);
	    }
		if(desktopId!=null){
			List<DesktopToUser> desktop=Lists.newArrayList();
			for(DesktopToUser dtu : desktopToUsers){
				Long id = dtu.getDesktop().getId();
				if(desktopId.equals(id)){
					desktop.add(dtu);
					break;
				}
			}
			return desktop;
		}
	    return desktopToUsers;
	}
	
	
	public DesktopToUser copyToDesktopToUser(Desktop desktop,Long userId){
		 DesktopToUser dtu=new DesktopToUser();
		 dtu.setDesktop(desktop);
		 dtu.setCreateTime(new Date());
		 dtu.setCreateUser(userId);
		 dtu.setDisplayNum(desktop.getDisplayNum());
		 dtu.setFontNum(desktop.getFontNum());
		 dtu.setHeight(desktop.getHeight());
		 dtu.setLastUpdateTime(desktop.getLastUpdateTime());
		 dtu.setLastUpdateUser(desktop.getLastUpdateUser());
		 dtu.setLayoutType(desktop.getLayoutType());
		 dtu.setLayoutVal(desktop.getLayoutVal());
		 dtu.setLevel(desktop.getLevel());
		 dtu.setStatus(DesktopToUser.STATUS_1);//默认设置为显示
		 dtu.setTitle(desktop.getTitle());
		 dtu.setUserId(userId);
		 dtu.setParentId(desktop.getParentId());
		 return dtu;
	}

	/**
	 * 根据当前用户id查询角色相关联的所有系统个人桌面
	 * @param userId
	 * @return
	 */
	public List<Desktop> findDesktop(Long userId){
		List<Desktop> list = desktopDao.find(FIND_LEVEL_DESKTOP,userId);
		return distinct(list);
	}
	
	/**
	 * 过滤重复ID的对象
	 * 
	 * @param desktopList
	 * @return
	 */
	public List<Desktop> distinct(List<Desktop> desktopList) {
		List<Desktop> list = new ArrayList<Desktop>();
		Set<Long> idSet = new HashSet<Long>();
		if(null!=desktopList && !desktopList.isEmpty()) {
			for(Desktop dt : desktopList) {
				if(idSet.add(dt.getId())) {
					list.add(dt);
				}
			}
		}
		return list;
	}
	/**
	 * 查询当前用户下所有Level=1的桌面
	 * @param parentId
	 * @return
	 */
	public List<Desktop> findLevel1DesktopByUser(User user){
		Long userId = user.getId();
		List<Desktop> list = desktopDao.find(FIND_LEVEL_DESKTOP,userId);
		List<Desktop> listLeve1 =Lists.newArrayList();
		for(Desktop d : list){//查找出当前用户所有Level=1的桌面
			if(d.getLevel()==1L){
				listLeve1.add(d);
			}
		}
		
		//查找出当前用户已经显示出的所有Level=1的桌面
		List<DesktopToUser> listDtu=desktopToUserDao.find(FIND_LEVEL1_DESKTOPTOUSER,userId,DesktopToUser.DESKTOPTOUSER_LEVEL1);
		for(DesktopToUser dtu : listDtu){
			Long dId1 = dtu.getDesktop().getId();		
			for(Desktop d1  :listLeve1){
				Long dId2 = d1.getId();
				if(dId1.equals(dId2)){
					listLeve1.remove(d1);
					break;
				}
			}
		}
		
		return listLeve1;
	}
	/**
	 * 查询用户个人桌面每一个Level1下的LEVEL2
	 * @param parentId
	 * @return
	 */
	public List<DesktopToUser> findLevel2DesktopToUserByParentId(Long parentId, Long userId){
		return desktopToUserDao.find(FIND_LEVEL2_DESKTOPTOUSER_BY_PARENTID, DesktopToUser.DESKTOPTOUSER_LEVEL2,parentId, userId);
	}
	
	
	/**
	 * 查询系统个人桌面每一个Level1下的Level2
	 * @param parentId
	 * @param userId
	 * @return
	 */
	public List<Desktop> findLevelDesktopByParentId(Long parentId,Long userId){
		List<Desktop> list = desktopDao.find(FIND_LEVEL2_DESKTOP_BY_PARENTID, userId,Desktop.DESKTOPTOUSER_LEVEL2,parentId,userId);
		return distinct(list);
	}
	

	
	
	public Desktop findById(Long id){
		Desktop desktop=desktopDao.get(id);
//		List<Role> roles=desktop.getRoleList();
//		for(Role r:roles){
//			desktop.getRoleIds().add(r.getId());
//		}
		return desktop;
	}
	
	
	public DesktopToUser findDtuById(Long id){
	     return desktopToUserDao.get(id);
	}
	
	
	
	/**
	 * 保存用户个人桌面
	 * 
	 * @param desktopToUser
	 * @param userId
	 */
	public void saveDeskTopToUser(DesktopToUser desktopToUser,Long userId) {
        if(desktopToUser.getId()!=null){
        	//修改
        	desktopToUser.setLastUpdateTime(new Date());
        	desktopToUser.setLastUpdateUser(userId);
        }else{
        	//新增
        	desktopToUser.setCreateTime(new Date());
        	desktopToUser.setCreateUser(userId);
        	
        }
		desktopToUserDao.save(desktopToUser);

	}
	
	
	
	/**
	 * 查询所有用户的个人桌面
	 * 
	 * @param page
	 * @param filters
	 * @return
	 */
	@Transactional(readOnly = true)
	public Page<DesktopToUser> findDeskTopToUserList(
			Page<DesktopToUser> page, List<PropertyFilter> filters) {
		return desktopToUserDao.findPage(page, filters);
	}

	/**
	 * 删除用户个人桌面
	 * 
	 * @param ids
	 */
	public void deleteDeskTopToUsers(Long id,Long userId)  {
			//修改父桌面的实际布局layoutVal
			DesktopToUser dtu=desktopToUserDao.get(id);
			Long desktopId=dtu.getParentId();
			DesktopToUser parentDtu=findDesktopToUser(desktopId,userId).get(0);
			String layoutVal=parentDtu.getLayoutVal();
			List<List<String>> newCols=updateParentLayoutVal(dtu.getDesktop().getId(),layoutVal);
			parentDtu.setLayoutVal(convertListToString(newCols));
			desktopToUserDao.delete(id);
			
	}
	
	
	/**
	 * 删除系统个人桌面
	 * @param ids
	 */

	public void deleteDeskTops(Long[] ids,Long userId) {
		for (Long id : ids) {
			Long parentId=desktopDao.get(id).getParentId();
			//如果parentId不为空，则删除的是子桌面，同时要修改父桌面layVal中的字段
			if(parentId!=null){
				//系统表的父桌面
				   Desktop parentDesktop=desktopDao.get(parentId);
				   //用个自定义表中的父桌面
				/*   List<DesktopToUser> dtus=findDesktopToUserByDesktopId(parentId);
				  for(DesktopToUser parentDtu:dtus){
					   String layoutVal2=parentDtu.getLayoutVal();
					   List<List<String>> newCols2=updateParentLayoutVal(id,layoutVal2);
					   //修改用户桌面的父桌面实际布局
					   parentDtu.setLayoutVal(convertListToString(newCols2));
				  }*/
				   String layoutVal=parentDesktop.getLayoutVal();
				   List<List<String>> newCols=updateParentLayoutVal(id,layoutVal);
				   //List<List<String>> cols=converStringToList(layoutVal);
				   //修改系统桌面的父桌面实际布局
				   parentDesktop.setLayoutVal(convertListToString(newCols));
			}else{
				//如果parentId为空，则删除的是父桌面
				//删除用户自定义表中的所有子桌面
				List<DesktopToUser> dtus=findDesktopToUserByParentId(id);
				for(DesktopToUser dtu:dtus){
					desktopToUserDao.delete(dtu);
				}
				
				//删除下面所有的子桌面
				List<Desktop> childDesktop=desktopToUserDao.find(FIND_DESKTOP_BY_PARENTID, id);
				for(Desktop dp:childDesktop){
					desktopDao.delete(dp);
				}
			}
			//删除系统个人桌面与角色的关联关系
			desktopDao.deleteDesktopRoles(id);
			//删除与之相关的用户个人桌面
			desktopDao.deleteDesktopToUsers(id);
			desktopDao.delete(id);
		   
		}
   }
	
	/**
	 * 修改指定父桌面实际布局
	 * @param id
	 * @param layVal
	 * @return
	 */
	public List<List<String>> updateParentLayoutVal(Long id,String layVal){
		List<List<String>> cols=converStringToList(layVal);
		List<List<String>> newCols=Lists.newArrayList();
		String delId="p"+id;
		for (List<String> ls : cols) {
			List<String> list=Lists.newArrayList();
			for (String l : ls) {
				if(!l.equals(delId)){
					list.add(l);
				}	
			}
			newCols.add(list);
		}
		return newCols;
		
	}

	
	/**
	 * 用户自定义个人桌面（将选中的桌面复制到desktopToUser中，并且修改desktopToUser的父桌面实际布局layVal）
	 * @param parentId
	 * @param ids
	 * @param userId
	 */
	public DesktopToUser addDesktopToPortal(Long parentId, Long[] ids, Long userId) {
		for (Long id : ids) {
			Desktop desktop = desktopDao.get(id);
			DesktopToUser dtu=copyToDesktopToUser(desktop, userId);
			desktopToUserDao.save(dtu);
		}
		//将选中的桌面复制到desktopToUser中
		
		List<DesktopToUser> dtus=findDesktopToUser(parentId,userId);
		DesktopToUser parentDtu =dtus.get(0);
		List<List<String>> cols = Lists.newArrayList();
		String layVal = parentDtu.getLayoutVal();
		if (StringUtils.isNotBlank(layVal)) {
			// layoutVal有值
			cols = converStringToList(layVal);
		}else{
	    	//layoutVal没值
	    	String layType=parentDtu.getLayoutType();
	    	String[] strType=layType.split(";");
	    	for(String str:strType){
	    		List<String> list=Lists.newArrayList();
	    		cols.add(list);
	    	}
	    }
		for(Long i:ids){
			cols.get(0).add("p" + i);
		}
		parentDtu.setLayoutVal(convertListToString(cols));
		return parentDtu;

	}
	public List<DesktopToUser> addDesktopToTabs(Long[] ids, Long userId) {
		List<DesktopToUser> list=Lists.newArrayList();
		if(ids!=null){
			for (Long id : ids) {
			Desktop desktop = desktopDao.get(id);
					
			DesktopToUser dtu=copyToDesktopToUser(desktop, userId);			
			desktopToUserDao.save(dtu);
			List<Desktop> listDesktop =  findLevelDesktopByParentId(id,userId);	
			for(Desktop d :listDesktop){
				DesktopToUser dtuc=copyToDesktopToUser(d, userId);
				desktopToUserDao.save(dtuc);				
			}
			list.add(dtu);
			}		
		}
		return list;	
	}
	
	
	 /**
	    * 根据id查找一个用户个人桌面
	    * 
	    * @param id
	    * @return
	    */
		public DesktopToUser findDesktopToUserById(Long id){
			return desktopToUserDao.get(id);
		}
		
		/**
		 * 根据系统个人桌面标号找到相关联的角色集合
		 * 
		 * @param id
		 * @return
		 */
		public List<Role> findRoleIdsById(Long id){
			return desktopDao.get(id).getRoleList();
		}
	

		/**
		 * 根据desktopId和userId查询desktopToUser
		 * @param desktopId
		 * @param userId
		 * @return
		 */
		public List<DesktopToUser> findDesktopToUser(Long desktopId,Long userId){
			return desktopToUserDao.find(FIND_DESKTOPTOUSER_BY_USERID, desktopId,userId);
		}
		
		/**
		 * 根据desktopId查询desktopToUser
		 * @param desktopId
		 * @param userId
		 * @return
		 */
		public List<DesktopToUser> findDesktopToUserByDesktopId(Long desktopId){
			return desktopToUserDao.find(FIND_DESKTOPTOUSER_BY_PARENTID, desktopId);
		}
		
		
		/**
		 * 根据parentId查询desktopToUser
		 * @param parentId
		 * @return
		 */
		public List<DesktopToUser> findDesktopToUserByParentId(Long parentId){
			return desktopToUserDao.find(FIND_DESKTOPTOUSER_BY_PARENTID, parentId);
		}		
		
		/**
		 * 保存页面拖动后的状态到桌面实际布局中
		 * @param id
		 * @param layoutVal
		 */
		public void saveDesktopLayout(Long id,Long userId,String layoutVal){
			DesktopToUser dtu=desktopToUserDao.get(id);
			dtu.setLayoutVal(layoutVal);
			desktopToUserDao.save(dtu);
		}
		
		/**
		 * 修改系统个人桌面类型layoutType
		 * @param desktop
		 * @param userId
		 * @param cols
		 */
		public DesktopToUser updateDesktopLayout(Long desktopId, Long userId,String layType) {
			List<List<String>> cols = Lists.newArrayList();
			List<DesktopToUser> dtus=findDesktopToUser(desktopId,userId);
			DesktopToUser dtu=dtus.get(0);
			String layVal = dtu.getLayoutVal();
			layVal += ",";
			System.out.println(layType);
			String[] strType=layType.split(";");
			String[] strVal = layVal.split(":");
			int count1 = strType.length;
			int count2 = strVal.length;
			List<List<String>> newCols = converStringToList(layVal);
			if (count1 != count2) {
				for (String s:strType) {
					List<String> list = Lists.newArrayList();
					cols.add(list);
				}
				int i = 0;
				for (List<String> ls : newCols) {
					for (String l : ls) {
						cols.get(i).add(l);//将一个String从一个双重集合中取出，放入到另外一个双重集合中的第一个位置
						if (++i == count1) {
							i = 0;
						}
					}
				}
				dtu.setLayoutType(layType);
				dtu.setLayoutVal(convertListToString(cols));
				desktopToUserDao.save(dtu);
			}
			return dtu;
		}
		
		
       /**
        * 修改列宽
        * @param parentId
        * @param userId
        * @param layType
        */
	public DesktopToUser updateCols(Long parentId, Long userId, String sType) {
		List<List<String>> cols = Lists.newArrayList();
		List<DesktopToUser> dtus = findDesktopToUser(parentId, userId);
		DesktopToUser dtu = dtus.get(0);
		dtu.setLayoutType(sType);
		desktopToUserDao.save(dtu);
		return dtu;
	}
	
	/**
	 * 修改用户自定义桌面
	 * @param desktopToUser
	 * @return
	 */
	public DesktopToUser doUpdate(DesktopToUser desktopToUser){
		 DesktopToUser dtu=desktopToUserDao.get(desktopToUser.getId());
		 dtu.setTitle(desktopToUser.getTitle());
		 dtu.setDisplayNum(desktopToUser.getDisplayNum());
		 dtu.setFontNum(desktopToUser.getFontNum());
		 dtu.setHeight(desktopToUser.getHeight());
		 dtu.setOptions(desktopToUser.getOptions());
		 desktopToUserDao.save(dtu);
		 return dtu;
	}
	
		/**
		 * 增加系统个人桌面
		 * @param desktop
		 * @param userId
		 */
		public void saveDeskTop(Desktop desktop ,Long userId)  {
			List<List<String>> cols=Lists.newArrayList();
			if(desktop.getId()!=null){
				//修改
				desktop.setLastUpdateTime(new Date());
				desktop.setLastUpdateUser(userId);
				String layType=desktop.getLayoutType();
				String layVal=desktop.getLayoutVal();
				layVal+=",";
		    	String[] strType=layType.split(";");
		    	String[] strVal=layVal.split(":");
		    	int count1=strType.length;
		    	int count2=strVal.length;
		    	List<List<String>> newCols=converStringToList(layVal);
		    	if(count1!=count2){
		    		for(String s:strType){
		    			List<String> list=Lists.newArrayList();
		    			cols.add(list);
		    			
		    		}
		    		int i=0;
		    		for(List<String> ls:newCols){
		    			for(String l:ls){
		    				cols.get(i).add(l);
		    				if(++i==count1){
		    					i=0;
		    				}
		    			}
		    		}
		    		 desktop.setLayoutVal(convertListToString(cols));
		    	}
			 desktopDao.save(desktop);
			}else{
				//增加
				desktop.setCreateTime(new Date());
				desktop.setCreateUser(userId);
				Long parentId=desktop.getParentId();
				//桌面为空，则添加的是第二级或者第三级桌面
				if(parentId!=null && parentId>0){
					Desktop parentDesktop=desktopDao.get(parentId);
					Long parentLevel=parentDesktop.getLevel();
					//父桌面级别为1，则添加的是二级桌面，同时将桌面类型和布局类型置为空，并且拼父桌面的layoutVal字段
					if(parentLevel==1){
						//二级桌面显示字数如果为空，设置默认值为5，显示条数为空，设置默认值为10
						if(desktop.getDisplayNum()==null){
							desktop.setDisplayNum(new Long(5));
							desktop.setFontNum(new Long(10));
						}else{
							desktop.setDisplayNum(desktop.getDisplayNum());
							desktop.setFontNum(desktop.getFontNum());
						}
						desktop.setLevel(new Long(2));
						desktop.setLayoutVal(null);
						desktop.setLayoutType(null);
						 cols=Lists.newArrayList();
					    String layVal=parentDesktop.getLayoutVal();
					    if(StringUtils.isNotBlank(layVal)){
					    	//layoutVal有值
					    	  cols=converStringToList(layVal);
					    }else{
					    	//layoutVal没值
					    	String layType=parentDesktop.getLayoutType();
					    	String[] strType=layType.split(";");
					    	for(String str:strType){
					    		List<String> list=Lists.newArrayList();
					    		cols.add(list);
					    	}
					    }
					     desktopDao.save(desktop);
					     cols.get(0).add("p"+desktop.getId());
				    	 parentDesktop.setLayoutVal(convertListToString(cols));
					}else if(parentLevel==2){
						//父桌面为2，则添加的是三级桌面，同时将桌面类型和布局类型置为空
						desktop.setLevel(new Long(3));
						desktop.setType(null);
						desktop.setLayoutVal(null);
						desktop.setLayoutType(null);
						desktopDao.save(desktop);
					}
				}else{
					//父桌面为空，则添加的是第一级桌面
					desktop.setLevel(new Long(1));
					desktop.setParentId(null);
					desktop.setType(null);
					desktopDao.save(desktop);
				}
				//保存桌面与超极管理员角色关系
				desktopDao.saveDesktopRoles(desktop.getId(), Constants.SUPER_ADMIN_ROLE_ID);
			}
			
/*			//向SYS_TO_USER关系表中添加关联关系
			String ids = desktop.getRoleListIds();
			Long desktopId = desktop.getId();
			desktopDao.deleteDesktopRoles(desktopId);
			if (StringUtils.isNotBlank(ids)) {
	               String rIds[] = ids.split(",");
				for (String x : rIds) {
					Long id = Long.valueOf(x);
					desktopDao.saveDesktopRoles(desktopId, id);
				}
			} */
			
		}

		public String convertListToString(List<List<String>> cols){
			List<String> buffer=Lists.newArrayList();
			if(cols!=null){
				for(List<String> ls:cols){
					buffer.add(StringUtils.join(ls, ","));
				}
			}
			return StringUtils.join(buffer, ":");
		}
		
		public List<List<String>> converStringToList(String layVal){
			layVal=layVal+",";
			String[] strVal=layVal.split(":");
			List<List<String>> cols=Lists.newArrayList();
				for(int i=0;i<strVal.length;i++){
					String str=strVal[i];
					List<String> list=Lists.newArrayList();
					if(StringUtils.isNotBlank(str)){
						String[] ss=str.split(",");
						for(String s:ss){
							if(StringUtils.isNotBlank(s)){
								list.add(s);
							}
						}
					}
					cols.add(list);
				}
			return cols;
			
		}


		/**
		 * 如果desktop表中sourcetype=sql,则调用DesktopToUserDao的findDataList查处内容，赋值给DesktopToUser的list属性并返回DesktopToUser对象
		 * @param id
		 * @param userId
		 * @return
		 */
		public DesktopToUser queryDesktopSource(Long desktopId,User user){
			DesktopToUser desktopToUser=findDesktopToUser(desktopId,user.getId()).get(0);
			Desktop desktop=desktopDao.get(desktopId);
			String sourceType=desktop.getSourceType();
			String source=desktop.getSource();
			Long displayNum=desktop.getDisplayNum();
			String orgCode = user.getOrganization().getOrganizationCode();
			List<String> codeList = Organization.getSplitCodeList(orgCode);
			if(sourceType.equals("sql")){
				Map<String, Object> param=new HashMap<String, Object>();
				param.put("userId", user.getId());
				param.put("desktopId", desktopId);
				param.put("parentOrgCodes", codeList);
				List<Map<String, Object>> list=desktopToUserDao.findDataList(source, displayNum.intValue(), param);
				desktopToUser.setList(list);
				
			}
			return desktopToUser;
		}
		
		

}
