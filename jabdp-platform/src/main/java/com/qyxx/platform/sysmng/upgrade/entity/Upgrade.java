/*
 * @(#)Upgrade.java
 * 2011-9-24 下午09:41:17
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.upgrade.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *  升级部署日志
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-9-24 下午09:41:17
 */
@Entity
@Table(name = "SYS_DEPLOY_LOG")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Upgrade implements Serializable{
	
	private static final long serialVersionUID = -416245190387841146L;
	
	//部署成功
	public static final String DEPLOY_FLAG_SUCCESS = "1";
	//部署失败
	public static final String DEPLOY_FLAG_FAIL = "0";
	//系统部署
	public static final String DEPLOY_TYPE_SYS = "0";
	//设计器部署
	public static final String DEPLOY_TYPE_DS = "1";
	
	private Long id;
	private String deployUrl;
	private String deployVersion;
	private String deployType;
	private String deployName;
	private String deployDesc;
	private Date deployTimeStart;
	private Date deployTimeEnd;
	private String ip;
	private String machineName;
	private String deployFlag;
	private String exceptionLog;
	private String loginName;
	private String realName;
	private String orgName;
	private Long rollbackId;
	
	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_DEPLOY_LOG_ID")
	@SequenceGenerator(name = "SEQ_SYS_DEPLOY_LOG_ID", sequenceName = "SEQ_SYS_DEPLOY_LOG_ID")
	public Long getId() {
		return id;
	}
	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return deployUrl
	 */
	@Column(name = "DEPLOY_URL", length = 500)
	public String getDeployUrl() {
		return deployUrl;
	}
	
	/**
	 * @param deployUrl
	 */
	public void setDeployUrl(String deployUrl) {
		this.deployUrl = deployUrl;
	}
	
	/**
	 * @return deployVersion
	 */
	@Column(name = "DEPLOY_VERSION", length = 100)
	public String getDeployVersion() {
		return deployVersion;
	}
	
	/**
	 * @param deployVersion
	 */
	public void setDeployVersion(String deployVersion) {
		this.deployVersion = deployVersion;
	}
	
	/**
	 * @return deployType
	 */
	@Column(name = "DEPLOY_TYPE", length = 50)
	public String getDeployType() {
		return deployType;
	}
	
	/**
	 * @param deployType
	 */
	public void setDeployType(String deployType) {
		this.deployType = deployType;
	}
	
	/**
	 * @return deployName
	 */
	@Column(name = "DEPLOY_NAME", length = 100)
	public String getDeployName() {
		return deployName;
	}
	
	/**
	 * @param deployName
	 */
	public void setDeployName(String deployName) {
		this.deployName = deployName;
	}
	
	/**
	 * @return deployDesc
	 */
	@Column(name = "DEPLOY_DESC", length = 2000)
	public String getDeployDesc() {
		return deployDesc;
	}
	
	/**
	 * @param deployDesc
	 */
	public void setDeployDesc(String deployDesc) {
		this.deployDesc = deployDesc;
	}
	
	/**
	 * @return deployTimeStart
	 */
	@Column(name="DEPLOY_TIME_START")
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getDeployTimeStart() {
		return deployTimeStart;
	}
	
	/**
	 * @param deployTimeStart
	 */
	public void setDeployTimeStart(Date deployTimeStart) {
		this.deployTimeStart = deployTimeStart;
	}
	
	/**
	 * @return deployTimeEnd
	 */
	@Column(name="DEPLOY_TIME_END")
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getDeployTimeEnd() {
		return deployTimeEnd;
	}
	
	/**
	 * @param deployTimeEnd
	 */
	public void setDeployTimeEnd(Date deployTimeEnd) {
		this.deployTimeEnd = deployTimeEnd;
	}
	
	/**
	 * @return ip
	 */
	@Column(name="OPERATION_IP",length=100)
	public String getIp() {
		return ip;
	}
	
	/**
	 * @param ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	/**
	 * @return machineName
	 */
	@Column(name="OPERATION_MACHINE_NAME",length=100)
	public String getMachineName() {
		return machineName;
	}
	
	/**
	 * @param machineName
	 */
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	
	/**
	 * @return deployFlag
	 */
	@Column(name="DEPLOY_FLAG",length=50)
	public String getDeployFlag() {
		return deployFlag;
	}
	
	/**
	 * @param deployFlag
	 */
	public void setDeployFlag(String deployFlag) {
		this.deployFlag = deployFlag;
	}
	
	/**
	 * @return exceptionLog
	 */
	@Lob
	@Column(name = "EXCEPTION_LOG", columnDefinition = "ntext")
	public String getExceptionLog() {
		return exceptionLog;
	}
	
	/**
	 * @param exceptionLog
	 */
	public void setExceptionLog(String exceptionLog) {
		this.exceptionLog = exceptionLog;
	}
	
	/**
	 * @return loginName
	 */
	@Column(name="LOGIN_NAME",length=50)
	public String getLoginName() {
		return loginName;
	}
	
	/**
	 * @param loginName
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	/**
	 * @return realName
	 */
	@Column(name="REAL_NAME",length=50)
	public String getRealName() {
		return realName;
	}
	
	/**
	 * @param realName
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	
	/**
	 * @return orgName
	 */
	@Column(name="ORG_NAME",length=100)
	public String getOrgName() {
		return orgName;
	}
	
	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	/**
	 * @return rollbackId
	 */
	@Column(name="ROLLBACK_ID")
	public Long getRollbackId() {
		return rollbackId;
	}
	
	/**
	 * @param rollbackId
	 */
	public void setRollbackId(Long rollbackId) {
		this.rollbackId = rollbackId;
	}
	
	

}
