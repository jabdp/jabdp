/*
 * @(#)NewsDao.java
 * 2012-9-4 下午05:58:26
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.notice.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.notice.entity.News;


/**
 *  
 *  @author ly
 *  @version 1.0 2012-9-4 下午05:58:26
 *  @since jdk1.6 
 */

/**
 * 系统新闻Dao类
 */
@Component
public class NewsDao extends HibernateDao<News, Long>{

	private static final String INSERT_NEWS_TO_ORG= "INSERT INTO SYS_NEWS_TO_ORG(ORG_ID, NEWS_ID) VALUES(?, ?)";
	private static final String DELETE_ORGANIZATION_ORGANIZATIONID = "DELETE FROM SYS_NEWS_TO_ORG WHERE ORG_ID = ?";
	private static final String DELETE_NEWS_NEWSID = "DELETE FROM SYS_NEWS_TO_ORG WHERE NEWS_ID = ?";
	/**
	 * 往关系表中添加关联关系
	 * @param organizationId
	 * @param newsId
	 */
	public void saveNewToOrg(Long organizationId,Long newsId){
		 getSession().createSQLQuery(INSERT_NEWS_TO_ORG).setLong(0, organizationId).setLong(1,newsId).executeUpdate();
	}
	/**
	 * 根据organizationId删除关系表中的记录
	 * @param organizationId
	 */
	
	public void deleteOrganizationId(Long organizationId){
		 getSession().createSQLQuery(DELETE_ORGANIZATION_ORGANIZATIONID).setLong(0, organizationId).executeUpdate();
		 
	}
	/**
	 * 根据NEWS_ID删除关系表中的记录
	 * @param NEWS_ID
	 */
	
	public void deleteByNewsId(Long nid){
		 getSession().createSQLQuery(DELETE_NEWS_NEWSID).setLong(0, nid).executeUpdate();
		 
	}

}
