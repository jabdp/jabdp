/*
 * @(#)UserConfigDao.java
 * 2014-5-29 下午00:00:27
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.userconfig.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.userconfig.entity.UserConfig;


/**
 *  用户信息配置数据库操作类
 *  @author tsd
 *  @version 1.0 2014-5-29 下午00:00:27
 *  @since jdk1.6 
 */
@Component
public class UserConfigDao extends HibernateDao<UserConfig, Long> {

}
