/*
 * @(#)LogAction.java
 * 2011-5-17 下午01:15:14
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.web;

import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.logmng.entity.Log;
import com.qyxx.platform.sysmng.logmng.service.LogManager;


/**
 *  日志管理Action
 *  @author YB
 *  @version 1.0
 *  @since 1.6 2011-5-17 下午01:15:14
 */
@Namespace("/sys/log")
@Results( { @Result(name = CrudActionSupport.RELOAD, location = "log.action", type = "redirect") })
public class LogAction extends CrudActionSupport<Log>{

	/**
	 * long
	 */
	private static final long serialVersionUID = 1L;
	private Log log;
	private Long id;
	private LogManager logManager;
	
	@Autowired
	public void setLogManager(LogManager logManager) {
		this.logManager = logManager;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Log getModel() {
		// TODO Auto-generated method stub
		return log;
	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	public String queryList() throws Exception {
		// TODO Auto-generated method stub
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(Struts2Utils.getRequest());
		//设置默认排序方式
		
		pager = logManager.searchLog(pager, filters);
		Struts2Utils.renderJson(pager);
		return null;
	}
	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		if (id != null) {
			log = logManager.getLog(id);
		} else {
			log = new Log();
		}
	}

}
