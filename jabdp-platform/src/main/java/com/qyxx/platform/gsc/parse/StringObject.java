/*
 * @(#)StringObject.java
 * 2011-7-21 下午11:32:12
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsc.parse;

import java.io.IOException;
import java.net.URI;
import javax.tools.SimpleJavaFileObject;

/**
 * 
 * @author gxj
 * @version 1.0
 * @since 1.6 2011-7-21 下午11:32:12
 */

public class StringObject extends SimpleJavaFileObject {

	private String contents = null;

	public StringObject(String className, String contents)
			throws Exception {
		//super(new URI(className), Kind.SOURCE);
		super(URI.create("string:///" + className.replace('.','/') + Kind.SOURCE.extension),  
	              Kind.SOURCE);  
		this.contents = contents;
	}

	public CharSequence getCharContent(boolean ignoreEncodingErrors)
																	throws IOException {
		return contents;
	}
}
