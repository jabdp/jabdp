package com.qyxx.platform.inf.adapter;

/**
 *  适配器接口
 *  
 *  @author gxj
 *  @version 1.0 2014-4-16 下午02:22:00
 *  @since jdk1.6
 */
public interface INeiAdapter {
	
	/**
	 * 初始化适配器
	 * @param adapterProperties
	 * @throws AdapterException
	 */
	public void init(AdapterProperties adapterProperties) throws AdapterException;
	
	/**
	 * 处理网元接口任务，返回任务对象
	 * @param task
	 * @return
	 * @throws AdapterException
	 */
	public NeiTask handle(NeiTask task) throws AdapterException;
	
	/**
	 * 处理网元接口任务，返回报告
	 * @param task
	 * @return
	 * @throws AdapterException
	 */
	public String handle(String... task) throws AdapterException;
	
	/**
	 * 判断是否有空闲的交互器
	 * @return true-空闲，false-忙
	 */
	public boolean isHaveIdleInteractant();
	
	/**
	 * 销毁网元接口适配器时,所做的操作,如:停止连接池等
	 * @throws AdapterException
	 */
	public void destroy() throws AdapterException;
	
	/**
	 * 返回适配器属性
	 */
	public AdapterProperties getAdapterProperties();
	
	/**
	 * 设置及更新适配器属性，主要为：工作状态，P-挂起状态，S-停止状态，R-运行中
	 * @param adapterStatus
	 */
	public void setAdapterProperties(AdapterProperties adapterProperties);

}
