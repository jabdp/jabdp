/*
 * @(#)ZtSmsAdapter.java
 * 2014-4-16 上午10:42:34
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.inf.sms;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.qyxx.platform.common.utils.httpclient.HttpClientUtils;
import com.qyxx.platform.inf.adapter.AdapterException;
import com.qyxx.platform.inf.adapter.AdapterProperties;
import com.qyxx.platform.inf.adapter.INeiAdapter;
import com.qyxx.platform.inf.adapter.NeiTask;


/**
 *  上海助通信息科技有限公司短信接口
 *  @author gxj
 *  @version 1.0 2014-4-16 上午10:42:34
 *  @since jdk1.6 
 */

public class ZtSmsAdapter implements INeiAdapter {

	private AdapterProperties ap;
	
	private Map<String, String> paramMap = new HashMap<String,String>();
	
	/**
	 * 初始化接口适配器
	 * @see com.qyxx.platform.inf.adapter.INeiAdapter#init(com.qyxx.platform.inf.adapter.AdapterProperties)
	 */
	@Override
	public void init(AdapterProperties adapterProperties)
															throws AdapterException {
		this.ap = adapterProperties;
		paramMap.put("username", this.ap.getUserName());
		paramMap.put("password", this.ap.getPassword());
		paramMap.put("dstime", "");
		paramMap.put("productid", StringUtils.defaultIfEmpty(this.ap.getParamValue("productid"),"61341"));
		paramMap.put("xh", "");
	}

	/**
	 * 处理指令
	 * @see com.qyxx.platform.inf.adapter.INeiAdapter#handle(com.qyxx.platform.inf.adapter.NeiTask)
	 */
	@Override
	public NeiTask handle(NeiTask task) throws AdapterException {
		paramMap.put("mobile", StringUtils.replace(task.getTargetCode(), ";", ","));//替换分号为逗号
		paramMap.put("content", task.getCmd());
		String result = HttpClientUtils.post(this.ap.getIpAddress(), paramMap);
		if(result.startsWith("1,")) {
			task.setIsReturnSuccess(true);
		}
		task.setReport(result);
		return task;
	}

	@Override
	public String handle(String... task) throws AdapterException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isHaveIdleInteractant() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void destroy() throws AdapterException {
		// TODO Auto-generated method stub
		this.ap = null;
		this.paramMap.clear();
		this.paramMap = null;
	}

	@Override
	public AdapterProperties getAdapterProperties() {
		// TODO Auto-generated method stub
		return ap;
	}

	@Override
	public void setAdapterProperties(
			AdapterProperties adapterProperties) {
		this.ap = adapterProperties;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		INeiAdapter nei = new ZtSmsAdapter();
		AdapterProperties testAp = new AdapterProperties();
		testAp.setIpAddress("http://www.ztsms.cn:8800/sendSms.do");
		testAp.setUserName("jwsoft");
		testAp.setPassword("Jw654321");
		testAp.setParamValue("productid", "61341");
		try {
			nei.init(testAp);
			NeiTask task = new NeiTask();
			task.setTargetCode("15305812560");
			task.setCmd("测试短信，仅供测试使用，谢谢");
			NeiTask result = nei.handle(task);
			System.out.println(result.getReport());
		} catch (AdapterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
