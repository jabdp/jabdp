package com.qyxx.platform.inf.adapter;

/**
 *  适配器常量类
 *  
 *  @author gxj
 *  @version 1.0 2014-4-16 下午02:21:51
 *  @since jdk1.6
 */
public class Constants {
	
	/**
	 * 连接类型-短连接
	 */
	public static final String SHORT_CONN = "SHORT";
	
	/**
	 * 连接类型-长连接
	 */
	public static final String LONG_CONN = "LONG";
	
	/**
	 * 交互器类
	 */
	public static final String INTERACTANT_CLASS = "interactant.class";
	
	/**
	 * 协议包类
	 */
	public static final String PACKET_CLASS = "packet.class";
	
	/**
	 * 协议包编码
	 */
	public static final String ENCODE = "packet.encode";
	
	/**
	 * 心跳消息
	 */
	public static final String HEART_MSG = "heart.msg";
	
	/**
	 * 心跳消息发送时间间隔
	 */
	public static final String HEART_MSG_INTERVAL = "heart.msg.interval";
	
	/**
	 * 登录消息
	 */
	public static final String LOGIN_MSG = "login.msg";
	
	/**
	 * 登录成功消息
	 */
	public static final String LOGIN_SUC_MSG = "login.suc.msg";
	
	/**
	 * 注销消息
	 */
	public static final String LOGOUT_MSG = "logout.msg";
	
	/**
	 * 注销成功消息
	 */
	public static final String LOGOUT_SUC_MSG = "logout.suc.msg";
	
    /**
     * 处理结果标记：1-成功；0-失败
     */
    public static final String RESULT_FLAG = "RESULT_FLAG";

    /**
     * 错误原因编号
     */
    public static final String ERROR_NO = "ERROR_NO";

    /**
     * 用户自定义返回属性序列
     */
    public static final String EXTATTR = "EXTATTR";
    
    /**
     * 数据库驱动名
     */
    public static final String DRIVER_NAME = "db.driver.name";
    
    /**
     * 接口存储过程KEY值
     */
    public static final String P_PROCEDUER_KEY = "P_PROCEDUER_KEY";

    /**
     * Oracle存储过程默认值
     */
    public static final String P_ORACLE_PROCEDUER_VALUE = "P_ISAP_INTERFACE(?,?,?,?,?)";
    
    /**
     * Sybase存储过程默认值
     */
    public static final String P_SYBASE_PROCEDUER_VALUE = "P_ISAP_INTERFACE(?,?,?,?)";

    /**
     * 1、取结果
     */
    public static final String P_COMMAND_ATT_FATCH = "1";

    /**
     * 0、执行指令发送操作
     */
    public static final String P_COMMAND_ATT_EXE = "0";
    
    /**
     * 数据库类型
     */
    public static final String DB_TYPE = "db.type";
    
    /**
     * 数据库类型-ORACLE
     */
    public static final String ORACLE = "ORACLE";
    
    /**
     * 数据库类型-SYBASE
     */
    public static final String SYBASE = "SYBASE";

}
