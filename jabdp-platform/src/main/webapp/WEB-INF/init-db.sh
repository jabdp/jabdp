#!/bin/sh
echo off

current_path=`dirname $0`
cd $current_path pwd

if ['$1'=='##']; then
  CLSPATH=${CURRENT_DIR};$2
else
  # 库文件所在的目录，相对于当前路径
  CURRENT_DIR=$PWD
  echo $CURRENT_DIR
  # java -version
  echo java环境变量
  java -version
  # 要启动的类名
  CLSNAME=com.qyxx.platform.common.utils.DbUtils
  # 设定CLSPATH
  CLSPATH="./lib/iPlatform-1.2.0.jar:./lib/ant-1.8.3.jar:./lib/mssql-jdbc-8.2.2.jre8.jar:./lib/mysql-connector-java-5.1.24.jar"
  echo $CLSPATH

  echo 启动数据库初始化程序---开始
  java -cp ${CLSPATH} ${CLSNAME} ${CURRENT_DIR}
  echo 启动数据库初始化程序---结束
fi
