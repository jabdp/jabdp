<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.jiawasoft.license.check.LicCheck" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="com.qyxx.platform.sysmng.accountmng.entity.User" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.apache.commons.lang.time.DateUtils" %>
<%@ page import="java.util.Date" %>
<%@ include file="/common/taglibs.jsp" %>
<% 
SystemParam systemParam = SpringContextHolder.getBean("systemParam");
String ctxPath = request.getContextPath();
String indexUrl = "index.action";
String systemIndexUrl = systemParam.getSystemIndexUrl();
if(StringUtils.isNotBlank(systemIndexUrl)) {
	indexUrl = systemIndexUrl; 
}
User user = (User)session.getAttribute("USER");
if(null!=user) {
	String roleIndexUrl = user.getIndexUrl();
	if(StringUtils.isNotBlank(roleIndexUrl)) {
		indexUrl = roleIndexUrl;
	}
}
String rdUrl = ctxPath + "/" + indexUrl;
String ci = LicCheck.getCheckInfo();
String tmp = StringUtils.substringAfter(ci, "授权日期：");
String authDate = StringUtils.substringBefore(tmp, "\n");
long days = 10L;
String tmp1 = StringUtils.substringAfter(ci, "有效期：");
String validDay = StringUtils.substringBefore(tmp1, "\n");
if(StringUtils.contains(validDay, "天")) {
	validDay = StringUtils.substringBefore(validDay, " 天");
	Date ad = DateUtils.parseDate(authDate, new String[]{"yyyy-MM-dd HH:mm:ss"});
	Long vd = new Long(validDay);
	days = (ad.getTime()+vd*24*3600*1000L-System.currentTimeMillis())/(24*3600*1000L);
} else { //无限制
	days = 100L;
}
if(days > 10) {
	response.sendRedirect(rdUrl);
} else {
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>首页跳转中...</title>
	<%@ include file="/common/meta-min.jsp" %>
</head>
<body>
	<div>
		授权日期剩余<%=days%>天，请联系佳哇软件公司！10秒后会自动跳转至首页...
	</div>
	<script type="text/javascript">
	setTimeout(function() {window.location.replace("<%=rdUrl%>");}, 10000);
	</script>
</body>
</html>
<%
}
%>