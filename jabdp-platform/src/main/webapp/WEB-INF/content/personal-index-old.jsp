<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="com.qyxx.platform.sysmng.accountmng.entity.User" %>
<%
SystemParam systemParam = SpringContextHolder.getBean("systemParam");
User user = (User)session.getAttribute("USER");
if(null==user) {
	response.sendRedirect(request.getContextPath() + "/login.action");
}
%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><%=systemParam.getSystemTitle()%></title>
<%@ include file="/common/meta-min.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.3.3/themes/metro/easyui.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.3.3/themes/icon.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-add.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.3.3/themes/portal.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/slides-JS/jquery.slides.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/jquery.marquee/css/jquery.marquee.css"/>
<script type="text/javascript" src="${ctx}/js/easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.3.3/locale/easyui-lang-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.3.3/jquery.portal.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/locale/data-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/common.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/json2.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/gscommon.js"></script>
<script type="text/javascript" src="${ctx}/js/slides-JS/jquery.slides.min.js"></script>

<script type="text/javascript" src="${ctx}/js/jquery.marquee/lib/MSClass.js"></script>
<script type="text/javascript">
	var _userList={};
	var _leve1List = [];
	$(function(){		
		 _userList=findAllUser();
		getDesktopLeve1List();
	});
	
	function doLogin() {
		window.location.replace("${ctx}/login.action?timeout=true");
	}
	
	function addBt(){
		var bts=[];
		for(var i=0,len=_leve1List.length;i<len;i++){
		var b_name = _leve1List[i].title;
		bts.push('<div onclick="reset(');
		bts.push(_leve1List[i].id);
		bts.push(');">');
		bts.push(b_name);
		bts.push('</div>');
		}
		$("#mm1").append(bts.join(""));
		$("#mb1").menubutton({
			 menu: '#mm1' 
		});
	}
	function reset(pId){
		
	/* 	var p_arr = $('#pp').portal("getPanels");
		for(var i=0,len=p_arr.length;i<len;i++){
			$('#pp').portal("remove",p_arr[i]);
		} */
		//$("#cen_pp").html("");
 		$("#cen_pp").html('<div id="pp" style="position:relative;" ></div>');
 		var divs=[];
 		var lh = "";
 		for(var i=0,len=_leve1List.length;i<len;i++){
 			if(_leve1List[i].id==pId){
				var layout = _leve1List[i].layoutType;
				var columns = layout.split(';');
				lh = columns.length;
				for(var j=0;j<lh;j++){
					divs.push('<div style="width:');
					divs.push(columns[j]);
					divs.push('"></div>');
				}
				break;
 			}
 		}
		$("#pp").html(divs.join(""));
		$('#pp').portal({
			fit:true,
			border:false
		});
		getDesktopLeve2List(pId,lh);	
	}
	function getDesktopLeve1List(){
		var options = {
				url : '${ctx}/sys/desktop/desktop!personalQueryLevel1Desktop.action',
				success : function(data) {
					if (data.msg) {
					$("#cen_pp").append('<div id="pp" style="position:relative;" ></div>');
					var list =	data.msg;
					_leve1List =data.msg;					
					var did = list[0].id;					
					var layout = list[0].layoutType;
					var columns = layout.split(';');
					var len = columns.length;
					var divs=[];
			 		for(var i=0;i<len;i++){
						divs.push('<div style="width:');
						divs.push(columns[i]);
						divs.push('"></div>');    	
					} 
					$("#pp").html(divs.join(""));
					$('#pp').portal({
						fit:true,
						border:false
					});
					addBt();
					getDesktopLeve2List(did,len);
					}
				}
			};
			fnFormAjaxWithJson(options,true);
	}
 	function getDesktopLeve2List(parentId,length){
  		var options = {
  				url:'${ctx}/sys/desktop/desktop!personalQueryLevel2Desktop.action',
  				 data : {
  	                 "parentId" :parentId
  	             },
  				success:function(data) {
  					if(data.msg) {
  						var panels=panelData(data.msg);
  						initPanels(panels,length);
  						
  					}
  				}
  		};
  		fnFormAjaxWithJson(options,true);			
  	}
	function initPanels(panels,length){
		addPanels(panels,length);
		disablePanelsDragging();
		$('#pp').portal("resize");
	}
	
	function disablePanelsDragging() {
		var p_arr = $('#pp').portal("getPanels");
		for(var i=0,len=p_arr.length;i<len;i++){
			$('#pp').portal("disableDragging",p_arr[i]);
		}
	}
	function addPanels(panels,len){	
			for(var j=0; j<panels.length; j++){
			 	var p = $('<div/>').attr('id',panels[j].id).appendTo('body');
				p.panel(panels[j]);	
				var ci = j%len;
			 	$('#pp').portal('add',{
					panel:p,
					columnIndex:ci
				}); 
			}	 
	}
 	function panelData(data){
		 var panels=[];
         var url="";
		for(var i=0;i<data.length;i++){
			var closeId=data[i].id;
			var id = data[i].id;
			var pid= data[i].parentId;
			var type=data[i].type;
			switch(type){
			case "toDoList":url="${ctx}/sys/portal/task-list.action?id=";break;
			case "textList": url="${ctx}/sys/portal/textList.action?id=";break;
			case "picTextList":url="${ctx}/sys/portal/picTextList.action?id=";break;
			case "picList":url="${ctx}/sys/portal/picture-slides.action?id=";break;
			case "datagrid": url="${ctx}/gs/process.action?entityName=contracts.MainTable";break;
			case "statistic2": url="${ctx}/sys/portal/statistic2.action?id=";break;
			case "statistic1": url="${ctx}/sys/portal/statistic1.action?id=";break;
			case "weather": url="${ctx}/sys/portal/weather.action?id=";break;
			case "datetime": url="${ctx}/sys/portal/dateTime.action?id=";break; 
			case "iframe":url="${ctx}/sys/portal/ifr.action?id=";break; 
			case "textSlides":url="${ctx}/sys/portal/text-slides.action?id=";break;
			}
			var temp=$.extend({"title":data[i].title,"height":data[i].height,"href":url+id,"moduleUrl":data[i].moduleUrl},{"id":pid,"closeId":closeId,"iconCls":"icon-default_Document"});
			if(data[i].moduleUrl){
				temp["tools"] = [{
						iconCls:'icon-more',
						handler:doOpenMorePage
				}];
			}
			panels.push(temp);
		}
		return panels;		
	}
 	function doOpenMorePage() {
 		var pa = $(this).parent().parent().next();
		var ps = pa.attr("id");
		var opt = $("#"+ps).panel("options");
		var title = opt.title;
		var moduleUrl = "${ctx}/" + opt.moduleUrl;
		openImageNews(moduleUrl, title);
 	}
	function initContent(type,id,uId) {
		 var options = {
					url:'${ctx}/sys/desktop/desktop!queryDesktopSource.action',
					 data : {
		                 "id" :id
		             },
					success:function(data) {
						    var obj=data.msg;
							var hg = obj.height-50;
							var wid = $("#"+uId).innerWidth();
						    var showDisplayNum=obj.displayNum;
						    var showFontNum=obj.fontNum;
						    var opts=obj.options;//{'createUser':true,'createTime':true}
						    var jsonData=obj.list;
						    var len=jsonData.length;
						    var content=[];
						    if(showDisplayNum<=len){
						    	len=showDisplayNum;
						    }
						    for(var i=0;i<len;i++){
						    	//创建人
						    	 var userName="";
						    	 var uObj=_userList[jsonData[i].createUser];
	                             if(uObj){
	                                 userName=uObj.realName;
	                             }else{
	                                 userNeme=jsonData[i].createUser;
	                             }
	                             //标题字数控制
	                             var title=jsonData[i].title;
	                             var showTitle="";
	                             if(showFontNum<=title.length){
	                            	 showTitle=title.substr(0,showFontNum)+"...";
	                             }else{
	                            	showTitle=title; 
	                             }
	                             //是否显示创建者, 创建时间
	                             var createUser="";
	                             var createTime="";
	                             if(opts){
	                            	 var opt=$.parseJSON(opts);
	                                 if(opt.createUser){
	                                	 createUser="["+userName+"]";
	                                 }
	                                 if(opt.createTime){
	                                	 createTime="("+jsonData[i].createTime+")";
	                                 }
	                             }else{
	                            	 var ct = jsonData[i].createTime;
	                            	 ct = ct.substr(0,10);
	                            	 createTime="("+ct+")";
	                             }
	                               if(type=="textList"){
	                            	   var str ="<li><a href='javascript:void(0);' onclick='openImageNews(\""+"${ctx}/"+obj.dataUrl+jsonData[i].id+"\",\""+jsonData[i].title+"\")' >"+showTitle+"<div class='create-text' style='float:right;'>"+createTime+createUser+"</div></a></li>";
									   content.push(str);
	                               }else if(type=="picList"){
	                            	   content.push("<div  style='z-index: 3; display: list-item;'><a  target='_blank' href='"+"${ctx}/"+obj.dataUrl+jsonData[i].id+"'><img  title='"+showTitle+"' src='"+obj.systemPath+jsonData[i].src+"' style='border:0;width:100%;height:" + hg + "px;'/><span>"+showTitle+"</span></a></div>");	                            	   
	                               }else if(type=="textSlides"){
	                            	   var str ="<li><a href='javascript:void(0);' onclick='openImageNews(\""+"${ctx}/"+obj.dataUrl+jsonData[i].id+"\",\""+jsonData[i].title+"\")' >"+showTitle+"</a></li>";
									   content.push(str);
									}
						    }
						    if(type=="textList"){
						    	//if(obj.moduleUrl){
						    	//   content.push("<div style='text-align:right'><a href='javascript:void(0);' onclick='openImageNews(\""+"${ctx}/"+obj.moduleUrl+"\",\""+obj.title+"\")' ><s:text name="system.sysmng.info.more.title"/></a></div>");
						    	//}
						    	   var res=content.join("");
								   $("#"+uId).append(res); 			   
						    }else if(type=="picList"){						     					    
						    		var res=content.join("");
						    		$("#"+uId).append(res);
									 if(len==1){
										 $("#"+uId).slidesjs({
											 width:wid,
										 	 height: hg,
								        	 navigation: false										 
										 });
								    }else if(len>1){
								    	$("#"+uId).slidesjs({
											 width:wid,
											 height: hg,
								        	 navigation: false,						     
	 							        	 play: {
								        		 auto: true,
								                 interval: 4000,
								                 pauseOnHover: true
								             } 
										 });
								    }
						    }else if(type=="iframe"){
								   var url=obj.dataUrl;
								   $("#"+uId).attr("src",url);
						    }else if(type=="textSlides"){
								var res=content.join("");
								var ul = uId+"_ul";
								$("#"+ul).append(res); 	
								new Marquee([uId,ul],2,1,wid,40,30,0,0);
							}  
					 }
			};
			fnFormAjaxWithJson(options,true); 		 
	}
	function switchToManageSystem(){
		window.location.replace("${ctx}/index.action");
	}
	function doLogout() {
		$.messager.confirm('<s:text name="system.logout.title"/>', '<s:text name="system.logout.confirm.title"/>', function(r){
			if (r){
				window.location.replace("${ctx}/logout.action");
			}
		});
	}
	function openImageNews(url,title){
		window.open(url);
	}
</script>
<style type="text/css">
<%--.desktopDiv .panel-header {
		border-width:0px;
		background:none;
		background-color:rgb(198, 239, 250);
		filter:none;
	}
	
	.desktopDiv .panel-body {
		border-width:0px;
	}
--%>
#north_pp {background:url(${imgPath}/layer.jpg) no-repeat top}
#cen_pp {background:url(${imgPath}/layer.jpg) no-repeat bottom;}
.panel-tool a {width:45px;}
.panel-title {color:#000;}
ul.text-list li {list-style-type:circle;}
ul.text-list li a {color:#000;display:block;padding:3px 5px 0px 0px;margin-top:3px;height:20px;}
ul.text-list li div.create-text {color:#999;}
ul.text-list li a:hover{background-color:#fffbce;color:#ff6012;text-decoration:none}
</style> 
</head>
<body class="easyui-layout" >
	<div id="north_pp" data-options="region:'north'"  style="height:60px;overflow:hidden;" border="false">
			<div border="false">
				<div style="float:left;padding-left:5px;">
					<div style="float:left;">
						<div><img src="${imgPath}/<%=systemParam.getSystemLogo()%>" style="height:40px;"/></div>
						<div style="padding-left:20px;padding-top:0px;"><span class="index_top_system_title"><%=systemParam.getSystemTitle()%></span></div>
					</div>
					<div style="float:left;padding-left:20px;padding-top:20px;font-size:16px;" >
						<a href="javascript:void(0)" onclick="switchToManageSystem()" id="bt_homepage"><span style="vertical-align:middle;"><img src="${imgPath}/index.png" style="border:0;"/></span>&nbsp;<s:text name="切换到管理平台"/></a>
					</div>
					<div style="clear:both;"></div>
				</div>
				<div style="float:left;">
						<div style="margin-left:80px;padding-top:24px;font-size:14px;">
							<s:text name="system.index.welcome.title">
								<s:param><s:property value="#session.USER.realName" /></s:param>
								<s:param><s:property value="#session.USER.organizationName" /></s:param>
							</s:text>
						</div>
				</div>
				<div style="float:right;padding-top:3px;padding-right:10px;" class="index_top_button">
					<div style="text-align:center;">
						<a href="javascript:void(0)" onclick="doLogout()" id="bt_logout"><span style="vertical-align:middle;"><img src="${imgPath}/o.png" /></span>&nbsp;<s:text name="system.logout.title"/></a>
					</div>
					<div style="text-align:right;padding-top:5px;">
						<a href="javascript:void(0)" id="mb1"  menu="#mm1">选择门户</a>
						<div id="mm1" style="width:150px;"></div>
					</div>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
	</div>
	<div class="desktopDiv" data-options="region:'center'" border="false">
	  <div class="easyui-layout" data-options="fit:true">
		<div id="cen_pp" data-options="region:'center'" border="false"> 
		</div>
	  </div>
	</div>
</body>
</html>

