<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@ page import="org.springframework.security.web.WebAttributes" %>
<%@ page import="org.springframework.security.web.authentication.session.SessionAuthenticationException" %>
<%@ page import="org.springframework.security.core.AuthenticationException" %>
<%@ page import="com.qyxx.platform.sysmng.utils.Constants" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="com.opensymphony.xwork2.interceptor.I18nInterceptor" %>
<%@ page import="com.opensymphony.xwork2.util.LocalizedTextUtil" %>
<%@ page import="java.util.Locale" %>
<%
	SystemParam systemParam = SpringContextHolder.getBean("systemParam");
	/*String systemLoginUrl = systemParam.getSystemLoginUrl();
	if(StringUtils.isNotBlank(systemLoginUrl)) {
		response.sendRedirect(request.getContextPath() + "/" + systemLoginUrl);
	}*/
	String username = "";
	Cookie[] cs = request.getCookies();
	if(null!=cs) {
		for(Cookie ck : cs) {
			if(Constants.USER_NAME_COOKIE.equals(ck.getName())) {
				username = java.net.URLDecoder.decode(ck.getValue(), Constants.DEFAULT_ENCODE);
				break;
			}
		}
	}
	String lang = request.getParameter("lang");
	String langText = "简体中文";
	if(StringUtils.isNotBlank(lang)) {
			String locale = "zh_CN";
			switch(lang) {
				case "zh-TW":
					locale = "zh_TW";
					langText = "繁體中文";
					break;
				case "en":
					locale = "en";
					langText = "English";
					break;
				case "zh-CN":
					locale = "zh_CN";
					break;
		}
		session.setAttribute(Constants.LOCALE_SESSION_ID, locale);
		session.setAttribute("LANG", lang);
		Locale local = LocalizedTextUtil.localeFromString(locale, null);
		session.setAttribute(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, local);
	}	
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
    <title><%=systemParam.getSystemTitle()%></title>
    <script>
    var type=navigator.appName,lang="zh-CN";
    if (type=="Netscape"){
    	lang = navigator.languages?navigator.languages[0]:navigator.language;
    } else{
    	lang = navigator.userLanguage;
    }
    //取得浏览器语言的前两个字母
    <%--lang = lang.substr(0,2);--%>
    <%--if(lang != "zh" && lang != "<%=lang%>") {--%>
    <%--	window.location.href="${ctx}/login-v2.action?lang=en";	--%>
    <%--}--%>
    </script>
    <link rel="stylesheet" href="${ctx}/js/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${ctx}/js/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${ctx}/js/toastr/toastr.min.css">
    <link rel="stylesheet" href="${ctx}/js/index/login.min.css">
	<style type="text/css">
	.user-login-msg {color:#333;}
	</style>
	<style>
			.pwdcheck {
				/*margin:50px auto;
				width:700px;
				height:50px;*/
			}
			.pwdcheck p.pwdInput input.pwd{
				width:150px;
				height:20px;
			}
			.pwdcheck p.pwdInput label {
				font-size:12px;
			}
			.pwdcheck p.pwdInput label.tips{
				color:red;
			}
			.pwdcheck p.pwdInput label span {
				color:#f00;
				padding:0 5px;
			}
			.pwdcheck p.pwdColor {
				width:150px;
				height:5px;
				margin-top:6px;
				margin-left:6px;
			}
			.pwdcheck p.pwdColor span {
				display:inline-block;
				width:40px;
				height:4px;
				float:left;
				margin-right:10px;
				background:#ccc;
			}
			
			.pwdcheck p.pwdText {
				margin-left:6px;
			}
			.pwdcheck p.pwdText span{
				margin-right:20px;
				margin-left:10px;
				font-size:12px;
				color:#ddd;
			}

			/* 密码强度改变的样式 */

			.pwdcheck p.pwdColor span.co1 {
				background:#f00;
			}
			.pwdcheck p.pwdColor span.co2 {
				background:#00ff66;
			}

			.pwdcheck p.pwdColor span.co3 {
				background:#0033cc;
			}	
			.form-horizontal label {color:#000;}	
</style>
</head>
<body class="signin">
    <div class="signinpanel">
        <div class="row">
            <div class="col-sm-7">
                <div class="signin-info">
                    <div class="logopanel m-b">
                        <h1><%=systemParam.getSystemTitle()%></h1>
                    </div>
                    <div class="m-b"></div>
                    <h4><s:text name="system.login.welcome.title"/> <strong><%=systemParam.getSystemTitle()%></strong></h4>
                    <ul class="m-b">
                        <li><i class="fa m-r-xs"></i> </li>
                        <li><i class="fa m-r-xs"></i> </li>
                        <li><i class="fa m-r-xs"></i> </li>
						<li><i class="fa m-r-xs"></i> </li>
                        <li><i class="fa m-r-xs"></i> </li>
                        <li><i class="fa m-r-xs"></i> </li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> <s:text name="system.login.browser.title"/></li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> <s:text name="system.login.resolution.title"/></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-5">
                <form method="post" action="${ctx}/j_spring_security_check" id="loginForm">
                    <h4 class="no-margins" style="text-align:center;"><img src="${imgPath}/<%=systemParam.getSystemLogo()%>" style="height:40px;vertical-align:top;"/></h4>
                    <p class="m-t-md user_login_msg" style="color:red;">&nbsp;
						<s:if test="#parameters.error!=null">
							<span><s:text name="system.login.error.userpass.title"/></span>&nbsp;&nbsp;
						</s:if>
						<s:if test="#parameters.timeout!=null">
							<span><s:text name="system.login.error.timeout.title"/></span>&nbsp;&nbsp;
						</s:if>
					</p>
                    <input type="text" class="form-control uname" placeholder="<s:text name="system.login.loginname.title"/>" id='j_username' name='j_username'
							<s:if test="#parameters.error!=null">
								value='<%=session.getAttribute("SPRING_SECURITY_LAST_USERNAME")%>'</s:if>
							<s:else>value='<%=username%>'</s:else> />
                    <input type="password" class="form-control pword m-b" placeholder="<s:text name="system.login.password.title"/>" id='j_password' name='j_password'/>
                    <button class="btn btn-success btn-block"><s:text name="system.login.login.title"/></button>
                </form>
            </div>
        </div>
        <div class="signup-footer">
            <div class="pull-left">
                <s:text name="system.index.copyright.title"><s:param>2018</s:param></s:text> <%=systemParam.getSystemPowerBy()%>
            </div>
        </div>
    </div>
    <div id="pwdModifyDiv" style="margin:10px;display:none;overflow:hidden;">
		<form action="" name="passWordForm" id="passWordForm" method="post" class="form-horizontal" role="form">
			<div class="form-group">
			    <label for="oldPass" class="col-sm-4 control-label"><s:text name="system.login.oldpassword.title" /><font color="red">*</font></label>
			    <div class="col-sm-8">
			      <input type="password" name="oldPass" id="oldPass" class="form-control" placeholder="请输入旧密码">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="newPass" class="col-sm-4 control-label"><s:text name="system.login.newpassword.title" /><font color="red">*</font></label>
			    <div class="col-sm-8">
			      <div class="pwdcheck">
												<p class="pwdInput">
												<input type="password" name="newPass" id="newPass" class="form-control" placeholder="请输入新密码">
												<label class="tips">由字母（区分大小写）、数字、符号组成6-18位</label>
												</p>
												<p class="pwdColor">
													<span class="c1"></span>
													<span class="c2"></span>
													<span class="c3"></span>
												</p>
												<p class="pwdText">
													<span>弱</span>
													<span>中</span>
													<span>强</span>
												</p>
											</div>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="rePass" class="col-sm-4 control-label"><s:text name="system.login.confirmpassword.title" /><font color="red">*</font></label>
			    <div class="col-sm-8">
			      <input type="password" name="rePass" id="rePass" class="form-control" placeholder="请确认新密码">
			    </div>
			  </div>
					<%-- 	<table align="center" border="0" cellpadding="0" cellspacing="1" class="table_form">
								  <tbody>								 				
									<tr>
										<th><label for="oldPass"><s:text name="system.login.oldpassword.title" /></label></th>
										<td align="left"><input  class="easyui-validatebox Itext" required="true" validtype="length[6,18]" invalidMessage="<s:text name="system.login.passwordLength.title"/>"/>&nbsp;<font color="red">*</font></td>
									  </tr>
									  <tr>
										<th valign="top"><label for="newPass"><s:text name="system.login.newpassword.title"/></label></th>
										<td align="left">
											<div class="pwdcheck">
												<p class="pwdInput">
												<input type="password" class="pwd easyui-validatebox Itext" id="newPass" name="newPass" required="true" maxlength="18">&nbsp;<font color="red">*</font>
												<label class="tips">由字母（区分大小写）、数字、符号组成6-18位</label>
												</p>
												<p class="pwdColor">
													<span class="c1"></span>
													<span class="c2"></span>
													<span class="c3"></span>
												</p>
												<p class="pwdText">
													<span>弱</span>
													<span>中</span>
													<span>强</span>
												</p>
											</div>
											<!-- <input type="password" name="newPass" id="newPass" class="easyui-validatebox Itext" required="true" validtype="length[6,18]" invalidMessage="<s:text name="system.login.passwordLength.title"/>"/>&nbsp;<font color="red">*</font> -->
										</td>
									  </tr>
									  <tr>
										<th><label for="confPass"><s:text name="system.login.confirmpassword.title" /></label></th>
										<td align="left"><input type="password" name="rePass" id="rePass" class="easyui-validatebox Itext" required="true" validType="equalTo['#newPass']" invalidMessage="<s:text name="system.login.passwordError.title"/>"/>&nbsp;<font color="red">*</font></td>
									  </tr>
								</tbody>
							   </table>--%>
		</form> 
	</div>
	<script src="${ctx}/js/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="${ctx}/js/layer/layer.js" type="text/javascript"></script>
	<script src="${ctx}/js/toastr/toastr.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/common/scripts/common-min.js"></script>
	<script type="text/javascript">
		<s:if test="#parameters.timeout!=null">
			if(window.top!=window) {
				window.top.document.location.replace("${ctx}/login.action?timeout=true");
			}
		</s:if>
		function doCheckUserIsLogin(userName, userPass) {
			var flag = false;
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!checkUserIsLogin.action',
					data : {
						"userName" : userName,
						"userPass" : userPass
					},
					success : function(data) {
						var fm = data.msg;
						if(fm) {
							switch(fm.flag) {
								case "0":
									var cMsg = "<div class='user-login-msg'>" + fm.msg + "。是否确认将其强制下线？</div>";
									layer.confirm('该用户已经登录', {
										  btn: ['确认','取消'], //按钮
										  title:"确认信息",
										  content:cMsg
										}, function(){
											doRemoveOnlineUser(userName);
										}, function(){
											//$("#j_username").val("");
											$("#j_password").val("");
											$("#j_username").focus();
									});
									break;
								case "2":
									$(".user_login_msg").text(fm.msg);
									break;
								default:
									//checkPass(userPass)<2 && confirm("亲，你设置的密码过于简单，未达安全等级，请登录系统后马上修改密码。从下周开始登录时将强制修改简单密码！");
									if(checkPass(userPass)<2) {
										layer.open({
											  area: ['500px', '360px'],
											  type: 1,
											  btn: ['保存'],
											  title: '密码设置过于简单，请修改密码后登录', //不显示标题
											  content: $('#pwdModifyDiv'), //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响
											  yes: function(index, layero){
												doUpdatePass(fm.msg, function() {
											    	layer.close(index);
											    });
											  }
										});
									} else {
										flag = true;
									}
							}
						}
					}
			};
			fnFormAjaxWithJson(options);
			return flag;
		}

		//密码强弱判断函数
		function checkPass(pass){
			var txt = $.trim(pass);//输入框内容 trim处理两端空格
			var num = /\d/.test(txt);//匹配数字
			var small = /[a-z]/.test(txt);//匹配小写字母
			var big = /[A-Z]/.test(txt);//匹配大写字母
			var corps = /\W/.test(txt);//特殊符号
			var val = num + small + big + corps; //四个组合
			return val;
		}
		
		function doRemoveOnlineUser(userName) {
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!removeLoginUser.action',
					data : {
						"userName" : userName
					},
					success : function(data) {
						$("#loginForm").unbind("submit");
						$("#loginForm").submit();
					}
			};
			fnFormAjaxWithJson(options);
		}
		
		function doUpdatePass(userId, callBack){
			if(!$("#oldPass").val()) {
				layer.msg("旧密码必须设置！", {time: 1500});
				return false;
			}
			if(!$("#newPass").val()) {
				layer.msg("新密码必须设置！", {time: 1500});
				return false;
			}
			if(!$("#rePass").val()) {
				layer.msg("确认密码必须设置！", {time: 1500});
				return false;
			}
			if($("#rePass").val() != $("#newPass").val()) {
				layer.msg("新密码与确认密码必须一致！", {time: 1500});
				return false;
			}
			if(checkpwd($("#newPass"))<2) {
				layer.msg("密码必须设置为中级等级及以上！", {time: 1500});
				return false;
			}
			var options = {
					async:false,
					url:'${ctx}/sys/account/user!updatePassWord.action',  
					data: {
						userId: userId,
						newPass: $("#newPass").val(),
						oldPass: $("#oldPass").val()
					},
				    success:function(data){  
				    	if(callBack) callBack();
				    	layer.msg('<s:text name="system.javascript.alertinfo.titleInfo"/>:密码已修改成功，请重新登录！', {time:1500});
				    	setTimeout(function(){
				  			window.top.location.replace("${ctx}/login-v2.action");
				  		},1000);
				    	
				    }  
				};
			fnFormAjaxWithJson(options);	
			return true;
		}
		
		$("#newPass").keyup(function(){
			/*var txt=$(this).val(); //获取密码框内容
			var len=txt.length; //获取内容长度
			if(txt=='' || len<6){
				$("p.pwdInput label").show();
				$("p.pwdInput label").addClass("tips");
			}else {
				$("p.pwdInput label").hide();
			}*/
			checkpwd($(this));
		});

		//全部都是灰色的
		function primary(){
			$("p.pwdColor span").removeClass("co1,co2,co3");
		}
		
		//密码强度为弱的时候
		function weak(){
			$("span.c1").addClass("co1");
			$("span.c2").removeClass("co2");
			$("span.c3").removeClass("co3");
		}
		//密码强度为中等的时候
		function middle(){
			$("span.c1").addClass("co1");
			$("span.c2").addClass("co2");
			$("span.c3").removeClass("co3");
		}
		
		//密码强度为强的时候
		function strong(){
			$("span.c1").addClass("co1");
			$("span.c2").addClass("co2");	
			$("span.c3").addClass("co3");
		}

		/**判断密码的强弱规则
		1、如果是单一的字符（全是数字 或 字母 ）长度小于 6  弱
		2、如果是两两混合 (数字+字母（大小） 或 数字+特殊字符  或 特殊字符+字母  长度大于 8  中)
		3、如果是三者组合 (数字 +大写字母+小写字母 或 数字+字母+特殊字符 长度>8  强）)
		**/
		
		//密码强弱判断函数
		function checkpwd(obj){
			var txt = $.trim(obj.val());//输入框内容 trim处理两端空格
			var len = txt.length;
			var val = checkPass(txt); //四个组合

			if(len<1){
				primary();
			}else if(len<6){
				weak();
			}else if(len>6 && len<=8){
				if(val==1){
					weak();
				}else if(val==2){
					middle();
				}
			}else if(len>8){
				if(val==1){
					weak();
				}else if(val==2){
					middle();
				}else if(val==3){
					strong();
				}
			}
			return val;
		}
		
		$(document).ready(function() {
			<%
				AuthenticationException authException = (AuthenticationException)session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
				if(authException!=null && authException instanceof SessionAuthenticationException) {
					String errorMsg = authException.getMessage();
			%>
					$(".user_login_msg").text("<%=errorMsg%>");
			<%
				}
			%>
			
			$("#loginForm").submit(function() {
				var userName = $("#j_username").val();
				if(!userName) {
					$(".user_login_msg").text("<s:text name="system.login.usernamecheck.title"/>");
					$("#j_username").focus();
					return false;
				}
				var pass = $("#j_password").val();
				if(!pass) {
					$(".user_login_msg").text("<s:text name="system.login.passwordcheck.title"/>");
					$("#j_password").focus();
					return false;
				}
				return doCheckUserIsLogin(userName, pass);
			});

			$("#j_username").focus();
		});
	</script>
</body>
</html>