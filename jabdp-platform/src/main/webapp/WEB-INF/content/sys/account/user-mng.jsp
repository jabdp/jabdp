<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
</head>
<body class="easyui-layout">
		<div region="west" split="true" title="<s:text name="system.index.menu.title"/>" style="width:280px;overflow:hidden;">
					<iframe scrolling="auto" frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
		</div>
		<div region="center" title="<s:text name="system.index.workspace.title"/>" style="overflow:hidden;">
					<iframe scrolling="auto" frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
		</div>
</body>
</html>