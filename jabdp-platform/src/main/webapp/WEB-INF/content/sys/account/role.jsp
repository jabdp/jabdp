<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.sysmng.user.manager.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
	<script type="text/javascript">
	var _isSupportBigData_ = false;//支持大数据
	var flag = null;
	$.extend($.fn.validatebox.defaults.rules, {//自定义检验角色名是否存在
		validateName : {
			validator : function(value, param) {
				var id = $("#id").val();
				var options = {
					url : '${ctx}/sys/account/role!validateName.action',
					data : {
						"id" :id,
						"roleName" : value
					},
					async:false,
					success : function(data) {
						flag = data.msg;
					}
				};
				fnFormAjaxWithJson(options,true);					
				return flag;
			},
			message : '名字已存在,请重新输入!'
		}
	});
	var setting = {
			check: {
				enable: true
				
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};
	var settingAuthority = {
			check : {
				enable : true,
				chkboxType: {"Y": "ps", "N": ""}
			},
			data : {
				simpleData : {
					enable : true
				}
			},
			callback : {
				onClick : onAuthorityClick
			}
	};
	var settingType = {
			data : {
				simpleData : {
					enable : true,
					pIdKey: "pid"
				}
			},
			check: {
				enable: true
				
			}
		};
	
	//单击所有可见字段可编辑不可修订权限
	function onAuthorityClick(event, treeId, treeNode) {
		
	}
	
	//初始化权限树
		function initMenu(isInit) {
			if(isInit) {
				var options = {
						url:'${ctx}/sys/account/authority!queryList.action',
						success:function(data) {
							if(data.msg) {
							$.fn.zTree.init($("#authorityTree"),settingAuthority,data.msg);
							}
						}
				};	
				fnFormAjaxWithJson(options, true);
			} else {
				var treeObj = $.fn.zTree.getZTreeObj("authorityTree");
				treeObj.checkAllNodes(false);
			}
		}
		//初始化桌面树
		function initDesktopMenu() {
			var options = {
					url:'${ctx}/sys/desktop/desktop!desktopList.action',
					success:function(data) {
						if(data.msg) {
						$.fn.zTree.init($("#desktopTree"),setting,data.msg);
						}
					}
			};
			fnFormAjaxWithJson(options, true);		
		}
		//初始化系统类别树
		function initSystemType(){
			var options = {
					url:'${ctx}/sys/common/common-type!getTypeList.action',
					success:function(data) {
						if(data.msg) {
						$.fn.zTree.init($("#systemTypeTree"),settingType,data.msg);
						}
					}
			};	
			fnFormAjaxWithJson(options, true);
		}
		
		//初始化当前组织下的用户树
		function initUserZtree(org_id) {
			var options = {
				url : '${ctx}/sys/account/authority!userList.action',
				data : {
					"orgId" : 1 //org_id
				},
				success : function(data) {
					if (data.msg) {
						$.fn.zTree.init($("#seeUserTree"), setting, data.msg);
					}
				}
			};

			fnFormAjaxWithJson(options, true);
		}
		
		//设置已选中的系统类型
		  function getSystemType(roleId,status) {			
			  var options = {
						url:'${ctx}/sys/common/common-type!getTypeList.action',
						data : {
							"id" : roleId
						},
						success:function(data) {
							if(data.msg) {
								$.fn.zTree.init($("#systemTypeTree"),settingType,data.msg);
								if(status=="view"){
								var zTree = $.fn.zTree.getZTreeObj("systemTypeTree");
								zTree.setting.callback.beforeCheck=zTreeBeforeCheck;
									}
								}
						}
				};
			  fnFormAjaxWithJson(options, true);
			} 
		//设置已选中的桌面
		  function getDesktopTree(roleId,status) {			
			  var options = {
						url:'${ctx}/sys/desktop/desktop!desktopList.action',
						data : {
							"roleId" : roleId
						},
						success:function(data) {
							if(data.msg) {
								$.fn.zTree.init($("#desktopTree"),setting,data.msg);
								if(status=="view"){
								var zTree = $.fn.zTree.getZTreeObj("desktopTree");
								zTree.setting.callback.beforeCheck=zTreeBeforeCheck;
									}
								}
						}
				};
			  fnFormAjaxWithJson(options, true);
			}  
		//设置已选中的权限	
		function getAuthorityTree(roleId,status){
			initMenu(false);
			var options = {
					//url:'${ctx}/sys/account/authority!queryList.action',
					url : "${ctx}/sys/account/authority!getRoleAuthIds.action",
					data : {
						//"id" : roleId
						"rid":roleId
					},
					async : false,
					success:function(data) {
						/*if(data.msg) {
							$.fn.zTree.init($("#authorityTree"),settingAuthority,data.msg);
							if(status=="view"){
								var zTree = $.fn.zTree.getZTreeObj("authorityTree");
								zTree.setting.callback.beforeCheck=zTreeBeforeCheck;
							}
						}*/
						var dataArr = data.msg;
						if (dataArr) {
							var treeObj = $.fn.zTree.getZTreeObj("authorityTree");
							for(var i in dataArr) {
								var node = treeObj.getNodeByParam("id", dataArr[i], null);
								if(node) {
									treeObj.checkNode(node, true, false);
								}
							}
							//treeObj.setting.callback.beforeCheck=zTreeBeforeCheck;
						}
					}
			};
			fnFormAjaxWithJson(options, true);
		}
		
		//设置已选中的可见用户
		function getSeeUserTree(roleId,status,org_id){
			var options = {
					url:'${ctx}/sys/account/authority!seeUserList.action',
					data : {
						"rid" : roleId,
						"orgId" : 1 //org_id
					},
					success:function(data) {
						
					if (data.msg) {
						$.fn.zTree.init($("#seeUserTree"), setting, data.msg);
						if (status == "view") {
							var zTree = $.fn.zTree.getZTreeObj("seeUserTree");
							zTree.setting.callback.beforeCheck = zTreeBeforeCheck;
						}
					}
				}
			};
			fnFormAjaxWithJson(options, true);
		}

		function zTreeBeforeCheck(treeId, treeNode) {
			return false;
		};

		function getIds(method) {
			var zTree = $.fn.zTree.getZTreeObj(method + "Tree");
			var nodes = zTree.getCheckedNodes(true);
			var nodeIds = [];
			$.each(nodes, function(k, v) {
				nodeIds.push(v.id);
			});
			$("#" + method + "Id").val(nodeIds.join(","));
		}
		
		function getSeeUserIds() {
			var u_zTree = $.fn.zTree.getZTreeObj("seeUserTree");
			var u_nodes = u_zTree.getCheckedNodes(true);
			var u_nodeIds = [];
			$.each(u_nodes, function(k, v) {
				if(v.userId) {
					u_nodeIds.push(v.userId);	
				}
			});
			$("#seeUserId").val(u_nodeIds.join(","));
		}

		function operFormatter(value, rowData, rowIndex) {
			var strArr = [];
			strArr.push('<a href="javascript:void(0);" onclick="doView(');
			strArr.push(rowData.id);
			strArr.push(')"><s:text name="system.button.view.title"/></a>&nbsp;<a href="javascript:void(0);" onclick="doModify(');
			strArr.push(rowData.id);
			strArr.push(')"><s:text name="system.button.modify.title"/></a>');
			strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doModify(');
			strArr.push(rowData.id);
			strArr.push(', true)"><s:text name="system.button.copy.title"/></a>');
			if (rowData.id != "1") {
				strArr
						.push('&nbsp;<a href="javascript:void(0);" onclick="doDelete(');
				strArr.push(rowData.id);
				strArr
						.push(')"><s:text name="system.button.delete.title"/></a>');
			}
			return strArr.join("");
		}
		var orgId = "${orgId}";

		function getOption() {

			return {
				title : '<s:text name="system.sysmng.role.list.title"/>',
				iconCls : "icon-search",
				width : 700,
				height : 450,
				nowrap : false,
				striped : true,
				fit : true,
				url : '${ctx}/sys/account/role!queryList.action?orgId=' + orgId,
				sortName : 'id',
				sortOrder : 'desc',
				idField : 'id',
				frozenColumns : [ [ {
					field : 'ck',
					checkbox : true
				}, {
					title : '<s:text name="system.search.id.title"/>',
					field : 'id',
					width : 50,
					sortable : true,
					align : 'right'
				} ] ],
				columns : [ [
						{
							field : 'roleName',
							title : '<s:text name="system.sysmng.role.name.title"/>',
							width : 120,
							sortable : true
						},
						{
							field : 'organizationName',
							title : '<s:text name="system.sysmng.role.organization.title"/>',
							width : 120,
							sortable : true
						},
						{
							field : 'visibleAttr',
							title : '<s:text name="可见用户属性"/>',
							width : 180,
							sortable : true
						},
						{
							field : 'roleDesc',
							title : '<s:text name="system.sysmng.role.desc.title"/>',
							width : 120,
							sortable : true
						},
						{
							field : 'roleStatus',
							title : '<s:text name="system.sysmng.role.status.title"/>',
							width : 120,
							align : 'center',
							formatter : function(value, rowData, rowIndex) {
								var st = "";
								switch (value) {
								case "0":
									st = "<s:text name="system.button.disable.title"/>";
									break;
								case "1":
									st = "<s:text name="system.button.enabled.title"/>";
									break;
								default:
									break;
								}
								return st;
							}
						},
						{
							field : 'oper',
							title : '<s:text name="system.button.oper.title"/>',
							width : 150,
							align : 'left',
							formatter : operFormatter
						} ] ],
				pagination : true,
				rownumbers : true,
				toolbar : [ {
					id : 'bt_del',
					text : '<s:text name="system.button.add.title"/>',
					iconCls : 'icon-add',
					handler : function() {
						doAdd(orgId);
					}
				}, '-', {
					id : 'bt_del',
					text : '<s:text name="system.button.delete.title"/>',
					iconCls : 'icon-remove',
					handler : function() {
						doDeleteIds();
					}
				}, '-' ],
				onDblClickRow : function(rowIndex, rowData) {
					doView(rowData.id);
				}
			};
		}

		function doQuery() {
			var param = $("#queryForm").serializeArrayToParam();
			$("#queryList").datagrid("load", param);
		}

		/* function doView(id) {
			var options = {
				url:'${ctx}/sys/log/exp-log!view.action',
				data:{"id":id},
				success:function(data) {
					if(data.msg) {
						$("#win_add_message").window("open");
					}
				}
			};
			fnFormAjaxWithJson(options);
		} */
		
		function initOrgTree() {
			$("#organizationIds").selectTree({
				url:"${ctx}/sys/account/organization!ztreeList.action",
				queryParams:{},
				filterUrl:"${ctx}/sys/account/organization!findOrgList.action",
				filterParam:"filter_INL_id",
				pIdKey:"pId"
			});
		}
		
		function initSeeUserTree() {
			if(_isSupportBigData_) {
				$("#seeUserIds").dialoguewindow({
	     			title:"可见用户",
	     			url:"/sys/dataFilter/user-select.action",
	     			filterUrl:"${ctx}/sys/account/user!queryUser.action",
	     			filterParam:"filter_INL_id",
	     			keyMap:{"idKey":"id","textKey":"nickName"}
	     		});
			} else {
				var userOpt = {
			 			title:"可见用户",
			 			pIdKey:"pId",
			 			multiple:true,
			 			url:"${ctx}/sys/account/authority!userTree.action",
			 			queryParams:{
			 				"orgId" : 1
			 			},
			 			filterUrl:"${ctx}/sys/account/user!queryUser.action",
			 			filterParam:"filter_INL_id",
			 			keyMap:{"idKey":"id","textKey":"nickName"},
			 			valMap:{"idKey":"id","textKey":"text"},
			 			idValPrefix:"user_"
			 	};
				$("#seeUserIds").selectTree(userOpt);
			}
		}
		

		$(document)
				.ready(
						function() {
							
							initOrgTree();
							
							initSeeUserTree();
							
							$("#b1")
									.toggle(
											function() {
												if (!$('#b1').linkbutton(
														'options').disabled) {
													$("#b1")
															.linkbutton(
																	{
																		text : '<s:text name="system.button.enabled.title"/>'
																	});
													$("#b1").linkbutton({
														iconCls : 'icon-ok'
													});
													$('#status')
															.text(
																	'<s:text name="system.button.disableStatus.title"/>');
													$('#roleStatus').val("0");
												}
											},
											function() {
												if (!$('#b1').linkbutton(
														'options').disabled) {
													$("#b1")
															.linkbutton(
																	{
																		text : '<s:text name="system.button.disable.title"/>'
																	});
													$("#b1").linkbutton({
														iconCls : 'icon-no'
													});
													$('#status')
															.text(
																	'<s:text name="system.button.enabledStatus.title"/>');
													$('#roleStatus').val("1");
												}
											});
							initMenu(true);
							focusEditor("queryRoleName");
							doQuseryAction("queryForm");
							$("#queryList").datagrid(getOption());
							$("#logTimeStart").focus(function() {
								WdatePicker({
									maxDate : '#F{$dp.$D(\'logTimeEnd\')}'
								});
							});
							$("#logTimeEnd").focus(function() {
								WdatePicker({
									minDate : '#F{$dp.$D(\'logTimeStart\')}'
								})
							});
							$("#bt_query").click(doQuery);
							$("#bt_reset").click(function() {
								$("#queryForm").form("clear");
								//$("#queryForm")[0].reset();
								doQuery();
							});

							/*$("#organizationIds").selectTree({
								onChange : function(newValue, oldValue) {
									if (newValue) {
										var mt = $("#organizationIds").data("method");
										if(mt == "add") {
											initUserZtree(newValue);
										}
									}
								}
							}); */

							$("#win_add_message").window({
								width : 650,
								height : 420,
								onOpen : function() {
									$(this).window("move", {
										top : ($(window).height() - 470) * 0.5,
										left : ($(window).width() - 650) * 0.5
									});
								},
								onClose : function() {
									//$("#saveForm").form("clear");
									$("#saveForm")[0].reset();
									$("#id").val("");
								}
							});
						});
		function cancelAdd() {
			$('#win_add_message').window('close');
			doEnabled();
		}

		function doDisabled() {
			$("input,select","#saveForm").attr("disabled", "disabled");
			//$("#organizationIds").selectTree("disable");
			//$("#seeUserIds").selectTree("disable");
		}

		function doEnabled() {
			//$("#organizationIds").selectTree("enable");
			//$("#seeUserIds").selectTree("enable");
			$("input,select","#saveForm").removeAttr("disabled");
		}
		function doView(id) {
			var options = {
				url : '${ctx}/sys/account/role!view.action',
				data : {
					"id" : id
				},
				success : function(data) {
					if (data.msg) {
						var json2fromdata = data.msg;
						$("#organizationIds").data("method", "view");
						$("#organizationIds").data("roleId", json2fromdata["id"]);
						$('#saveForm').form('load', json2fromdata);
						$("#organizationIds").selectTree('setValue',json2fromdata.organizationId);
						if(_isSupportBigData_) {
							$("#seeUserIds").dialoguewindow("setValue", json2fromdata.seeUserIds);
						} else {
							$("#seeUserIds").selectTree("setValue", json2fromdata.seeUserIds);
						}
						if (json2fromdata.roleStatus == "1") {
							$("#b1")
									.linkbutton(
											{
												text : '<s:text name="system.button.disable.title"/>'
											});
							$("#b1").linkbutton({
								iconCls : 'icon-no'
							});
							$('#status')
									.text(
											'<s:text name="system.button.enabledStatus.title"/>');
							$('#b1').linkbutton('disable');
						} else {
							$("#b1")
									.linkbutton(
											{
												text : '<s:text name="system.button.enabled.title"/>'
											});
							$("#b1").linkbutton({
								iconCls : 'icon-ok'
							});
							$('#status')
									.text(
											'<s:text name="system.button.disableStatus.title"/>');
							$('#b1').linkbutton('disable');
						}
						$("#win_add_message")
								.window("setTitle",
										"<s:text name="system.sysmng.user.view.title"/>");
						$("#mm").hide();
						$("#win_add_message").window("open");
						doDisabled();
						getAuthorityTree(id, "view");
						getDesktopTree(id, "view");
						getSystemType(id, "view");	
						//getSeeUserTree(id, "view", json2fromdata["organizationId"]);
					}
				}
			};
			fnFormAjaxWithJson(options);
		}
		function doModify(id, isCopy) {
			var options = {
				url : '${ctx}/sys/account/role!modifyInfo.action',
				data : {
					"id" : id
				},
				success : function(data) {
					if (data.msg) {
						var json2fromdata = data.msg;
						$("#organizationIds").data("method", "modify");
						$("#organizationIds").data("roleId", json2fromdata["id"]);
						if(isCopy) {
							delete json2fromdata["id"];
						}
						$('#saveForm').form('load', json2fromdata);
						$("#organizationIds").selectTree('setValue',json2fromdata.organizationId);
						if(_isSupportBigData_) {
							$("#seeUserIds").dialoguewindow("setValue", json2fromdata.seeUserIds);
						} else {
							$("#seeUserIds").selectTree("setValue", json2fromdata.seeUserIds);
						}
						if (json2fromdata.roleStatus == "1") {
							$("#b1")
									.linkbutton(
											{
												text : '<s:text name="system.button.disable.title"/>'
											});
							$("#b1").linkbutton({
								iconCls : 'icon-no'
							});
							$('#status')
									.text(
											'<s:text name="system.button.enabledStatus.title"/>');
							$('#b1').linkbutton('enable');
						} else {
							$("#b1")
									.linkbutton(
											{
												text : '<s:text name="system.button.enabled.title"/>'
											});
							$("#b1").linkbutton({
								iconCls : 'icon-ok'
							});
							$('#status')
									.text(
											'<s:text name="system.button.disableStatus.title"/>');
							$('#b1').linkbutton('enable');
						}
						$("#win_add_message")
								.window("setTitle",
										"<s:text name="system.sysmng.user.modify.title"/>");
						$("#mm").show();
						$("#win_add_message").window("open");
						focusEditor("roleName");
						doEnabled();
						getAuthorityTree(id, "modify");
						getDesktopTree(id, "modify");
						getSystemType(id, "modify");
						initSeeUserTree();
						//getSeeUserTree(id, "modify", json2fromdata["organizationId"]);
					}
				}
			};
			fnFormAjaxWithJson(options);
		}
		function doAdd(orgId) {
			var options = {
				url : '${ctx}/sys/account/role!addInfo.action',
				data : {
					"orgId" : orgId
				},
				success : function(data) {
					if (data.msg) {
						$("#organizationIds").data("method","add");
						$("#mm").show();
						$("#organizationIds").selectTree('setValue',
								data.msg.parentId);
						initMenu(false);
						initDesktopMenu();
						initSystemType();
						$("#win_add_message").window("open");
						focusEditor("roleName");
						doEnabled();
						$("#b1")
								.linkbutton(
										{
											text : '<s:text name="system.button.disable.title"/>'
										});
						$("#b1").linkbutton({
							iconCls : 'icon-no'
						});
						$('#status')
								.text(
										'<s:text name="system.button.enabledStatus.title"/>');
						$('#b1').linkbutton('enable');
						$('#roleStatus').val("1");
					}
				},
				traditional : true
			};
			fnFormAjaxWithJson(options);
		}
		function saveRole(method) {
			//$("#saveForm").form("validate");
			var options = {
				//resetForm:true,
				url : '${ctx}/sys/account/role!save.action',
				success : function(data) {
					if (data.msg) {
						/* $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.succees"/>', 'info'); */
						$("#queryList").datagrid(getOption());
						if (method == 'add') {
							$('#win_add_message').window('close');
						}

					}
				}
			};

			if (method == 'add') {
				getIds('authority');
				getIds('desktop');
				getIds('systemType');
				//getSeeUserIds();
				fnAjaxSubmitWithJson('saveForm', options);
			}

		}
		function doDelete(id) {
			$.messager.confirm(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.info"/>',
					function(r) {
						if (r) {
							var options = {
								url : '${ctx}/sys/account/role!delete.action',
								data : {
									"id" : id
								},
								success : function(data) {
									if (data.msg) {
										/* $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.succees"/>', 'info'); */
										$("#queryList").datagrid(getOption());
									}
								}
							};
							fnFormAjaxWithJson(options);
						}
					});
		}
		function doDeleteIds() {
			var ids = [];
			var rows = $('#queryList').datagrid('getSelections');
			for (var i = 0; i < rows.length; i++) {
				if (rows[i].id == 1) {
					return $.messager
							.alert(
									'<s:text name="system.javascript.alertinfo.errorInfo"/>',
									'不能选择超级管理角色进行删除', 'info');
				} else {
					ids.push(rows[i].id);
				}

			}

			if (ids != null && ids.length > 0) {
				$.messager
						.confirm(
								'<s:text name="system.javascript.alertinfo.title"/>',
								'<s:text name="system.javascript.alertinfo.info"/>',
								function(r) {
									if (r) {
										var options = {
											url : '${ctx}/sys/account/role!deleteRoles.action',
											data : {
												"ids" : ids

											},
											success : function(data) {
												if (data.msg) {
													/* $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.succees"/>', 'info'); */
													$("#queryList").datagrid(
															getOption());
												}
											},
											traditional : true
										};
										fnFormAjaxWithJson(options);
									}
								})

			} else {
				$.messager
						.alert(
								'<s:text name="system.javascript.alertinfo.title"/>',
								'<s:text name="system.javascript.alertinfo.question"/>',
								'info');
			}
		}
	</script>
</head>
<body class="easyui-layout">
	<div region="north" border="false" title="<s:text name="system.search.title"/>" split="true" style="height:100px;padding:0px;overflow: hidden;" iconCls="icon-search">
			<form action="${ctx}/sys/account/role!queryList.action?orgId=${orgId}" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="roleName"><s:text name="system.sysmng.role.name.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_roleName" id="queryRoleName" class="Itext"></input></td>
							<th><label for="roleDesc"><s:text name="system.sysmng.role.desc.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_roleDesc" id="roleDesc" class="Itext"></input></td>
							<td><button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
							<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button></td></tr>
						<tr><th><label for="visibleAttr"><s:text name="可见用户属性"/>:</label></th>
							<td><input type="text" name="filter_LIKES_visibleAttr" id="queryVisibleAttr" class="Itext"></input></td>
							<th></th><td></td><td></td>
						</tr>	
					</tbody>
				</table>
			</form>
		</div>
		<div region="center" border="false" title="" style="overflow:hidden;width: 580px; padding1: 1px;">
			<table id="queryList"></table>
		</div>
	<!--win弹出框  -->
	<!--win弹出框 新增 -->
	<div id="win_add_message" class="easyui-window" closed="true" modal="true"
		title="<s:text name="system.sysmng.user.add.title"/>"
		iconCls="icon-save"
		style="width: 650px; height: 500px; padding: 0px; background: #fafafa;">
		<div class="easyui-layout" fit="true">
			<div region="center" border="false"
				style="padding: 2px; background: #fff; overflow: auto; border: 1px solid #ccc;">
				<form action="${ctx}/sys/account/role!save.action"
					name="saveForm" id="saveForm" method="post">
					<input type="hidden"  id="id" name="id" />
					<table align="center" border="0" cellpadding="0" cellspacing="1"
						class="table_form">
						<tbody>
							<tr>
								<th><label for="roleName"><s:text
											name="system.sysmng.role.name.title" />:</label>
								</th>
								<td><input type="text" name="roleName" id="roleName" class="easyui-validatebox Itext"  required="true" validType="validateName"></input>
								</td>
								<th><label for="organizationIds"><s:text
											name="system.sysmng.role.organization.title" />:</label>
								</th>
								<td><input id="organizationIds"
									<%-- url="${ctx}/sys/account/organization!queryList.action"--%>
									name="organizationId" style="width: 180px" required="true" class="Itext"></input>
								</td>
							</tr>
							<tr>
								<th><label for="roleStatus"><s:text
											name="system.sysmng.role.status.title" />:</label>
								</th>
									<td >
										<input type="hidden" name="roleStatus" id="roleStatus" /> <span
											id="status" style="color: red"></span> <a href="#"
											class="easyui-linkbutton" id="b1" ></a>
									</td>
									 <th><label for="roleDesc"><s:text
											name="system.sysmng.role.desc.title" />:</label>
								</th>
								<td ><input type="text" name="roleDesc" class="Itext"
									id="roleDesc" style="width: 180px"></input>
								</td>
							</tr>
							<tr>
								<th><label for="indexUrl"><s:text
											name="首页地址" />:</label>
								</th>
								<td colspan="3">
									<input type="text" name="indexUrl" id="indexUrl" size="75" class="Itext"></input>	
								</td>
							</tr>
							<tr>
								<th><label for="portalUrl"><s:text
											name="门户地址" />:</label>
								</th>
								<td colspan="3">
									<input type="text" name="portalUrl" id="portalUrl" size="75" class="Itext"></input>
								</td>
							</tr>
							<tr>
								<th><label for="visibleAttr"><s:text
											name="可见用户属性" />:</label>
								</th>
								<td colspan="3">
									<input type="text" name="visibleAttr" id="visibleAttr" size="75" class="Itext"></input>
									<span>如：INL_kefu_OR_yewuyuan（INL_用户属性名1_OR_用户属性名2）</span>
								</td>
							</tr>
							<tr>
							<th valign="top"><label ><s:text name="system.sysmng.role.setAuthority.title"/>:</label>
							</th>
							<td valign="top">
								<input type="hidden" name="authorityIds" id="authorityId"/>
								<ul id="authorityTree" class="ztree"></ul>
							</td>	
							
							<th valign="top"><label for="desktopName"><s:text name="system.sysmng.role.setDesktop.title"/>:</label>
							</th>
							<td colspan="2" valign="top">
							<input type="hidden" name="desktopIds" id="desktopId" />
							<ul id="desktopTree" class="ztree"></ul></td>		
							</tr>
							<tr>
								<th valign="top"><label ><s:text name="系统分类"/>:</label></th>
								<td valign="top">
									<input type="hidden" name="systemTypeIds" id="systemTypeId"/>
									<ul id="systemTypeTree" class="ztree"></ul>	
								</td>
								<th valign="top"><label ><s:text name="可见用户"/>:</label></th>
								<td valign="top">
									<input type="text" name="seeUserIds" id="seeUserIds" style="width:400px;" class="Itext"/>
									<%-- <ul id="seeUserTree" class="ztree"></ul>--%>
								</td>		
							</tr>
						</tbody>	
					</table>

				</form>
			</div>
			<div region="south" border="false"
				style="text-align: right; height: 30px; line-height: 30px;">
				<a id="mm" class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="saveRole('add')"><s:text
						name="system.button.save.title" /> </a> <a class="easyui-linkbutton"
					iconCls="icon-cancel" href="javascript:void(0)"
					onclick="cancelAdd();"><s:text
						name="system.button.cancel.title" /> </a>
			</div>
		</div>
	</div>
	<!--win弹出框  -->
</body>
</html>

