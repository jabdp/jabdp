<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title" />
</title>
<%@ include file="/common/meta-gs.jsp"%>

<script type="text/javascript">
	//定义一个全局变量保存操作状态
	var operMethod = "${operMethod}";
	var _userList = {};

	function doInitForm(data, callFunc) {
		if (callFunc) {
			callFunc();
		}
		if (data.msg) {
			var jsonData = data.msg;
			if(operMethod=="edit"){
				$("#showVlaue").html("<s:text name='system.sysmng.news.draft.title'/>");
				jsonData["status"] ="0";
				addState();
			}else if(operMethod=="view"){
				if (jsonData["status"] == "1") {
					$("#showVlaue").html("已发布");
					viewAnnounceState();
				} else if (jsonData["status"] == "0") {
					$("#showVlaue").html("<s:text name='system.sysmng.news.draft.title'/>");
					viewDraftState();
				}
				//initTreeChecked(jsonData.userIds);
			}
			$('#viewForm').form('load', jsonData);
			
			$("#noticeToUserIds").dialoguewindow("setValue", jsonData.noticeToUserIds);
		}
	}

	 function initRichTextBox() {
		 	 var config = {
			 	/*toolbar :
				       [
				          ['Source', '-', 'Preview', '-'],
				         ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
				         ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],        
				         '/',
				         ['Bold', 'Italic', 'Underline'],
				         ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'],
				         ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
				         ['Link', 'Unlink', 'Anchor'],
				         ['addpic','Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],//此处的addpic为我们自定义的图标，非CKeditor提供。
				         '/',
				         ['Styles', 'Format', 'Font', 'FontSize'],
				         ['TextColor', 'BGColor']  ],  
				          ustomConfig : 'config_en.js'  */
			/* toolbar : [
					 [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList',
							'-', 'Link', 'Unlink' ], [ 'UIColor', 'Preview' ] ],
			customConfig : 'config_en.js' */
		}; 
		$('.jquery_ckeditor').ckeditor(config);
	}
 
	$(document).ready(function() {
		typeTree();
		initUserZtree();
		initRichTextBox();
		if (operMethod == "add") {
			//新增 
			addState();
			focusFirstElement();			
		} else if (operMethod == "view") {

			//查看
			doModify("${param.id}", doDisabled);
		} else if (operMethod == "edit") {
			//复制
			doModify("${param.id}", doEnable);
		}

	});

	//设置焦点
	function focusFirstElement() {
		focusEditor("title");
	}

	//将控件设置成可编辑状态
	function setFormToEdit() {
		/* var status = $("#status").val();
		if (status == "1") {
			$('#release').hide();
		} else if (status == "0") {
			$('#repeal').hide();
		} */
		addState();
		//var zTree = $.fn.zTree.getZTreeObj("userTree");
		//zTree.setting.callback.beforeCheck=true;
		operMethod = "view";
		doEnable();
		focusFirstElement();
	}

	function doCopyMainTab() {
		var id = $("#id").val();
		if (id == null) {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.copy"/>',
							'info');
			return;
		} else {
			parent.addTab(
					'<s:text name="system.sysmng.notice.copy.title"/>-' + id,
					'${ctx}/sys/notice/notice!edit.action?id=' + id);
		}
	}
	//查询公用类型表
	function typeTree(id) {
		var options = {
			url : '${ctx}/sys/notice/notice!queryNoticeType.action',
			success : function(data) {
				initTypeTree(data.msg, id);
			},
			async : false//同步
		};
		fnFormAjaxWithJson(options, true);
	}

	//初始化类型选择combobox
	function initTypeTree(data, id) {
		//调用gscommon里的初始化方法
		selectType(data, "type", {
			"key" : "id",
			"caption" : "name"
		}, function() {
			$("#aboutType").window("open");
			focusEditor("newType");
		});
		//每次将新增的值设为默认值
		if (id) {
			$('#type').combobox('setValue', id);
		}
	}

	function doCanel() {
		typeTree();
		$("#aboutType").window("close");
	}

	function doUpdate() {
		var newType = $("#newType").val();
		addTypeTree(newType);
	}

	
	//新增新闻类型
	function addTypeTree(type) {
		var flag = $("#typeForm").form("validate");
		if (flag) {
			var options = {
				url : '${ctx}/sys/notice/notice!addNoticeType.action',
				data : {
					"name" : type
				},
				success : function(data) {
					//重新查一遍，刷新combobox里的值
					typeTree(data.msg);
					$("#aboutType").window("close");
				}
			};
			fnFormAjaxWithJson(options, true);

		}
	}
	//页面数据初始化
	function doModify(id, callFunc) {
		var options = {
			url : '${ctx}/sys/notice/notice!queryNotice.action',
			data : {
				"id" : id
			},
			success : function(data) {
				if(data.msg["status"] =="1"){
					$('#tt').tabs('add',{
						title:'<s:text name="system.sysmng.notice.readDetail.title"/>',
						content:'<table id="noticeReadDetail"></table>',
						selected : false
					});			
				 doModifyReadDetail(id);
				}
				doInitForm(data, callFunc);
		
			}
		};
		fnFormAjaxWithJson(options, true);
	}

	function saveObj(st) {
		if (st == "1") {
			$("#status").val("");
			$("#status").val("1");
		} else if (st == "0") {
			$("#status").val("");
			$("#status").val("0");
		}
		getIds();
		if (operMethod == "edit" || operMethod == "add") {
			$("#id").val("");
			$("#atmId").val("");
		}

		var options = {
			url : '${ctx}/sys/notice/notice!save.action',
			success : function(data) {
				operMethod = "view";
				doModify(data.msg, doAfterSave);
			}
		};
		fnAjaxSubmitWithJson("viewForm", options);

	}

	function doRepeal(st) {
		var id = $("#id").val();
		if (st == "1") {
			$("#status").val("1");
		} else if (st == "0") {
			$("#status").val("0");
		}
		getIds();
		if (operMethod == "edit" || operMethod == "add") {
			$("#id").val("");
		}
		var options = {
			url : '${ctx}/sys/notice/notice!repeal.action',
			data : {
				"id" : id,
				"status" : st
			},
			success : function(data) {
				doModify(data.msg, doAfterSave);
				$("#tt").tabs("close","<s:text name='system.sysmng.notice.readDetail.title'/>");
			}

		};
		fnFormAjaxWithJson(options, true);

	}

	function doAfterSave() {
		if (parent.refreshTabData) {
			parent.refreshTabData("<s:text name='system.sysmng.notice.manage.title'/>");
		}
		operMethod = "view";
		doDisabled();
	}

	function doEnable() {
		$("input[type!=hidden],textarea,select").removeAttr("disabled");
		$("input.easyui-combobox").combobox('enable');
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(false);
			} catch (e) {
				$(this).ckeditorGet().setReadOnly(false);
			}
		});
	}

	//将页面上的控件置为不可编辑状态
	function doDisabled() {
		$("input[type!=hidden],textarea,select").attr("disabled", "disabled");
		$("input.easyui-combobox").combobox('disable');
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(true);
			} catch (e) {
				$(this).ckeditorGet().setReadOnly(true);
			}
		});
	}

	function formClear() {
		$("#viewForm")[0].reset();
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setData("");
			} catch (e) {
				$(this).ckeditorGet().setData("");
			}
		});

	}

	//查看里的新增
	function doAdd() {
		addState();
		operMethod = "add";
		doEnable();
		formClear();
		//initUserZtree();
		$("#status").val("0");
		$("#showVlaue").html("<s:text name='system.sysmng.news.draft.title'/>");
		$("#tt").tabs("close","<s:text name='system.sysmng.notice.readDetail.title'/>");
	}

	//添加附件
	function doAccessory() {
		var attachId = $("#atmId").val();
		var id = $("#id").val();
		  var options ={"attachId":attachId,
 				 "id":id,
 				 "key":"",
 				 "entityName":"",
 				 "dgId":"queryList",
 				 "flag":"mainEdit",
 				 "readOnly" : false
 					};
		doAttach(options);
	}

	//更新附件ID flag: mainQuery-主实体查询，subEdit-子实体编辑，mainEdit-主实体编辑
	function doUpdateAttach(opts) {
		var options = {
			url : '${ctx}/sys/notice/notice!updateNotice.action',
			data : {
				"entityName" : opts.entityName,
				"id" : opts.id,
				"atmId" : opts.attachId
			},
			async : false,
			success : function(data) {
				if (data.msg) {
					$("#atmId").val(opts.attachId);
					if (parent.refreshTabData) {
						parent.refreshTabData("<s:text name='system.sysmng.notice.manage.title'/>");
					}
					//doAfterSave();
					/* if(flag == "mainQuery") {
						$("#" + dgId).datagrid("reload");
					} else if(flag == "subEdit"){
						$("#" + dgId).edatagrid("reload");
					} */
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	}

	//构造Ztree用户候选人
	var settingZtree = {
		check : {
			enable : true
		},
		data : {
			simpleData : {
				enable : true
			}
		}
	};

	//用户初始化
	function initUserZtree() {
		var org_id = $("#orgnizationId").val();
		/*var options = {
			url : '${ctx}/sys/account/authority!userList.action',
			data : {
				"orgId" : org_id
			},
			async:false,
			success : function(data) {
				if (data.msg) {
					$.fn.zTree.init($("#userTree"), settingZtree, data.msg);
					if(operMethod=="view"){
					var zTree = $.fn.zTree.getZTreeObj("userTree");
					zTree.setting.callback.beforeCheck=zTreeBeforeCheck;
					}
				}
			}
		};

		fnFormAjaxWithJson(options, true);*/
		$("#noticeToUserIds").dialoguewindow({
 			title:"用户",
 			url:"/sys/dataFilter/user-select.action",
 			queryParams:{"orgId" : org_id},
 			filterUrl:"${ctx}/sys/account/user!queryUser.action",
 			filterParam:"filter_INL_id",
 			keyMap:{"idKey":"id","textKey":"nickName"}
 		});
	}
	
	function zTreeBeforeCheck(treeId, treeNode) {
	    return false;
	};
	
	function initTreeChecked(userIds) {
		if (userIds && userIds.length > 0) {
			var u_zTree = $.fn.zTree.getZTreeObj("userTree");
			if (u_zTree) {
				$.each(userIds, function(k, v) {
					var node = u_zTree.getNodeByParam("id", "user_" + v);
					// console.log(node);
					if (node) {
						u_zTree.checkNode(node, true, true);
					}
				});

			}

		}
	}

	function getIds() {
		/*var u_zTree = $.fn.zTree.getZTreeObj("userTree");
		var u_nodes = u_zTree.getCheckedNodes(true);
		var u_nodeIds = [];
		$.each(u_nodes, function(k, v) {
			if(v.userId) {
				u_nodeIds.push(v.userId);	
			}
		});
		$("#noticeToUserIds").val(u_nodeIds.join(","));*/

	}

	//根绝id查询所有用户阅读状态
	function doModifyReadDetail(id) {
		//_userList = findAllUser();
		var options = {
			url : '${ctx}/sys/notice/notice!queryNoticeDetail.action',
			data : {
				"id" : id
			},
			success : function(data) {
				transToDatagridJson(data);
			}
		};
		fnFormAjaxWithJson(options, true);
	}

	//初始化datagrid
	function transToDatagridJson(data) {
		var dataTemp = {
			"totle" : 0,
			"rows" : []
		};
		var dataJson = data.msg;
		for ( var i = 0; i < dataJson.length; i++) {
			var fieldJson = $.extend({
				"userId" : "",
				"status" : "",
				"readTime" : ""
			}, dataJson[i].notice);//userIds
			dataTemp.rows.push(fieldJson);

		}

		$('#noticeReadDetail').datagrid({
			width : 'auto',
			height : 'auto',
			border : false,
			fitColumns : true,
			columns : [ [ {
				field : 'userId',
				title : '<s:text name="system.sysmng.user.name.title"/>',
				width : 100,
				sortable : true,
				formatter : function(value, rowData, rowIndex) {
					/*var uObj=_userList[value];
                    if(uObj){
                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
						return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                    }else{
                        return value;
                    }*/
                    return rowData["userName"];
				}
			}, {
				field : 'status',
				title : '<s:text name="system.sysmng.notice.isRead.title"/>',
				width : 100,
				sortable : true,
				formatter : function(value, rowData, rowIndex) {
					if (value == "1") {
						return "已阅";
					} else if (value == "0") {
						return "未阅";
					}
				}

			}, {
				field : 'readTime',
				title : '<s:text name="system.sysmng.notice.readTime.title"/>',
				width : 100,
				sortable : true,
				formatter : function(value, row, index) {
					if (value) {
						return fnFormatDate(value, 'yyyy-MM-dd HH:mm:ss');
					}
				}
			} ] ]

		});
		$("#noticeReadDetail").datagrid("loadData", dataJson);

	}
	function addState(){//新增状态  只显示“关闭”，“保存”
		$('#tsave').show();//保存，关闭按钮由状态判断
		$('#tadd').hide();//新增
		$('#tedit').hide();//修改
		$('#tattach').hide();//附件
		$('#tcopy').hide();//复制
		$('#repeal').hide();//取消发布
		$('#release').hide();//正式发布
	}
	function viewDraftState(){//(草稿)的查看状态  
		$('#tsave').hide();//保存，关闭按钮由状态判断
		$('#tadd').show();//新增
		$('#tattach').show();//附件
		$('#tcopy').show();//复制
		
		//草稿才可以修改和正式发布
		$('#tedit').show();//修改
		$('#release').show();//正式发布
		$('#repeal').hide();//取消发布
	}
	function viewAnnounceState(){//(已发布)的查看状态  
		$('#tsave').hide();//保存，关闭按钮由状态判断
		$('#tadd').show();//新增
		$('#tattach').show();//附件
		$('#tcopy').show();//复制
		
		//已发布的隐藏修改和正式发布
		$('#repeal').show();//取消发布
		$('#tedit').hide();//修改
		$('#release').hide();//正式发布
	}
</script>
</head>

<body class="easyui-layout" fit="true" title="maintable-view">
	<div region="north" style="overflow: hidden;"  border="false">
		<div class="datagrid-toolbar">
			<a id="tsave" href="javascript:saveObj('0');"
				class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/><s:text name="system.sysmng.news.draft.title"/></a>
			<c:if test="${operMethod=='view'}">
				<a id="tclose"
					href="javaScript:window.parent.closeTab('<s:text name="system.sysmng.notice.view.title"/>-${param.id}')"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/></a>
			</c:if>
			<c:if test="${operMethod=='add'}">
				<a id="tclose"
					href="javaScript:window.parent.closeTab('<s:text name="system.sysmng.notice.add.title"/>')"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/></a>
			</c:if>
			<c:if test="${operMethod=='edit'}">
				<a id="tclose"
					href="javaScript:window.parent.closeTab('<s:text name="system.sysmng.notice.copy.title"/>-${param.id}')"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/></a>
			</c:if>
			<security:authorize
				url="/sys/attach/attach.action?entityName=contracts.MainTable">
				<a id="tattach" href="javaScript:doAccessory();"
					class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name='system.button.accessory.title'/></a>
			</security:authorize>
			<a id="tadd" href="javaScript:doAdd();" class="easyui-linkbutton"
				plain="true" iconCls="icon-add"><s:text name="system.button.add.title"/></a> <a id="tedit" href="#"
				onclick="setFormToEdit();" class="easyui-linkbutton" plain="true"
				iconCls="icon-edit"><s:text name="system.button.modify.title"/></a> <a id="tcopy"
				href="javaScript:doCopyMainTab();" class="easyui-linkbutton"
				plain="true" iconCls="icon-copy"><s:text name="system.button.copy.title"/></a> <a id="repeal"
				href="javaScript:doRepeal('0');" class="easyui-linkbutton"
				plain="true" iconCls="icon-undo"><s:text name="system.sysmng.news.cancelAnnounce.title"/></a> <a id="release"
				href="javaScript:doRepeal('1');" class="easyui-linkbutton"
				plain="true" iconCls="icon-ok"><s:text name="system.sysmng.news.announce.title"/></a>
		</div>
	</div>
	<div region="center" style="position: relative; overflow: auto;" border="false">
		<form action="" method="post" name="viewForm" id="viewForm"
			style="margin: 0px;">
			<input type="hidden" id="id" name="id" />
			 <input type="hidden" id="atmId" name="atmId" />
			<div id="tt" class="easyui-tabs">
				<div selected="true" title="<s:text name="system.sysmng.notice.detail.title"/>" style="padding: 2px;">
					<table border="0" cellpadding="0" cellspacing="1"
						class="table_form">
						<tr>
							<th><label><s:text name="system.sysmng.desktop.modelTitle.title"/></label>
							</th>
							<td><input type="text" name="title" id="notice.title"
								class="easyui-validatebox Itext" required="true" style="width: 400px" />
							</td>
							<th><label><s:text name="system.sysmng.desktop.key.title"/></label>
							</th>
							<td><input type="text" name="keyworlds"
								id="notice.keyworlds" class="easyui-validatebox Itext" 
								style="width: 400px" />
							</td>
						</tr>
						<tr>
							<th><s:text name="system.sysmng.notice.content.title"/></th>
							<td colspan="3"><textarea class="jquery_ckeditor"
									id="contractrichTextBox" name="content"></textarea>
							</td>
						</tr>
						<tr>
						<th><label><s:text name="system.sysmng.notice.style.title"/></label></th>
								<td><input type="hidden" name="style" id="style" value="01" /> 
								<div id="showStyle"><s:text name="system.sysmng.notice.common.title"/></div></td>
							<th><label><s:text name="system.sysmng.notice.status.title"/></label></th>
							<td><input type="hidden" name="status" id="status" value="0" />
								<div id="showVlaue"><s:text name="system.sysmng.news.draft.title"/></div></td>
						</tr>
						<tr>

							<th><label><s:text name="system.sysmng.notice.user.title"/></label>
							</th>
							<td><input type="text" name="noticeToUserIds" id="noticeToUserIds" style="width:400px;"/> 
								<input type="hidden" id="orgnizationId" value="${orgId}" />
								<!-- <ul id="userTree" class="ztree"></ul>  --> 
							</td>
								<th><label><s:text name="system.sysmng.notice.type.title"/></label></th>
							<td><input id="type" class="easyui-combobox" name="type"
								style="width: 200px;" required="true"></input></td>
						</tr>
					</table>
				</div>
				
			</div>
	</div>

	</form>

<%--	 
		<div id="aboutType1" class="easyui-window" closed="true"
			minimizable="false" modal="true" title="<s:text name="system.sysmng.notice.addNoticeType.title" />"
			style="width: 250px; height: 100px;">
			<div style="text-align: center; padding: 10px 10px;"
				class="easyui-layout" fit="true">
				<div region="center" border="false">
					<form action="" name="passForm" id="typeForm" method="post"
						style="margin: 0px;">
						<table align="center" border="0" cellpadding="0" cellspacing="1"
							class="table_form" style="height:50px;">
							<tbody>
								<tr>
									<th><label for="newType"><s:text name="system.sysmng.notice.type.title" />:</label>
									</th>
									<td><input type="text" name="newType" id="newType"
										class="easyui-validatebox" required="true" />
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
				<div region="south" border="false" style="text-align: right;">
					<a id="mm" class="easyui-linkbutton" iconCls="icon-ok"
						href="javascript:void(0);" onclick="doUpdate();"><s:text
							name="system.button.ok.title" /> </a> <a class="easyui-linkbutton" iconCls="icon-cancel"
						href="javascript:void(0);" onclick="doCanel();"><s:text
							name="system.button.cancel.title" /> </a>
				</div>
			</div>
		</div> --%>
		
	<div id="aboutType" class="easyui-window" title="<s:text name="system.sysmng.notice.addNoticeType.title" />" data-options="iconCls:'icon-save',closed:true" style="width:300px;height:150px;padding:5px;">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'center'" style="padding:10px;">
                <form action="" name="passForm" id="typeForm" method="post"
						style="margin: 0px;">
						<table align="center" border="0" cellpadding="0" cellspacing="1"
							class="table_form" >
							<tbody>
								<tr>
									<th><label for="newType"><s:text name="system.sysmng.notice.type.title" />:</label>
									</th>
									<td><input type="text" name="newType" id="newType"
										class="easyui-validatebox" required="true" />
									</td>
								</tr>
							</tbody>
						</table>
					</form>
            </div>
            <div data-options="region:'south',border:false" style="text-align:right;padding:5px 0 0;">
                <a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" onclick="doUpdate()" style="width:80px">
                	<s:text name="system.button.ok.title" />
                </a>
                <a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" href="javascript:void(0)" onclick="doCanel()" style="width:80px">
                	<s:text name="system.button.cancel.title" />
                </a>
            </div>
        </div>
    </div>

</body>
</html>
