<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="新闻公告"/></title>
<link href="${ctx}/js/picNews/style/css.css" rel="stylesheet" type="text/css" />
<%@ include file="/common/meta.jsp" %>
</head>
<body id="layout_body" class="easyui-layout" fit="true">
<div region="center" title="" border="false">
  <div id="wrapper" style="padding:5px;">
	<div id="picSlideWrap" class="clearfix">
      <div><h3 class="titleh3" id="showTitle"></h3></div>
      <div><h4 class="titleh4" id="lastUpdateTime"></h4></div>
      <div id="content"><p id="showContent"></p></div>
　　     <div style="padding-right: 30px;"><p align="right" id="showCreateUser"></p></div>
	  <div id="attachment" style="padding:2px;" class="attbg" ui-type="attCon"></div>
	  <div id="visitor" class=""></div>
	  <div id="message_board" class="tie-area"></div>
    </div> 
  </div>
</div>    
</body> 
<script type="text/javascript">
var _userList = {}; // 所有用户
var _id = "${param.id}"; // 当前新闻ID
function doInitForm(data, callFun) { // 页面Document初始化
	if(data.msg) {
		var jsonData = data.msg;
		$("#showTitle").html(jsonData["title"]);
		$("#showContent").html(jsonData["content"]);
		var dtTime="";
		if(jsonData["lastUpdateTime"]) {
			dtTime=jsonData["lastUpdateTime"];
		} else {
			dtTime=jsonData["createTime"];
		}
		$("#lastUpdateTime").html("发布时间：" + dtTime + "                      " + jsonData["clickCount"] + "人" + "        参与");
		
		$("#showCreateUser").html("编辑：" + jsonData["createUserCaption"]);
		/*var uId = jsonData["createUser"];
		var uObj = _userList[uId];
		if(uObj) {
			$("#showCreateUser").html("编辑：" + uObj.realName);
		} else {
			$("#showCreateUser").html("编辑：未知者");
		}*/
	}
	if(callFun) {
		callFun();
	}
	laodAttach(data.msg.atmId);
	laodVisitor();
	laodMessageBoard();
}
function laodVisitor() { // 加载以前访问者记录
	var url = "${ctx}/sys/notice/visitor-view.action?id=" + _id;
	$("#visitor").load(url);
}
function laodAttach(attachId) { // 加载附件列表
	if(attachId!=null){
		var url = "${ctx}/sys/attach/attach-notice.action?attachId="+attachId;
		$("#attachment").load(url);
	}
}
function laodMessageBoard() { // 加载留言板
	var url = "${ctx}/sys/tie/message-board-view.action?id=" + _id + "&moduleKey=SYS_NEWS";
	$("#message_board").load(url);
}
function doModify() { // 页面数据初始化
	var options = {
		url : '${ctx}/sys/notice/news!viewNewsInit.action',
		data : {
			"id" : _id
		},
		success : function(data) {
			doInitForm(data, function() {
				if(parent.refreshTabData) {
					parent.refreshTabData("<s:text name='system.sysmng.news.manage.title'/>");
				}
			});
		}
	};
	fnFormAjaxWithJson(options);
}
$(function() {
	//_userList = findAllUser();
	doModify();
});
</script>
</html>
