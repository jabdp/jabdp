<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title" />
</title>
<%@ include file="/common/meta-gs.jsp"%>
	<%-- <link href="${ctx}/js/easyui/${themeColor}/panel.css" rel="stylesheet" type="text/css"/>--%>
    <script type="text/javascript" src="${ctx}/js/easyui-1.4/easyui.xpPanel.js"></script> 
<script type="text/javascript">
            var _userList={};
            var _readStatusDict = [{"key":"0","caption":"未阅读"},{"key":"1","caption":"已阅读"}];
			var _styleDict = [{"key":"01","caption":"普通通知"},{"key":"02","caption":"预警通知"},{"key":"03","caption":"待办事宜通知"},{"key":"04","caption":"业务确认通知"},{"key":"05","caption":"提醒通知"},{"key":"06","caption":"业务办理通知"}];
			//var _styleDict = {"01":"普通通知","02":"预警通知","03":"待办事宜通知","04":"业务确认通知","05":"提醒通知","06":"业务办理通知"};
            function getOption() {
                return {
                    width : 'auto',
                    height : 'auto',
                    nowrap : false,
                    striped : true,
                    fit : true,
                    url :'${ctx}/sys/notice/notice!queryNoticeToUserList.action',
                    sortName : 'b.createTime',
                    sortOrder : 'desc',
                    frozenColumns : [
                        [
                            {
								field : "atmId",
								title : "<s:text name='system.button.accessory.title'/>",
								width : 35,
								formatter:function(value, rowData, rowIndex) {
									if(value) {
										var astr = [];
										astr.push('<a class="easyui-linkbutton l-btn l-btn-plain" href="javascript:void(0);"');
										 <security:authorize url="/sys/attach/attach.action"> 
											astr.push(' onclick="doAccessory(');
											astr.push(value);
											astr.push(',');
											astr.push(rowData["id"]);
											astr.push(');" ');
										</security:authorize> 
										astr.push('>');
										astr.push('<span class="l-btn-left"><span class="l-btn-text"><span class="l-btn-empty icon-attach">&nbsp;</span></span></span></a>');
										return astr.join("");
									} else {
										return "<span></span>";
									}
								}
                            }  
                        ]
                    ],
                    columns : [
                        [
	                                       /*  {
	                                            field : 'id',
	                                            title : '<s:text name="编号"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            hidden:true
	                                            
	                                        } , */
	                                        {
	                                            field : 'ntuId',
	                                            title : '<s:text name="system.sysmng.notice.id.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            hidden:true
	                                            
	                                        } ,
	                                        {
	                                            field : 'title',
	                                            title : '<s:text name='system.sysmng.desktop.modelTitle.title'/>',
	                                            width : 300,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'keyworlds',
	                                            title : '<s:text name='system.sysmng.desktop.key.title'/>',
	                                            width : 200,
	                                            sortable : true
	                                        } /* ,
	                                        {
	                                            field : 'content',
	                                            title : '<s:text name="正文"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } */ ,
	                                        {
	                                            field : 'createUser',
	                                            title : '<s:text name="system.sysmng.desktop.createUser.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
	                                            	/*var uObj=_userList[value];
                                                    if(uObj){
                                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
														return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                                                    }else{
                                                        return value;
                                                    }*/
                                                    return rowData["createUserCaption"];
                                                 }
	                                        } ,
	                                        {
	                                            field : 'createTime',
	                                            title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                              
	                                        }  ,
	                                        {
	                                            field : 'readStatus',
	                                            title : '<s:text name="system.sysmng.notice.isRead.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value,rowData,rowIndex){
	                                            	if(value=="1"){
	                                            		return "已阅读";
	                                            	}else if(value=="0"){
	                                            		return "未阅读";
	                                            	}
	                                            }
	                                            
	                                        }
	                                        ,
	                                        {
	                                            field : 'readTime',
	                                            title : '<s:text name="system.sysmng.notice.readTime.title"/>',
	                                            width : 80,
	                                            sortable : true/* ,
                                                formatter: function(value,row,index){
													if(value){
														return fnFormatDate(value, 'yyyy-MM-dd');
													}
												} */
	                                            
	                                            
	                                        } ,
	                                        {
	                                            field : 'type',
	                                            title : '<s:text name="system.sysmng.notice.type.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:getTypeValue
	                                        } ,
	                                        {
	                                            field : 'style',
	                                            title : '<s:text name="system.sysmng.notice.style.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
	                                            	 if(value=="01"){
	                                                	 return "普通通知";
	                                            	 }else if(value=="02"){
	                                            		 return "预警通知";
	                                            	 }else if(value=="03"){
	                                            		 return "待办事宜通知";
	                                            	 }else if(value=="04"){
	                     								return "<s:text name='业务确认通知'/>";
	                     							}else if(value=="05"){
	                     								return "<s:text name='提醒通知'/>";
	                     							}else if(value=="06"){
	                     								return "<s:text name='业务办理通知'/>";
	                     							}
	                                             }/* , 
	                                         	styler:function(value,row,index){
	                                         		alert("aaaaaaaaaaaaaa");
	                    							if (value=="03"){
	                    								return 'background-color:#ffee00;color:red;';
	                    							}
	                    						} */
	                                        }
                        ]
                    ],
                    onDblClickRow:function(rowIndex, rowData){
                    	 if(rowData.style=="01"){
                     		doDblView(rowData.id,rowData.ntuId,rowData.atmId,rowData.style);                  		
 						}else if(rowData.style=="03" ){//待办通知(办理任务)
 							//doSignin(rowData.moduleDataId,rowData.moduleKey);
 							if(rowData.readStatus=="0"){
 								doRead(rowData.ntuId);
 							}							
 						}else if(rowData.style=="05" ){//提醒通知(查看历史)
 							parent.addTab('<s:text name="system.sysmng.process.dealHistory.title"/>-'+rowData.moduleDataId, 
 							'${ctx}/gs/process!getProcess.action?processInstanceId=' + rowData.moduleDataId +'&rptData='+(rowData.moduleFields ? rowData.moduleFields : "")); 					
 							if(rowData.readStatus=="0"){
 	 							doRead(rowData.ntuId);
 	 						}	
 						}else if(rowData.style=="06"){
 							if(rowData.moduleFields=="true"){
 								parent.addTab('<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'+rowData.moduleDataId, '${ctx}/gs/gs-mng!view.action?entityName='+rowData.moduleKey+'&id=' + rowData.moduleDataId);	
 							}else if(rowData.moduleFields=="false"){
 								parent.addTab('<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'+rowData.moduleDataId, '${ctx}/gs/gs-mng!view.action?entityName='+rowData.moduleKey+'&id=' + rowData.moduleDataId+'&isEdit=2');
 							}
 							if(rowData.readStatus=="0"){
 	 							doRead(rowData.ntuId);
 	 						}	
 						}else if(rowData.style=="04") {
 							var url = '${ctx}/gs/gs-mng!view.action?entityName='+rowData.moduleKey+'&id=' + rowData.moduleDataId + '&ntuId=' + rowData.ntuId + "&initOperMethod=modify";;
							parent.addTab('<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'+rowData.moduleDataId, url);
							if(rowData.moduleFields && rowData.moduleFields.indexOf("autoRead") >= 0){
								doRead(rowData.ntuId);
							}
						}else if(rowData.style=="02") {//业务预警通知
							if(rowData.moduleKey) {
								var url = '${ctx}/gs/gs-mng!view.action?entityName='+rowData.moduleKey+'&id=' + rowData.moduleDataId + '&ntuId=' + rowData.ntuId + "&initOperMethod=modify";;
								parent.addTab('<s:text name="system.button.view.title"/><s:text name="业务预警通知"/>-'+rowData.moduleDataId, url);
								/*if(rowData.readStatus=="0"){
									doRead(rowData.ntuId);
								}*/
							} else {
								doDblView(rowData.id,rowData.ntuId,rowData.style);
							}
						}
                    	$('#queryList').datagrid('unselectRow',rowIndex);
                    },
                    
    				onRowContextMenu  : function(e, rowIndex, rowData){
    					e.preventDefault();
    					var __mm = $('#mm');
    					__mm.data("id", rowData.id);
    					__mm.data("ntuId", rowData.ntuId);
    					__mm.menu('show', {
    						left: e.pageX,
    						top: e.pageY
    					});
    				},
                    pagination : true,
                    rownumbers : true/* ,
                    toolbar :[
                        {
                            id : 'bt_view',
                            text : '<s:text name="system.button.view.title"/>',
                            iconCls : "icon-search",
                            handler : function() {
                                doView();
                            }
                        }
                    ] */
                };
            }
    		function doRead(nTouId){
				var options = {
						url : '${ctx}/sys/notice/notice!doRead.action',
						data : {
							"noticeToUserId" : nTouId
						},
						success : function(data) {
							if(data.msg){
						
							}
						}
					};
					fnFormAjaxWithJson(options); 						
			}
            
            //刷新列表
             function doRefreshDataGrid() {
     			$("#queryList").datagrid("load");
     		}
         
            //查询
            function doQuery() {
                var param = $("#queryForm").serializeArrayToParam();
                $("#queryList").datagrid("load", param);
            }

          
 
            /* 
            function doView() {
            	 var id=0;
            	 var rows = $('#queryList').datagrid('getSelections');
            	  if(rows.length!=1){
            		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
            	     return ;
            	 } else{
            		 id=rows[0].id;
            		 ntuId=rows[0].ntuId;
            	 }
            	 doDblView(id,ntuId); 
            } */
            
            
           //查看tab
            function doDblView(id,ntuId,style) {       
            	parent.addTab('<s:text name="system.sysmng.notice.view.title"/>-'+id, '${ctx}/sys/notice/notice!viewNoticeToUser.action?id=' + id+"&nTouId="+ntuId+"&style="+style);
            }
            
            //右键查看
            function doCmView() {
            	var __mm = $("#mm");
            	var id = __mm.data("id");
            	doDblView(id,__mm.data("ntuId"));
            }
			
            var typeObj = {"id":"id","pid":"pid","text":"name"};
	   		var typeJsonParam = {"type":"notice","url":"${ctx}/sys/common/common-type!queryList.action"};
	   		var typeJson = getJsonObjByParam(typeJsonParam);
            
	   		function getTypeValue(value, rowData, rowIndex) {
                return getColumnValueByIdText(typeObj, typeJson, value);
            }
            
             var doTreeQuery_type = function(ids) {
		  		var param = {"filter_INL_type":ids.join(",")};													                
	            $("#queryList").datagrid("load", param);
			  };
			  
			function initCombo() {
				$("#readStatus").combobox({
					data:_readStatusDict,
					valueField:"key",
					textField:"caption",
					multiple:true
				});
				$("#style").combobox({
					data:_styleDict,
					valueField:"key",
					textField:"caption",
					multiple:true
				});
			}
             
            $(document).ready(function() {
            	// _userList=findAllUser();
            	 initCombo();
               //回车查询
                 doQuseryAction("queryForm");
                $("#queryList").datagrid(getOption());
                $("#bt_query").click(doQuery);
                $("#bt_reset").click(function() {
                    //$("#queryForm")[0].reset();
                    $("#queryForm").form("clear");
                    collapseAll();
					doQuery();
                });
                

                $("#queryFormId").hide();
				 $("#a_exp_clp").hide();
					$("#a_switch_query").toggle(function() {
						$("#queryFormId").show();
						$("#a_exp_clp").show();
						$("#tree_type").hide();
					}, function() {
						$("#queryFormId").hide();
						$("#a_exp_clp").hide();
						$("#tree_type").show();
					});
				  $("#a_switch_query").show();
                
            });
         	//查看附件
            function doAccessory(attachId,id) {
    			if(attachId && id){          
	           	var options ={"attachId":attachId,
	           				 "id":id,
	           				 "key":"",
	           				 "entityName":"",
	           				 "dgId":"queryList",
	           				 "flag":"mainQuery",
	           				 "readOnly" : true
	           				 };
            	 doAttach(options);
        		}
         	}
            </script>
</head>
<body class="easyui-layout" fit="true">
	<div region="west" title="<s:text name='system.search.title'/>"
		split="true" border="false"
		style="width: 215px; padding: 0px;"
		iconCls="icon-search" tools="#pl_tt">
		<div id="tree_type"><div style="padding-top:4px;width:100%;" class="easyui-panel" href="${ctx}/sys/common/dict-tree.action?type=notice&fname=doTreeQuery_type&fsname=doTreeAfterSave_type&isReadOnly=true" border="0"></div></div>
		<form action="" name="queryForm" id="queryForm">
			<div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">
				<div class="xpstyle" title="<s:text name="system.sysmng.desktop.modelTitle.title"/>" collapsible="true"
					collapsed="true">
					<input type="text" name="filter_LIKES_title" id="title" class="Itext"/>
				</div>
				<div class="xpstyle" title="<s:text name="system.sysmng.desktop.key.title"/>" collapsible="true"
					collapsed="true">
					<input type="text" name="filter_LIKES_keyworlds" id="key"  class="Itext"/>

				</div>
				
				<div class="xpstyle" title="<s:text name="system.sysmng.notice.isRead.title"/>" collapsible="true"
					collapsed="true">
					<input type="text" name="filter_INS_a.status" id="readStatus" style="width:150px;"  class="Itext"/>
				</div>
				
				<div class="xpstyle" title="<s:text name="system.sysmng.notice.style.title"/>" collapsible="true"
					collapsed="true">
					<input type="text" name="filter_INS_b.style" id="style" style="width:150px;" class="Itext"/>
				</div>
			</div>
			<div style="text-align:center;padding:8px 8px;">
				<button type="button" id="bt_query" class="button_small">
					<s:text name="system.search.button.title" />
				</button>
				&nbsp;&nbsp;
				<button type="button" id="bt_reset" class="button_small">
					<s:text name="system.search.reset.title" />
				</button>
			</div>
			</div>
		</form>

	</div>
	<div region="center" title="" border="false">
		<table id="queryList" border="false"></table>

	</div>
	<div id="mm" class="easyui-menu" style="width: 120px;">
		<div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
	</div>
	<div id="pl_tt">
		<a href="#" id="a_switch_query" class="icon-menu" title="<s:text name="system.button.switchQueryType.title"/>"
			style="display: none;"></a>
		<a href="#" id="a_exp_clp" class="accordion-expand" title="<s:text name="system.button.expand.title"/>"></a> 	
	</div>
</body>
</html>