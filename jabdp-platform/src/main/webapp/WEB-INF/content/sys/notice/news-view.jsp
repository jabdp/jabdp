<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title" /></title>
<%@ include file="/common/meta-gs.jsp"%>
<script type="text/javascript">
	//定义一个全局变量保存操作状态
	var operMethod = "${operMethod}";
	function doInitForm(data, callFunc) {
		if (callFunc) {
			callFunc();
		}
		if (data.msg) {
			var jsonData = data.msg;
			if(operMethod=="edit"){
				$("#showVlaue").html("<s:text name="system.sysmng.news.draft.title"/>");
				jsonData["status"] ="0";
				addState();
			}else if(operMethod=="view"){
				if (jsonData["status"] == "1") {
					$("#showVlaue").html("已发布");
					viewAnnounceState();
				} else if (jsonData["status"] == "0") {
					$("#showVlaue").html("<s:text name="system.sysmng.news.draft.title"/>");
					viewDraftState();
				}
			}
			$('#viewForm').form('load', jsonData);
			$("#tt").tabs('add', {
			    title : '<s:text name='system.sysmng.notice.readDetail.title'/>',
			    href : '${ctx}/sys/notice/visitor-grid.action?id=' + $("#id").val()
			}).tabs('select', '新闻详情');
		}
	}
	var setting = {
			check: {
				enable: true,
				chkboxType: { "Y": "", "N": ""}
				
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};
	function initOrganization(){
		var options = {
				url:'${ctx}/sys/account/organization!ztreeList.action',
				success:function(data) {
					if(data.msg) {
					$.fn.zTree.init($("#organizationTree"),setting,data.msg);
					zTree = $.fn.zTree.getZTreeObj("organizationTree");
					//zTree.expandAll(true);
					}
				}
		};	
		fnFormAjaxWithJson(options, true);
	}
	//已选中的组织
	  function getOrganization(newsId,status) {			
		  var options = {
					url:'${ctx}/sys/account/organization!ztreeList.action',
					data : {
						"id" :newsId
					},
					success:function(data) {
						if(data.msg) {
							$.fn.zTree.init($("#organizationTree"),setting,data.msg);
							 zTree = $.fn.zTree.getZTreeObj("organizationTree");
							if(status=="view"){
							zTree.setting.callback.beforeCheck=zTreeBeforeCheck;
								}
							//zTree.expandAll(true);
							}
					}
			};
		  fnFormAjaxWithJson(options, true);
		} 
	
	  function zTreeBeforeCheck(treeId, treeNode) {
		    return false;
		};
	function getIds(){
			var zTree = $.fn.zTree.getZTreeObj("organizationTree");
			var nodes = zTree.getCheckedNodes(true);
			var nodeIds = [];
			$.each(nodes, function(k,v) {
				nodeIds.push(v.id);
			});
			$("#organizationId").val(nodeIds.join(","));
		}
	function initRichTextBox() {
		var config = {
			/* toolbar : [
					[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList',
							'-', 'Link', 'Unlink' ], [ 'UIColor', 'Preview' ] ],
			customConfig : 'config_en.js' */
		};
		$('.jquery_ckeditor').ckeditor(config);
	}

	$(document).ready(function() {
		typeTree();
		initRichTextBox();
		if (operMethod == "add") {
			//新增 
			addState();
			focusFirstElement();
			initOrganization();
		} else if (operMethod == "view") {
			//查看
			getOrganization("${param.id}",operMethod);
			doModify("${param.id}", doDisabled);
		} else if (operMethod == "edit") {
			//复制
			getOrganization("${param.id}",operMethod);
			doModify("${param.id}", doEnable);
			/*$("tsave").show();
			$("#repeal").show();
			$("#release").show();
			$('#tcopy').hide();
			$('#tadd').hide();
			$('#tedit').hide(); */
		}
	});

	//设置焦点
	function focusFirstElement() {
		focusEditor("title");
	}

	//将控件设置成可编辑状态
	function setFormToEdit() {
		/*var status = $("#status").val();
		 if (status == "1") {
			$('#release').hide();
		} else if (status == "0") {
			$('#repeal').hide();
		} */
		addState();
		operMethod = "view";
		var zTree = $.fn.zTree.getZTreeObj("organizationTree");
		zTree.setting.callback.beforeCheck=true;
		doEnable();
		focusFirstElement();
	}

	function doCopyMainTab() {
		var id = $("#id").val();
		if (id == null) {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.copy"/>',
							'info');
			return;
		} else {
			parent.addTab(
					'<s:text name="system.sysmng.news.copy.title"/>-' + id,
					'${ctx}/sys/notice/news!edit.action?id=' + id);
		}
	}

	//查询公用类型表
	function typeTree(id) {
		var options = {
			url : '${ctx}/sys/notice/news!queryNewsType.action',
			success : function(data) {
				initTypeTree(data.msg, id);
			},
			async : false
		};
		fnFormAjaxWithJson(options, true);
	}

	//初始化类型选择combobox
	function initTypeTree(data, id) {
		//调用gscommon里的初始化方法
		selectType(data, "type", {
			"key" : "id",
			"caption" : "name"
		}, function() {
			$("#aboutType").window("open");
			focusEditor("newType");
		});
		//每次将新增的值设为默认值
		if (id) {
			$('#type').combobox('setValue', id);
		}
	}

	function doCanel() {
		typeTree();
		$("#aboutType").window("close");
	}

	function doUpdate() {
		var newType = $("#newType").val();
		addTypeTree(newType);
	}

	//新增新闻类型
	function addTypeTree(type) {
		var flag = $("#typeForm").form("validate");
			if (flag) {
				var options = {
						url : '${ctx}/sys/notice/news!addNewsType.action',
						data : {
							"name" : type
						},
						success : function(data) {
							//重新查一遍，刷新combobox里的值
							typeTree(data.msg);
							$("#aboutType").window("close");
						}
					};
					fnFormAjaxWithJson(options, true);
			}
		
	}

	//页面数据初始化
	function doModify(id, callFunc) {
		var options = {
			url : '${ctx}/sys/notice/news!queryNews.action',
			data : {
				"id" : id
			},
			success : function(data) {
				//doAfterSave();//修改保存后刷新父页面
				doInitForm(data, callFunc);
				//getOrganization(id,operMethod);
			}
		};
		fnFormAjaxWithJson(options, true);
	}

	function saveObj(st) {
		if (st == "1") {
			$("#status").val("1");
		} else if (st == "0") {
			$("#status").val("0");
		}
		if (operMethod == "edit" || operMethod == "add") {
			$("#id").val("");
			$("#atmId").val("");
			operMethod = "view";
			}
		getIds();
		var options = {
			url : '${ctx}/sys/notice/news!save.action',
			success : function(data) {
				doModify(data.msg, doAfterSave);
			}

		};
		fnAjaxSubmitWithJson("viewForm", options);
	}

	function doRepeal(st) {
		var id = $("#id").val();
		if (st == "1") {
			$("#status").val("1");
		} else if (st == "0") {
			$("#status").val("0");
		}
		if (operMethod == "edit" || operMethod == "add") {
			$("#id").val("");
		}
		var options = {
			url : '${ctx}/sys/notice/news!repeal.action',
			data : {
				"id" : id,
				"status" : st
			},
			success : function(data) {
				doModify(data.msg, doAfterSave);
				$("#tt").tabs("close","<s:text name='system.sysmng.notice.readDetail.title'/>");
			}

		};
		fnFormAjaxWithJson(options, true);		
	}

	function doAfterSave() {
		if (window.top.refreshTabData) {
			window.top.refreshTabData("<s:text name='system.sysmng.news.manage.title'/>");
		}
		operMethod = "view";
		doDisabled();
	}

	function doEnable() {
		$("input[type!=hidden],textarea,select").removeAttr("disabled");
		$("input.easyui-combobox").combobox('enable');
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(false);
			} catch (e) {
				$(this).ckeditorGet().setReadOnly(false);
			}
		});
	}

	//将页面上的控件置为不可编辑状态
	function doDisabled() {
		$("input[type!=hidden],textarea,select").attr("disabled", "disabled");
		$("input.easyui-combobox").combobox('disable');
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(true);
			} catch (e) {
				$(this).ckeditorGet().setReadOnly(true);
			}
		});
	}

	function formClear() {
		$("#viewForm")[0].reset();
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setData("");
			} catch (e) {
				$(this).ckeditorGet().setData("");
			}
		});
	}

	//查看里的新增
	function doAdd() {
		addState();
		operMethod = "add";
		doEnable();
		formClear();
		initOrganization();
		$("#status").val("0");
		$("#showVlaue").html("<s:text name='system.sysmng.news.draft.title'/>");
		$("#tt").tabs("close","<s:text name='system.sysmng.notice.readDetail.title'/>");
	}

	//添加附件
	function doAccessory() {
		var attachId = $("#atmId").val();
		var id = $("#id").val();
	    var options ={"attachId":attachId,
    				 "id":id,
    				 "key":"",
    				 "entityName":"",
    				 "dgId":"queryList",
    				 "flag":"mainEdit",
    				 "readOnly" : false
    					};
		doAttach(options);
	}

	//更新附件ID flag: mainQuery-主实体查询，subEdit-子实体编辑，mainEdit-主实体编辑
	function doUpdateAttach(opts) {
		var options = {
			url : '${ctx}/sys/notice/news!updateNews.action',
			data : {
				"entityName" : opts.entityName,
				"id" : opts.id,
				"atmId" : opts.attachId
			},
			async : false,
			success : function(data) {
				if (data.msg) {
					 $("#atmId").val(opts.attachId);
					 if (parent.refreshTabData) {
							parent.refreshTabData("<s:text name='system.sysmng.news.manage.title'/>");
						}
					//doAfterSave();
					/* if(flag == "mainQuery") {
						$("#" + dgId).datagrid("reload");
					} else if(flag == "subEdit"){
						$("#" + dgId).edatagrid("reload");
					} */
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	}
	
	function addState(){//新增状态  只显示“关闭”，“保存”
		$('#tsave').show();//保存，关闭按钮由状态判断
		$('#tadd').hide();//新增
		$('#tedit').hide();//修改
		$('#tattach').hide();//附件
		$('#tcopy').hide();//复制
		$('#repeal').hide();//取消发布
		$('#release').hide();//正式发布
	}
	function viewDraftState(){//(草稿)的查看状态  
		$('#tsave').hide();//保存，关闭按钮由状态判断
		$('#tadd').show();//新增
		$('#tattach').show();//附件
		$('#tcopy').show();//复制
		
		//草稿才可以修改和正式发布
		$('#tedit').show();//修改
		$('#release').show();//正式发布
		$('#repeal').hide();//取消发布
	}
	function viewAnnounceState(){//(已发布)的查看状态  
		$('#tsave').hide();//保存，关闭按钮由状态判断
		$('#tadd').show();//新增
		$('#tattach').show();//附件
		$('#tcopy').show();//复制
		
		//已发布的隐藏修改和正式发布
		$('#repeal').show();//取消发布
		$('#tedit').hide();//修改
		$('#release').hide();//正式发布
	}

</script>
</head>
<body class="easyui-layout" fit="true" title="maintable-view">
	<div region="north" style="overflow: hidden;" border="false">
		<div class="datagrid-toolbar">
			<c:if test="${operMethod=='view'}">
				<a id="tclose"
					href="javaScript:window.parent.closeTab('<s:text name="system.sysmng.news.view.title"/>-${param.id}')"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/></a>
			</c:if>
			<c:if test="${operMethod=='add'}">
				<a id="tclose"
					href="javaScript:window.parent.closeTab('<s:text name="system.sysmng.news.add.title"/>')"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/></a>
			</c:if>
			<c:if test="${operMethod=='edit'}">
				<a id="tclose"
					href="javaScript:window.parent.closeTab('<s:text name="system.sysmng.news.copy.title"/>-${param.id}')"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/></a>
			</c:if>
			<a id="tsave" href="javascript:saveObj('0');"
				class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/><s:text name="system.sysmng.news.draft.title"/></a>
			<security:authorize
				url="/sys/attach/attach.action?entityName=contracts.MainTable">
				<a id="tattach" href="javaScript:doAccessory();"
					class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name='system.button.accessory.title'/></a>
			</security:authorize>
			<a id="tadd" href="javaScript:doAdd();" class="easyui-linkbutton"
				plain="true" iconCls="icon-add"><s:text name="system.button.add.title"/></a> <a id="tedit" href="#"
				onclick="setFormToEdit();" class="easyui-linkbutton" plain="true"
				iconCls="icon-edit"><s:text name="system.button.modify.title"/></a> <a id="tcopy"
				href="javaScript:doCopyMainTab();" class="easyui-linkbutton"
				plain="true" iconCls="icon-copy"><s:text name="system.button.copy.title"/></a> <a id="repeal"
				href="javaScript:doRepeal('0');" class="easyui-linkbutton"
				plain="true" iconCls="icon-undo"><s:text name="system.sysmng.news.cancelAnnounce.title"/></a> <a id="release"
				href="javaScript:doRepeal('1');" class="easyui-linkbutton"
				plain="true" iconCls="icon-ok"><s:text name="system.sysmng.news.announce.title"/></a>
		</div>
	</div>
	<div region="center" style="position: relative; overflow: auto;" border="false">
		<form action="" method="post" name="viewForm" id="viewForm"
			style="margin: 0px;">
			<input type="hidden" id="id" name="id" /> <input type="hidden"
				id="atmId" name="atmId" />
				<div id="tt" class="easyui-tabs">
				<div selected="true" title="<s:text name="新闻详情"/>" style="padding: 2px;">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tr>
					<th><label><s:text name="system.sysmng.desktop.modelTitle.title"/></label></th>
					<td><input type="text" name="title" id="title"
						class="easyui-validatebox Itext" required="true" style="width: 400px" />
					</td>
					<th><label><s:text name="system.sysmng.desktop.key.title"/></label></th>
					<td><input type="text" name="keyworlds" id="keyworlds"
						class="easyui-validatebox Itext" style="width: 400px" />
					</td>
				</tr>
				<tr>
					<th><s:text name="system.sysmng.notice.content.title"/></th>
					<td colspan="3"><textarea class="jquery_ckeditor"
							id="contractrichTextBox" name="content"></textarea></td>
				</tr>
				<tr>
					<th><label><s:text name="system.sysmng.news.newsType.title"/></label>
					</th>
					<td><input id="type" class="easyui-combobox" name="type"
						required="true" style="width: 200px;"> </input>
					</td>
					<th><label><s:text name="system.sysmng.notice.status.title"/></label>
					</th>
					<td><input type="hidden" name="status" id="status" value="0" />
						<div id="showVlaue"><s:text name="system.sysmng.news.draft.title"/></div>
					</td>
				</tr>
				<tr>
					<th><label ><s:text name="组织结构"/>:</label>
						</th>
							<td colspan="3">
						<input type="hidden" name="organizationIds" id="organizationId"/>
							<ul id="organizationTree" class="ztree"></ul>	
					</td>		
				</tr>
				
			</table>
			</div></div>
		</form>

		<div style="visibility: hidden;">
			<div id="aboutType" class="easyui-window" closed="true"
				minimizable="false" modal="true" title="<s:text name="system.sysmng.news.addNewsType.title" />"
				style="width: 280px; height: 160px;">
				<div style="text-align: center; padding: 2px 2px;"
					class="easyui-layout" fit="true">
					<div region="center" border="false">
						<form action="" name="passForm" id="typeForm" method="post"
							style="margin: 0px;">
							<table align="center" border="0" cellpadding="0" cellspacing="1"
								class="table_form">
								<tbody>
									<tr>
										<th><label for="newType"><s:text name="system.sysmng.news.newsType.title" />:</label></th>
										<td><input type="text" name="newType" id="newType"
											class="easyui-validatebox" required="true" /></td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div region="south" border="false" style="text-align: center;height:40px;padding:4px;">
						<a id="mm" class="easyui-linkbutton" iconCls="icon-ok"
							href="javascript:void(0);" onclick="doUpdate();"><s:text
								name="system.button.ok.title" /> </a> <a class="easyui-linkbutton"
							iconCls="icon-cancel" href="javascript:void(0);"
							onclick="doCanel();"><s:text name="system.button.cancel.title" /> </a>
					</div>
				</div>
			</div>
		</div>

	</div>

</body>
</html>
