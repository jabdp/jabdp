<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><s:text name="数据共享设置"/></title>
<%@ include file="/common/meta-css.jsp" %>
</head>
<body class="easyui-layout">
	<div region="center" title="" border="false">
		<div>
			<input type="hidden" name="entityName" id="entityName" value="${param.entityName}"/>
			<input type="hidden" name="id" id="id" value="${param.id}"/>
			<input type="hidden" name="userIds" id="userId"/>
			<ul id="userTree" class="ztree"></ul>
		</div>
	</div>
	<%@ include file="/common/meta-js.jsp" %>
	<script type="text/javascript">
	var settingZtree = {
		check : {
			enable : true
		},
		data : {
			simpleData : {
				enable : true
			}
		}
	};

	//初始化当前组织下的用户树
	function initUserZtree(org_id) {
		var options = {
			url : '${ctx}/sys/account/authority!userList.action',
			data : {
				"orgId" : 1 //org_id
			},
			success : function(data) {
				if (data.msg) {
					$.fn.zTree.init($("#userTree"), settingZtree, data.msg);
					
					initUserZtreeCheckStatus();
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	}
	//初始化用户选中状态
	function initUserZtreeCheckStatus() {
		var id = $("#id").val();
		if(id) { //选中用户
			var entityName = $("#entityName").val();
			var options = {
					url : '${ctx}/gs/data-share-config!view.action',
					data : {
						"entityName":entityName,
						"entityId":id
					},
					success : function(data) {
						var shareUserIds = data.msg;
						if(shareUserIds && shareUserIds.length) {
							for(var i=0,len=shareUserIds.length;i<len;i++) {
								var shareUserId = shareUserIds[i];
								doCheckNodesOnTree("userTree","id","user_"+shareUserId,true);
							}
						}
					}
			};
			fnFormAjaxWithJson(options, true);
		}
	}
	
	//保存数据共享设置
	function saveDsc(dataObj) {
		var obj = getIds();
		var names = obj["names"];
		var log = "由【"+$curUserName$+"】";
		if (names.length) {
			log = log + "共享数据给【" + names.join(",") +"】";
		} else {
			log = log + "取消数据共享";
		}
		var rtnFlag = false;
		var options = {
			url : '${ctx}/gs/data-share-config!save.action?entityName=' + dataObj.entityName,
			data: {
				"entityIds":dataObj.entityIds,
				"ids":obj["ids"],
				"log":log
			},
			async : false,
			traditional:true,
			success : function(data) {
				if(data.msg) {
					alert("数据共享设置完成！");
					rtnFlag = true;
				}
			}
		};
		fnFormAjaxWithJson(options, true);
		return rtnFlag;	
	}
	//获取所有树上的选取的节点ID
	function getIds() {
		return getIdsByKey('user', "userId");
	}
	
	function getIdsByKey(method, key) {
		var vKey = key || "id"; 
		var zTree = $.fn.zTree.getZTreeObj(method + "Tree");
		var nodes = zTree.getCheckedNodes(true);
		var nodeIds = [];
		var nodeNames = [];
		$.each(nodes, function(k, v) {
			if(v[vKey]) {
				nodeIds.push(v[vKey]);
				nodeNames.push(v["name"]);
			}
		});
		var ids = nodeIds.join(",");
		$("#" + method + "Id").val(nodeIds.join(","));
		return {"ids":nodeIds,"names":nodeNames};
	}
	
	//选中或取消选中
	function doCheckNodesOnTree(treeId, paramName, paramVal, checkFlag) {
		var treeObj = $.fn.zTree.getZTreeObj(treeId);
		var nodes = treeObj.getNodesByParam(paramName, paramVal, null);
		for (var i=0, l=nodes.length; i < l; i++) {
			treeObj.checkNode(nodes[i], checkFlag, true);
		}
	}
	
	function initMenu() {
		initUserZtree();
	}
	
	$(document).ready(function() {
		initMenu();
	});
	</script>
</body>
</html>