<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/meta.jsp"%>
</head>  
<body class="easyui-layout" >
   <div region="center"  style="overflow:hidden;" border="false">
   		<div class="easyui-tabs" fit="true" border="false">
   			<div title="<s:text name="system.sysmng.process.flowDiagram.title"/>"  >
			<iframe  src="${ctx}/gs/process!view.action?processInstanceId=${param.processInstanceId}&entityName=${param.entityName}" frameborder="0" style="width:100%;height:100%"></iframe>		
			</div>
			<div title="<s:text name="system.sysmng.process.history.title"/>"   >
			<iframe  src="${ctx}/sys/workflow/approve-his-log.action?processInstanceId=${param.processInstanceId}&entityName=${param.entityName}&id=${param.id}" frameborder="0" style="width:100%;height:100%"></iframe>
			</div>
   		</div>
   </div>
</body>
</html>