<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/meta-css.jsp"%>
</head>  
<body>
  <div class="easyui-layout" fit="true">
   <div region="center"  style="overflow:hidden;" border="false">
   	<div id="ttt" fit="true" border="false" >
   	<!--  重新发送环节或者修改单据-->
   	<c:choose>
	   <c:when test="${restart==1 || modify=='1'}">
	   		<div  title="<s:text name="审批单据"/>" style="overflow:hidden;"> 
				<iframe id="_edit_form_frm_" src="" jpsrc="${ctx}/gs/gs-mng!view.action?isEdit=1&entityName=${bname}&id=${bid}" frameborder="0" style="width:100%;height:100%"></iframe>		
			</div>
	   </c:when>
   	   <c:when test="${not empty modify && modify=='2'}"><%-- 查看单据/修订单据 --%>
	   		<div  title="<s:text name="审批单据"/>" style="overflow:hidden;"> 
				<iframe  src="" jpsrc="${ctx}/gs/gs-mng!view.action?isEdit=2&entityName=${bname}&id=${bid}&isChangeTitle=false" frameborder="0" style="width:100%;height:100%"></iframe>		
			</div>
	   </c:when>
   	   <c:when test="${not empty modify}"><%-- 自定义表单 --%>
   			<div  title="<s:text name="审批单据"/>" style="overflow:hidden;">
				<iframe  src="" jpsrc="${ctx}/gs/gs-mng!openFormView.action?id=${bid}&formName=${modify}" frameborder="0" style="width:100%;height:100%"></iframe>		
			</div>
   	   </c:when>
  	</c:choose>  	
					
   	<c:if test="${isRuningMng !=true}">
   		<c:if test="${empty rptNameList}">
   		<div  title="<s:text name="system.sysmng.process.report.title"/>"  tools="#p-tools" style="overflow:hidden;"> 
			<iframe  src="" jpsrc="${ctx}/sys/report/report.action?id=${bid}&entityName=${bname}&rptName=" frameborder="0" style="width:100%;height:100%"></iframe>		
			<div id="p-tools">
				<a href="#" class="icon-mini-refresh" onclick="refresh()"></a>
			</div>
		</div>
		
		</c:if>		
	
   		<c:if test="${!empty rptNameList}">
			<c:forEach var="rpt" items="${rptNameList}" varStatus="status" >
				<div  title="${rpt.value}-${status.index}" tools="#p-tools_${status.index}" style="overflow:hidden;"> 
					<iframe src="" jpsrc="${ctx}/sys/report/report.action?id=${bid}&entityName=${bname}&rptName=${rpt.key}" frameborder="0" style="width:100%;height:100%"></iframe>	
					<div id="p-tools_${status.index}">
						<a href="#" class="icon-mini-refresh" onclick="refresh('${rpt.key}')"></a>
					</div>
				</div>		
			</c:forEach>
		</c:if>	
	</c:if>		
	<!-- 流程图跟踪页面-->
			<div title="<s:text name="system.sysmng.process.flowDiagram.title"/>"  style="overflow:hidden;">
			<iframe src="" jpsrc="${ctx}/gs/process!view.action?processInstanceId=${processInstanceId}&entityName=${bname}" frameborder="0" style="width:100%;height:100%"></iframe>		
			</div>
			<div title="<s:text name="system.sysmng.process.history.title"/>"   style="overflow:hidden;">
			<iframe src="" jpsrc="${ctx}/sys/workflow/approve-his-log.action?processInstanceId=${processInstanceId}&id=${bid}&entityName=${bname}" frameborder="0" style="width:100%;height:100%"></iframe>		
			<%-- <table class="easyui-datagrid" id="queryList" border="false"></table>--%>
			</div>	
		</div>	
		</div>
		<!--动态设置“同意”和”不同意“自定义按钮 -->
<c:if test="${isHistory !=true}">
  <div region="south" style="overflow:hidden; text-align: center; padding:6px;height:50px;" split="true">
  	<c:if test="${not empty approveDesp}">
  	<div class="easyui-layout" fit="true">
  		<div region="west" style="width:40%;" border="false">
  		<a href="javascript:doChangeHisTab();" title="点击查看审批历史信息"><span style="color:red;">审批备注：${approveDesp}</span></a>
  		</div>
  		<div region="center" border="false">
  	</c:if>	
		<c:if test="${restart!=1}">
				<c:if test="${empty approveTitle || approveTitle != 'none'}">
				<a id="agree" class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="agree(1)"> 
					<c:choose>
						<c:when test="${!empty approveTitle}">
   			    			${approveTitle}
   						</c:when>
						<c:otherwise>
							<s:text name="system.sysmng.process.approve.title" />
						</c:otherwise>
					</c:choose> 
				</a>
				</c:if>
				<c:if test="${empty rejectTitle || rejectTitle != 'none'}">
				<a id="reject" class="easyui-linkbutton"iconCls="icon-cancel" 
				href="javascript:void(0)" onclick="reject(0)">
					<c:choose>
						<c:when test="${!empty rejectTitle}">
   			    			${rejectTitle}
   						</c:when>
						<c:otherwise>
							<s:text name="system.sysmng.process.reject.title"/> 
						</c:otherwise>
					</c:choose> 
				</a>
				</c:if>
				<c:if test="${!empty button}">
					<a id="dynamicBt" class="easyui-linkbutton" href="javascript:void(0)" onclick="agree(2)">${button}</a> 
				</c:if>							
		</c:if>	
		<c:if test="${restart==1}">
			<c:if test="${empty approveTitle || approveTitle != 'none'}">
			<a  id="agree" class="easyui-linkbutton" iconCls="icon-ok" 
					href="javascript:void(0)" onclick="againSend(1)"><s:text name="system.sysmng.process.resend.title"/></a> 
			</c:if>		
			<c:if test="${empty rejectTitle || rejectTitle != 'none'}">
			<a  id="reject"  class="easyui-linkbutton"iconCls="icon-cancel" 
				href="javascript:void(0)" onclick="cancelSend(0)"><s:text name="system.sysmng.process.cancel.title"/> </a>	
			</c:if>		
		</c:if>
				<a id="pass" class="easyui-linkbutton" iconCls="icon-redo"  
				href="javascript:void(0)" onclick="doPassOn()"><s:text name="system.sysmng.process.passOn.title"/></a>	
				<a id="description" class="easyui-linkbutton" iconCls="icon-edit"  
				href="javascript:void(0)" onclick="doDaiBanMiaoShu()"><s:text name="待办描述"/></a>
	<c:if test="${not empty approveDesp}">			
			</div>
		</div>
	</c:if>
	</div>	
	</c:if>
   </div>
   <div style="display:none;">
<!-- 设置自定义选择审批人页面 -->
	<div id="handle_message" closed="true" modal="true" title="<s:text name="确认信息"/>"
		style=" width: 500px; height:450px; padding: 0px;">
	 <div class="easyui-layout" fit="true">
		<div region="center" border="false"
			style="padding: 2px; background: #fff; overflow: auto; border: 1px solid #ccc;">
			<form action="" name="handleForm" id="handleForm" method="post">
				<input type="hidden" id="taskId" name="taskId" value="${task.id}" />
				<input type="hidden" id="taskName" name="taskName"
					value="${task.name}" /> <input type="hidden" id="executionId"
					name="executionId" value="${task.executionId}" /> <input
					type="hidden" id="processInstanceId" name="processInstanceId"
					value="${task.processInstanceId}" /> <input type="hidden"
					id="approve" name="activiti_approve" /> <input type="hidden"
					id="resend" name="activiti_resend" />
				<table align="center">
					<tbody>
						<tr id="userSel">
							<th><label><s:text
										name="system.sysmng.process.customUser.title" />:</label></th>
							<td><input type="hidden" name="userIds" id="userIds" />
								<ul style="text-align: center; width: 250px;"
									id="roleIds" class="ztree"></ul></td>
						</tr>
						<tr id="reason">
							<th><label><s:text
										name="意见（原因）" />：</label></th>
							<td><textarea name="reason"
									style="text-align: left; width: 250px; height: 120px;"></textarea>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
			</div>
			<div region="south" border="false" style="text-align: center; padding:6px;height:40px;">
				<a class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="doComplete()"><s:text
						name="system.button.submit.title" /></a> <a class="easyui-linkbutton"
					iconCls="icon-cancel" href="javascript:void(0)"
					onclick="doCancel()"><s:text name="system.button.cancel.title" />
				</a>
			</div>
		</div>
	</div>
	<!-- 自定义选择下推数据的接收人 -->
	<div id="selFormUser" closed="true" modal="true"
		title="<s:text name="system.sysmng.authority.setUser.title"/>"
		style="width: 500px; height: 350px; padding: 0px;">
		<div class="easyui-layout" fit="true">
		<div region="center" border="false"
			style="padding: 2px; background: #fff; overflow: auto; border: 1px solid #ccc;">
		<table align="center">
			<tbody>
				<tr>
					<th><label><s:text name="设置下推数据接收人" />:</label></th>
					<td><input type="hidden" name="selUserId" id="selUserId" />
						<ul style="text-align: center; width: 250px; height: 150px;"
							id="formUser" class="ztree"></ul></td>
				</tr>
			</tbody>
		</table>
		</div>
		<div region="south" border="false" style="text-align: center; padding:6px;height:40px;">
			<a class="easyui-linkbutton" iconCls="icon-ok"
				href="javascript:void(0)" onclick="setFormUser()"><s:text
					name="system.button.submit.title" /></a> <a class="easyui-linkbutton"
				iconCls="icon-cancel" href="javascript:void(0)"
				onclick="javascript:$('#selFormUser').window('close');"><s:text
					name="system.button.cancel.title" /> </a>
		</div>
		</div>
	</div>
	<!-- 设置任务代办描述 -->
	<div  id="dc" closed="true" modal="true" title="<s:text name="待办描述"/>"
		style=" width: 500px; height:350px; padding: 0px;">
		<div class="easyui-layout" fit="true">
		<div region="center" border="false"
			style="padding: 2px; background: #fff; overflow: auto; border: 1px solid #ccc;">
			<form action="" name="descriptionForm" id="descriptionForm" method="post">
					<table align="center">
						<tbody>	
						<tr id="descriptionContent">
							<th><label ><s:text name="描述内容"/>:</label>
								</th>
								<td>
							<textarea style="text-align: left; width: 250px;height:120px;"  name="description" id ="description">${description}</textarea>	
							</td>			
						</tr>			
					</tbody>	
				</table>
			</form>
			</div>	
			<div region="south" border="false" style="text-align: center; padding:6px;height:40px;">
				<a  class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="doDescription(${task.id})"><s:text name="system.button.submit.title"/></a> 
				<a class="easyui-linkbutton" iconCls="icon-cancel" href="javascript:void(0)" onclick="$('#dc').window('close')"><s:text name="system.button.cancel.title"/></a>
			</div>	
			</div>
	</div> 
	<!-- 任务转派页面 -->
	<div id="pass_win" closed="true" modal="true" title="<s:text name="system.sysmng.process.setPassOnUser.title"/>"
		style=" width: 400px; height:120px; padding: 0px;">	
		<iframe  src="${ctx}/gs/process!passOn.action?taskId=${task.id}" frameborder="0" style="width:100%;height:100%"></iframe>
	</div>	
	</div>
<%@ include file="/common/meta-js.jsp"%>	
<script type="text/javascript">
	$(document).ready(function() {
		$('#ttt').tabs({
			 onSelect: function(title,index) {
				 var tb = $('#ttt').tabs('getTab',title);
					if(tb) {
						var tbby = tb.panel("body");
						if(tbby) {
							var tb_ifr = tbby.find("iframe");
							if(tb_ifr) {
								var jpsrc = tb_ifr.attr("jpsrc");
								if(jpsrc && !tb_ifr.attr("src")) {
									tb_ifr.attr("src", jpsrc);
								}
							}
						}
					}
			 }
		});
		
		<%-- $("#queryList").datagrid(getOption());--%>
		$("#handle_message").window({
			width:500,
			height:450,
			onOpen:function() {
				$(this).window("move", {
					top:($(window).height()-450)*0.5,
					left:($(window).width()-500)*0.5
				});
			}
		});
		$("#selFormUser").window({
			width:500,
			height:350,
			onOpen:function() {
				$(this).window("move", {
					top:($(window).height()-350)*0.5,
					left:($(window).width()-500)*0.5
				});
			}
		});
		$("#dc").window({
			width:500,
			height:350,
			onOpen:function() {
				$(this).window("move", {
					top:($(window).height()-350)*0.5,
					left:($(window).width()-500)*0.5
				});
			}
		});
		$("#pass_win").window({
			width:400,
			height:120,
			onOpen:function() {
				$(this).window("move", {
					top:($(window).height()-120)*0.5,
					left:($(window).width()-400)*0.5
				});
			}
		});
	});

	//初始化流程流程的datagrid历史记录
var pId = "${processInstanceId}";
<%--function getOption() {
	return {
		width : 'auto',
		height : 'auto',
		fit: true,
		nowrap : false,
		striped : true,
		url : '${ctx}/gs/process!getProcessInfoList.action?processInstanceId=' + pId,
		sortName : 'id',
		sortOrder : 'desc',
		remoteSort: true,
		idField : 'id',
		pageSize: 5,
		pageList: [5, 10, 20],
		columns : [ [
				{
					field : 'dealName',
					title : '<s:text name="system.sysmng.process.dealUser.title"/>',
					width : 180,
					sortable : true
				},
				{
					field : 'dealTime',
					title : '<s:text name="system.sysmng.process.dealTime.title"/>',
					width : 100,
					sortable : true
				},
				{
					field : 'taskName',
					title : '<s:text name="system.sysmng.process.task.title"/>',
					width : 180,
					sortable : true
				},
				{
					field : 'isApprove',
					title : '<s:text name="system.sysmng.process.isApprove.title"/>',
					width : 100,
					align : 'center',
					formatter:function(value,rowData,rowIndex){
						if(value==1){
							return "同意";
						}else if(value==0){
							return "不同意";
						}
						else{
							return "";
						}
					}
				}, {
					field : 'reason',
					title : '<s:text name="system.sysmng.process.reason.title"/>',
					width : 400,
					align : 'center'
				}
		] ],
		pagination : true,
		rownumbers : true
	};
}--%>

var setting = {
		check: {
			enable: true
		},
		data: {
			simpleData: {
				enable: true
			}
		}
	};
//根据下一个环节的审批角色，判断是否弹出，自定义选择审批人的窗口
 function initMenu(name,bn)  {
	 var pId = $("#processInstanceId").val();
		var options = {
				url : '${ctx}/gs/process!getRolesList.action',
				data : {
					"processInstanceId":pId,
					"taskId": "${task.id}",
					"variableName" : name,
					"value" :bn
				},
				async:false,
				success : function(data) {
					if (data.msg){
						if($.isArray(data.msg)){
							if(data.msg.length>1){//多个用户
								$.fn.zTree.init($("#roleIds"), setting, data.msg);
								zTree = $.fn.zTree.getZTreeObj("roleIds");
								zTree.expandAll(true);
								$("#handle_message").window("open");
							}else{//没有用户
								doComplete();
							}
						} else {//一个用户
								$("#resend").val(bn);
								$("#userIds").val(data.msg);
								doComplete();
						}
					}else{//没有用户
						doComplete();
					}				
				}
			};
			fnFormAjaxWithJson(options,true);
	} 
	//不同意时，初始化窗口
 function rejectMenu(name,bn)  {
	 var pId = $("#processInstanceId").val();
		var options = {
				url : '${ctx}/gs/process!getRolesList.action',
				data : {
					"processInstanceId":pId,
					"taskId": "${task.id}",
					"variableName" : name,
					"value" :bn
				},
				async:false,
				success : function(data) {
					$("#userSel").hide();
					if (data.msg){
						if($.isArray(data.msg)){
							if(data.msg.length>1){//多个用户
								$("#userSel").show();
								$.fn.zTree.init($("#roleIds"), setting, data.msg);
								zTree = $.fn.zTree.getZTreeObj("roleIds");
								zTree.expandAll(true);
							}
						} else {//一个用户
							$("#userIds").val(data.msg);
							if(bn == "1") {//同意时自动提交
								doComplete();
								return;
							}
						}
					} else {
						if(bn == "1") {//同意时自动提交
							doComplete();
							return;
						}
					}
					$("#handle_message").window("open");
				}
			};
			fnFormAjaxWithJson(options,true);
	} 
	//初始化化自定义选择用户窗口的用户树
 function getFormUser(roles){
	 var setting2 = {
				check: {
					enable: true,
					chkStyle: "radio",
					radioType: "level"
				},
				data: {
					simpleData: {
						enable: true
					}
				}
			};
		var options = {
				url : '${ctx}/gs/process!getRolesList.action',
				data : {
					"taskId": "${task.id}",
					"roles":roles
				},
				async:false,
				success : function(data) {
					if (data.msg){
						/*$.each(data.msg, function(k,v) {
							v.checked = false;
						});*/
							$.fn.zTree.init($("#formUser"), setting2, data.msg);
							var zTree = $.fn.zTree.getZTreeObj("formUser");
							zTree.expandAll(true);							
							$("#selFormUser").window("open");
					}
				}
			};
			fnFormAjaxWithJson(options,true);
 }
	//获取自定义选择用户的ID，然后设置到下一个环节的审批人员中
 function setFormUser(){
	var zTree = $.fn.zTree.getZTreeObj("formUser");
	var nodeIds = [];
	if(zTree!=null){
		var nodes = zTree.getCheckedNodes(true);		
		$.each(nodes, function(k,v) {
			nodeIds.push(v.id);
		});
	}
	 var userId = nodeIds.join(",");
	 var options = {
				url : '${ctx}/gs/process!setDataUser.action',
				data : {
					"taskId": "${task.id}",
					"userId": userId
				},
				success : function(data) {
					if (data.msg){
						$("#selFormUser").window("close");
						var bool = $("#approve").val();
						initMenu("approve",bool);
						$("#userSel").show();
					 	$("#reason").hide();
					}
				}
			};
			fnFormAjaxWithJson(options,true);
 }
//获取用户树中选择的ID
 function getIds(){
		var zTree = $.fn.zTree.getZTreeObj("roleIds");
		if(zTree!=null){
			var nodes = zTree.getCheckedNodes(true);
			if(nodes.length){
				var nodeIds = [];
				$.each(nodes, function(k,v) {
					nodeIds.push(v.id);
				});
				$("#userIds").val(nodeIds.join(","));				
			}else{
				$.messager.alert('提示信息','至少选择一个办理人员','info');
				return false;
			}
		}
		return true;
	}
	//点击“同意”时触发的事件
 function agree(bool){
	/*$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.sureAgree"/>', function(r) {
		if(r) {
			 
		}
	 });*/
	 if(!doSaveFormBeforeSubmit()) return;
	 $("#approve").val(bool);
	 var name ="approve";
	 var roles = "${roles}";
	 if(roles){		
		getFormUser(roles);
	 }else{
	 	/*initMenu(name,bool);
		$("#userSel").show();
	 	$("#reason").hide();*/	
		rejectMenu(name,bool);
	}
 }
//点击“不同意”时触发的事件
 function reject(bool){
	 if(!doSaveFormBeforeSubmit()) return;
	 var name ="approve";
	 $("#approve").val(bool);
	 rejectMenu(name,bool);
 }
 //完成任务
 function doComplete(){
	 var flag = true;	
	 flag = getIds();
	 if(flag){
			var options = {
					url : '${ctx}/gs/process!complete.action',
					success : function(data) {
						if (data.msg) {
							
							 doClose();
							
							//window.parent.closeTab('${task.name}');
						}
					}
				};
			fnAjaxSubmitWithJson('handleForm', options);
	}	 
 }
 //是否再次发送审批请求
 function  againSend(bn){
	 //自动保存方法
	 /*var tb = $('#ttt').tabs('getTab','<s:text name="system.sysmng.process.report.title"/>');
		if(tb) {
			var tbby = tb.panel("body");
			if(tbby) {
				var tb_ifr = tbby.find("iframe");
				if(tb_ifr) {
					var win = tb_ifr[0].contentWindow.window;
					if(win) {
						var status = win.operMethod;
						if(status=="modify"){
							if(win.doSaveObj){
								win.doSaveObj();
							}
						}
					}
				}
			}
		}*/
	 if(!doSaveFormBeforeSubmit()) return;
	 $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.sureAgree"/>', function(r) {
		 if (r) {
	 		var name ="resend";
	 		initMenu(name,bn);
	 		$("#resend").val(bn);
	 		$("#userSel").show();
		 }
	 });
 }
 //取消发送审批请求
 function  cancelSend(bn){
	 if(!doSaveFormBeforeSubmit()) return;
	 $("#handle_message").window("open");
	 $("#resend").val(bn);
	 $("#userSel").hide();
 }
 function doCancel() {
	 $("#handle_message").window('close');
	}
 function doHide(){
	 $("#agree").hide();
	 $("#reject").hide();
	 $("#pass").hide();
 }
 //打开任务转派窗口
 function doPassOn(){
	 $("#pass_win").window("open");
 }
 function doClose(){
	/*var name = $("#taskName").val();
	var id = $("#taskId").val();
	 window.parent.closeTab(name+'_'+id);*/
	if(window.parent.closeCurrentTab) {
		window.parent.closeCurrentTab();
	} 
 }
 //打开任务描述窗口
 function doDescription(taskId){
	    var pId = $("#processInstanceId").val();
		var options = {
				url : '${ctx}/gs/process!setTaskDescription.action',
				data : {
					"taskId" : taskId,
					"processInstanceId" : pId
				},
				success : function(data) {
					if (data.msg) {
						$('#dc').window('close');
					}
				}
			};
		fnAjaxSubmitWithJson('descriptionForm', options);
 }
 function refresh(){
	 var tb = $('#ttt').tabs('getSelected');
		if(tb) {
			var tbby = tb.panel("body");
			if(tbby) {
				var tb_ifr = tbby.find("iframe");
				if(tb_ifr) {
					var src = tb_ifr.attr("src");
					var win = tb_ifr[0].contentWindow.window;
					if(win) {
						win.location.replace(src);
					}
				}
			}
		}
 }
 function doDaiBanMiaoShu() {
	 $('#dc').window('open');
 }
 function doSaveFormBeforeSubmit() {
	 	if($("#_edit_form_frm_").length) {
	 		var tabWin = $("#_edit_form_frm_")[0].contentWindow.window;
	 		if(tabWin && tabWin.doSaveObj && tabWin.operMethod == "modify") {//修改状态下
				return tabWin.doSaveObj();
			} else {
				return true;
			}
	 	} else {
	 		return true;
	 	}
 }
 
 function doChangeHisTab() {
	 $('#ttt').tabs("select", "<s:text name="system.sysmng.process.history.title"/>");
 }
 </script>	
</body>
</html>