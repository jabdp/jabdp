<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="流程办理历史"/></title>
<%@ include file="/common/meta-gs.jsp" %>
	<script type="text/javascript">
		function getOption() {
			return {
				iconCls:"icon-search",
				width:600,
				height:350,
				border:false,
				nowrap: false,
				striped: true,
				fit: true,
				url:'${ctx}/gs/process!processHistory.action',
				sortName: 'startTime',
				sortOrder: 'desc',
				frozenColumns:[[
	                {field:'ck',checkbox:true},
	                {field:'moduleName',title:'<s:text name="system.sysmng.process.moduleName.title"/>',width:100,sortable:true}
				]],
				columns:[[
							{field:'name',title:'<s:text name="system.sysmng.process.taskName.title"/>',width:150},
							{field:'assignee',title:'<s:text name="system.sysmng.process.dealUser.title"/>',width:80},
							{field:'startTime',title:'<s:text name="system.sysmng.process.startTime.title"/>',width:140,sortable:true},
							{field:'endTime',title:'<s:text name="system.sysmng.process.endTime.title"/>',width:140,sortable:true}
						]],
				pagination:true,
				rownumbers:true,
				toolbar:[{
					id:'bt_view',
					text:'<s:text name="system.sysmng.process.searchProcessInfo.title"/>',
					iconCls:'icon-search',
					handler:function(){
						doViews();
					}
				},'-'],
				onDblClickRow :function(rowIndex, rowData){
					doView(rowData.processInstanceId);
				}
			};
		}

		function uniqueArr(ids) {
			var i = 0;
			while (i < ids.length) {
				var current = ids[i];
				for (k = ids.length; k > i; k--) {
					if (ids[k] === current) {
						ids.splice(k, 1);
					}
				}
				i++;
			}
			 return ids.sort();
		}
		function doViews() {
			var rows = $('#queryList').datagrid('getSelections');
			if (rows.length > 0){
				var ids = [];	
					for ( var i = 0; i < rows.length; i++) {
						ids.push(rows[i].processInstanceId);
					}
				ids = uniqueArr(ids);//去除重复数据
			for(var j = 0; j < ids.length; j++){
				var pId = ids[j];	
				doView(pId);
				}	
			} else {
				$.messager
						.alert(
								'<s:text name="system.javascript.alertinfo.title"/>',
								'<s:text name="system.javascript.alertinfo.recordInfo"/>',
								'info');
			}
		}
		//显示流程办理历史
		function doView(pId) {
			top.addTab('<s:text name="system.sysmng.process.dealHistory.title"/>-'+pId, 
					'${ctx}/gs/process!getProcess.action?processInstanceId=' + pId + '&isHistory=true');
		}
		function doQuery() {
			var param = $("#queryForm").serializeArrayToParam();
			$("#queryList").datagrid("load", param);
		}

		$(document).ready(function() {
			//focusEditor("loginUsername");
			doQuseryAction("queryForm");
			$("#queryList").datagrid(getOption());

			$("#endTimeStart").focus(function() {
				WdatePicker({
					maxDate : '#F{$dp.$D(\'endTimeEnd\')}'
				});
			});
			$("#endTimeEnd").focus(function() {
				WdatePicker({
					minDate : '#F{$dp.$D(\'endTimeStart\')}'
				});
			});

			$("#bt_query").click(doQuery);
			$("#bt_reset").click(function() {
				$("#queryForm").form("clear");
				doQuery();
			});
		});
	</script>
</head>
<body class="easyui-layout" fit="true">
		<div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">			
			<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="name"><s:text name="system.sysmng.process.taskName.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_name" id="name" class="Itext"></input></td>
						</tr>
						<tr>
							<th><label for="endTimeStart"><s:text name="system.sysmng.process.endTime.title"/>:</label></th>
							<td><input type="text" name="filter_GED_endTime" id="endTimeStart" readonly="readonly" class="Idate Wdate" style="width:160px;"></input>
								<br/>--<br/><input type="text" name="filter_LED_endTime" id="endTimeEnd" readonly="readonly" class="Idate Wdate" style="width:160px;"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
								<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div region="center" border="false" title="<s:text name="system.sysmng.process.processedInfoHistoryList.title"/>">
		<%-- <div id="tt" class="easyui-tabs" fit="true" border="false">
		<div  title="<s:text name="system.sysmng.process.processedInfoHistoryList.title"/>" >--%>
		<table id="queryList"></table>
		<%--</div>
		</div>--%>
		</div>
</body>
</html>

