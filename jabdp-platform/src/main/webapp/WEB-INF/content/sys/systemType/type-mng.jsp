<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.sysmng.authority.manager.title"/></title>
<%@ include file="/common/meta-gs.jsp"%>
<style>
span.tmpzTreeMove_arrow{z-index:999999;}
ul.ztree.zTreeDragUL {z-index:999999;}
</style>
<script type="text/javascript">
function checkTypeValUnique() {
	var value = $("#typeVal").val();
	var id = $("#id").val();
	var pid = $("#pid").val();
	var flag = true;
	if(pid==""){
		var options = {
			url : '${ctx}/sys/common/common-type!validateTypeVal.action',
			data : {
				"id" :id,
				"typeVal" : value
			},
			async:false,
			success : function(data) {
				flag = data.msg;
			}
		};
		fnFormAjaxWithJson(options,true);
	}
	return flag;
}

	var setting = {
		edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false,
				drag: {
					isCopy:false
				}
		},
		data : {
			simpleData : {
				enable : true,
				idKey: "id",
				pIdKey: "pid"
			}
		},
		view : {
			dblClickExpand : false,
			fontCss: setFontCss
		},
		callback : {
			onRightClick : OnRightClick,
			onClick : modifyType,
			beforeDrop: zTreeBeforeDrop,
			onDrop: zTreeOnDrop
		}
	};
	
	function zTreeOnDrop(event, treeId, treeNodes, targetNode, moveType) {
	    if(!moveType) return false;
	    var moveId = treeNodes[0].id;
	    var targetId = targetNode.id;
		if(moveType == "prev" || moveType == "next") {
			targetId = targetNode.pid;
	    }
		//根据父节点获取子节点顺序
		var treeObj = $.fn.zTree.getZTreeObj("sysTypeList");
		var parentNode = treeObj.getNodeByParam("id", targetId, null);
		var idSort = $.map(parentNode.children, function(node) {
			return node.id;
		});
	    var options = {
				url : '${ctx}/sys/common/common-type!save.action',
				data : {
					"id" : moveId,
					"pid": targetId,
					"idSort":idSort.join(",")
				},
				success : function(data) {
					if (data.msg) {
						treeNodes[0].pid = targetId;
					}
				}
		};
		fnFormAjaxWithJson(options);
	}
	
	function zTreeBeforeDrop(treeId, treeNodes, targetNode, moveType) {
	    var moveTypeVal = treeNodes[0].typeVal;
	    var targetVal = targetNode.typeVal;
		if(moveTypeVal != targetVal) {
	    	alert("不允许拖动操作！");
	    	return false;
	    }
	};
	
	function setFontCss(treeId, treeNode) {
		var status = treeNode.status;
		return status == 0 ? {color:"red"} : {};
	}
	function initSysTypeZtree(){
		var options = {
			url : '${ctx}/sys/common/common-type!getAllSystemType.action',
			success : function(data) {
				if (data.msg) {
					var newNode = {
							id : null,
							name : "<s:text name='系统分类'/>",
							iconSkin:"iconMenu",
							open:true
					};
					data.msg.push(newNode);
					zTree = $.fn.zTree.init($("#sysTypeList"), setting, data.msg);
					//zTree = $.fn.zTree.getZTreeObj("sysTypeList");
					//zTree.expandAll(true);
					var childs = zTree.getNodes()[0].children;
					if(childs && childs.length)
						zTree.expandNode(childs[0], true);//默认展开第一级节点
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	}
	
	var settingZtree = {
			check : {
				enable : true
			},
			data : {
				simpleData : {
					enable :true
				}
			}
		};

	function getRoleZtree(typeId) {
		var options = {
			url : '${ctx}/sys/common/common-type!getRoleList.action',
			data : {
				"id" : typeId
			},
			success : function(data) {
				if (data.msg) {
					$.fn.zTree.init($("#rolesTree"), settingZtree, data.msg);
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	}		
	
	function OnRightClick(event, treeId, treeNode) {
		event.preventDefault();
		zTree.selectNode(treeNode);
		if (treeNode.name=="系统分类") {
			$('#mm').menu('show', {
				left : event.pageX,
				top : event.pageY
			});
		} else {
			$('#mm2').menu('show', {
				left : event.pageX,
				top : event.pageY
			});
		}

	}

	function addType() {
		$('#saveForm')[0].reset();
		var nodes = zTree.getSelectedNodes();
		var pId = nodes[0].id;
		var p_typeVal = nodes[0].typeVal;
		if(pId!=null){						
			$("#typeVal").attr("readonly", "readonly");
			$("#typeVal").val(p_typeVal);
		}else {
			$("#typeVal").removeAttr("readonly");
		}
		$("#id").val("");
		$("#pid").val(pId);
		getRoleZtree();		
		$("#b1").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
		$("#b1").linkbutton({iconCls : 'icon-no'});
		$('#typeStatus').text('<s:text name="system.button.enabledStatus.title"/>');
		$('#status').val("1");
		$("#win_type_data").panel("open");	
	}
	function modifyType() {
		var nodes = zTree.getSelectedNodes();
		var typeId = nodes[0].id;
		if(typeId!=null){
		var options = {
			url : '${ctx}/sys/common/common-type!getSystemType.action',
			data : {
				"id" : typeId
			},
			success : function(data) {
				if (data.msg) {
					var entity = data.msg;
					if(entity.pid!=null){	
						$("#typeVal").attr("readonly", "readonly");
					}else{
						$("#typeVal").removeAttr("readonly");
					}
					$("#saveForm").form('load',entity);
					//console.log("333");
			 		 if (entity.status == "1") {
							$("#b1").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
							$("#b1").linkbutton({iconCls : 'icon-no'});
							$('#typeStatus').text('<s:text name="system.button.enabledStatus.title"/>');
						} else {
							$("#b1").linkbutton({text : '<s:text name="system.button.enabled.title"/>'});
							$("#b1").linkbutton({iconCls : 'icon-ok'});
							$('#typeStatus').text('<s:text name="system.button.disableStatus.title"/>');
						} 
					getRoleZtree(typeId);
					$("#win_type_data").panel("open");
				}
			}
		};
		fnFormAjaxWithJson(options);
		}
	}

	
	function delType() {
		var nodes = zTree.getSelectedNodes();
		var tid = nodes[0].id;
		$.messager
				.confirm(
						'<s:text name="system.javascript.alertinfo.title"/>',
						'<s:text name="system.javascript.alertinfo.info"/>',
						function(r) {
							if (r) {
								var options = {
									url : '${ctx}/sys/common/common-type!deleteSystemType.action',
									data : {
										"id" : tid
									},
									success : function(data) {
										if (data.msg) {
											initSysTypeZtree();
										}
									}
								};
								fnFormAjaxWithJson(options);
							}
						});
	}

	function saveType() {
		//默认选中超级管理员
		var treeObj = $.fn.zTree.getZTreeObj("rolesTree");
		var node = treeObj.getNodeByParam("id", "r_1");//超级管理员
		treeObj.checkNode(node, true, false);//默认选中超级管理员角色
		var flag = getRoleIds();
		if (flag) {
			if(checkTypeValUnique()) {
				var options = {
						url : '${ctx}/sys/common/common-type!saveSystemType.action',
						success : function(data) {
							if (data.msg) {
								var id = $("#id").val();
								var name = $("#name").val();
								cancel();
								if(!id) { //新增情况
									initSysTypeZtree();
								} else { //修改情况
									var node = zTree.getNodeByParam("id", id);
									node.name = name;
									zTree.updateNode(node);
								}
							}
						}
					};
					fnAjaxSubmitWithJson('saveForm', options);
			} else {
				$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', '类别已存在,请重新输入!', 'info');
				$("#typeVal").focus();
			}
		}
	}
	
	function getRoleIds() {
		var a_zTree = $.fn.zTree.getZTreeObj("rolesTree");
		var a_nodes = a_zTree.getCheckedNodes(true);
		var a_nodeIds = [];
		$.each(a_nodes, function(k, v) {
			if(v.iconSkin == "iconRole") {
				a_nodeIds.push(v.idNum);
			}
		});
		$("#roleIds").val(a_nodeIds.join(","));
		return true;
	}

	function cancel() {
		$("#win_type_data").panel('close');
	}
	
		//选中或取消选中
	function doCheckNodesOnTree(treeId, param) {
		var treeObj = $.fn.zTree.getZTreeObj(treeId);
		treeObj.cancelSelectedNode();
		if(!param) {
			return;
		}
		var nodes = treeObj.getNodesByParamFuzzy("name", param, null);
		//for (var i=0, l=nodes.length; i < l; i++) {
		for (var i=nodes.length-1; i >= 0; i--) {	
			treeObj.selectNode(nodes[i],true);
		}
	}
	
	function _doDictTreeQuery_() {
		var val = $("#_dictTree_qd_").val();
		doCheckNodesOnTree("sysTypeList", val);
	}

	$(document)
			.ready(
					function() {
						initSysTypeZtree();

						$("#b1")
								.toggle(
										function() {
											$("#b1")
													.linkbutton(
															{
																text : '<s:text name="system.button.enabled.title"/>'
															});
											$("#b1").linkbutton({
												iconCls : 'icon-ok'
											});
											$('#typeStatus')
													.text(
															'<s:text name="system.button.disableStatus.title"/>');
											$('#status').val("0");
										},
										function() {
											$("#b1")
													.linkbutton(
															{
																text : '<s:text name="system.button.disable.title"/>'
															});
											$("#b1").linkbutton({
												iconCls : 'icon-no'
											});
											$('#typeStatus')
													.text(
															'<s:text name="system.button.enabledStatus.title"/>');
											$('#status').val("1");
										});
						
						$("#win_type_data").panel({
							onClose:function() {
								$('#saveForm')[0].reset();
							}
						});
						
						$("#_dictTree_qd_").bind('keydown', 'return',function (evt){evt.preventDefault(); _doDictTreeQuery_(); return false; });
					});
</script>
</head>
<body class="easyui-layout">

	<div region="west" split="true" title="<s:text name="系统分类" />" style="width: 300px; padding: 10px;" border="false">
		<div>
		<input type="text" id="_dictTree_qd_" style="width:130px" class="Itext"/>&nbsp;&nbsp;<button type="button" class="button_small" onclick="_doDictTreeQuery_()">树查询</button></div>
		<ul id="sysTypeList" class="ztree"></ul>
		</div>
		<div id="mm" class="easyui-menu" style="width: 120px;">
			<div onclick="addType()" iconCls="icon-add">
				<s:text name="新增系统分类" />
			</div>
		</div>
		<div id="mm2" class="easyui-menu" style="width: 120px;">
			<div onclick="addType()" iconCls="icon-add">
				<s:text name="新增系统分类" />
			</div>
			<div onclick="modifyType()" iconCls="icon-edit">
				<s:text name="修改系统分类" />
			</div>
			<div onclick="delType()" iconCls="icon-remove">
				<s:text name="删除系统分类" />
			</div>
		</div>
	</div>



	<div region="center" title="<s:text name="系统分类管理"/>" border="false">
		<div id="win_type_data" class="easyui-panel" closed="true"
			fit="true" closable="true" modal="true" title="" border="false"
			style="padding: 0px; background: #fafafa;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false"
					style="padding: 2px; background: #fff; overflow: auto; border: 1px solid #ccc;">
					<form action="" name="saveForm" id="saveForm" method="post">
						<table align="center" border="0" cellpadding="0" cellspacing="1"
							class="table_form">
							<tbody>
								<input type="hidden" name="id" id="id" /> 
								<input type="hidden" name="pid" id="pid" /> 
								<input type="hidden" name="version" id="version" /> 
								<input type="hidden" name="typeBelong" id="typeBelong" /> 
								<tr>
									<th><label for="name"><s:text
												name="系统类型名称" />:</label></th>
									<td>
									<input class="easyui-validatebox Itext" required="true" validType="length[1,100]" type="text"
									name="name"	id="name" style="width:300px;">
									</input>
									</td>				
									<th><label for="typeVal"><s:text
												name="类别" />:</label></th>
									<td>
									<input class="easyui-validatebox Itext" required="true" validType="length[1,200]" type="text"
									name="typeVal"	id="typeVal">
									</input>
									</td>					
								</tr>
								<tr>
									<th valign="top"><label for="status"><s:text
												name="状态" />:</label></th>
									<td valign="top"><input type="hidden" name="status" id="status" /> <span
											id="typeStatus" style="color: red"></span> <a href="#"
											class="easyui-linkbutton" id="b1" ></a>
									</td>
									
									<th valign="top"><label><s:text name="选择角色"/>:</label></th>
									<td colspan="1" valign="top">
									<input type="hidden" name="roleIds" id="roleIds" />
										<ul id="rolesTree" class="ztree"></ul></td>
								</tr>
						<%-- 		<tr>
									<th><label><s:text name="system.sysmng.role.setAuthority.title"/>:</label></th>
									<td colspan="1" valign="top"><input type="hidden" name="authorityIds"
										id="authorityId" />
										<ul id="authorityTree" class="ztree"></ul></td>
									<th><label><s:text name="system.sysmng.authority.setUser.title"/>:</label></th>
									<td colspan="1" valign="top"><input type="hidden" name="userId"
										id="userId" />
										<ul id="userTree" class="ztree"></ul></td>
								</tr>
								<tr>
										<th><label for="desktopName"><s:text name="system.sysmng.role.setDesktop.title"/>:</label>
					</th>
					<td colspan="3">
					<input type="hidden" name="desktopIds" id="desktopId" />
						<ul id="desktopTree" class="ztree"></ul></td>		
						</tr>
						 --%>
							</tbody>
						</table>

					</form>
				</div>
				<div region="south" border="false"
					style="text-align: center; height: 40px; padding:4px;">
					<a class="easyui-linkbutton" iconCls="icon-ok"
						href="javascript:void(0)" onclick="saveType()"><s:text
							name="system.button.save.title" /> </a> <a class="easyui-linkbutton"
						iconCls="icon-cancel" href="javascript:void(0)"
						onclick="cancel()"><s:text
							name="system.button.cancel.title" /> </a>
				</div>
			</div>
		</div>


	</div>
</body>
</html>