<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/common/meta-css.jsp" %>
	<title><s:text name="system.index.title"/></title>
</head>
<body>
	<div class="easyui-layout" fit="true">
	<div region="center" border="false">
		<table id="_dataList_" border="false">
		</table>	
	</div>
	</div>
	<form method="post" name="_hidFilterFrm_" id="_hidFilterFrm_" >
		<textarea style="display:none;" name="_hid_filter_params_" id="_hid_filter_params_">${param.filterParam}</textarea>
	</form>
	<%@ include file="/common/meta-js.jsp" %>
	<script type="text/javascript">
		function getFilterParams__() {
			var param =$("#_hid_filter_params_").val();
			return $.parseJSON(param);
		}
		
		function getFilterOption__(jsonData){
			var data = jsonData["list"];	
			var mapping = jsonData["map"];
		  	var fields = [];
		   $.each(mapping,function(i,n){
			   var field = {width:100};
			   field["field"] = n.key;
			   field["title"] = n.caption;
			   if(n.order == "hidden") {
				   field["hidden"] = true;
			   }
			   fields.push(field);
		   });	   
		   var fColumns=[];
		   var field={};
		   field["field"]="ck";
		   field["checkbox"]=true;
		   fColumns.push(field);		   
		   var dataList={
	    		   "total":data.length,                                                      
				   "rows":data
	       };
		   
		   $('#_dataList_').edatagrid({  
			    width : 'auto',
				height : 'auto',
				fit:true,
				fitColumns : true,
				nowrap : false,
				striped : true,
				rownumbers:true,
				//idField:'key',  
				frozenColumns:[fColumns],
				columns:[fields]					 							
		   });  
			 $('#_dataList_').edatagrid("loadData",dataList);
		}
		//获取所有数据
		function _getAllFilterData_() {
			var list = $('#_dataList_').datagrid("getRows");
			list = $.extend(true, [], list);
			return list;
		}
		//获取所有选中的数据
		function _getSelectFilterData_() {
			var list = $('#_dataList_').datagrid("getSelections");
			list = $.extend(true, [], list);
			return list;
		}
		//获取所有未选中的数据
		function _getUnSelectFilterData_() {
			var allList = _getAllFilterData_();
			var selList = _getSelectFilterData_();
			var unSelList = [];
			if(allList && allList.length) {
				if(selList && selList.length) {
					for(var i=0,len=selList.length;i<len;i++) {
						var selObj = selList[i];
						for(var j=0,jLen=allList.length;j<jLen;j++) {
							var allObj = allList[j];
							if(JSON.compObj(allObj, selObj)) {
								allList.splice(j, 1);
								break;
							}
						}
					}
				} 
				unSelList = allList;
			}
			return unSelList;
		}
		//默认确定按钮执行操作
		function saveFilterData__(key){
			var list = $('#_dataList_').datagrid("getSelections");
			if(list && list.length > 0) {
				var data={
			    		   "total":list.length,                                                      
						   "rows":list
			    };
				$('#'+key).edatagrid("loadData",data);
				return true;
			} else {
				$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.operRecord"/>', 'info');
				return false;
			}
		}
		$(function(){
			var filterParam = getFilterParams__();
			var sqlKey = "${param.sqlKey}";
			if(sqlKey != "") {
				var data = jwpf.getFilterData(sqlKey,filterParam);
				getFilterOption__(data);
			} else {
				var data = $(body).data("dgOpt");
				if(data) {
					getFilterOption__(data);	
				}
			}
		});	
</script>
</body>
</html>
