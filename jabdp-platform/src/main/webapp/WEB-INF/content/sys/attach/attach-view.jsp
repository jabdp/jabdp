<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="附件信息"/></title>
<%@ include file="/common/meta.jsp"%>
<script type="text/javascript">
	function getOptionAttach() {
		return {
			width : '100%',
			height : 'auto',
			fitColumns : true,
			nowrap : false,
			striped : true,
			url : '${ctx}/sys/attach/attach!queryList.action',
			queryParams:{"attachId":"${param.attachId}"},
			sortName : 'id',
			sortOrder : 'desc',
			remoteSort: true,
			idField : 'id',
			pageSize: 5,
			pageList: [5, 10, 20],
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			}  ] ],
			columns : [ [ {
				field : 'fileName',
				title : '<s:text name="system.attach.attachName.title"/>',
				width : 180,
				sortable : true
			}, {
				field : 'fileSize',
				title : '<s:text name="system.attach.attachSize.title"/>(B)',
				width : 140,
				sortable : true
			}, {
				field : 'createUser',
				title : '<s:text name="system.sysmng.user.createuser.title"/>',
				width : 120,
				sortable : true,
				formatter : function(value, rowData, rowIndex){	
                    /*var uObj=_userList[value];
                    if(uObj){
                    	var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
						return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                    }else{
                        return value;
                    }*/
					return rowData["createUserCaption"];
				}
			}, {
				field : 'createTime',
				title :  '<s:text name="system.sysmng.user.createtime.title"/>',
				width : 150,
				sortable : true
			},{
				field : 'oper',
				title : '<s:text name="system.button.oper.title"/>',
				width : 80,
				align : 'left',
				formatter : operFormatterAttach
			} ] ],
			pagination : true,
			rownumbers : true,
			toolbar : [ 
			{
				id : 'bt_batch_download',
				text : '<s:text name="批量下载"/>',
				iconCls : 'icon-download-ico',
				handler : function() {
					doDownloadAttach();
				}
			}
			],
			onDblClickRow :function(rowIndex, rowData){
				var url = "${systemParam.virtualPicPath}" + rowData.filePath;
				window.open(url);
			}
		};
	}

	function operFormatterAttach(value, rowData, rowIndex) {
		return '<a href="${systemParam.virtualPicPath}'
		+ rowData.filePath
		+ '" target= "_blank") ><s:text name="system.button.view.title"/></a>';
	}
	
	function downloadFile(url) {   
		try{ 
			var elemIF = document.createElement("iframe");   
			elemIF.src = url;   
			elemIF.style.display = "none";   
			document.body.appendChild(elemIF);   
		} catch(e){ 
	
		} 
	}  
	
	function getDownloadFileUrl(fileName, filePath) {
		var downUrl = ["${ctx}/sys/attach/upload-file!downloadFile.action?filedataFileName="];
		downUrl.push(encodeURIComponent(fileName));
		downUrl.push("&filePath=");
		downUrl.push(encodeURIComponent(filePath));
		return downUrl.join("");
	}
	
	//批量下载文件
	function doDownloadAttach() {
		var rows = $("#attachList").datagrid('getSelections');
		if (rows && rows.length) {
			for(var i=0,len=rows.length;i<len;i++) {
				var rowData = rows[i];
				//var url = "/jwpf/upload/gs-attach/"+ rowData.filePath;
				//var url = "${systemParam.virtualPicPath}"+ rowData.filePath;
				var url = getDownloadFileUrl(rowData.fileName, rowData.filePath);
				downloadFile(url);
				//console.log(rowData.id);
				//window.open(url, "附件" + rowData.id);
			}
		} else {
			$.messager.alert(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.operRecord"/>',
					'info');
		}
	}

	//var _userList={}; 
	$(document).ready(function() {
		//_userList=findAllUser(); 
		$("#attachList").datagrid(getOptionAttach());
	});
</script>
</head>
<body class="easyui-layout">
	<div region="center" style="padding: 3px 2px" border="false">
		<table id="attachList">
		</table>
		<div style="display: none;">
			<input type="hidden" name="attachId" id="attachId"
				value="${param.attachId}" />
		</div>
	</div>
</body>
</html>




