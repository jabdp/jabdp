<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="com.qyxx.platform.sysmng.accountmng.entity.User" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="com.qyxx.platform.sysmng.utils.Constants" %>
<%@ page import="com.opensymphony.xwork2.interceptor.I18nInterceptor" %>
<%@ page import="com.opensymphony.xwork2.util.LocalizedTextUtil" %>
<%@ page import="java.util.Locale" %>
<%
SystemParam systemParam = SpringContextHolder.getBean("systemParam");
String portalUrl = "";
String systemPortalUrl = systemParam.getSystemPortalUrl();
if(StringUtils.isNotBlank(systemPortalUrl)) {
	portalUrl = systemPortalUrl;
}
User user = (User)session.getAttribute("USER");
if(null!=user) {
	String rolePortalUrl = user.getPortalUrl();
	if(StringUtils.isNotBlank(rolePortalUrl)) {
		portalUrl = rolePortalUrl;
	}
} else {
	response.sendRedirect(request.getContextPath() + "/login-v2.action");
}
if(StringUtils.isNotBlank(portalUrl)) {
	portalUrl = request.getContextPath() + "/" + portalUrl;
}
request.setAttribute("portalUrl", portalUrl);
String lang = request.getParameter("lang");
String langText = "简体中文";
if(StringUtils.isNotBlank(lang)) {
	String locale = "zh_CN";
	switch(lang) {
		case "zh-TW":
			locale = "zh_TW";
			langText = "繁體中文";
			break;
		case "en":
			locale = "en";
			langText = "English";
			break;
		case "zh-CN":
			locale = "zh_CN";
			break;
	}
	session.setAttribute(Constants.LOCALE_SESSION_ID, locale);
	session.setAttribute("LANG", lang);
	Locale local = LocalizedTextUtil.localeFromString(locale, null);
	session.setAttribute(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, local);
	session.setAttribute("THEME_VERSION","bootstrap");
}
%>
<%@ include file="/common/taglibs.jsp" %>
<!doctype html>
<html>
  <head>
    <title><%=systemParam.getSystemTitle()%></title>
    <script>
    var type=navigator.appName,lang="zh-CN";
    if (type=="Netscape"){
    	lang = navigator.languages?navigator.languages[0]:navigator.language;
    } else{
    	lang = navigator.userLanguage;
    }
    //取得浏览器语言的前两个字母
    <%--lang = lang.substr(0,2);--%>
    <%--if(lang != "zh" && lang != "<%=lang%>") {--%>
    <%--	window.location.href="${ctx}/index-v2.action?lang=en";	--%>
    <%--}--%>
    </script>
	<%@ include file="/common/index-meta-css-v2.jsp" %>
	<style>
	.dropdown-hover:hover .dropdown-menu {
		display: block;
		margin-top: 0;
	}
	</style>
  </head>
  <body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden;">
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom" id="top-navbar">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;">
                    <div class="navbar-header">
                    	<span class="navbar-brand-logo"><img src="${imgPath}/<%=systemParam.getSystemLogo()%>" style="height:64%;" alt="logo" /></span>
                    	<span class="navbar-brand-custom"><%=systemParam.getSystemTitle()%></span>
                        <!-- <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search" id="top-search">
                            </div>
                        </form> -->
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" href="#" onclick="openNotices(0)" title="<s:text name="system.index.approvelist.title"/>">
                                <i class="fa fa-comments"></i> <span class="label label-warning" id="_span_approveNum_">0</span>
                            </a>
                            <%--<ul class="dropdown-menu dropdown-messages" id="_ul_approveList_">
                            <li><div class="text-center link-block">
								<a class="J_menuItem" href="${ctx}/sys/workflow/process-history.action">
								<i class="fa fa-comments"></i> <strong> <s:text name="system.index.viewall.title"/><s:text name="system.index.approvelist.title"/></strong>
								</a></div></li>
                            </ul> --%>
                        </li>
						<li class="dropdown">
                            <a class="dropdown-toggle count-info" href="#" onclick="openNotices(1)" title="<s:text name="system.index.noticelist.title"/>">
                                <i class="fa fa-bell"></i> <span class="label label-warning" id="_span_noticeNum_">0</span>
                            </a>
                            <%-- <ul class="dropdown-menu dropdown-messages"  id="_ul_noticeList_">
                            <li><div class="text-center link-block">
								<a class="J_menuItem" href="${ctx}/sys/notice/notice!noticeToUserList.action">
								<i class="fa fa-bell"></i> <strong> <s:text name="system.index.viewall.title"/><s:text name="system.index.noticelist.title"/></strong>
								</a></div></li>
                            </ul>--%>
                        </li>
                        <li class="hidden-xs">
                            <a href="#" class="J_tabZoom" title="<s:text name="system.index.zoominout.title"/>"> <i class="fa fa-arrows-alt"></i></a>
                        </li>
                        <li class="hidden-xs">
                            <a href="${ctx}/index.action" title="<s:text name="切换至经典主题"/>" target="_self"> <i class="fa fa-star"></i></a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" aria-expanded="false" data-toggle="dropdown" href="#">
                                <i class="fa fa-language"></i> <%=langText%> <b class="caret"></b>
                            </a>
                            <ul role="menu" class="dropdown-menu dropdown-menu-right animated fadeInRight">
		                        <li class="ls_zh-CN"><a href="${ctx}/index-v2.action?lang=zh-CN">简体中文</a>
		                        </li>
		                        <li class="ls_zh-TW"><a href="${ctx}/index-v2.action?lang=zh-TW">繁體中文</a>
		                        </li>
		                        <li class="ls_en"><a href="${ctx}/index-v2.action?lang=en">English</a>
		                        </li>
		                    </ul>
                        </li>
                        <li class="dropdown dropdown-hover">
                        								<a data-toggle="dropdown" class="dropdown-toggle" href="#">
							                                <i class="glyphicon glyphicon-user"></i> ${sessionScope.USER.nickname} <b class="caret"></b>
							                            </a>
							                            <ul role="menu" class="dropdown-menu dropdown-menu-right animated fadeInRight">
							                                <li><a href="#"><s:text name="system.sysmng.user.name.title"/>：<s:property value="#session.USER.realName" /></a></li>
							                                <li><a href="#"><s:text name="system.sysmng.role.organization.title"/>：<s:property value="#session.USER.organizationName" /></a></li>
							                                <li class="divider"></li>
							                                <li><a class="J_menuItem" href="${ctx}/sys/account/profile.action" data-index="myProfile"><s:text name="system.index.modifyprofile.title"/></a>
							                                </li>
							                                <s:if test='#session.USER.isSuperAdmin=="1"'>
							                                <li class="divider"></li>
							                                <li><a href="#" data-index="my1" onclick="doRefreshCache()"><s:text name="system.index.syncauthcache.title"/></a></li>
							                                <%--<li><a href="#" data-index="my2" onclick="doRefreshSysParam()"><s:text name="system.index.syncsysparam.title"/></a></li> --%>
																<%--<li><a class="J_menuItem" href="${ctx}/index!authInfo.action"><s:text name="system.index.versioninfo.title"/></a></li>--%>
							                                </s:if>
							                                <li class="divider"></li>
							                                <li><a href="${ctx}/logout.action"><s:text name="system.logout.title"/></a>
							                                </li>
							                            </ul>
                        </li>
						<%-- <li class="hidden-xs">
                            <a href="${ctx}/logout.action" data-index="0"><i class="fa fa-sign-out"></i> <s:text name="system.logout.title"/></a>
                        </li>--%>
                    </ul>
                </nav>
            </div>
            <div class="row content-tabs">
                <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i>
                </button>
                <nav class="page-tabs J_menuTabs">
                    <div class="page-tabs-content">
                        <a href="javascript:;" class="active J_menuTab" data-id="${portalUrl}"><s:text name="system.index.workspace.title"/></a>
                    </div>
                </nav>
                <button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
                </button>
                <button class="roll-nav roll-right dropdown J_tabClose" style="width:84px;" data-toggle="dropdown"><s:text name="system.index.closetab.title"/><span class="caret"></span>
                </button>
                    <ul role="menu" class="dropdown-menu dropdown-menu-right animated fadeInRight">
                        <li class="J_tabShowActive"><a><s:text name="system.index.postab.title"/></a>
                        </li>
                        <li class="divider"></li>
                        <li class="J_tabCloseAll"><a><s:text name="system.index.closealltab.title"/></a>
                        </li>
                        <li class="J_tabCloseOther"><a><s:text name="system.index.closeothertab.title"/></a>
                        </li>
                    </ul>
                    <%-- 
                <div class="btn-group roll-nav roll-right">
                </div>--%>
            </div>
            <div class="row J_mainContent" id="content-main">
            	<div class="container-fluid J_iframe" data-id="${portalUrl}" style="padding:0px;">
            		<!--左侧导航开始-->
            		<div class="row">
            		<div class="col-sm-2">
            			<nav class="navbar-default navbar-static-side" role="navigation">
			            <div class="nav-close"><i class="fa fa-times-circle"></i>
			            </div>
			            <div class="sidebar-collapse">
							<%-- <ul class="nav">
			                    <li class="nav-header" >
			                        <div class="dropdown profile-element">
										<div class="row">
											<div class="col-sm-4">
			                            <span><img id="myAvatar" alt="Avatar" class="img-circle" src="${ctx}/jslib/index/img/profile_small.jpg" style="width:64px;height:64px;"/></span>
											</div>
											<div class="col-sm-8">
												<div class="row">
													<div class="col-sm-12">
														<span class="clear">
						                                <span class="block" style="color:#fff"><strong class="font-bold">${sessionScope.USER.organizationName}</strong></span>
						                                </span>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12">
														<a data-toggle="dropdown" class="dropdown-toggle" href="#">
							                                <span class="clear">
							                                <span class="text-muted text-xs block"><span id="span_nickname">${sessionScope.USER.nickname}</span> <b class="caret"></b></span>
							                                </span>
							                            </a>
							                            <ul class="dropdown-menu dropdown-menu-right animated fadeInRight m-t-xs" style="right:12px;">
							                                <li><a class="J_menuItem" href="${ctx}/sys/account/profile.action" data-index="myProfile"><s:text name="system.index.modifyprofile.title"/></a>
							                                </li>
							                                <s:if test='#session.USER.isSuperAdmin=="1"'>
							                                <li class="divider"></li>
							                                <li><a href="#" data-index="my1" onclick="doRefreshCache()"><s:text name="system.index.syncauthcache.title"/></a></li>
							                                <li><a href="#" data-index="my2" onclick="doRefreshSysParam()"><s:text name="system.index.syncsysparam.title"/></a></li>
							                                <li><a class="J_menuItem" href="${ctx}/index!authInfo.action"><s:text name="system.index.versioninfo.title"/></a>
							                                </s:if>
							                                <li class="divider"></li>
							                                <li><a href="${ctx}/logout.action"><s:text name="system.logout.title"/></a>
							                                </li>
							                            </ul>
													</div>
												</div>
												
											</div>
										</div>
			                        </div>
			                        <div class="logo-element">BSS
			                        </div>
			                    </li>
			                    <li>
			                    	<a class="navbar-minimalize btn" href="javascript:;" title="<s:text name="system.index.expandshrink.title"/>"><i class="fa fa-exchange"></i></a>
			                    </li>
			                </ul>
			                --%>
			                <div id="left-menu">
				                <ul class="nav metismenu" id="side-menu">
				                </ul>
			                </div>
						</div>
					</nav>
            		</div>
            		<div class="col-sm-10" style="margin-left:-15px;padding:0px;height:500px;" id="portalIframe">
            			<iframe name="iframe0" width="100%" height="100%" src="${portalUrl}" frameborder="0" data-id="${portalUrl}" seamless></iframe>
            		</div>
            		</div>
            	</div>
            </div>
        </div>
    </div>	
    <div style="display:none;">
    		<div id="aboutDoAction"  closed="true" minimizable="false" modal="true" resizable="false" title="<s:text name='system.top.message.title'/>" style="width:600px;height:400px;">
				<div class="easyui-layout" fit="true">
				<%-- <div region="north" title="" border="false" style="height: 37px; padding: 1px;">
					<form action="" name="toDoForm" id="toDoForm">
						<table border="0" cellpadding="0" cellspacing="1" class="table_form">
							<tbody>
								<tr>
									<th><label for="keyWords"><s:text name="关键字" />:</label></th>
									<td><input type="text" name="keyWords" id="keyWords"></input></td>						
									<td><button type="button" id="_bt_query_"><s:text name="system.search.button.title" /></button>
									&nbsp;&nbsp;
									<button type="button" id="_bt_reset_"><s:text name="system.search.reset.title" /></button></td>								
								</tr>
							</tbody>
						</table>
					</form>
				</div> --%>
				<div region="center"  title="" border="false" style="padding: 1px; overflow: hidden;">
					<div id="tnotice" fit="true" border="false" tools="#about_do_tab-tools">
						<div selected="true"
							title="<s:text name="system.sysmng.notice.todoSomething.title"/>">
							<table id="toDoList" border="false"></table>
						</div>
						<div title="<s:text name="system.sysmng.notice.list.title"/>">
							<table id="noReadNoticeDetail" border="false"></table>
						</div>
					</div>
				</div>
				<div region="south" border="false" align="right" style="padding:1px;height:25px;overflow:hidden;">
					弹出通知方式选择：
					<select id="aboutDoActionConfig" style="width:120px" class="Itext">
						<option value="1" selected="selected">消息变化时弹出</option>
	 					<option value="0">不弹出</option>
					</select>
					<input type="button" value="设置" style="height:22px" onClick="setNoticesConfig()"></input>
				</div>
			</div>
			</div>
			<div id="about_do_tab-tools" style="border-width:0 0 1px 0;">
			    <a id="_batch_agree_" href="#" class="easyui-linkbutton" plain="true" iconCls="icon-contacts-ico" onclick="doBatchView();" title="批量查看">批量查看</a>
				<a id="_batch_read_" href="#" class="easyui-linkbutton" plain="true" iconCls="icon-contacts-ico" onclick="doReadNotice();" title="批量阅读">批量阅读</a>
				<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-scheduled_Tasks" onclick="addTab('待办历史','${ctx}/sys/workflow/process-history.action');$('#aboutDoAction').window('close');" title="待办历史">待办历史</a>	
				<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-chat-ico" onclick="addTab('通知历史','${ctx}/sys/notice/notice!noticeToUserList.action');$('#aboutDoAction').window('close');" title="通知历史">通知历史</a>		
			</div>
			<div id="approveListToolBar">
				<div class="container-fluid" style="margin:2px 2px;">
					<form class="form-inline" id="approveListQueryForm" onsubmit="return false;">
						<div class="form-group"> <label for="approveListKey">关键字</label> <input type="text" class="form-control input-sm" id="approveListKey" name="approveListKey" placeholder="请输入关键字搜索"> </div>
						<button type="submit" class="btn btn-default btn-sm" onclick="doQueryToDo('approve')"><i class="glyphicon glyphicon-search"></i> 查询</button> 
						<button type="button" class="btn btn-default btn-sm" onclick="doResetQueryToDo('approve')"><i class="glyphicon glyphicon-repeat"></i> 重置</button>
					</form> 
				</div>
			</div>	
			<div id="noticeListToolBar">
				<div class="container-fluid" style="margin:2px 2px;">
					<form class="form-inline" id="noticeListQueryForm" onsubmit="return false;">
						<div class="form-group"> <label for="noticeListKey">关键字</label> <input type="text" class="form-control input-sm" id="noticeListKey" name="noticeListKey" placeholder="请输入关键字搜索"> </div>
						<button type="submit" class="btn btn-default btn-sm" onclick="doQueryToDo('notice')"><i class="glyphicon glyphicon-search"></i> 查询</button> 
						<button type="button" class="btn btn-default btn-sm" onclick="doResetQueryToDo('notice')"><i class="glyphicon glyphicon-repeat"></i> 重置</button>
					</form> 
				</div>
			</div>
	</div>		
	<%@ include file="/common/index-meta-js-v2.jsp" %>
	<!-- comet -->
	<script type="text/javascript" src="${ctx}/dwr/engine.js"></script>
	<script type="text/javascript" src="${ctx}/dwr/util.js"></script>
	<script type="text/javascript" src="${ctx}/dwr/interface/comet.js"></script>
	<!-- comet end -->
	<script type="text/javascript">
		dwr.engine.setErrorHandler(function(message, ex) {
	    	dwr.engine._debug("Error: " + ex.name + ", " + ex.message, true);
	    	if (message == null || message == "") {} //alert("A server error has occurred.");
	    	else if (message.indexOf("0x80040111") != -1) dwr.engine._debug(message);
	    	else $MsgUtil.alert(message);
	    });
		
		function chengeNoticeNum(num) { // 修改通知数量
			if(num) {
				if(num.approveNum >= 0) { //审批数量
					$("#_span_approveNum_").text(num.approveNum||"0");
				}
				if(num.noticeNum >= 0) { //通知数量
					$("#_span_noticeNum_").text(num.noticeNum||"0");
				}
				var noticesOpenType = readCookie("noticesOpenType");
				if(noticesOpenType != "0") {
					openNotices("0");
				}
			}
		}

		function initCometPush() { // 初始化服务器推送
			dwr.engine.setActiveReverseAjax(true);
			comet.setUserIdAttribute(); // 保存连接信息
		}
		
		function doRefreshCache() {
			var options = {
					url:'${ctx}/index!refreshCacheData.action',
					success:function(data) {
						$MsgUtil.alert('<s:text name="system.top.synchronizationData.title"/>', "success");
					}
			};
			fnFormAjaxWithJson(options);
		}
		
		//查询当前用户所有的待办事宜
		function getToDoList() {
			return {
				width : 'auto',
				height : 'auto',
				url : '${ctx}/gs/process!toDoList.action',
				fitColumns : true,
				nowrap : false,
				fit : true,
				pagination : true,
				rownumbers : true,
				remoteSort : true,
				striped : true,
				pageSize : 10,
				pageList : [ 10, 20, 30, 50, 100 ],
				sortName : 'createTime',
				sortOrder : 'desc',
				toolbar : '#approveListToolBar',
				frozenColumns : [ [ {
					field : 'ck',
					checkbox : true
				} ] ],
				columns : [ [
						{
							field : 'moduleName',
							title : '<s:text name="system.sysmng.process.moduleName.title"/>',
							width : 60,
							sortable : false
						},
						{
							field : 'name',
							title : '<s:text name="system.sysmng.process.taskName.title"/>',
							width : 80,
							sortable : false<%--,
							formatter : function(value, rowData, rowIndex) {
								var newName = value;
								if (rowData.field) {
									newName = value + '-' + rowData.field;
								}
								return newName;
							}--%>
						},
						{
							field : 'createTime',
							title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
							width : 80,
							sortable : false
						},
						{
							field : 'startUser',
							title : '<s:text name="system.sysmng.process.startUser.title"/>',
							width : 80,
							sortable : false,
							formatter : function(value, rowData, rowIndex) {
								return rowData["startUserCaption"];
								<%--/*var val = _userLoginNameList[value];
								if(val) {
								 return val;
								} else {
								 return value;
								}*/--%>
							}
						},
						{
							field : 'field',
							title : '<s:text name="任务描述"/>',
							width : 180,
							sortable : false
						},
						{
							field : 'oper',
							title : '<s:text name="system.button.oper.title"/>',
							width : 80,
							align : 'left',
							formatter : function(value, rowData, rowIndex) {
								var strArr = [];
								if (rowData.assignee == null) {
									strArr
											.push('<a href="javascript:void(0);" onclick="doSignin(\'');
									strArr.push(rowData.id + '\',\''
											+ rowData.name);
									strArr
											.push('\')"><s:text name="system.sysmng.process.signFor.title"/></a>');
								} else if (rowData.assignee != null) {
									strArr
											.push('<a href="javascript:void(0);" onclick="doSignin(\'');
									strArr.push(rowData.id + '\',\''
											+ rowData.name);
									strArr
											.push('\')"><s:text name="system.sysmng.process.transact.title"/></a>');
								}
								return strArr.join("");
							}
						} ] ]
			};

		}
		
		//初始化当前用户未读的消息通知datagrid
		function getCurrentUserNotices() {
			return {
				width : 'auto',
				height : 'auto',
				url : '${ctx}/sys/notice/notice!queryNoReadNotice.action',
				fitColumns : true,
				nowrap : false,
				fit : true,
				pagination : true,
				rownumbers : true,
				pageSize : 10,
				pageList : [ 10, 20, 30, 50, 100 ],
				remoteSort : true,
				striped : true,
				toolbar : '#noticeListToolBar',
				frozenColumns : [ [ {
					field : 'ck',
					checkbox : true
				} ] ],
				columns : [ [
						{
							field : 'title',
							title : '<s:text name="system.sysmng.desktop.modelTitle.title"/>',
							width : 160,
							sortable : false
						},
						{
							field : 'style',
							title : '<s:text name="system.sysmng.notice.style.title"/>',
							width : 80,
							sortable : false,
							sortOrder : 'asc',
							formatter : function(value, rowData, rowIndex) {
								if (value == "01") {
									return "<s:text name='system.sysmng.notice.common.title'/>";
								} else if (value == "02") {
									return "<s:text name='system.sysmng.notice.warning.title'/>";
								} else if (value == "03") {
									return "<s:text name='system.sysmng.notice.todo.title'/>";
								} else if (value == "04") {
									return "<s:text name='业务确认通知'/>";
								} else if (value == "05") {
									return "<s:text name='提醒通知'/>";
								} else if (value == "06") {
									return "<s:text name='业务办理通知'/>";
								}

							},
							styler : function(value, row, index) {
								if (value == "01") {
									return 'color:blue;';
								} else if (value == "02") {
									return 'color:red;';
								} else if (value == "03") {
								}
							}
						},
						{
							field : 'createUser',
							title : '<s:text name="system.sysmng.desktop.createUser.title"/>',
							width : 100,
							sortable : false,
							formatter : function(value, rowData, rowIndex) {
								<%--/*var uObj=_userList[value];
								if(uObj){
									var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
									return uObj.realName + "[" + uObj.loginName + "]" + nickName;
								}else{
								    return value;
								}*/--%>
								return rowData["createUserCaption"];
							}
						},
						{
							field : 'createTime',
							title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
							width : 120,
							sortable : false
						} ] ],
				onClickRow : function(rowIndex, rowData) {
					$("#aboutDoAction").window("close");
					if (rowData.style == "01") {
						doDblView(rowData.id, rowData.ntuId, rowData.style);
					} else if (rowData.style == "03") {//待办通知(办理任务)
						doSignin(rowData.moduleDataId, rowData.moduleKey);
						doRead(rowData.ntuId);
					} else if (rowData.style == "05") {//提醒通知(查看历史)
						parent
								.addTab(
										'<s:text name="system.sysmng.process.dealHistory.title"/>-'
												+ rowData.moduleDataId,
										'${ctx}/gs/process!getProcess.action?processInstanceId='
												+ rowData.moduleDataId
												+ '&rptData='
												+ (rowData.moduleFields ? rowData.moduleFields
														: ""));
						doRead(rowData.ntuId);
					} else if (rowData.style == "06") {
						if (rowData.moduleFields == "true") {
							parent.addTab(
									'<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'
											+ rowData.moduleDataId,
									'${ctx}/gs/gs-mng!view.action?entityName='
											+ rowData.moduleKey + '&id='
											+ rowData.moduleDataId);
						} else if (rowData.moduleFields == "false") {
							parent.addTab(
									'<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'
											+ rowData.moduleDataId,
									'${ctx}/gs/gs-mng!view.action?entityName='
											+ rowData.moduleKey + '&id='
											+ rowData.moduleDataId
											+ '&isEdit=2');
						}
						doRead(rowData.ntuId);
					} else if (rowData.style == "04") {
						var url = '${ctx}/gs/gs-mng!view.action?entityName='
								+ rowData.moduleKey + '&id='
								+ rowData.moduleDataId + '&ntuId='
								+ rowData.ntuId + "&initOperMethod=modify";
						parent.addTab(
								'<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'
										+ rowData.moduleDataId, url);
						if (rowData.moduleFields
								&& rowData.moduleFields.indexOf("autoRead") >= 0) {
							doRead(rowData.ntuId);
						}
					} else if (rowData.style == "02") {//业务预警通知
						if (rowData.moduleKey) {
							var url = '${ctx}/gs/gs-mng!view.action?entityName='
									+ rowData.moduleKey
									+ '&id='
									+ rowData.moduleDataId
									+ '&ntuId='
									+ rowData.ntuId + "&initOperMethod=modify";
							parent.addTab(
									'<s:text name="system.button.view.title"/><s:text name="业务预警通知"/>-'
											+ rowData.moduleDataId, url);
							//doRead(rowData.ntuId);
						} else {
							doDblView(rowData.id, rowData.ntuId, rowData.style);
						}
					}
					$('#queryList').datagrid('unselectRow', rowIndex);
				}
			};
		}
		
		function doSignin(id, name) {
			window.parent.addTab(name + '_' + id,
					'${ctx}/gs/process!getProcess.action?taskId=' + id);
			$("#aboutDoAction").window("close");
		}

		function doDblView(id, ntuId, style) {
			parent.addTab('<s:text name="system.sysmng.notice.view.title"/>-' + id,
					'${ctx}/sys/notice/notice!viewNoticeToUser.action?id=' + id
							+ "&nTouId=" + ntuId + "&style=" + style);
		}
		
		function doRead(nTouId) {
			var options = {
				url : '${ctx}/sys/notice/notice!doRead.action',
				async : false,
				data : {
					"noticeToUserId" : nTouId
				},
				success : function(data) {
					if (data.msg) {

					}
				}
			};
			fnFormAjaxWithJson(options);
		}
		
		//快速查询消息或待办事宜
		function doQueryToDo(type) {
			if (type=="approve") {
				var queryValue = $("#approveListKey").val();
				if(!queryValue) {
					alert("请输入关键字！");
					return false;
				}
				//$("#toDoList").datagrid("clearSelections");
				var data = $("#toDoList").datagrid("getData");
				var rowData = data.rows;
				var len = rowData.length;
				for (var i = 0; i < len; i++) {
					var taskName = "";
					if (rowData[i].field) {
						taskName = rowData[i].name + '-' + rowData[i].field;
					} else {
						taskName = rowData[i].name;
					}
					var isSelect = taskName.indexOf(queryValue);
					if (isSelect > -1) {
						$("#toDoList").datagrid("selectRow", i);
					}
				}
			} else if(type=="notice") {
				var queryValue = $("#noticeListKey").val();
				if(!queryValue) {
					alert("请输入关键字！");
					return false;
				}
				//$("#noReadNoticeDetail").datagrid("clearSelections");
				var fp = {
					"filter_LIKES_title" : queryValue
				};
				$("#noReadNoticeDetail").datagrid("load", fp);
			}
		}
		
		function doResetQueryToDo(type) {
			if(type == "approve") {
				$("#approveListKey").val("");
				$("#toDoList").datagrid("load");
			} else if(type == "notice") {
				$("#noticeListKey").val("");
				$("#noReadNoticeDetail").datagrid("options")["queryParams"] = {};
				$("#noReadNoticeDetail").datagrid("load");
			}
		}
		
		//批量阅读
		function doReadNotice() {
			var ntuList = $("#noReadNoticeDetail").datagrid("getSelections");
			if (ntuList && ntuList.length) {
				for ( var i in ntuList) {
					var ntuObj = ntuList[i];
					doRead(ntuObj.ntuId);
				}
				$("#noReadNoticeDetail").datagrid("load");
				$("#noReadNoticeDetail").datagrid("clearSelections");
			} else {
				alert("请选择一条通知");
				return;
			}
		}
		function doBatchView() {
			var ntuList = $("#toDoList").datagrid("getSelections");
			if (ntuList && ntuList.length) {
				var tids = [];
				var testObj = null;
				var moduleName = "待办事宜";
				for ( var i in ntuList) {
					var obj = ntuList[i];
					var tmpMn = obj["moduleName"];
					if (testObj === null) {
						testObj = {};
						testObj[tmpMn] = true;
					} else {
						if (testObj[tmpMn] !== true) {
							alert("只能对相同的模块名进行批量查看操作！");
							return;
						}
					}
					tids.push(obj["id"]);
					if (i == 0) {
						moduleName = tmpMn;
					}
				}
				var url = "${ctx}/sys/workflow/batch-view-process.action?taskIds="
						+ tids.join(",")
						+ "&moduleName="
						+ encodeURIComponent(moduleName);
				addTab(moduleName + "-待办事宜-批量查看", url);
				$("#aboutDoAction").window("close");
			} else {
				alert("请选择一条待办事宜");
				return;
			}
		}
		
		function initNotices() {
			$("#tnotice").tabs({
				onSelect:function(title,index) {
					if(index==0) {
						$("#_batch_agree_").show();
						$("#_batch_read_").hide();
					} else {
						$("#_batch_agree_").hide();
						$("#_batch_read_").show();
					}
				}
			});
			$("#aboutDoAction").window({
						minimizable : false,
						collapsible : false,
						onClose : function() {
						},
						onOpen : function() {
							$("#toDoList").datagrid(getToDoList());
							$("#noReadNoticeDetail").datagrid(
									getCurrentUserNotices());
							var tabIndex = $("#aboutDoAction").data("tabIndex");
							if(tabIndex > 0) {
								$("#tnotice").tabs("select", tabIndex);
							} else {
								$("#tnotice").tabs("select", 0);
							}
						}
			});
		}
		
		function openNotices(tabIndex) {
			$("#aboutDoAction").data("tabIndex", tabIndex);
			$("#aboutDoAction").window("open");
		}
		
		function setNoticesConfig() {
			var value = $("#aboutDoActionConfig").val();
			if (value == "1") {
				createCookie('noticesOpenType', value, 365);
				alert("成功设置通知弹出方式为消息变化时弹出！");
			} else if (value == "0") {
				createCookie('noticesOpenType', value, 365);
				alert("成功设置通知弹出方式为不自动弹出！");
			}
		}
		
		<%-- 
		function doRefreshSysParam() {
			var options = {
					url:'${ctx}/esapi/es/sysparam/reset',
					success:function(data) {
						$MsgUtil.alert(data.msg, "success");
					}
			};
			fnFormAjaxWithJson(options);
		}
		
		
		function updateAvatar(avatar) {
			var imgUrl = $ES.getAttachUrl(avatar);
			if(imgUrl) {
				$("#myAvatar").attr("src",imgUrl);
			}
		}--%>

		$(function() {
			<%-- updateAvatar("<s:property value="#session.USER.avatar" />");--%>
			initNotices();
			initCometPush();
			//initIm();
		});
	</script>
  </body>
</html>