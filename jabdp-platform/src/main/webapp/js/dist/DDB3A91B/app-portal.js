/*
  SlidesJS 3.0.3 http://slidesjs.com
  (c) 2013 by Nathan Searles http://nathansearles.com
  Updated: March 15th, 2013
  Apache License: http://www.apache.org/licenses/LICENSE-2.0
*/
(function(){(function(a,b,c){var d,e,f;return f="slidesjs",e={width:940,height:528,start:1,navigation:{active:!0,effect:"slide"},pagination:{active:!0,effect:"slide"},play:{active:!1,effect:"slide",interval:5e3,auto:!1,swap:!0,pauseOnHover:!1,restartDelay:2500},effect:{slide:{speed:500},fade:{speed:300,crossfade:!0}},callback:{loaded:function(){},start:function(){},complete:function(){}}},d=function(){function b(b,c){this.element=b,this.options=a.extend(!0,{},e,c),this._defaults=e,this._name=f,this.init()}return b}(),d.prototype.init=function(){var c,d,e,f,g,h,i=this;return c=a(this.element),this.data=a.data(this),a.data(this,"animating",!1),a.data(this,"total",c.children().not(".slidesjs-navigation",c).length),a.data(this,"current",this.options.start-1),a.data(this,"vendorPrefix",this._getVendorPrefix()),"undefined"!=typeof TouchEvent&&(a.data(this,"touch",!0),this.options.effect.slide.speed=this.options.effect.slide.speed/2),c.css({overflow:"hidden"}),c.slidesContainer=c.children().not(".slidesjs-navigation",c).wrapAll("<div class='slidesjs-container'>",c).parent().css({overflow:"hidden",position:"relative"}),a(".slidesjs-container",c).wrapInner("<div class='slidesjs-control'>",c).children(),a(".slidesjs-control",c).css({position:"relative",left:0}),a(".slidesjs-control",c).children().addClass("slidesjs-slide").css({position:"absolute",top:0,left:0,width:"100%",zIndex:0,display:"none",webkitBackfaceVisibility:"hidden"}),a.each(a(".slidesjs-control",c).children(),function(b){var c;return c=a(this),c.attr("slidesjs-index",b)}),this.data.touch&&(a(".slidesjs-control",c).on("touchstart",function(a){return i._touchstart(a)}),a(".slidesjs-control",c).on("touchmove",function(a){return i._touchmove(a)}),a(".slidesjs-control",c).on("touchend",function(a){return i._touchend(a)})),c.fadeIn(0),this.update(),this.data.touch&&this._setuptouch(),a(".slidesjs-control",c).children(":eq("+this.data.current+")").eq(0).fadeIn(0,function(){return a(this).css({zIndex:10})}),this.options.navigation.active&&(g=a("<a>",{"class":"slidesjs-previous slidesjs-navigation",href:"#",title:"Previous",text:"Previous"}).appendTo(c),d=a("<a>",{"class":"slidesjs-next slidesjs-navigation",href:"#",title:"Next",text:"Next"}).appendTo(c)),a(".slidesjs-next",c).click(function(a){return a.preventDefault(),i.stop(!0),i.next(i.options.navigation.effect)}),a(".slidesjs-previous",c).click(function(a){return a.preventDefault(),i.stop(!0),i.previous(i.options.navigation.effect)}),this.options.play.active&&(f=a("<a>",{"class":"slidesjs-play slidesjs-navigation",href:"#",title:"Play",text:"Play"}).appendTo(c),h=a("<a>",{"class":"slidesjs-stop slidesjs-navigation",href:"#",title:"Stop",text:"Stop"}).appendTo(c),f.click(function(a){return a.preventDefault(),i.play(!0)}),h.click(function(a){return a.preventDefault(),i.stop(!0)}),this.options.play.swap&&h.css({display:"none"})),this.options.pagination.active&&(e=a("<ul>",{"class":"slidesjs-pagination"}).appendTo(c),a.each(Array(this.data.total),function(b){var c,d;return c=a("<li>",{"class":"slidesjs-pagination-item"}).appendTo(e),d=a("<a>",{href:"#","data-slidesjs-item":b,html:b+1}).appendTo(c),d.click(function(b){return b.preventDefault(),i.stop(!0),i.goto(1*a(b.currentTarget).attr("data-slidesjs-item")+1)})})),a(b).bind("resize",function(){return i.update()}),this._setActive(),this.options.play.auto&&this.play(),this.options.callback.loaded(this.options.start)},d.prototype._setActive=function(b){var c,d;return c=a(this.element),this.data=a.data(this),d=b>-1?b:this.data.current,a(".active",c).removeClass("active"),a("li:eq("+d+") a",c).addClass("active")},d.prototype.update=function(){var b,c,d;return b=a(this.element),this.data=a.data(this),a(".slidesjs-control",b).children(":not(:eq("+this.data.current+"))").css({display:"none",left:0,zIndex:0}),d=b.width(),c=this.options.height/this.options.width*d,this.options.width=d,this.options.height=c,a(".slidesjs-control, .slidesjs-container",b).css({width:d,height:c})},d.prototype.next=function(b){var c;return c=a(this.element),this.data=a.data(this),a.data(this,"direction","next"),void 0===b&&(b=this.options.navigation.effect),"fade"===b?this._fade():this._slide()},d.prototype.previous=function(b){var c;return c=a(this.element),this.data=a.data(this),a.data(this,"direction","previous"),void 0===b&&(b=this.options.navigation.effect),"fade"===b?this._fade():this._slide()},d.prototype.goto=function(b){var c,d;if(c=a(this.element),this.data=a.data(this),void 0===d&&(d=this.options.pagination.effect),b>this.data.total?b=this.data.total:1>b&&(b=1),"number"==typeof b)return"fade"===d?this._fade(b):this._slide(b);if("string"==typeof b){if("first"===b)return"fade"===d?this._fade(0):this._slide(0);if("last"===b)return"fade"===d?this._fade(this.data.total):this._slide(this.data.total)}},d.prototype._setuptouch=function(){var b,c,d,e;return b=a(this.element),this.data=a.data(this),e=a(".slidesjs-control",b),c=this.data.current+1,d=this.data.current-1,0>d&&(d=this.data.total-1),c>this.data.total-1&&(c=0),e.children(":eq("+c+")").css({display:"block",left:this.options.width}),e.children(":eq("+d+")").css({display:"block",left:-this.options.width})},d.prototype._touchstart=function(b){var c,d;return c=a(this.element),this.data=a.data(this),d=b.originalEvent.touches[0],this._setuptouch(),a.data(this,"touchtimer",Number(new Date)),a.data(this,"touchstartx",d.pageX),a.data(this,"touchstarty",d.pageY),b.stopPropagation()},d.prototype._touchend=function(b){var c,d,e,f,g,h,i,j=this;return c=a(this.element),this.data=a.data(this),h=b.originalEvent.touches[0],f=a(".slidesjs-control",c),f.position().left>.5*this.options.width||f.position().left>.1*this.options.width&&250>Number(new Date)-this.data.touchtimer?(a.data(this,"direction","previous"),this._slide()):f.position().left<-(.5*this.options.width)||f.position().left<-(.1*this.options.width)&&250>Number(new Date)-this.data.touchtimer?(a.data(this,"direction","next"),this._slide()):(e=this.data.vendorPrefix,i=e+"Transform",d=e+"TransitionDuration",g=e+"TransitionTimingFunction",f[0].style[i]="translateX(0px)",f[0].style[d]=.85*this.options.effect.slide.speed+"ms"),f.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd",function(){return e=j.data.vendorPrefix,i=e+"Transform",d=e+"TransitionDuration",g=e+"TransitionTimingFunction",f[0].style[i]="",f[0].style[d]="",f[0].style[g]=""}),b.stopPropagation()},d.prototype._touchmove=function(b){var c,d,e,f,g;return c=a(this.element),this.data=a.data(this),f=b.originalEvent.touches[0],d=this.data.vendorPrefix,e=a(".slidesjs-control",c),g=d+"Transform",a.data(this,"scrolling",Math.abs(f.pageX-this.data.touchstartx)<Math.abs(f.pageY-this.data.touchstarty)),this.data.animating||this.data.scrolling||(b.preventDefault(),this._setuptouch(),e[0].style[g]="translateX("+(f.pageX-this.data.touchstartx)+"px)"),b.stopPropagation()},d.prototype.play=function(b){var c,d,e,f=this;return c=a(this.element),this.data=a.data(this),!this.data.playInterval&&(b&&(d=this.data.current,this.data.direction="next","fade"===this.options.play.effect?this._fade():this._slide()),a.data(this,"playInterval",setInterval(function(){return d=f.data.current,f.data.direction="next","fade"===f.options.play.effect?f._fade():f._slide()},this.options.play.interval)),e=a(".slidesjs-container",c),this.options.play.pauseOnHover&&(e.unbind(),e.bind("mouseenter",function(){return f.stop()}),e.bind("mouseleave",function(){return f.options.play.restartDelay?a.data(f,"restartDelay",setTimeout(function(){return f.play(!0)},f.options.play.restartDelay)):f.play()})),a.data(this,"playing",!0),a(".slidesjs-play",c).addClass("slidesjs-playing"),this.options.play.swap)?(a(".slidesjs-play",c).hide(),a(".slidesjs-stop",c).show()):void 0},d.prototype.stop=function(b){var c;return c=a(this.element),this.data=a.data(this),clearInterval(this.data.playInterval),this.options.play.pauseOnHover&&b&&a(".slidesjs-container",c).unbind(),a.data(this,"playInterval",null),a.data(this,"playing",!1),a(".slidesjs-play",c).removeClass("slidesjs-playing"),this.options.play.swap?(a(".slidesjs-stop",c).hide(),a(".slidesjs-play",c).show()):void 0},d.prototype._slide=function(b){var c,d,e,f,g,h,i,j,k,l,m=this;return c=a(this.element),this.data=a.data(this),this.data.animating||b===this.data.current+1?void 0:(a.data(this,"animating",!0),d=this.data.current,b>-1?(b-=1,l=b>d?1:-1,e=b>d?-this.options.width:this.options.width,g=b):(l="next"===this.data.direction?1:-1,e="next"===this.data.direction?-this.options.width:this.options.width,g=d+l),-1===g&&(g=this.data.total-1),g===this.data.total&&(g=0),this._setActive(g),i=a(".slidesjs-control",c),b>-1&&i.children(":not(:eq("+d+"))").css({display:"none",left:0,zIndex:0}),i.children(":eq("+g+")").css({display:"block",left:l*this.options.width,zIndex:10}),this.options.callback.start(d+1),this.data.vendorPrefix?(h=this.data.vendorPrefix,k=h+"Transform",f=h+"TransitionDuration",j=h+"TransitionTimingFunction",i[0].style[k]="translateX("+e+"px)",i[0].style[f]=this.options.effect.slide.speed+"ms",i.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd",function(){return i[0].style[k]="",i[0].style[f]="",i.children(":eq("+g+")").css({left:0}),i.children(":eq("+d+")").css({display:"none",left:0,zIndex:0}),a.data(m,"current",g),a.data(m,"animating",!1),i.unbind("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd"),i.children(":not(:eq("+g+"))").css({display:"none",left:0,zIndex:0}),m.data.touch&&m._setuptouch(),m.options.callback.complete(g+1)})):i.stop().animate({left:e},this.options.effect.slide.speed,function(){return i.css({left:0}),i.children(":eq("+g+")").css({left:0}),i.children(":eq("+d+")").css({display:"none",left:0,zIndex:0},a.data(m,"current",g),a.data(m,"animating",!1),m.options.callback.complete(g+1))}))},d.prototype._fade=function(b){var c,d,e,f,g,h=this;return c=a(this.element),this.data=a.data(this),this.data.animating||b===this.data.current+1?void 0:(a.data(this,"animating",!0),d=this.data.current,b?(b-=1,g=b>d?1:-1,e=b):(g="next"===this.data.direction?1:-1,e=d+g),-1===e&&(e=this.data.total-1),e===this.data.total&&(e=0),this._setActive(e),f=a(".slidesjs-control",c),f.children(":eq("+e+")").css({display:"none",left:0,zIndex:10}),this.options.callback.start(d+1),this.options.effect.fade.crossfade?(f.children(":eq("+this.data.current+")").stop().fadeOut(this.options.effect.fade.speed),f.children(":eq("+e+")").stop().fadeIn(this.options.effect.fade.speed,function(){return f.children(":eq("+e+")").css({zIndex:0}),a.data(h,"animating",!1),a.data(h,"current",e),h.options.callback.complete(e+1)})):f.children(":eq("+d+")").stop().fadeOut(this.options.effect.fade.speed,function(){return f.children(":eq("+e+")").stop().fadeIn(h.options.effect.fade.speed,function(){return f.children(":eq("+e+")").css({zIndex:10})}),a.data(h,"animating",!1),a.data(h,"current",e),h.options.callback.complete(e+1)}))},d.prototype._getVendorPrefix=function(){var a,b,d,e,f;for(a=c.body||c.documentElement,d=a.style,e="transition",f=["Moz","Webkit","Khtml","O","ms"],e=e.charAt(0).toUpperCase()+e.substr(1),b=0;f.length>b;){if("string"==typeof d[f[b]+e])return f[b];b++}return!1},a.fn[f]=function(b){return this.each(function(){return a.data(this,"plugin_"+f)?void 0:a.data(this,"plugin_"+f,new d(this,b))})}})(jQuery,window,document)}).call(this);

/*!
 * Marquee jQuery Plug-in
 *
 * Copyright 2009 Giva, Inc. (http://www.givainc.com/labs/) 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * 	http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Date: 2009-05-20
 * Rev:  1.0.01
 */
;(function($){
	// set the version of the link select
	$.marquee = {version: "1.0.01"};
	
	$.fn.marquee = function(options) {
		var method = typeof arguments[0] == "string" && arguments[0];
		var args = method && Array.prototype.slice.call(arguments, 1) || arguments;
		// get a reference to the first marquee found
		var self = (this.length == 0) ? null : $.data(this[0], "marquee");
		
		// if a method is supplied, execute it for non-empty results
		if( self && method && this.length ){

			// if request a copy of the object, return it			
			if( method.toLowerCase() == "object" ) return self;
			// if method is defined, run it and return either it's results or the chain
			else if( self[method] ){
				// define a result variable to return to the jQuery chain
				var result;
				this.each(function (i){
					// apply the method to the current element
					var r = $.data(this, "marquee")[method].apply(self, args);
					// if first iteration we need to check if we're done processing or need to add it to the jquery chain
					if( i == 0 && r ){
						// if this is a jQuery item, we need to store them in a collection
						if( !!r.jquery ){
							result = $([]).add(r);
						// otherwise, just store the result and stop executing
						} else {
							result = r;
							// since we're a non-jQuery item, just cancel processing further items
							return false;
						}
					// keep adding jQuery objects to the results
					} else if( !!r && !!r.jquery ){
						result = result.add(r);
					}
				});

				// return either the results (which could be a jQuery object) or the original chain
				return result || this;
			// everything else, return the chain
			} else return this;
		// initializing request
		} else {
			// create a new marquee for each object found
			return this.each(function (){
				new $.Marquee(this, options);
			});
		};
	};

	$.Marquee = function (marquee, options){
		options = $.extend({}, $.Marquee.defaults, options);
		
		var self = this, $marquee = $(marquee), $lis = $marquee.find("> li"), current = -1, hard_paused = false, paused = false, loop_count = 0;

		// store a reference to this marquee
		$.data($marquee[0], "marquee", self);
		
		// pause the marquee
		this.pause = function (){
			// mark as hard pause (no resume on hover)
			hard_paused = true;
			// pause scrolling
			pause();
		}
		
		// resume the marquee
		this.resume = function (){
			// mark as hard pause (no resume on hover)
			hard_paused = false;
			// resume scrolling
			resume();
		}
		
		// update the marquee
		this.update = function (){
			var iCurrentCount = $lis.length;

			// update the line items
			$lis = $marquee.find("> li");
			
			// if we only have one item, show the next item by resuming playback (which will scroll to the next item)
			if( iCurrentCount <= 1 ) resume();
		}

		// code to introduce the new marquee message
		function show(i){
			// if we're already scrolling an item, stop processing
			if( $lis.filter("." + options.cssShowing).length > 0 ) return false;
			
			var $li = $lis.eq(i);
			
			// run the beforeshow callback
			if( $.isFunction(options.beforeshow) ) options.beforeshow.apply(self, [$marquee, $li]);

			var params = {
				top: (options.yScroll == "top" ? "-" : "+") + $li.outerHeight() + "px"
				, left: 0
			};
			
			$marquee.data("marquee.showing", true);
			$li.addClass(options.cssShowing);
	
			$li.css(params).animate({top: "0px"}, options.showSpeed, options.fxEasingShow, function (){ 
				// run the show callback
				if( $.isFunction(options.show) ) options.show.apply(self, [$marquee, $li]);
				$marquee.data("marquee.showing", false);
				scroll($li);
			});
		}

		// keep the message on the screen for the user to read, scrolling long messages
		function scroll($li, delay){
			// if paused, stop processing
			if( paused == true ) return false;

			// get the delay speed
			delay = delay || options.pauseSpeed;
			// if	item is wider than marquee, then scroll
		//	if( doScroll($li) ){
				setTimeout(function (){
					// if paused, stop processing (we need to check to see if the pause state has changed)
					if( paused == true ) return false;

					var width = $li.outerWidth(), endPos = width * -1, curPos = parseInt($li.css("left"), 10);

					// scroll the message to the left					
					$li.animate({left: endPos + "px"}, ((width + curPos) * options.scrollSpeed), options.fxEasingScroll, function (){ finish($li); });
				}, delay);
		//	}
		/*	else if ( $lis.length > 1 ){
				setTimeout(function (){
					// if paused, stop processing (we need to check to see if the pause state has changed)
					if( paused == true ) return false;

					// scroll the message down
					$li.animate({top: (options.yScroll == "top" ? "+" : "-") + $marquee.innerHeight() + "px"}, options.showSpeed, options.fxEasingScroll);
					// finish showing this message
					finish($li);
				}, delay);
			}*/
			
		}
		
		function finish($li){
			// run the aftershow callback, only after we've displayed the first option
			if( $.isFunction(options.aftershow) ) options.aftershow.apply(self, [$marquee, $li]);
			
			// mark that we're done scrolling this element
			$li.removeClass(options.cssShowing);
			
			// show the next message
			showNext();
		}

		// this function will pause the current animation
		function pause(){
			// mark the message as paused
			paused = true;
			// don't stop animation if we're just beginning to show the marquee message
			if( $marquee.data("marquee.showing") != true ){
				// we must dequeue() the animation to ensure that it does indeed stop animation
				$lis.filter("." + options.cssShowing).dequeue().stop();
			}
		}
		
		// this function will resume the previous animation
		function resume(){
			// mark the message as resumed
			paused = false;
			// don't resume animation if we haven't completed introducing the message
			if( $marquee.data("marquee.showing") != true ) scroll($lis.filter("." + options.cssShowing), 1);
		}

		// determine if we should pause on hover
		if( options.pauseOnHover ){
			$marquee.hover(
				function (){
					// if hard paused, prevent hover events
					if( hard_paused ) return false;
					// pause scrolling
					pause();
				}
				, function (){
					// if hard paused, prevent hover events
					if( hard_paused ) return false;
					// resume scrolling
					resume();
				}
			);
		}
		
		// determines if the message needs to be scrolled to read
		function doScroll($li){
			return ($li.outerWidth() > $marquee.innerWidth());
		}

		// show the next message in the queue		
		function showNext(){
			// increase the current counter (starts at -1, to indicate a new marquee beginning)
			current++;
			
			// if we only have 1 entry and it doesn't need to scroll, just cancel processing
			if( current >= $lis.length ){
				// if we've reached our loop count, cancel processing
				if( !isNaN(options.loop) && options.loop > 0 && (++loop_count >= options.loop ) ) return false;
				current = 0;
			} 
			
			// show the next message
			show(current);
		}
		
		// run the init callback
		if( $.isFunction(options.init) ) options.init.apply(self, [$marquee, options]);
		
		// show the first item
		showNext();
	};

	$.Marquee.defaults = {
		  yScroll: "top"                          // the position of the marquee initially scroll (can be either "top" or "bottom")
		, showSpeed: 850                          // the speed of to animate the initial dropdown of the messages
		, scrollSpeed: 12                         // the speed of the scrolling (keep number low)
		, pauseSpeed: 5000                        // the time to wait before showing the next message or scrolling current message
		, pauseOnHover: true                      // determine if we should pause on mouse hover
		, loop: -1                                // determine how many times to loop through the marquees (#'s < 0 = infinite)
		, fxEasingShow: "swing"                   // the animition easing to use when showing a new marquee
		, fxEasingScroll: "linear"                // the animition easing to use when showing a new marquee

		// define the class statements
		, cssShowing: "marquee-showing"

		// event handlers
		, init: null                              // callback that occurs when a marquee is initialized
		, beforeshow: null                        // callback that occurs before message starts scrolling on screen
		, show: null                              // callback that occurs when a new marquee message is displayed
		, aftershow: null                         // callback that occurs after the message has scrolled
	};

})(jQuery);

/**
 * jQuery.timers - Timer abstractions for jQuery
 * Written by Blair Mitchelmore (blair DOT mitchelmore AT gmail DOT com)
 * Licensed under the WTFPL (http://sam.zoy.org/wtfpl/).
 * Date: 2009/10/16
 *
 * @author Blair Mitchelmore
 * @version 1.2
 *
 **/

jQuery.fn.extend({
	everyTime: function(interval, label, fn, times) {
		return this.each(function() {
			jQuery.timer.add(this, interval, label, fn, times);
		});
	},
	oneTime: function(interval, label, fn) {
		return this.each(function() {
			jQuery.timer.add(this, interval, label, fn, 1);
		});
	},
	stopTime: function(label, fn) {
		return this.each(function() {
			jQuery.timer.remove(this, label, fn);
		});
	}
});

jQuery.extend({
	timer: {
		global: [],
		guid: 1,
		dataKey: "jQuery.timer",
		regex: /^([0-9]+(?:\.[0-9]*)?)\s*(.*s)?$/,
		powers: {
			// Yeah this is major overkill...
			'ms': 1,
			'cs': 10,
			'ds': 100,
			's': 1000,
			'das': 10000,
			'hs': 100000,
			'ks': 1000000
		},
		timeParse: function(value) {
			if (value == undefined || value == null)
				return null;
			var result = this.regex.exec(jQuery.trim(value.toString()));
			if (result[2]) {
				var num = parseFloat(result[1]);
				var mult = this.powers[result[2]] || 1;
				return num * mult;
			} else {
				return value;
			}
		},
		add: function(element, interval, label, fn, times) {
			var counter = 0;
			
			if (jQuery.isFunction(label)) {
				if (!times) 
					times = fn;
				fn = label;
				label = interval;
			}
			
			interval = jQuery.timer.timeParse(interval);

			if (typeof interval != 'number' || isNaN(interval) || interval < 0)
				return;

			if (typeof times != 'number' || isNaN(times) || times < 0) 
				times = 0;
			
			times = times || 0;
			
			var timers = jQuery.data(element, this.dataKey) || jQuery.data(element, this.dataKey, {});
			
			if (!timers[label])
				timers[label] = {};
			
			fn.timerID = fn.timerID || this.guid++;
			
			var handler = function() {
				if ((++counter > times && times !== 0) || fn.call(element, counter) === false)
					jQuery.timer.remove(element, label, fn);
			};
			
			handler.timerID = fn.timerID;
			
			if (!timers[label][fn.timerID])
				timers[label][fn.timerID] = window.setInterval(handler,interval);
			
			this.global.push( element );
			
		},
		remove: function(element, label, fn) {
			var timers = jQuery.data(element, this.dataKey), ret;
			
			if ( timers ) {
				
				if (!label) {
					for ( label in timers )
						this.remove(element, label, fn);
				} else if ( timers[label] ) {
					if ( fn ) {
						if ( fn.timerID ) {
							window.clearInterval(timers[label][fn.timerID]);
							delete timers[label][fn.timerID];
						}
					} else {
						for ( var fn in timers[label] ) {
							window.clearInterval(timers[label][fn]);
							delete timers[label][fn];
						}
					}
					
					for ( ret in timers[label] ) break;
					if ( !ret ) {
						ret = null;
						delete timers[label];
					}
				}
				
				for ( ret in timers ) break;
				if ( !ret ) 
					jQuery.removeData(element, this.dataKey);
			}
		}
	}
});

jQuery(window).bind("unload", function() {
	jQuery.each(jQuery.timer.global, function(index, item) {
		jQuery.timer.remove(item);
	});
});
/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2008 George McGinley Smith
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		//alert(jQuery.easing.default);
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158; 
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});

/*
 *
 * TERMS OF USE - EASING EQUATIONS
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2001 Robert Penner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */
/*

	GalleryView - jQuery Content Gallery Plugin
	Author:		Jack Anderson
	Version:	3.0 DEVELOPMENT

  	See README.txt for instructions on how to markup your HTML
*/

// Make sure Object.create is available in the browser (for our prototypal inheritance)
// Courtesy of Douglas Crockford
if (typeof Object.create !== 'function') {
    Object.create = function (o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}

(function ($) {
	// custom image object
	var gvImage = function (img) {

		this.src = { 
			panel: img.attr('src'),
			//href: img.attr('href'),
			//ck: img.attr('ck'),
			frame: img.data('frame') || img.attr('src')
		};
		this.scale = {
			panel: null,
			frame: null
		};
		this.height = 0;
		this.width = 0;
		this.attrs = {
			title: img.attr('title') || img.attr('alt'),
			description: img.data('description'),
			href: img.attr('href'),
			ck: img.attr('ck')
		};
		this.href = null;
		this.dom_obj = null;
		
		return this;
	},
	
	// utility function wrapper
	gv = {
		getInt: function(i) {
			i = parseInt(i, 10);
			return isNaN(i) ? 0 : i;
		},
		innerWidth: function(elem) {
			return this.getInt(elem.css('width')) || 0;	
		},
		outerWidth: function(elem) {
			return 	this.innerWidth(elem) + 
					this.extraWidth(elem);
		},
		extraWidth: function(elem) {
			return	this.getInt(elem.css('paddingLeft')) +
					this.getInt(elem.css('paddingRight')) +
					this.getInt(elem.css('borderLeftWidth')) +
					this.getInt(elem.css('borderRightWidth'));	
		},
		innerHeight: function(elem) {
			return this.getInt(elem.css('height'))|| 0;
		},
		outerHeight: function(elem) {
			return 	this.innerHeight(elem) + 
					this.extraHeight(elem);
		},
		extraHeight: function(elem) {
			return 	this.getInt(elem.css('paddingTop')) +
					this.getInt(elem.css('paddingBottom')) +
					this.getInt(elem.css('borderTopWidth')) +
					this.getInt(elem.css('borderBottomWidth'));
		}
	},
	
	/*
		GalleryView - Object
			The main gallery class
	*/		
	GalleryView = {
		// array of dom elements
		elems: [
			'.gv_galleryWrap',	'.gv_gallery',		'.gv_panelWrap',		'.gv_panel',
			'img.gv_image',		'.gv_infobar',		'.gv_filmstripWrap',	'.gv_filmstrip',
			'.gv_frame',		'.gv_thumbnail', 	'.gv_caption', 			'img.gv_thumb',
			'.gv_navWrap',		'.gv_navNext',		'.gv_navPrev',			'.gv_navPlay',
			'.gv_panelNavNext',	'.gv_panelNavPrev',	'.gv_overlay',			'.gv_showOverlay',
			'.gv_imageStore'
		],
		
		// create a jQuery element and apply attributes
		createElem: function(attrs,elem) {
			elem = document.createElement(elem);
			var $elem = $(elem);
			return $elem.attr(attrs);
		},
		
		// get the position of an element with respect
		// to the gallery wrapper
		getPos: function (el) {
			var self = this,
				dom = this.dom,
				el = el[0],
				el_id = el.id,
				left = 0,
				top = 0,
				gPos, gLeft, gTop;
				
			if (!el) { return { top: 0, left: 0 }; }
			
			if (el.offsetParent) {
				do {
					left += el.offsetLeft;
					top += el.offsetTop;
				} while (el = el.offsetParent);
			}
			
			//If we want the position of the gallery itself, return it
			if (el_id === self.id) { return { left: left, top: top }; }
			
			//Otherwise, get position of element relative to gallery
			else {
				gPos = self.getPos(dom.galleryWrap);
				gLeft = gPos.left;
				gTop = gPos.top;
				return { left: left - gLeft, top: top - gTop };
			}
		},
		
		// determine if mouse is within the boundary of the gallery wrapper
		mouseIsOverGallery: function (x, y) {
			var self = this,
				dom = this.dom,
				pos = this.getPos(dom.gv_galleryWrap),
				top = pos.top,
				left = pos.left;
				
			return x > left && x < left + gv.outerWidth(dom.gv_galleryWrap) && y > top && y < top + gv.outerHeight(dom.gv_galleryWrap);
		},
		
		// determine if mouse is within the boundary of the panel
		mouseIsOverPanel: function (x, y) {
			var self = this,
				dom = this.dom,
				pos = this.getPos(dom.gv_panelWrap),
				gPos = this.getPos(dom.gv_galleryWrap),
				top = pos.top + gPos.top,
				left = pos.left + gPos.left;
				
			return x > left && x < left + gv.outerWidth(dom.gv_panelWrap) && y > top && y < top + gv.outerHeight(dom.gv_panelWrap);
		},
		
		// create gvImage objects for each image in gallery
		storeImages: function() {
			var self = this;
			this.sourceImgs = $('li>img',this.$el);
			this.numImages = this.sourceImgs.length;
			this.gvImages = [];
			this.sourceImgs.each(function(i,img) {
				self.gvImages[i] = new gvImage($(img));
			});
		},
		
		setDimensions: function() {
			var self = this,
				dom = this.dom,
				widths = {
					prev: gv.innerWidth(dom.gv_navPrev),
					play: gv.innerWidth(dom.gv_navPlay),
					next: gv.innerWidth(dom.gv_navNext),
					filmstrip: this.opts.frame_width,
					fsMax: 0,
					fsFull: 0
				},
				heights = {
					prev: gv.innerHeight(dom.gv_navPrev),
					play: gv.innerHeight(dom.gv_navPlay),
					next: gv.innerHeight(dom.gv_navNext),
					filmstrip: this.opts.frame_height + (this.opts.show_captions ? gv.outerHeight(dom.gv_caption) : 0),
					fsMax: 0,
					fsFull: 0
				},
				panels = [];
			
			// nav
			if(this.filmstripOrientation === 'horizontal') {
				dom.gv_navWrap.css({
					width: widths.prev + widths.play + widths.next,
					height: Math.max(heights.prev,heights.play,heights.next)
				});
			} else {
				if(this.opts.filmstrip_style === 'scroll' && this.opts.frame_width < (widths.prev + widths.play + widths.next)) {
					dom.gv_navWrap.css({
						width: Math.max(widths.prev, widths.play, widths.next),
						height: heights.prev + heights.play + heights.next
					});
				} else {
					dom.gv_navWrap.css({
						width: widths.prev + widths.play + widths.next,
						height: Math.max(heights.prev,heights.play,heights.next)
					});
				}
			}
			
			if(this.filmstripOrientation === 'vertical' && widths.filmstrip < (widths.prev + widths.play + widths.next)) {
				dom.gv_navWrap.css({
					width: Math.max(widths.prev, widths.play, widths.next),
					height: heights.prev + heights.play + heights.next
				});
			} else {
				dom.gv_navWrap.css({
					width: widths.prev + widths.play + widths.next,
					height: Math.max(heights.prev,heights.play,heights.next)
				});
			}
			
			// panels
			dom.gv_panel.css({
				width: this.opts.panel_width,
				height: this.opts.panel_height
			});
			dom.gv_panelWrap.css({
				width: gv.outerWidth(dom.gv_panel),
				height: gv.outerHeight(dom.gv_panel)
			});
			dom.gv_overlay.css({
				width: this.opts.panel_width
			});
			
			
			
			$.each(this.gvImages,function(i,img) {
				dom.gv_panelWrap.append(dom.gv_panel.clone(true));
			});
			
			dom.gv_panels = dom.gv_panelWrap.find('.gv_panel');
			dom.gv_panels.remove();
			
			// filmstrip
			dom.gv_thumbnail.css({
				width: this.opts.frame_width,
				height: this.opts.frame_height
			});
			dom.gv_frame.css({
				width: gv.outerWidth(dom.gv_thumbnail),
				height: gv.outerHeight(dom.gv_thumbnail) + (this.opts.show_captions ? gv.outerHeight(dom.gv_caption) : 0),
				marginRight: this.opts.frame_gap,
				marginBottom: this.opts.frame_gap
			});
			
			
			if(this.filmstripOrientation === 'horizontal') {
				this.filmstripSize = Math.floor((gv.outerWidth(dom.gv_panelWrap) - gv.outerWidth(dom.gv_navWrap)) / (gv.outerWidth(dom.gv_frame) + this.opts.frame_gap));
				widths.fsMax = this.filmstripSize * (gv.outerWidth(dom.gv_frame) + this.opts.frame_gap);
				widths.fsFull = this.gvImages.length * (gv.outerWidth(dom.gv_frame) + this.opts.frame_gap);
				widths.filmstrip = Math.min(widths.fsMax,widths.fsFull);
				if(this.opts.filmstrip_style !== 'scroll') {
					heights.filmstrip = (Math.ceil(this.gvImages.length / this.filmstripSize) * (gv.outerHeight(dom.gv_frame) + this.opts.frame_gap)) - this.opts.frame_gap;
				}
			} else {
				this.filmstripSize = Math.floor((gv.outerHeight(dom.gv_panelWrap) - gv.outerHeight(dom.gv_navWrap)) / (gv.outerHeight(dom.gv_frame) + this.opts.frame_gap));
				heights.fsMax = this.filmstripSize * (gv.outerHeight(dom.gv_frame) + this.opts.frame_gap);
				heights.fsFull = this.gvImages.length * (gv.outerHeight(dom.gv_frame) + this.opts.frame_gap);
				heights.filmstrip = Math.min(heights.fsMax,heights.fsFull);
				if(this.opts.filmstrip_style !== 'scroll') {
					widths.filmstrip = (Math.ceil(this.gvImages.length / this.filmstripSize) * (gv.outerWidth(dom.gv_frame) + this.opts.frame_gap)) - this.opts.frame_gap;
				}
			}
			dom.gv_filmstripWrap.css({
				width: widths.filmstrip,
				height: heights.filmstrip
			});
			
			// gallery
			if(this.opts.show_filmstrip) {
				if(this.filmstripOrientation === 'horizontal') {
					dom.gv_gallery.css({
						width: gv.outerWidth(dom.gv_panelWrap),
						height: gv.outerHeight(dom.gv_panelWrap) + this.opts.frame_gap + (this.opts.show_filmstrip ? Math.max(gv.outerHeight(dom.gv_filmstripWrap),gv.outerHeight(dom.gv_navWrap)) : gv.outerHeight(dom.gv_filmstripWrap))
					});
				} else {
					dom.gv_gallery.css({
						width: gv.outerWidth(dom.gv_panelWrap) + this.opts.frame_gap + (this.opts.show_filmstrip ? Math.max(gv.outerWidth(dom.gv_filmstripWrap),gv.outerWidth(dom.gv_navWrap)) : gv.outerWidth(dom.gv_filmstripWrap)),
						height: gv.outerHeight(dom.gv_panelWrap)
					});	
				}
			} else {
				dom.gv_gallery.css({
					width: gv.outerWidth(dom.gv_panelWrap),
					height: gv.outerHeight(dom.gv_panelWrap)
				});	
			}
			
			dom.gv_galleryWrap.css({
					width: gv.outerWidth(dom.gv_gallery),
					height: gv.outerHeight(dom.gv_gallery),
					padding: this.opts.frame_gap
			});
		},
		
		setPositions: function() {
			var self = this,
				dom = this.dom,
				navVert = 0, fsVert = 0,
				navHorz = 0, fsHorz = 0,
				vert, horz;
			
			// determine vertical or horizontal offset
			// if negative, apply to filmstrip, otherwise apply to navbar
			if(this.filmstripOrientation === 'horizontal') {
				vert = Math.round((gv.outerHeight(dom.gv_filmstripWrap) - gv.outerHeight(dom.gv_navWrap)) / 2);
				if(vert < 0) { fsVert = -1 * vert; }
				else { navVert = vert; }
			} else {
				horz = Math.round((gv.outerWidth(dom.gv_filmstripWrap) - gv.outerWidth(dom.gv_navWrap)) / 2);
				if(horz < 0) { fsHorz = -1 * horz; }
				else { navHorz = horz; }
			}
			
			// for horizontal filmstrips w/o navigation, center the filmstrip under the panel
			if(!this.opts.show_filmstrip_nav && this.filmstripOrientation === 'horizontal') {
				fsHorz = Math.floor((gv.outerWidth(dom.gv_panelWrap) - gv.outerWidth(dom.gv_filmstripWrap)) / 2);
			}
			
			dom.gv_panelNavNext.css({ top: (gv.outerHeight(dom.gv_panel) - gv.outerHeight(dom.gv_panelNavNext)) / 2, right: 10 });
			dom.gv_panelNavPrev.css({ top: (gv.outerHeight(dom.gv_panel) - gv.outerHeight(dom.gv_panelNavPrev)) / 2, left: 10 });
			
			// pin elements to gallery corners according to filmstrip position
			switch(this.opts.filmstrip_position) {
				case 'top':
					dom.gv_navWrap.css({ top: navVert, right: navHorz });
					dom.gv_panelWrap.css({ bottom: 0, left: 0 });
					dom.gv_filmstripWrap.css({ top: fsVert, left: fsHorz });
					break;
				
				case 'right':
					dom.gv_navWrap.css({ bottom: navVert, right: navHorz });
					dom.gv_panelWrap.css({ top: 0, left: 0 });
					dom.gv_filmstripWrap.css({ top: fsVert, right: fsHorz });
					break;
				
				case 'left':
					dom.gv_navWrap.css({ bottom: navVert, left: navHorz });
					dom.gv_panelWrap.css({ top: 0, right: 0 });
					dom.gv_filmstripWrap.css({ top: fsVert, left: fsHorz });
					break;
				
				default:
					dom.gv_navWrap.css({ bottom: navVert, right: navHorz });
					dom.gv_panelWrap.css({ top: 0, left: 0 });
					dom.gv_filmstripWrap.css({ bottom: fsVert, left: fsHorz });
					break;
			}
			
			if(this.opts.overlay_position === 'top') {
				dom.gv_overlay.css({ top: 0, left: -99999 });
				dom.gv_showOverlay.css({ top: 0, left: 0 });
			} else {
				dom.gv_overlay.css({ bottom: 0, left: -99999 });
				dom.gv_showOverlay.css({ bottom: 0, left: 0 });
			}
			
			if(!this.opts.show_filmstrip_nav) {
				dom.gv_navWrap.remove();	
			}
		},
		
		buildFilmstrip: function() {
			var self = this,
				dom = this.dom,
				framesLength = this.gvImages.length * ((this.filmstripOrientation === 'horizontal' ? this.opts.frame_width : this.opts.frame_height) + this.opts.frame_gap);
			
			dom.gv_frame.append(dom.gv_thumbnail);
			if(this.opts.show_captions) { 
				dom.gv_frame.append(dom.gv_caption);
			}
			dom.gv_thumbnail.css('opacity',this.opts.frame_opacity);
			
			dom.gv_thumbnail.bind({
				mouseover: function() {
					if(!$(this).hasClass('current')) {
						$(this).stop().animate({opacity:1},250);
					}
				},
				mouseout: function() {
					if(!$(this).hasClass('current')) {
						$(this).stop().animate({opacity:self.opts.frame_opacity},250);
					}
				}
			});
			
			// Drop a clone of the frame element into the filmstrip for each source image
			$.each(this.gvImages,function(i,img) {
				dom.gv_frame.clone(true).prependTo(dom.gv_filmstrip);
			});
			
			dom.gv_filmstrip.css({
				width: gv.outerWidth(dom.gv_frame),
				height: gv.outerHeight(dom.gv_frame)
			});
			
			// If we are scrolling the filmstrip, and we can't show all frames at once,
			// make two additional copies of each frame
			if(this.opts.filmstrip_style === 'scroll') {
				if(this.filmstripOrientation === 'horizontal') {
					if(framesLength > gv.innerWidth(dom.gv_filmstripWrap)) {
						dom.gv_filmstrip.find('.gv_frame').clone(true).appendTo(dom.gv_filmstrip).clone(true).appendTo(dom.gv_filmstrip);
						dom.gv_filmstrip.css('width',framesLength * 3);
						this.scrolling = true;
					} else {
						dom.gv_filmstrip.css('width',framesLength);
					}
				} else {
					if(framesLength > gv.innerHeight(dom.gv_filmstripWrap)) {
						dom.gv_filmstrip.find('.gv_frame').clone(true).appendTo(dom.gv_filmstrip).clone(true).appendTo(dom.gv_filmstrip);
						dom.gv_filmstrip.css('height',framesLength * 3);
						this.scrolling = true;
					} else {
						dom.gv_filmstrip.css('height',framesLength);
					}
				}
			} else {
				dom.gv_filmstrip.css({
					width: parseInt(dom.gv_filmstripWrap.css('width'),10)+this.opts.frame_gap,
					height: parseInt(dom.gv_filmstripWrap.css('height'),10)+this.opts.frame_gap
				});
			}
			dom.gv_frames = dom.gv_filmstrip.find('.gv_frame');
			$.each(dom.gv_frames,function(i,frame) {
				$(frame).data('frameIndex',i);						  
			});
			dom.gv_thumbnails = dom.gv_filmstrip.find('div.gv_thumbnail');
		},
		
		buildGallery: function() {
			var self = this,
				dom = this.dom;
			
			this.setDimensions();
			this.setPositions();
			
			if(this.opts.show_filmstrip) {
				this.buildFilmstrip();
			}
		},
		
		showInfoBar: function() {
			if(!this.opts.show_infobar) { return; }
			var self = this,
				dom = this.dom;
			
			dom.gv_infobar.stop().stopTime('hideInfoBar_' + self.id).html((this.iterator+1) + ' of ' + this.numImages).show().css('opacity',this.opts.infobar_opacity);
			
			dom.gv_infobar.oneTime(2000 + this.opts.transition_speed,'hideInfoBar_' + self.id,function() {
					dom.gv_infobar.fadeOut(1000);
				});
		},
		
		initImages: function() {
			var self = this,
				dom = this.dom;
			$.each(this.gvImages,function(i,gvImage) {
				var img = $('<img/>');
				img.css('visibility','hidden').data('index',i);
				img.bind('load.galleryview',function() {
					var _img = $(this),
						index = _img.data('index'),
						width = this.width,
						height = this.height,
						imghref = gvImage.attrs["href"],
						imgck = gvImage.attrs["ck"],
						imgtitle = gvImage.attrs["title"],
						parent = dom[(_img.data('parent')).type].eq((_img.data('parent')).index),
						widthFactor = gv.innerWidth(parent) / width,
						heightFactor = gv.innerHeight(parent) / height,
						parentType = parent.hasClass('gv_panel') ? 'panel' : 'frame',
						heightOffset = 0, widthOffset = 0;
					
					gvImage.scale[parentType] = self.opts[parentType+'_scale'] === 'fit' ? Math.min(widthFactor,heightFactor) : Math.max(widthFactor,heightFactor);
					
					widthOffset = Math.round((gv.innerWidth(parent) - (width * gvImage.scale[parentType])) / 2);
					heightOffset = Math.round((gv.innerHeight(parent) - (height * gvImage.scale[parentType])) / 2);	
					
					_img.css({
						width: width * gvImage.scale[parentType],
						height: height * gvImage.scale[parentType],
						top: heightOffset,
						left: widthOffset
					});
					_img.hide().css('visibility','visible');
					_img.remove().appendTo(parent);
					
					_img.css("cursor","pointer");
					
					if(parentType == "panel")
					{
						_img.click(function(){
							if(imgck){
							var func = eval(imgck);
                             func.call(func,imghref,imgtitle);
							}
							else{
								window.open(imghref);
							}
							
						})
					}
					
					
					if(parentType === 'frame') {
						_img.fadeIn();
						parent.parent().removeClass('gv_frame-loading');
						parent.parent().find('.gv_caption').html(gvImage.attrs.title);
					} else if(index === self.opts.start_frame - 1) {
						parent.prependTo(dom.gv_panelWrap);
						parent.removeClass('gv_panel-loading');
						_img.fadeIn();
						self.showInfoBar();
					} else {
						parent.removeClass('gv_panel-loading');
						_img.show();
					}
				});
				
				// store eventual image container as data property
				// append to temporary storage element and set src
				if(self.opts.show_panels) {
					img.clone(true)
						.data('parent',{type:'gv_panels',index:i})
						.appendTo(dom.gv_imageStore)
						.attr('href',gvImage.src.href)
						.attr('onclick',gvImage.src.onclick)
						.attr('src',gvImage.src.panel);
				}
				
				if(self.opts.show_filmstrip) {
					img.clone(true)
						.data('parent',{type:'gv_thumbnails',index:i})
						.appendTo(dom.gv_imageStore)
						.attr('src',gvImage.src.frame);
					
					if(dom.gv_frames.length > dom.gv_panels.length) {
						img.clone(true)
							.data('parent',{type:'gv_thumbnails',index:i+self.numImages})
							.appendTo(dom.gv_imageStore)
							.attr('src',gvImage.src.frame);
							
						img.clone(true)
							.data('parent',{type:'gv_thumbnails',index:i+self.numImages+self.numImages})
							.appendTo(dom.gv_imageStore)
							.attr('src',gvImage.src.frame);
					}
				}
			});
		},
		
		showNext: function() {
			this.navAction = 'next';
			this.showItem(this.frameIterator+1);
		},
		
		showPrev: function() {
			this.navAction = 'prev';
			this.showItem(this.frameIterator-1);
		},
		
		showItem: function(i) {
			if(isNaN(i)) { return; }
			if(!this.opts.show_filmstrip) { i = i % this.numImages; }
			
			var self = this,
				dom = this.dom,
				frame_i = i,
				newPanelStart,
				oldPanelEnd,
				oldIterator,
				panel,
				playing = false;
				
			// don't go out of bounds
			if(i >= this.numImages) {
				i = i % this.numImages;
			} else if(i < 0) {
				i = this.numImages - 1;
				if(dom.gv_frames != undefined) {
					frame_i = dom.gv_frames.length - 1;
				} else {
					frame_i = dom.gv_panels.length - 1;
				}
			}
			
			panel = dom.gv_panels.eq(i);
			
			playing = this.playing;
			
			if(playing) {
				this.stopSlideshow(false);
			}
			
			this.unbindActions();
			
			dom.gv_gallery.oneTime(this.opts.transition_speed,'bindActions_' + self.id,function(){ if(playing) { self.startSlideshow(false); } self.bindActions(); });
			
			switch(this.opts.panel_animation) {
				case 'crossfade':
					dom.gv_panels.eq(this.iterator).fadeOut(this.opts.transition_speed,function(){$(this).remove();});
					panel.hide().prependTo(dom.gv_panelWrap).fadeIn(this.opts.transition_speed);
					break;
				case 'fade':
					dom.gv_panels.eq(this.iterator).remove();
					panel.hide().prependTo(dom.gv_panelWrap).fadeIn(this.opts.transition_speed);
					break;
				case 'slide':
					if(this.navAction === 'next' || (this.navAction === 'frame' && frame_i > this.iterator)) {
						newPanelStart = gv.outerWidth(dom.gv_panel);
						oldPanelEnd = -1 * gv.outerWidth(dom.gv_panel);
					} else {
						newPanelStart = -1 * gv.outerWidth(dom.gv_panel);
						oldPanelEnd = gv.outerWidth(dom.gv_panel);
					}
					
					panel.css({ left:newPanelStart }).appendTo(dom.gv_panelWrap).animate(
						{ left:0 },
						{ duration: this.opts.transition_speed,easing: this.opts.easing }
					);
					
					dom.gv_panels.eq(this.iterator).animate(
						{ left: oldPanelEnd },
						{ duration: this.opts.transition_speed, easing: this.opts.easing, complete: function(){ $(this).remove(); } }
					);
					break;
				default:
					dom.gv_panels.eq(this.iterator).remove();
					panel.prependTo(dom.gv_panelWrap);
					break;
			}
			
			this.updateOverlay(i);
			
			this.iterator = i;
			this.updateFilmstrip(frame_i);
			this.showInfoBar();
			
			
		},
		
		updateOverlay: function(i) {
			var self = this,
				dom = this.dom;
			
			if(this.overlayVisible) {
				this.hideOverlay(null,function(){
					if(self.gvImages[i]) {
						dom.gv_overlay.html('<h4>'+self.gvImages[i].attrs.title+'</h4><p>'+self.gvImages[i].attrs.description+'</p>');
						self.showOverlay();
					}
				});
			} else {
				if(self.gvImages[i]) {
					dom.gv_overlay.html('<h4>'+self.gvImages[i].attrs.title+'</h4><p>'+self.gvImages[i].attrs.description+'</p>');
					dom.gv_overlay.css(this.opts.overlay_position,-1 * dom.gv_overlay.outerHeight());
				}
			}
			
		},
		
		hideOverlay: function(s,callback) {
			var self = this,
				dom = this.dom,
				endOverlay = {},
				endButton = {},
				speed = s || self.opts.transition_speed / 2;
				
			callback = callback || function(){};
			
			endOverlay[this.opts.overlay_position] = -1 * dom.gv_overlay.outerHeight();
			endButton[this.opts.overlay_position] = 0;
			
			dom.gv_overlay.animate(endOverlay,{ 
				duration: speed, 
				easing: 'swing', 
				complete: callback
			});
			dom.gv_showOverlay.animate(endButton,{
				duration: speed,
				easing: 'swing'
			});
			
			this.overlayVisible = false;
		},
		
		showOverlay: function(s) {
			var self = this,
				dom = this.dom,
				startOverlay = {},
				endOverlay = {},
				endButton = {},
				speed = s || self.opts.transition_speed / 2;
			
			startOverlay[this.opts.overlay_position] = -1 * dom.gv_overlay.outerHeight();
			startOverlay.left = 0;
			
			endOverlay[this.opts.overlay_position] = 0;
			
			endButton[this.opts.overlay_position] = dom.gv_overlay.outerHeight();
			
			dom.gv_overlay.css(startOverlay);
			dom.gv_overlay.animate(endOverlay,{ duration: speed, easing: 'swing' });
			dom.gv_showOverlay.animate(endButton,{ duration: speed, easing: 'swing' });
			
			this.overlayVisible = true;
		},
		
		updateFilmstrip: function(to) {
			if(!this.opts.show_filmstrip) { this.frameIterator = to; return; }
			var self = this,
				dom = this.dom,
				targetThumbs = dom.gv_thumbnails.eq(this.iterator),
				filmstripIterator,
				distance;
			
			if(this.scrolling) {
				targetThumbs = targetThumbs.
								add(dom.gv_thumbnails.eq(this.iterator + this.numImages)).
								add(dom.gv_thumbnails.eq(this.iterator + (2 * this.numImages)));	
			}
			
			dom.gv_thumbnails.removeClass('current').animate({ opacity: this.opts.frame_opacity });
			targetThumbs.stop().addClass('current').animate({ opacity: 1 },500);
			
			
			if(this.scrolling) {
				if(this.filmstripOrientation === 'horizontal') {
					distance = (gv.outerWidth(dom.gv_frame) + this.opts.frame_gap) * (this.frameIterator - to);
					
					if(distance > 0) {
						distance = '+=' + Math.abs(distance);
					} else {
						distance = '-=' + Math.abs(distance);
					}
					dom.gv_filmstrip.animate({
						left: distance
					},{
						duration: this.opts.transition_speed, 
						easing: this.opts.easing, 
						complete: function(){
							if(to < self.numImages) {
								dom.gv_filmstrip.css('left',gv.getInt(dom.gv_filmstrip.css('left'))-(self.numImages*(gv.outerWidth(dom.gv_frame)+self.opts.frame_gap)));	
							} else if(to >= (self.numImages * 2)) {
								dom.gv_filmstrip.css('left',gv.getInt(dom.gv_filmstrip.css('left'))+(self.numImages*(gv.outerWidth(dom.gv_frame)+self.opts.frame_gap)));	
							}
							self.frameIterator = (to % self.numImages) + self.numImages;
						}
					});
				} else {
					distance = (gv.outerHeight(dom.gv_frame) + this.opts.frame_gap) * (this.frameIterator - to);
					
					if(distance > 0) {
						distance = '+=' + Math.abs(distance);
					} else {
						distance = '-=' + Math.abs(distance);
					}
					dom.gv_filmstrip.animate({
						top: distance
					},{
						duration: this.opts.transition_speed, 
						easing: this.opts.easing, 
						complete: function(){
							// adjust filmstrip position to ensure that there is always at least one frame behind
							// and (2 * filmstripSize) ahead
							if(to === 0) {
								dom.gv_filmstrip.css('top',gv.getInt(dom.gv_filmstrip.css('top'))-(self.numImages*(gv.outerHeight(dom.gv_frame)+self.opts.frame_gap)));	
								self.frameIterator = self.numImages;
							} else if(to > ((self.numImages * 3) - (self.filmstripSize * 2))) {
								dom.gv_filmstrip.css('top',gv.getInt(dom.gv_filmstrip.css('top'))+(self.numImages*(gv.outerHeight(dom.gv_frame)+self.opts.frame_gap)));	
								self.frameIterator = to - self.numImages;
							} else {
								self.frameIterator = to;
							}
						}
					});
				}
				
			} else {
				this.frameIterator = to;
			}
		},
		
		startSlideshow: function(changeIcon) {
			var self = this,
				dom = this.dom;
				
			if(!self.opts.enable_slideshow) { return; }
			
			if(changeIcon) {
				dom.gv_navPlay.removeClass('gv_navPlay').addClass('gv_navPause');
			}
			this.playing = true;
			dom.gv_galleryWrap.everyTime(this.opts.transition_interval,'slideshow_'+this.id,function(){ self.showNext(); });
		},
		
		stopSlideshow: function(changeIcon) {
			var self = this,
				dom = this.dom;
				
			if(changeIcon) {
				dom.gv_navPlay.removeClass('gv_navPause').addClass('gv_navPlay');
			}
			this.playing = false;
			dom.gv_galleryWrap.stopTime('slideshow_'+this.id);
		},
		
		enablePanning: function() {
			var self = this,
				dom = this.dom;
				
			if(!self.opts.enable_slideshow) { return; }
			
			dom.gv_panel.css('cursor','url(http://www.google.com/intl/en_ALL/mapfiles/openhand.cur), n-resize');
			if(this.opts.pan_style === 'drag') {
				dom.gv_panelWrap.delegate('.gv_panel img','mousedown.galleryview',function(e) {
					self.isMouseDown = true;
					$(this).css('cursor','url(http://www.google.com/intl/en_ALL/mapfiles/closedhand.cur), n-resize');
				}).delegate('.gv_panel img','mouseup.galleryview',function(e) {
					self.isMouseDown = false;
					$(this).css('cursor','url(http://www.google.com/intl/en_ALL/mapfiles/openhand.cur), n-resize');
				}).delegate('.gv_panel img','mousemove.galleryview',function(e) {
					var distY, distX,
						image = $(this),
						new_top, new_left;

					if(self.isMouseDown) {
						distY = e.pageY - self.mouse.y;
						distX = e.pageX - self.mouse.x;
						new_top = gv.getInt(image.css('top')) + distY;
						
						new_left = gv.getInt(image.css('left')) + distX;
							
						image.css('cursor','url(http://www.google.com/intl/en_ALL/mapfiles/closedhand.cur), n-resize');
						
						if(new_top > 0) new_top = 0;
						if(new_left > 0) new_left = 0;
						
						if(new_top < (-1 * (gv.outerHeight(image) - gv.innerHeight(dom.gv_panel)))) { new_top = -1 * (gv.outerHeight(image) - gv.innerHeight(dom.gv_panel)); }
						if(new_left < (-1 * (gv.outerWidth(image) - gv.innerWidth(dom.gv_panel)))) { new_left = -1 * (gv.outerWidth(image) - gv.innerWidth(dom.gv_panel)); }
						
						image.css('top',new_top);
						image.css('left',new_left);
					} else {
						image.css('cursor','url(http://www.google.com/intl/en_ALL/mapfiles/openhand.cur), n-resize');	
					}
				});
			} else {
				
			}
		},
		
		bindActions: function() {
			var self = this,
				dom = this.dom;
			
			dom.gv_showOverlay.bind('click.galleryview',function(){
				if(self.overlayVisible) {
					self.hideOverlay(250);
				} else {
					self.showOverlay(250);
				}
			});
			
			dom.gv_navWrap.delegate('div','click.galleryview',function(){
				var el = $(this);
				if(el.hasClass('gv_navNext')) {
					self.showNext();
				} else if(el.hasClass('gv_navPrev')) {
					self.showPrev();
				} else if(el.hasClass('gv_navPlay')) {
					self.startSlideshow(true);
				} else if(el.hasClass('gv_navPause')) {
					self.stopSlideshow(true);
				}
				return false;
			});
			
			dom.gv_panelNavNext.bind('click.galleryview',function(){ 
				self.showNext(); 
				return false;
			});
			dom.gv_panelNavPrev.bind('click.galleryview',function(){
				self.showPrev(); 
				return false;
			});
			
			dom.gv_filmstripWrap.delegate('.gv_frame','click.galleryview',function(){
				var el = $(this),
					i = el.data('frameIndex');
				
				this.navAction = 'frame';
				self.showItem(i);
				return false;
			});
			
			dom.gv_panelWrap.bind('mouseover.galleryview',function(){
				self.showPanelNav();
			}).bind('mouseout.galleryview',function(){
				self.hidePanelNav();
			});
		},
		
		unbindActions: function() {
			var self = this,
				dom = this.dom;
				
			dom.gv_showOverlay.unbind('click.galleryview');
			dom.gv_panelNavNext.unbind('click.galleryview');
			dom.gv_panelNavPrev.unbind('click.galleryview');
			dom.gv_navWrap.undelegate('div','click.galleryview');
			dom.gv_filmstripWrap.undelegate('.gv_frame','click.galleryview');
		},
		
		showPanelNav: function() {
			var self = this,
				dom = this.dom;
				
			dom.gv_panelNavNext.show();
			dom.gv_panelNavPrev.show();	
		},
		
		hidePanelNav: function() {
			var self = this,
				dom = this.dom;
				
			dom.gv_panelNavNext.hide();
			dom.gv_panelNavPrev.hide();	
		},
		
		init: function(options,el) {
			var self = this,
				dom = this.dom = {};
				
			this.opts = $.extend({},$.fn.galleryView.defaults,options);
			this.el = el;
			this.$el = $(el);
			this.id = el.id;
			this.iterator = this.frameIterator = this.opts.start_frame - 1;
			this.overlayVisible = false;
			this.playing = false;
			this.scrolling = false;
			this.isMouseDown = false;
			this.mouse = { x: 0, y: 0 };
			this.filmstripOrientation = (this.opts.filmstrip_position === 'top' || this.opts.filmstrip_position === 'bottom') ? 'horizontal' : 'vertical';
			
			$(document).bind('mousemove.galleryview',function(e) {
				self.mouse = {x: e.pageX, y: e.pageY};	   
			});
			
			// create all necessary DOM elements
			$.each(this.elems,function(i,elem) {
				var elem = elem.split('.');
				
				// if there is no tag name, assume <div>
				if(elem[0] === '') { elem[0] = 'div'; }
				
				// add jQuery element to dom object
				dom[elem[1]] = self.createElem({'class':elem[1]},elem[0]);
			});
			
			dom.gv_imageStore.appendTo($('body'));
			
			dom.gv_galleryWrap.delegate('img','mousedown.galleryview',function(e){ if(e.preventDefault) { e.preventDefault(); } });
			
			dom.gv_panel.addClass('gv_panel-loading');
			dom.gv_frame.addClass('gv_frame-loading');
			
			// nest DOM elements
			dom.gv_galleryWrap.hide().append(dom.gv_gallery);
			
			if(this.opts.show_panels) {
				dom.gv_gallery.append(dom.gv_panelWrap);
				if(this.opts.show_panel_nav) {
					dom.gv_panelWrap.append(dom.gv_panelNavNext,dom.gv_panelNavPrev);
				}
				if(this.opts.show_infobar) {
					dom.gv_panelWrap.append(dom.gv_infobar);
				}
			}
			
			if(this.opts.show_filmstrip) {
				dom.gv_gallery.append(
					dom.gv_filmstripWrap.append(
						dom.gv_filmstrip
					)
				);
				
				if(this.opts.show_filmstrip_nav) {
					dom.gv_gallery.append(
						dom.gv_navWrap.append(
							dom.gv_navPrev,
							(this.opts.enable_slideshow?dom.gv_navPlay:$('<span></span>')),
							dom.gv_navNext
						)
					);
				}
			}
			
			if(this.opts.enable_overlays) {
				dom.gv_panelWrap.append(dom.gv_overlay,dom.gv_showOverlay);	
			}
			
			if(this.opts.show_captions) {
				dom.gv_frame.append(dom.gv_caption).appendTo(dom.gv_gallery);	
			}
			
			//swap out source element with gallery
			this.$el.replaceWith(dom.gv_galleryWrap);
			
			if(this.opts.pan_images) {
				this.enablePanning();
			}
			
			// convert source images into gvImage objects
			this.storeImages();
			
			// block out dimensions/positions of gallery elements
			this.buildGallery();
			
			// begin loading images into gallery
			this.initImages();
			
			// set up transitions, buttons
			this.bindActions();
			
			// remove temporary frame element
			dom.gv_frame.remove();
			
			// show gallery
			dom.gv_galleryWrap.show();
			
			if(this.opts.autoplay) {
				this.startSlideshow(true);
			}
			
			this.updateOverlay(this.iterator);
			this.updateFilmstrip(this.frameIterator);
		}
		
	}; // END GalleryView
	
	/*
		MAIN PLUGIN CODE
	*/
	$.fn.galleryView = function (options) {
		if (this.length) {
			return this.each(function () {
				var gallery = Object.create(GalleryView);
				gallery.init(options,this);
			});
		}
	};
	
	/*
		Default Options
			Object literal storing default plugin options
	*/
	$.fn.galleryView.defaults = {
	
		// General Options
		transition_speed: 1000, 		//INT - duration of panel/frame transition (in milliseconds)
		transition_interval: 5000, 		//INT - delay between panel/frame transitions (in milliseconds)
		easing: 'swing', 				//STRING - easing method to use for animations (jQuery provides 'swing' or 'linear', more available with jQuery UI or Easing plugin)
		
		// Panel Options
		show_panels: true, 				//BOOLEAN - flag to show or hide panel portion of gallery
		show_panel_nav: true, 			//BOOLEAN - flag to show or hide panel navigation buttons
		enable_overlays: false, 			//BOOLEAN - flag to show or hide panel overlays
		panel_width: 800, 				//INT - width of gallery panel (in pixels)
		panel_height: 400, 				//INT - height of gallery panel (in pixels)
		panel_animation: 'fade', 		//STRING - animation method for panel transitions (crossfade,fade,slide,none)
		panel_scale: 'crop', 			//STRING - cropping option for panel images (crop = scale image and fit to aspect ratio determined by panel_width and panel_height, fit = scale image and preserve original aspect ratio)
		overlay_position: 'bottom', 	//STRING - position of panel overlay (bottom, top)
		pan_images: false,				//BOOLEAN - flag to allow user to grab/drag oversized images within gallery
		pan_style: 'drag',				//STRING - panning method (drag = user clicks and drags image to pan, track = image automatically pans based on mouse position
		pan_smoothness: 15,				//INT - determines smoothness of tracking pan animation (higher number = smoother)
		
		// Filmstrip Options
		start_frame: 1, 				//INT - index of panel/frame to show first when gallery loads
		show_filmstrip: true, 			//BOOLEAN - flag to show or hide filmstrip portion of gallery
		show_filmstrip_nav: true, 		//BOOLEAN - flag indicating whether to display navigation buttons
		enable_slideshow: true,			//BOOLEAN - flag indicating whether to display slideshow play/pause button
		autoplay: false,				//BOOLEAN - flag to start slideshow on gallery load
		show_captions: false, 			//BOOLEAN - flag to show or hide frame captions	
		filmstrip_size: 3, 				//INT - number of frames to show in filmstrip-only gallery
		filmstrip_style: 'scroll', 		//STRING - type of filmstrip to use (scroll = display one line of frames, scroll filmstrip if necessary, showall = display multiple rows of frames if necessary)
		filmstrip_position: 'bottom', 	//STRING - position of filmstrip within gallery (bottom, top, left, right)
		frame_width: 80, 				//INT - width of filmstrip frames (in pixels)
		frame_height: 40, 				//INT - width of filmstrip frames (in pixels)
		frame_opacity: 0.4, 			//FLOAT - transparency of non-active frames (1.0 = opaque, 0.0 = transparent)
		frame_scale: 'crop', 			//STRING - cropping option for filmstrip images (same as above)
		frame_gap: 5, 					//INT - spacing between frames within filmstrip (in pixels)
		
		// Info Bar Options
		show_infobar: true,				//BOOLEAN - flag to show or hide infobar
		infobar_opacity: 1				//FLOAT - transparency for info bar
	};
})(jQuery);
/**
 * portal - jQuery EasyUI
 * 
 * Licensed under the GPL:
 *   http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2010 stworthy [ stworthy@gmail.com ] 
 * 
 * Dependencies:
 *   draggable
 *   panel
 * 
 */
(function($){
	/**
	 * initialize the portal
	 */
	function init(target){
		$(target).addClass('portal');
		var table = $('<table border="0" cellspacing="0" cellpadding="0"><tr></tr></table>').appendTo(target);
		var tr = table.find('tr');
		
		var columnWidths = [];
		var totalWidth = 0;
		$(target).children('div:first').addClass('portal-column-left');
		$(target).children('div:last').addClass('portal-column-right');
		$(target).find('>div').each(function(){	// each column panel
			var column = $(this);
			totalWidth += column.outerWidth();
			columnWidths.push(column.outerWidth());
			
			var td = $('<td class="portal-column-td"></td>').appendTo(tr)
			column.addClass('portal-column').appendTo(td);
			column.find('>div').each(function(){	// each portal panel
				var p = $(this).addClass('portal-p').panel({
					doSize:false,
					cls:'portal-panel'
				});
				makeDraggable(target, p);
			});
		});
		for(var i=0; i<columnWidths.length; i++){
			columnWidths[i] /= totalWidth;
		}
		
		$(target).bind('_resize', function(){
			var opts = $.data(target, 'portal').options;
			if (opts.fit == true){
				setSize(target);
			}
			return false;
		});
		
		return columnWidths;
	}
	
	function setSize(target){
		var t = $(target);
		var opts = $.data(target, 'portal').options;
		if (opts.fit){
			var p = t.parent();
			opts.width = p.width();
			opts.height = p.height();
		}
		if (!isNaN(opts.width)){
			if ($.boxModel == true){
				t.width(opts.width - (t.outerWidth() - t.width()));
			} else {
				t.width(opts.width);
			}
		} else {
			t.width('auto');
		}
		if (!isNaN(opts.height)){
			if ($.boxModel == true){
				t.height(opts.height - (t.outerHeight() - t.height()));
			} else {
				t.height(opts.height);
			}
		} else {
			t.height('auto');
		}
		
		var hasScroll = t.find('>table').outerHeight() > t.height();
		var width = t.width();
		var columnWidths = $.data(target, 'portal').columnWidths;
		var leftWidth = 0;
		
		// calculate and set every column size
		for(var i=0; i<columnWidths.length; i++){
			var p = t.find('div.portal-column:eq('+i+')');
			var w = Math.floor(width * columnWidths[i]);
			if (i == columnWidths.length - 1){
//				w = width - leftWidth - (hasScroll == true ? 28 : 10);
				w = width - leftWidth - (hasScroll == true ? 18 : 0);
			}
			if ($.boxModel == true){
				p.width(w - (p.outerWidth()-p.width()));
			} else {
				p.width(w);
			}
			leftWidth += p.outerWidth();
			
			// resize every panel of the column
			p.find('div.portal-p').panel('resize', {width:p.width()});
		}
		opts.onResize.call(target, opts.width, opts.height);
	}
	
	/**
	 * set draggable feature for the specified panel
	 */
	function makeDraggable(target, panel){
		var spacer;
		panel.panel('panel').draggable({
			handle:'>div.panel-header>div.panel-title',
			proxy:function(source){
				var p = $('<div class="portal-proxy">proxy</div>').insertAfter(source);
				p.width($(source).width());
				p.height($(source).height());
				p.html($(source).html());
				p.find('div.portal-p').removeClass('portal-p');
				return p;
			},
			onBeforeDrag:function(e){
				e.data.startTop = $(this).position().top + $(target).scrollTop();
			},
			onStartDrag:function(e){
				$(this).hide();
				spacer = $('<div class="portal-spacer"></div>').insertAfter(this);
				setSpacerSize($(this).outerWidth(), $(this).outerHeight());
			},
			onDrag:function(e){
				var p = findPanel(e, this);
				if (p){
					if (p.pos == 'up'){
						spacer.insertBefore(p.target);
					} else {
						spacer.insertAfter(p.target);
					}
					setSpacerSize($(p.target).outerWidth());
				} else {
					var c = findColumn(e);
					if (c){
						if (c.find('div.portal-spacer').length == 0){
							spacer.appendTo(c);
							setSize(target);
							setSpacerSize(c.width());
						}
					}
				}
			},
			onStopDrag:function(e){
				$(this).css('position', 'static');
				$(this).show();
				spacer.hide();
				$(this).insertAfter(spacer);
				spacer.remove();
				setSize(target);
				panel.panel('move');
				
				var opts = $.data(target, 'portal').options;
				opts.onStateChange.call(target);
			}
		});
		
		/**
		 * find which panel the cursor is over
		 */
		function findPanel(e, source){
			var result = null;
			$(target).find('div.portal-p').each(function(){
				var pal = $(this).panel('panel');
				if (pal[0] != source){
					var pos = pal.offset();
					if (e.pageX > pos.left && e.pageX < pos.left + pal.outerWidth()
							&& e.pageY > pos.top && e.pageY < pos.top + pal.outerHeight()){
						if (e.pageY > pos.top + pal.outerHeight() / 2){
							result = {
								target:pal,
								pos:'down'
							};
						} else {
							result = {
								target:pal,
								pos:'up'
							}
						}
					}
				}
			});
			return result;
		}
		
		/**
		 * find which portal column the cursor is over
		 */
		function findColumn(e){
			var result = null;
			$(target).find('div.portal-column').each(function(){
				var pal = $(this);
				var pos = pal.offset();
				if (e.pageX > pos.left && e.pageX < pos.left + pal.outerWidth()){
					result = pal;
				}
			});
			return result;
		}
		
		/**
		 * set the spacer size
		 */
		function setSpacerSize(width, height){
			if ($.boxModel == true){
				spacer.width(width - (spacer.outerWidth() - spacer.width()));
				if (height){
					spacer.height(height - (spacer.outerHeight() - spacer.height()));
				}
			} else {
				spacer.width(width);
				if (height){
					spacer.height(height);
				}
			}
		}
	}
	
	
	$.fn.portal = function(options, param){
		if (typeof options == 'string'){
			return $.fn.portal.methods[options](this, param);
		}
		
		options = options || {};
		return this.each(function(){
			var state = $.data(this, 'portal');
			if (state){
				$.extend(state.options, options);
			} else {
				state = $.data(this, 'portal', {
					options: $.extend({}, $.fn.portal.defaults, $.fn.portal.parseOptions(this), options),
					columnWidths: init(this)
				});
			}
			if (state.options.border){
				$(this).removeClass('portal-noborder');
			} else {
				$(this).addClass('portal-noborder');
			}
			setSize(this);
		});
	};
	
	$.fn.portal.methods = {
		options: function(jq){
			return $.data(jq[0], 'portal').options;
		},
		resize: function(jq, param){
			return jq.each(function(){
				if (param){
					var opts = $.data(this, 'portal').options;
					if (param.width) opts.width = param.width;
					if (param.height) opts.height = param.height;
				}
				setSize(this);
			});
		},
		getPanels: function(jq, columnIndex){
			var c = jq;	// the panel container
			if (columnIndex >= 0){
				c = jq.find('div.portal-column:eq(' + columnIndex + ')');
			}
			var panels = [];
			c.find('div.portal-p').each(function(){
				panels.push($(this));
			});
			return panels;
		},
		add: function(jq, param){	// param: {panel,columnIndex}
			return jq.each(function(){
				var c = $(this).find('div.portal-column:eq(' + param.columnIndex + ')');
				var p = param.panel.addClass('portal-p');
				p.panel('panel').addClass('portal-panel').appendTo(c);
				makeDraggable(this, p);
				p.panel('resize', {width:c.width()});
			});
		},
		remove: function(jq, panel){
			return jq.each(function(){
				var panels = $(this).portal('getPanels');
				for(var i=0; i<panels.length; i++){
					var p = panels[i];
					if (p[0] == $(panel)[0]){
						p.panel('destroy');
					}
				}
			});
		},
		disableDragging: function(jq, panel){
			panel.panel('panel').draggable('disable');
			return jq;
		},
		enableDragging: function(jq, panel){
			panel.panel('panel').draggable('enable');
			return jq;
		}
	};
	
	$.fn.portal.parseOptions = function(target){
		var t = $(target);
		return {
			width: (parseInt(target.style.width) || undefined),
			height: (parseInt(target.style.height) || undefined),
			border: (t.attr('border') ? t.attr('border') == 'true' : undefined),
			fit: (t.attr('fit') ? t.attr('fit') == 'true' : undefined)
		};
	};
	
	$.fn.portal.defaults = {
		width:'auto',
		height:'auto',
		border:true,
		fit:false,
		onResize:function(width,height){},
		onStateChange:function(){}
	};
})(jQuery);
<!--
/*MSClass (Class Of Marquee Scroll - General Uninterrupted Scrolling(JS)) Ver 2.9*\

@ Code By : Yongxiang Cui(333) E-Mail:zhadan007@21cn.com http://www.popub.net
@ Update��: 2012-01-08 (Ver 2.9.120108)
@ Examples: http://www.popub.net/script/MSClass.html

\***============== Please keep the above copyright information ==============***/

eval(function(MSClass){var d=MSClass,p=d[0],k=d[1],c=k.length,a=62,e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)d[e(c)]=k[c]||e(c);k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}(['17 1V(){1c c=1W,b=1K,a;13(!(c 71 b.47)){18(1v b[0]=="2F"&&b[0].1j&&b.1j<5)||(1v b[0]!="2F"&&b.1j<5)?2z("70 6Z 6Y 6X 6W!"):6V 1V(b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10],b[11],b[12])}c.$=17(d){18 1R.6U(d)};c.t=(c.F=["3G 2.9.6T","6S 6R 1V 6Q","6P 6O 6N(6M)","6L://6K.6J.6I/6H/3G.6G","6F 6E(4K) 6D@6C.6B"]).6A("\\n");13(!((c.c=1v(c.a=b[0]["3G"]||b[0]||b[0][0])=="2F"?c.$(c.a[0])||c.$(c.a.4J):c.$(c.a))&&c.6z.1Y().2X(6y>>4K,6x>>>4)==c.F[1].2X(6w>>>6v,14>>6u))){18 2z("4f ["+(c.a.4J||c.a[0]||c.a)+"] 6t 6s!")||(c.c=-1)}13(c.c.3F=="3q"||c.c.3F=="4p"||c.c.3F=="6r"){c.a=[0,c.c.4e];c.c=c.c.3B}c.1a=b[1]||b[0]["1a"]||0;c.1g=b[2]||b[0]["1g"]||1;c.1E=c.W=(a=b[3]||b[0]["1E"]||0)==0?1s(c.c.16.1P):a;c.1D=c.H=(a=b[4]||b[0]["1D"]||0)==0?1s(c.c.16.1S):a;c.1X=(a=b[5]||b[0]["1X"]||0)<20?(a*10||20):a;c.2g=(a=b[6]||b[0]["2g"]||0)<=4I?a*2K:a;c.2k=(a=b[7]||b[0]["2k"]||0)<=4I?(a*2K||38):a;c.1e=b[8]||b[0]["1e"]||0;c.2r=b[9]||b[0]["2r"]||0;c.2q=b[10]||b[0]["2q"];c.3i=c.$(b[11]||b[0]["6q"])||0;c.3h=c.$(b[12]||b[0]["6p"])||0;c.c.16.3m=c.c.16.4a=c.c.16.49="2C";13(b.1j>=5||b[0]["6o"]){c.3e()}}1V.3P.3e=17(){13(1W.c==-1||1W.i>=0){13(1W.i==2){1W.2v()}18 1x}1c k=1W,M=k.c,b,w,Z,1b,1u,1H,z,n,T=0,g="6n",1f=1V,R=0,s=[],h=0,O=0,d=0,r={6m:-2,6l:-1,6k:-1,28:0,6j:0,6i:1,6h:1,2Q:2,6g:3,6f:4,6e:5},S=[],1q=0,V=0,v=0,1O=0,I=0,B=0,a=0,F=0,X=0,l=0,J=0,2j=0,D=0,x=0,u=0,W=0,1A=0,c=[],1G=1C,1h=1C,t=0,o=0,Y=k.1e,P=k.2g,C=0,U=0,A=[],2l=0,N=0,Q=0;k.I=k.j=1+(k.i=k.2i=-1);13(!1f.H){1p.$?0:1p.$=k.$;1f.m=(1R.3A)?1:0;1f.4j=6d.6c.6b("6a")>0;1f.H=17(m,j,i){i?0:i=0;18 1f.m?(m.2e[j]!="4H"&&m.2e[j]!="4G"&&m.2e[j]!="4F"&&m.2e[j]!="69"&&m.2e[j]!="68"&&m.2e[j]!="3y")?m.2e[j]:i:(1p.2u(m,1C)[j]!="4H"&&1p.2u(m,1C)[j]!="4G"&&1p.2u(m,1C)[j]!="4F"&&1p.2u(m,1C)[j]!="4g"&&1p.2u(m,1C)[j]!="3y")?1p.2u(m,1C)[j]:i};(1f.K=17(m,j,i){1f.m?m.67("4E"+j,i):m.66(j,i,1x)})(28.1R,"65",17(i){13((i||1p.1J).64==63){2z(k.t)}});1f.L=17(m,j,i){1f.m?m.62("4E"+j,i):m.61(j,i,1x)};1f.1Z={4q:17(i,m,j){18 m},60:17(i,m,j){18 m*(i/=j)*i},5Z:17(i,m,j){18-m*(i/=j)*(i-2)},5Y:17(i,m,j){18(i/=j/2)<1?m/2*i*i:-m/2*((--i)*(i-2)-1)},5X:17(i,m,j){18 m*(i/=j)*i*i},5W:17(i,m,j){18 m*((i=i/j-1)*i*i+1)},5V:17(i,m,j){18(i/=j/2)<1?m/2*i*i*i:m/2*((i-=2)*i*i+2)},5U:17(i,m,j){18 m*(i/=j)*i*i*i},5T:17(i,m,j){18-m*((i=i/j-1)*i*i*i-1)},5S:17(i,m,j){18(i/=j/2)<1?m/2*i*i*i*i:-m/2*((i-=2)*i*i*i-2)},5R:17(i,m,j){18 m*(i/=j)*i*i*i*i},5Q:17(i,m,j){18 m*((i=i/j-1)*i*i*i*i+1)},5P:17(i,m,j){18(i/=j/2)<1?m/2*i*i*i*i*i:m/2*((i-=2)*i*i*i*i+2)},5O:17(i,m,j){18-m*1d.4D(i/j*(1d.1I/2))+m},5N:17(i,m,j){18 m*1d.2H(i/j*(1d.1I/2))},5M:17(i,m,j){18-m/2*(1d.4D(1d.1I*i/j)-1)},5L:17(i,m,j){18 i==0?0:m*1d.1U(2,10*(i/j-1))},5K:17(i,m,j){18 i==j?m:m*(-1d.1U(2,-10*i/j)+1)},5J:17(i,m,j){18 i==0?0:i==j?m:(i/=j/2)<1?m/2*1d.1U(2,10*(i-1)):m/2*(-1d.1U(2,-10*--i)+2)},5I:17(i,m,j){18-m*(1d.30(1-(i/=j)*i)-1)},5H:17(i,m,j){18 m*1d.30(1-(i=i/j-1)*i)},5G:17(i,m,j){18(i/=j/2)<1?-m/2*(1d.30(1-i*i)-1):m/2*(1d.30(1-(i-=2)*i)+1)},2P:17(j,1m,1o){1c m=1.2t,19=0,i=1m;13(j==0){18 0}13((j/=1o)==1){18 1m}13(!19){19=1o*0.3}13(i<1d.3E(1m)){i=1m;m=19/4}1r{m=19/(2*1d.1I)*1d.3D(1m/i)}18-(i*1d.1U(2,10*(j-=1))*1d.2H((j*1o-m)*(2*1d.1I)/19))},43:17(j,1m,1o){1c m=1.2t,19=0,i=1m;13(j==0){18 0}13((j/=1o)==1){18 1m}13(!19){19=1o*0.3}13(i<1d.3E(1m)){i=1m;m=19/4}1r{m=19/(2*1d.1I)*1d.3D(1m/i)}18 i*1d.1U(2,-10*j)*1d.2H((j*1o-m)*(2*1d.1I)/19)+1m},5F:17(j,1m,1o){1c m=1.2t,19=0,i=1m;13(j==0){18 0}13((j/=1o/2)==2){18 1m}13(!19){19=1o*(0.3*1.5)}13(i<1d.3E(1m)){i=1m;m=19/4}1r{m=19/(2*1d.1I)*1d.3D(1m/i)}13(j<1){18-0.5*(i*1d.1U(2,10*(j-=1))*1d.2H((j*1o-m)*(2*1d.1I)/19))}18 i*1d.1U(2,-10*(j-=1))*1d.2H((j*1o-m)*(2*1d.1I)/19)*0.5+1m},3k:17(i,19,m){1c j=1.2t;18 19*(i/=m)*i*((j+1)*i-j)},42:17(i,19,m){1c j=1.2t;18 19*((i=i/m-1)*i*((j+1)*i+j)+1)},5E:17(i,19,m){1c j=1.2t;18(i/=m/2)<1?19/2*(i*i*(((j*=1.4C)+1)*i-j)):19/2*((i-=2)*i*(((j*=1.4C)+1)*i+j)+2)},4B:17(i,m,j){18 m-1f.1Z.3C(j-i,m,j)},3C:17(i,m,j){18(i/=j)<1/2.2d?m*7.2Z*i*i:i<2/2.2d?m*(7.2Z*(i-=1.5/2.2d)*i+0.2d):i<2.5/2.2d?m*(7.2Z*(i-=2.25/2.2d)*i+0.5D):m*(7.2Z*(i-=2.5C/2.2d)*i+0.5B)},5A:17(i,m,j){18 i<j/2?1f.1Z.4B(i*2,m,j)*0.5:1f.1Z.3C(i*2-j,m,j)*0.5+m*0.5},5z:17(i,m,j){18 i*m/j}}}13(1v k.W=="1L"||1v k.1E=="1L"){k.1E=M.3B.4t*(1s(k.W)||1s(k.1E))/2D}13(1v k.H=="1L"||1v k.1D=="1L"){k.1D=M.3B.4s*(1s(k.H)||1s(k.1D))/2D}13(!4A(k.1E)){M.16.1P=k.1E+"1l"}13(!4A(k.1D)){M.16.1S=k.1D+"1l"}13(1v k.1a=="1L"){k.1a=r[k.1a.1Y().2L()]}Q=k.1a>1?1:0;b=Q?"<2G 4v=\'0\' 4u=\'0\' 16=\'3x-2Y:2Y;1N:3w;\'><2s><1T 3p=2x 16=\'4z-4y: 3o;4x-1M:4w-3A;\'>2p</1T><1T 3p=2x 16=\'4z-4y: 3o;4x-1M:4w-3A;\'>2p</1T></2s></2G>":"<2G 4v=\'0\' 4u=\'0\' 16=\'3x-2Y:2Y;\'><2s><1T>2p</1T></2s><2s><1T>2p</1T></2s></2G>";w=Q?k.1E:k.1D;1u=Q?"4t":"4s";1b=Q?"3O":"3N";Z=Q?"2Q":"28";k.n=M.1z;13(k.1g.1j){T=(17(i){k.1g=k.1g[0]<1?4r(k.1g[0].1Y().3n("0.","")):4r(k.1g[0]);1B(1n 5y 1f.1Z){1c j=++j||0;13((1n.2X(4)==i||i.2X(4)==1n||1n==i||i==j)&&j>=0){R=j;18 1n}}})(k.1g[1].1Y().2L()).1Y()}13(k.1g<1){T=k.1g}13(k.1g>=w){k.2r=5}3V(k.2r){21 0:k.1e=k.1e||w;1M;21 1:Y=k.1e=w;2l=o=1;1M;21 3:k.I=1A=1;1M;21 4:Y=Y||1;C=1;1M;21 5:T="4q";k.1g=k.1e=k.1e||w;1M;5x:k.2r==2?h=1:F=1f.m?k.2r:1-(h=1);13(Y>0){2l=N=1-(h=0);k.1e=w}13(1f.m&&F>0){M.16.3j="29(5w="+(P/2K*0.2)+",3l="+(F-10)+")";13(F>=33||F<10){M.2o.29.3l=1d.2A(1d.46()*22)}M.2o.29.45();M.2o.29.44()}k.1g=w;R=T=1-(o=1)}13(k.2q){1c 3z=(1v k.2q)=="1L"?[k.2q]:k.2q;1B(1n=0;1n<3z.1j;1n++){1c H=k.$(3z[1n]);13(H&&H.16.1N=="2m"){V=1;s[1n]=[H,1f.H(H,"2n","3y"),1f.H(H,"28")];H.16.2n="4b";H.16.2M="2C";H.16.28="-5v";H.16.1N="3u"}}}(k.G=17(5u){13(1v k.a=="2F"&&(!k.a.1j||k.a.1j>0)){1h=k.$(k.a[1])||k.$(k.a.5t);13(1h){13(1h.2U=="3q"||1h.2U=="4p"){1h.16.2T=1h.16.3x=1h.16.3s="0";1f.H(1h,"4o")=="5s"?1h.16.4o="1.15":0;13(Q&&1f.m){1h.16.1P="5r"}}1h.16.1N="3w-3u";t=1h.16.4h=1h.16.3s=0;1h.16.3r="2m";1B(1c 1t=0,2b=1h.1w.1j;1t<2b;1t++){1c 1k=1h.1w[1t];13(1k.2R==1){1k.16.3r="2m";13(Q){1c 4k=1s(1k.16.4n=1f.H(1k,"4n")),m=1s(1k.16.2W=1f.H(1k,"2W")),1o=1s(1k.16.4m=1f.H(1k,"4m")),3v=1s(1k.16.4l=1f.H(1k,"4l"));13(1f.H(1k,"1N")!="2G-5q"){1k.16.5p=1k.16.5o="2Q";1k.16.1N="3w";t+=(A[A.1j]=1k[1u]+4k+m+1o+3v)}1r{1f.4j?t+=(A[A.1j]=1k[1u]):t+=(A[A.1j]=1k[1u]+1o+3v)}}1r{1k.16.1N="3u";1k.16.5n="5m";A[A.1j]=1k[1u]}13(1A){c[c.1j]=1k}}}U=A.1j;k.1a<=1?t=1h[1u]:0;Q?(1h.16.1P=t+"1l")||(1h.16.1S=k.1D+"1l"):(1h.16.1S=t+"1l")||(1h.16.1P=k.1E+"1l");I=1s(M[1u]/(k.1e||w));1O=k.1i=1;13(1v k.a=="2F"&&(!k.a.1j||k.a.1j>1)){1c 2c=k.$(k.a[2])||k.a[2]||k.$(k.a.5l);13(2c&&(2c.1j||2c.2U)){1c 2E=(k.a[3]||k.a.5k||"2w").1Y().2L();(2E=="2w"||2E=="3g")?0:2E="2w";1c 1m=(k.a[4]||k.a.5j||0);k.1e=k.1e||w;1c 2a=1d.48(t/k.1e);1c 19=2a*k.1e-t;13(19>0){1B(1c 1t=1h.1w.1j-1;1t>=0;1t--){1c 1k=1h.1w[1t];13(1k.2R==1){Q?1k.16.2W=1s(1f.H(1k,"2W"))+19+"1l":1k.16.4i=1s(1f.H(1k,"4i"))+19+"1l";t+=19;1M}}}1B(1c 2V=0,3t=2c.1j||1,1Q=0,1y=2c;2V<3t;2V++){13(3t>1){1y=k.$(2c[2V])}1y.16.4h=1y.16.3s=0;1y.16.3r="2m";13(1y.2U=="3q"){1y.16.2T="0"}I=1y.1w.1j;1B(1c 1t=0;1t<I;1t++){13(1y.1w[1t].2R==1){13(2a==1Q||2a<=1){1y.1w[1t].16.1N="2m";5i}1y.1w[1t].16.2T=1f.H(1y.1w[1t],"2T","4g");1y.1w[1t].16.3m="2C";S[1Q]=1y.1w[1t];S[1Q][2E]=(17(i){18 17(){W=1F(17(){v=1;1B(1c 2S=0;2S<S.1j;2S++){S[2S].2B=""}S[i].2B=g;f(i)},1m)}})(1Q);S[1Q].31=(17(i){18 17(){2h(W);13(v){v=0;13(k.i==0){2y(x);x=2J(D,k.1X)}1r{2h(u);u=1F(p,P)}E(i)}}})(1Q);1Q+=1}}}I=1Q;a=I>1?1:0;13(S.1j>0){S[0].2B=g}13(I>0&&2a>I){B=2a-I;1B(1c 2b=0;2b<S.1j;2b++){S[2b].2w=S[2b].31=17(){}}I=2a;2z("4f ["+M.4e+"] 4d 5h 5g 5f 5e ["+B+"] 5d 5c 4d!")||0}}}Q?1h.16.1S=k.1D+"1l":1h.16.1P=k.1E+"1l";13(!1A&&(o==0||o==2)&&!h&&F==0&&k.1a>=0&&k.1a<=3){Q?1h.16.1P=t*2+"1l":1h.16.1S=t*2+"1l";1h.1z+=1h.1z}1r{Q?1h.16.1P=t+"1l":1h.16.1S=t+"1l"}}}})();13(!1O&&1A){1B(1c 1n=0,4c=M.1w.1j;1n<4c;1n++){13(M.1w[1n].2R==1){c[c.1j]=M.1w[1n]}}}13(!1O&&!1A&&(k.1a==0||k.1a==1)){M.1z=b.3n(/2p/g,M.1z)}1r{13(!1O&&!1A&&(k.1a==2||k.1a==3)){M.3p=2x;M.16.5b="3o";(k.1e==0&&P==0)?M.1z+=M.1z:M.1z=b.3n(/2p/g,M.1z)}1r{13(k.1a>=4||k.1a<=-1){13(k.1a==5||k.1a==-2){X=1}k.1a=22%k.1a;k.I=1}}}1q=k.I;1H=k.1e;z=k.1a;1O?M.16.2n=1f.H(M,"2n","5a"):0;13(2l){n=1R.59("58");n.16.1P=k.1E+"1l";n.16.1S=k.1D+"1l";n.16.2n="4b";n.16[Z]=k.1a%2?-w+"1l":w+"1l";Q?n.16.28="0":n.16.2Q="0";n.1z=M.1z;n.16.3m=n.16.4a=n.16.49="2C";M.3Z(n)}1c L=(1v T=="1L"&&o==0)?2:o,D=17(){T>0?k.1g=1d.48((k.1e-d)*T):0;y[L]()},E=17(j){13(k.i==2){18 1x}13(X){z=z>1?2:0}},f=17(j){13(k.i==2||(o==1&&j+1==k.1i)){18 1x}k.2f();13(o==0){1c m=1s(M[1b]/1H)+1;13(j+1==m&&M[1b]%1H==0){18 1x}13(m>I){m-=I}j+=1;v=1;13(j<=m){k.1e=1H*(m-j)+M[1b]%1H;k.1a=Q?3:1}1r{13(j-m==I-1){13(R==24){T="2P"}13(R==27||R==26){T="3k"}}13(m<=2&&j==I&&R==23){T="2P"}k.1e=1H*(j-m)-M[1b]%1H;k.1a=Q?2:0}k.1i=j}1r{13(o==1){13(k.i==0&&1q){k.1a=Q?5-k.1a:1-k.1a;z=k.1a}13(2l&&!N){k.1a%2?n.16[Z]=-w+M[1b]+"1l":n.16[Z]=w+M[1b]+"1l";j==I?n[1b]=1-(k.1i=1):n[1b]=w*j}k.1i=j+1}}13(k.1e>0){p(0,j)}},2O=17(m){2h(O);1c 19=m||M,i=N?2D:5,j=N?-5:5;(17(){1f.m?19.16.3j="40(2N="+i+")":19.16.2N=i/2D;i+=j;(j<0?i>=0:i<=2D)?O=1F(1K.47,k.1X):j<0?19.16.2M="2C":0})()},p=17(m,j){1c 19=k.1a%2;13(h){2O()}13(N){1c 1o=j>=0?j:k.1i;n[1b]=M[1b];n.16[Z]=1o==I?0:w*1o+"1l";n.16.2M="3X";2O(n)}13(F>0){13(F>=33||F<10){M.2o.29.3l=1d.2A(1d.46()*22)}M.2o.29.45();M.2o.29.44()}13(k.j){u=1F(p,P)}1r{2y(x);l=J=d=k.i=0;13(1q&&T>0){13((k.1a==2||k.1a==0)&&M[1u]-w-M[1b]<k.1e){k.1e=M[1u]-w-M[1b]}13(19&&M[1b]<k.1e){k.1e=M[1b]}}13(1O&&!v){19?k.1i-=1:k.1i+=1;13(1q&&o==1&&k.1i>I){k.1i=1}13(k.1i>I){1q?k.1i-=1:k.1i-=I}13(k.1i<=0){1q?k.1i=1:k.1i=I}19&&X?k.1i=1:0;13(a&&B==0){1B(1c j=0;j<I;j++){S[j].2B=""}S[k.1i-1].2B=g}}13(1q&&(1A||I>1)){13((R==22||R==24)&&((!19&&k.1i==2)||(19&&k.1i==I-1))){T="43"}13((R==23||R==24)&&(1A||(!19&&k.1i==I)||(19&&k.1i==1))){T="2P"}13((R==25||R==27)&&((!19&&k.1i==2)||(19&&k.1i==I-1))){T="42"}13((R==26||R==27)&&(1A||(!19&&k.1i==I)||(19&&k.1i==1))){T="3k"}}13(1q&&k.2i>=0){k.2i=-1;k.3T()}13(C){k.1e=K(19)}x=2J(D,k.1X);k.3S()}},q=17(){2y(x);l=J=d=1-(k.i=1);k.1e=1H;k.1a=z;13(v){18 1x}13(1q){13(o==1){}1r{13(M[1b]==0||M[1b]>=M[1u]-w){13(1A){G()}1r{13(X){k.1e=z%2?1H:M[1b]}k.2i=k.1a;k.3U();z=k.1a=Q?5-k.1a:1-k.1a}}}}k.3R();u=1F(p,P)},K=17(1o){1B(1c m=0,j=0;m<Y;m++){1c 19=(19=M.3Y("41")/1+1||1)>U?1:19;M.3d("41",19);13(1o){19=U-19+1}j+=A[19-1]}18 j},G=17(){13(Y>0){13(1G){2O(1G)}}1c j=!1O?M:1h,19=k.1a%2;1c m=19?c.1j-1:0;1G=c[m].57(2x);k.1e=c[m][1u];13(Y>0){1f.m?1G.16.3j="40(2N = 0)":1G.16.2N=0}19?j.56(1G,j.1w[0]):j.3Z(1G);j.55(c[m]);19?M[1b]=k.1e:M[1b]=M[1u]-w-k.1e;13(19){c.54(1G);c.53()}1r{c.52(1G);c.51()}},e=17(){k.y=(t*2||M[1u])/2;13(o==0&&k.y<=w+k.1g&&!1q){13(1O){1c j=M.3Y("3c")/1+1||1;M.3d("3c",j);13(j<5){k.G();1F(e,0)}}1r{M.1z=k.n}18 1x}13(1q){k.2i=Q?5-k.1a:1-k.1a}13(V){1B(1n=0;1n<s.1j;1n++){1c i=s[1n][0];i.16.2n=s[1n][1];i.16.1N="2m";i.16.2M="3X";i.16.28=s[1n][2]}}13(P>0&&k.3i){k.3i.3g=17(){k.3f(Q?5-k.1a:1-k.1a)}}13(P>0&&k.3h){k.3h.3g=17(){k.3f(k.1a)}}M[1b]=k.i=l=J=0;13(1A){13(!k.1a%2){M[1b]=M[1u]}G()}13(2l){n[1b]=k.1e}2j=1d.2A(k.1e/k.1g);13(k.2k>=50){k.i=1;13(k.2k==4Z){P=k.2g=3W}k.2k=3W;18}1F(17(){13((k.1e>=0&&k.l)||(P==0&&k.1e==-2&&k.l)){k.l()}p()},k.2k-38)},y=[17(){1c i=k.1a%2;d+=k.1g;13((d>=k.1e&&P>0)||(1q&&d>k.1g&&(M[1b]<=0||M[1b]>=M[1u]-w))){i?M[1b]-=k.1e+k.1g-d:M[1b]+=k.1e+k.1g-d;q();18}3V(i){21 0:13(M[1b]>=k.y&&!1q){M[1b]-=k.y}M[1b]+=k.1g;1M;21 1:13(M[1b]<=k.1g&&!1q){M[1b]+=k.y}M[1b]-=k.1g}},17(){13(N||h){q();18 M[1b]=k.1i==1?0:w*(k.1i-1)}1c i=k.1a%2;R>0?d+=l:d+=k.1g;13((R==0&&d>=k.1e&&P>0)||(R>0&&l>2j)){i?n.16[Z]=(1s(n.16[Z])-(k.1e-k.1g-d))+"1l":n.16[Z]=(1s(n.16[Z])+(k.1e-k.1g-d))+"1l";q();M[1b]=n[1b];i?n.16[Z]=-w+M[1b]+"1l":n.16[Z]=w+M[1b]+"1l";k.1i==I?n[1b]=0:n[1b]=w*k.1i}1r{13(R>0){J+=(k.1g=1d.2A(1f.1Z[T](l++,k.1e,2j))-J)}i?n.16[Z]=(1s(n.16[Z])+k.1g)+"1l":n.16[Z]=(1s(n.16[Z])-k.1g)+"1l"}},17(){13(l<=2j){J+=(k.1g=1d.2A(1f.1Z[T](l++,k.1e,2j))-J);13(M[1b]<=k.1g&&!1q){M[1b]+=k.y}k.1a%2?M[1b]-=k.1g:M[1b]+=k.1g}1r{13(M[1b]>k.y&&!1q){M[1b]-=k.y}q()}}];k.3f=17(j){13(k.i==1){1c i=-1;13(1v 1K[0]=="4Y"){i=1K[0]}13(1v 1K[0]=="1L"){z=i=r[1K[0].1Y().2L()]}13(i<0||i>3){2z("4X 4W 4V!");18 1x}13(k.2i==i){18 1x}k.1a=i;13(1v 1K[0]=="1L"||1q){z=k.1a}k.2I()}};k.3U=k.3T=k.3S=k.3R=17(){};k.2I=17(){2h(u);u=1F(p,1K[0])};k.3b=17(i){1p.3Q=1C;1F(17(){1p.3Q=17(){k.3b()}},2K);k.3a();k.3e()};k.3a=k.4U=17(){k.39();M.1z=k.n;M.3d("3c",M[1b]=k.2I=k.3b=k.3a=k.2v=k.39=k.2f=k.i=l=J=0);k.i=-1};k.2v=17(){13(k.i==2&&!1K[0]){k.i=d>1?0:1;x=2J(D,k.1X);18 1x}13(k.i==0){2y(x);x=2J(D,k.1X)}13(k.i==1){k.2I(P)}};k.39=17(){13(k.i!=2){k.2f();k.i=2}};k.2f=k.4T=17(){2h(W);2y(x);2h(u)};1F(e,38)};1V.3P.l=17(){1c i=1W,f=1V,h=1C,j=1C,k=0,l=0,c=0,e=0,b=i.1a>1?"3O":"3N",a=17(m){1c m=m||1p.1J;13(i.2g!=0||(m.3M&&m.3M!=1)||(m.3L&&m.3L!=1)||i.i==2||i.i==-1){18 1x}13(k==0){i.i=k=1;i.2f();m.3K?m.3K():m.4S=2x;m.3J?m.3J():m.4R=1x;c=i.c[b];l=i.1a>1?1p.1J?m.37:m.36:1p.1J?m.35:m.34;h=m.4Q||m.4P;j=h.16.32;h.16.32="4O";13(f.m){h.4N()}f.K(1R,"3I",g);f.K(1R,"3H",d)}},g=17(n){1c n=n||1p.1J;e=i.1a>1?1p.1J?n.37:n.36:1p.1J?n.35:n.34;1c m=l-e+c;13(m<=0||m>=i.y){m<=0?i.c[b]+=i.y:i.c[b]=i.y-m;l=i.1a>1?1p.1J?n.37:n.36:1p.1J?n.35:n.34;c=i.c[b];m=l-e+c}i.c[b]=m},d=17(m){13(f.m){h.4M()}f.L(1R,"3I",g);f.L(1R,"3H",d);k=i.j=0;h.16.32=j;i.2v()};13(!i.I){f.K(i.c,"4L",a)}i.c.2w=17(){13((i.i==0&&i.2g>0)||i.i==2||i.i==-1){18 1x}i.j=1;i.2f()};i.c.31=17(){13(i.j){i.j=0;i.2v()}}};','|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||if|||style|function|return|ap|Direction|aa|var|Math|ScrollStep|ah|Step|am|Counter|length|aF|px|ar|ak|aq|window|an|else|parseInt|aw|ai|typeof|childNodes|false|aE|innerHTML|af|for|null|Height|Width|setTimeout|al|ae|PI|event|arguments|string|break|display|ab|width|aA|document|height|td|pow|Marquee|this|Timer|toString|Tween||case|||||||top|revealTrans|aC|av|ay|75|currentStyle|Pause|DelayTime|clearTimeout|Bound|ao|WaitTime|ag|none|position|filters|MSCLASS_TEMP_HTML|HiddenID|SwitchType|tr|70158|getComputedStyle|Continue|onmouseover|true|clearInterval|alert|round|className|hidden|100|ax|object|table|sin|Play|setInterval|1000|toLowerCase|visibility|opacity|ac|inelastic|left|nodeType|aG|fontSize|tagName|az|marginRight|substr|collapse|5625|sqrt|onmouseout|cursor||pageY|clientY|pageX|clientX|800|Stop|Reset|ReStart|fixnum|setAttribute|Start|Run|onclick|NextBtn|PrevBtn|filter|inback|Transition|overflow|replace|nowrap|noWrap|UL|listStyle|padding|at|block|aD|inline|border|static|ad|all|parentNode|outbounce|asin|abs|nodeName|MSClass|mouseup|mousemove|preventDefault|stopPropagation|button|which|scrollTop|scrollLeft|prototype|onresize|OnPause|OnScroll|UnBound|OnBound|switch|3600000|visible|getAttribute|appendChild|alpha|nownum|outback|outelastic|play|apply|random|callee|ceil|overflowY|overflowX|absolute|aj|pages|id|The|12px|margin|marginBottom|IsFF|au|borderRightWidth|borderLeftWidth|marginLeft|lineHeight|OL|def|parseFloat|scrollHeight|scrollWidth|cellpadding|cellspacing|keep|word|space|white|isNaN|inbounce|525|cos|on|0px|medium|auto|120|MSClassID|333|mousedown|releaseCapture|setCapture|move|target|srcElement|returnValue|cancelBuble|Terminate|Destroy|set|wrong|Parameters|number|100000|60000|shift|push|pop|unshift|removeChild|insertBefore|cloneNode|div|createElement|relative|whiteSpace|actual|than|less|numbers|tab|or|continue|TabTimeout|TabEvent|TabID|both|clear|cssFloat|styleFloat|cell|100000px|normal|ContentID|aB|10000px|Duration|default|in|simple|inoutbounce|984375|625|9375|inoutback|inoutelastic|inoutcirc|outcirc|incirc|inoutexpo|outexpo|inexpo|inoutsine|outsine|insine|inoutquint|outquint|inquint|inoutquart|outquart|inquart|inoutcubic|outcubic|incubic|inoutquad|outquad|inquad|removeEventListener|detachEvent|113|keyCode|keydown|addEventListener|attachEvent|1em|30pt|Firefox|indexOf|userAgent|navigator|rollbacktop|alterleft|right|down|bottom|up|alterup|altertop|rollbackleft|active|AutoStart|NextBtnID|PrevBtnID|DL|error|initialization|97|99|79|123|73737|constructor|join|com|21cn|zhadan007|Cui|Yongxiang|html|script|net|popub|www|http|JS|Scrolling|Uninterrupted|General|Scroll|Of|Class|120108|getElementById|new|accept|not|does|mode|Set|instanceof'.split('|')]))

//-->