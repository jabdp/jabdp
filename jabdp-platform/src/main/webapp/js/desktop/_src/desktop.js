myLib.NS("desktop");
myLib.desktop = {
	winWH:function(){
		$('body').data('winWh',{'w':$(window).width(),'h':$(window).height()});
 	},
 	ctx:function(ctx){
 		$('body').data('ctx',ctx);
 	},
 	pageIndex:function(pageIndex){
 		$('body').data('pageIndex',pageIndex);
 	},
	desktopPanel:function(){
		$('body').data('panel',{
			'taskBar':{
				'_this':$('#taskBar'),
				'task_lb':$('#task_lb')
			},
			'navBar':$('#navBar'),
			'wallpaper':$('#wallpapers'),
			'lrBar':{
				'_this':$('#lr_bar'),
				'default_app':$('#default_app'),
				'start_block':$('#start_block'),
				'start_btn':$('#start_btn'),
				'start_item':$('#start_item'),
				'default_tools':$('#default_tools')
			},		
			'desktopPanel':{
				'_this':$('#desktopPanel'),
				'innerPanel':$('#desktopInnerPanel'),
				'deskIcon':$('ul.deskIcon')
			},
			'powered_by':$('a.powered_by')
		});
	},
	//为多个元素附加iconData信息
	iconDataInit:function(data){
		for(var a in data){
			//console.log(a);
			//console.log(data[a]);
			$("#"+a).data("iconData",data[a]);
		}
	},
	getMydata:function(){
		return $('body').data();
	},
	mouseXY:function(){
		var mouseXY=[];
		$(document).bind('mousemove',function(e){
			mouseXY[0]=e.pageX;
			mouseXY[1]=e.pageY;
        });
		return mouseXY;
	},
	contextMenu:function(jqElem,data,menuName,textLimit){
		var _this=this,
			mXY=_this.mouseXY();
		jqElem.smartMenu(data,{
            name: menuName,
			textLimit:textLimit,
			afterShow:function(){
				var menu=$("#smartMenu_"+menuName);
				//console.log(menu);
				var myData=myLib.desktop.getMydata(),
		            wh=myData.winWh;//获取当前document宽高
 				var menuXY=menu.offset(),menuH=menu.height(),menuW=menu.width();
				if(menuXY.top>wh['h']-menuH){
					menu.css('top',mXY[1]-menuH-2);
				}
				if(menuXY.left>wh['w']-menuW){
					menu.css('left',mXY[0]-menuW-2);
				}
			}
		});
		$(document.body).click(function(event){
			event.preventDefault(); 			  
			$.smartMenu.hide();
		});
	}	
};
	

	



//navbar
//myLib.NS("desktop.navBar");
//myLib.desktop.navBar={
//	init:function(){
//		var myData=myLib.desktop.getMydata(),
//			$navBar=myData.panel.navBar,
//			$innerPanel=myData.panel.desktopPanel.innerPanel,
//			$navTab=$navBar.find("a"),
//			$deskIcon=myData.panel.desktopPanel['deskIcon'],
//			desktopWidth=$deskIcon.width(),
//			lBarWidth=myData.panel.lrBar["_this"].outerWidth();
//		
//		$navBar.draggable({
// 			scroll:false
//		});
//			 
//		$navTab.droppable({
//			scope:'a',
//			over:function(event,ui){
//				$(this).trigger("click");
//				var i=$navTab.index($(this));
//					//ui.draggable
//					//.css({left:event.pageX+$deskIcon.width()*i});
// 			},
//            drop: function(event,ui){
//            	var i=$navTab.index($(this));
// 				ui.draggable.addClass("desktop_icon").insertBefore($deskIcon.eq(i).find(".add_icon")).find("span").addClass("icon"); 
//                myLib.desktop.deskIcon.init();
//				myLib.desktop.lrBar.init();
//			}
//		}).click(function(event){
//				event.preventDefault();
//				event.stopPropagation();
//				var i=$navTab.index($(this));
// 				myLib.desktop.deskIcon.desktopMove($innerPanel,$deskIcon,$navTab,500,desktopWidth+lBarWidth,i);
//		});
//		
//		//console.log("navBar");
//	}
//};

//桌面背景设置
myLib.NS("desktop.wallpaper");
myLib.desktop.wallpaper={
	init:function(imgUrl){
		var myData=myLib.desktop.getMydata(),
		winWh=myData.winWh,
		wallpaper=myData.panel.wallpaper,
		_this=this;
 		if(imgUrl!==null){
			wallpaper.html("<img src='"+imgUrl+"'></img>");
			var img=wallpaper.find("img");	
			myLib.getImgWh(imgUrl,function(imgW,imgH){
 				if(imgW<=winWh.w){
					img.css('width',winWh.w);
				}else{
					img.css({"margin-left":-(imgW-winWh.w)/2});
				}
				if(imgH<=winWh.h){
					img.css('height',winWh.h);
				}else{
					img.css({"margin-top":-(imgH-winWh.h)/2});
				}
			});
		}
		//如果窗口大小改变，更新背景布局大小
		window.onresize=function(){
     		_this.init(imgUrl);
		};
	}
};

