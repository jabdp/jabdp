//桌面背景设置
myLib.NS("desktop.wallpaper");
myLib.desktop.wallpaper = {
	//初始化桌面
	init:function(imgUrl){
		var myData=myLib.desktop.getMydata(),
		winWh=myData.winWh,
		wallpaper=myData.panel.wallpaper,
		_this=this;
 		if(imgUrl!==null){
			wallpaper.html("<img src='"+imgUrl+"'></img>");
			var img=wallpaper.find("img");	
			myLib.getImgWh(imgUrl,function(imgW,imgH){
 				if(imgW<=winWh.w){
					img.css('width',winWh.w);
				}else{
					img.css({"margin-left":-(imgW-winWh.w)/2});
				}
				if(imgH<=winWh.h){
					img.css('height',winWh.h);
				}else{
					img.css({"margin-top":-(imgH-winWh.h)/2});
				}
			});
		}
		//如果窗口大小改变，更新背景布局大小
		window.onresize=function(){
     		_this.init(imgUrl);
		};
	},
	
};