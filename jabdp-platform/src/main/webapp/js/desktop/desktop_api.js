/**
 * 开发版本的文件导入
 */
(function() {
    var paths = [
            'myLib.js',
            'desktop.js',
            'menu/menu.js',
            'deskIcon/deskIcon.js',
            'window/window.js',
            'taskBar/taskBar.js',
            'lrBar/lrBar.js'
        ],
        baseURL = 'js/desktop/_src/';
    for(var pi in paths) {
        document.write('<script type="text/javascript" src="'+ baseURL + paths[pi] +'"></script>');
    }
})();
