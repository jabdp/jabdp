var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer'); // 处理css中浏览器兼容的前缀  
var rename = require('gulp-rename'); //重命名  
var cssnano = require('gulp-cssnano'); // css的层级压缩合并
var sass = require('gulp-sass'); //sass
var jshint = require('gulp-jshint'); //js检查 ==> npm install --save-dev jshint gulp-jshint（.jshintrc：https://my.oschina.net/wjj328938669/blog/637433?p=1）  
var uglify = require('gulp-uglify'); //js压缩  
var concat = require('gulp-concat'); //合并文件  
var imagemin = require('gulp-imagemin'); //图片压缩 
var base64 = require('gulp-base64');//base64处理
var gutil = require('gulp-util');
var Config = require('./gulpfile.config.js');
var replace = require('gulp-replace');
var del = require('del');

//======= gulp build 打包资源 ===============
function prod() {
    /** 
     * HTML处理 
     */
    gulp.task('html', function () {
        return gulp.src(Config.html.src).pipe(gulp.dest(Config.html.dist));
    });
    /** 
     * assets文件夹下的所有文件处理 
     */
    gulp.task('assets', function () {
        return gulp.src(Config.assets.src).pipe(gulp.dest(Config.assets.dist));
    });
    /** 
     * CSS样式处理 
     */
    gulp.task('css', function () {
        return gulp.src(Config.css.src).pipe(autoprefixer('last 2 version')).pipe(base64()).pipe(concat(Config.css.build_name)).pipe(gulp.dest(Config.css.dist)).pipe(rename({
                suffix: '.min'
            })).pipe(cssnano()) //执行压缩  
            .pipe(gulp.dest(Config.css.dist));
    });
    
    /** 
     * CSS样式处理 
     */
    gulp.task('css-portal', function () {
        return gulp.src(Config.cssPortal.src).pipe(autoprefixer('last 2 version')).pipe(base64()).pipe(concat(Config.cssPortal.build_name)).pipe(gulp.dest(Config.css.dist)).pipe(rename({
                suffix: '.min'
            })).pipe(cssnano()) //执行压缩  
            .pipe(gulp.dest(Config.css.dist));
    });
    
    /** 
     * CSS样式处理 
     */
    gulp.task('css-easyui-blue', function () {
        return gulp.src(Config.cssEasyuiBlue.src).pipe(autoprefixer('last 2 version')).pipe(base64()).pipe(concat(Config.cssEasyuiBlue.build_name)).pipe(gulp.dest(Config.cssEasyuiBlue.dist)).pipe(rename({
                suffix: '.min'
            })).pipe(cssnano()) //执行压缩  
            .pipe(gulp.dest(Config.cssEasyuiBlue.dist));
    });
    
    /** 
     * CSS样式处理 
     */
    gulp.task('css-easyui-gray', function () {
        return gulp.src(Config.cssEasyuiGray.src).pipe(autoprefixer('last 2 version')).pipe(base64()).pipe(concat(Config.cssEasyuiGray.build_name)).pipe(gulp.dest(Config.cssEasyuiGray.dist)).pipe(rename({
                suffix: '.min'
            })).pipe(cssnano()) //执行压缩  
            .pipe(gulp.dest(Config.cssEasyuiGray.dist));
    });
    
    /** 
     * CSS样式处理 
     */
    gulp.task('css-easyui-bootstrap', function () {
        return gulp.src(Config.cssEasyuiBootstrap.src).pipe(autoprefixer('last 2 version')).pipe(base64()).pipe(concat(Config.cssEasyuiBootstrap.build_name)).pipe(gulp.dest(Config.cssEasyuiBootstrap.dist)).pipe(rename({
                suffix: '.min'
            })).pipe(cssnano()) //执行压缩  
            .pipe(gulp.dest(Config.cssEasyuiBootstrap.dist));
    });
    
    /** 
     * SASS样式处理 
     */
    gulp.task('sass', function () {
        return gulp.src(Config.sass.src).pipe(autoprefixer('last 2 version')).pipe(sass()).pipe(gulp.dest(Config.sass.dist)).pipe(rename({
                suffix: '.min'
            })) //rename压缩后的文件名  
            .pipe(cssnano()) //执行压缩  
            .pipe(gulp.dest(Config.sass.dist));
    });
    /** 
     * js处理 
     */
    gulp.task('js', function () {
        return gulp.src(Config.js.src).pipe(jshint('.jshintrc')).pipe(jshint.reporter('default')).pipe(gulp.dest(Config.js.dist)).pipe(rename({
            suffix: '.min'
        })).pipe(uglify()).pipe(gulp.dest(Config.js.dist));
    });
    /** 
     * 合并所有js文件并做压缩处理 
     */
    gulp.task('js-v1-concat', function () {
        return gulp.src(Config.jsV1.src).pipe(jshint('.jshintrc')).pipe(jshint.reporter('default')).pipe(concat(Config.jsV1.build_name)).pipe(gulp.dest(Config.js.dist)).pipe(rename({
            suffix: '.min'
        })).pipe(uglify()).pipe(gulp.dest(Config.js.dist));
    });
    
    /** 
     * 合并所有js文件并做压缩处理 
     */
    gulp.task('js-v2-concat', function () {
        return gulp.src(Config.jsV2.src).pipe(jshint('.jshintrc')).pipe(jshint.reporter('default')).pipe(concat(Config.jsV2.build_name)).pipe(gulp.dest(Config.js.dist)).pipe(rename({
            suffix: '.min'
        })).pipe(uglify()).pipe(gulp.dest(Config.js.dist));
    });
    
    /** 
     * 合并所有js文件并做压缩处理 
     */
    gulp.task('js-portal-concat', function () {
        return gulp.src(Config.jsPortal.src).pipe(jshint('.jshintrc')).pipe(jshint.reporter('default')).pipe(concat(Config.jsPortal.build_name)).pipe(gulp.dest(Config.js.dist)).pipe(rename({
            suffix: '.min'
        })).pipe(uglify()).pipe(gulp.dest(Config.js.dist));
    });
    
    /** 
     * 合并所有js文件并做压缩处理 
     */
    gulp.task('js-index-concat', function () {
        return gulp.src(Config.jsIndex.src).pipe(jshint('.jshintrc')).pipe(jshint.reporter('default')).pipe(concat(Config.jsIndex.build_name)).pipe(gulp.dest(Config.js.dist)).pipe(rename({
            suffix: '.min'
        })).pipe(uglify()).pipe(gulp.dest(Config.js.dist));
    });
    
    /** 
     * 合并所有js文件并做压缩处理 
     */
    gulp.task('js-concat', function () {
        return gulp.src(Config.js.src).pipe(jshint('.jshintrc')).pipe(jshint.reporter('default')).pipe(concat(Config.js.build_name)).pipe(gulp.dest(Config.js.dist)).pipe(rename({
            suffix: '.min'
        })).pipe(uglify()).on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        }).pipe(gulp.dest(Config.js.dist));
    });
    
    /** 
     * 复制需要文件
     */
    gulp.task('copy', function () {
        return gulp.src(Config.copy.src).pipe(gulp.dest(Config.copy.dist));
    });
    
    /** 
     * 图片处理 
     */
    gulp.task('images', function () {
        return gulp.src(Config.img.src).pipe(imagemin({
            optimizationLevel: 3
            , progressive: true
            , interlaced: true
        })).pipe(gulp.dest(Config.img.dist));
    });

    gulp.task('deleteJSP', function() {
        return del([Config.jspPath], {
            force: true
        });
    });

    gulp.task('cleanDist', function() {
        return del([Config.baseDist+"**/*"], {
            force: true
        });
    });

    gulp.task('revJSP', function () {
        return gulp.src(Config.jspTplPath)
            .pipe(replace(Config.jspTplVersion, Config.jsVersion))
            .pipe(rename(Config.jspFileName))
            .pipe(gulp.dest(Config.jspDir));
    });

    gulp.task('build', ['cleanDist', 'css', 'css-portal', 'css-easyui-blue', 'css-easyui-gray', 'css-easyui-bootstrap', 'js-v1-concat', 'js-v2-concat', 'js-portal-concat', 'js-index-concat', 'js-concat', 'copy', 'deleteJSP', 'revJSP']);
}
module.exports = prod;