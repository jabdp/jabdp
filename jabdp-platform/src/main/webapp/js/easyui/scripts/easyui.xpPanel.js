/**
 * 初始化XP Panel
 */
function collapseAll() {
	$("div.xpstyle-panel .xpstyle").panel("collapse");
	$("div.xpstyle-panel").find("div.panel-title").removeClass(
			"panel-title-bold");
	$(this).removeClass("icon-collapse");
	$(this).addClass("icon-expand");
	$(this).attr("title", $.jwpf.system.button.expand);
}

function expandAll() {
	$("div.xpstyle-panel .xpstyle").panel("expand");
	$("div.xpstyle-panel").find("div.panel-title").addClass("panel-title-bold");
	$(this).removeClass("icon-expand");
	$(this).addClass("icon-collapse");
	$(this).attr("title", $.jwpf.system.button.contract);
}

function initXpStylePanel() {
	var xpPanels = $("div.xpstyle-panel div.panel-header");
	xpPanels.find("div.panel-tool a").bind("click", function(evt) {
		var titleDiv = $(this).parent().prev();
		if ($(this).hasClass("panel-tool-expand")) {
			titleDiv.removeClass("panel-title-bold");
		} else {
			titleDiv.addClass("panel-title-bold");
		}
		return false;
	});
	xpPanels.click(function(evt) {
		$(this).find("div.panel-tool a").trigger("click");
		return false;
	});
}

$(function() {
	initXpStylePanel();
	$("#a_exp_clp").toggle(expandAll, collapseAll);
});