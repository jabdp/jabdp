function switchStylestyle(styleName) {
	$('link[rel=stylesheet][colorTitle]').each(function(i) {
		this.disabled = true;
		if (this.getAttribute('colorTitle') == styleName)
			this.disabled = false;
	});

	$("iframe").contents().find('link[rel=stylesheet][colorTitle]').each(
			function(i) {
				this.disabled = true;
				if (this.getAttribute('colorTitle') == styleName)
					this.disabled = false;
			});
	
	/*$('link[rel=stylesheet][colorTitle]').each(function(i) {
		var sHref = $(this).attr("href").replace($(this).attr("colorTitle"), styleName);
		$(this).attr("colorTitle", styleName);
		$(this).attr("href", sHref);
	});

	$("iframe").contents().find('link[rel=stylesheet][colorTitle]').each(
			function(i) {
				var sHref = $(this).attr("href").replace($(this).attr("colorTitle"), styleName);
				$(this).attr("colorTitle", styleName);
				$(this).attr("href", sHref);
			});*/
	createCookie('style', styleName, 365);
}
// cookie
function createCookie(name, value, days) {
	/*if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else
		var expires = "";
	document.cookie = name + "=" + value + expires + "; path=/";*/
	store.set(name, value);
}

function readCookie(name) {
	/*var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for ( var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;*/
	return store.get(name);
}
function eraseCookie(name) {
	//createCookie(name, "", -1);
	store.remove(name);
}